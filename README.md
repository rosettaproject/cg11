README

This software is a java porting of the Council of Four board game.

Rules:
http://www.craniocreations.it/wp-content/uploads/2018/04/CONSIGLIO-regolamento.pdf

-------------------------------------------------------------------------------------------------

 INSTRUCTIONS TO RUN THE PROGRAM

 Start the server

 - after opening the project, go into "cg11/src/main/java"
 - in the package "it.polimi.ingsw.cg11.server.gameserver" run the "Server.java" class
 - insert from the console the requested turns timer in seconds

 Start a client

 - in the package "it.polimi.ingsw.cg11.client.gameclient" run the "Client.java" class
 - insert the client username
 - type "local" to play the game offline
 - type either "socket" or "rmi" to choose the type of connection
 - repeat this process to add as many users to the game as desired, but remember that a timer will automatically start the game after 20 seconds

----------------------------------------------------------------------------------------------