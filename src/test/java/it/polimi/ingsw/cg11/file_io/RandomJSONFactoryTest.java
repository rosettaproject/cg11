package it.polimi.ingsw.cg11.file_io;

import java.io.FileNotFoundException;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.RandomStandardMapFactory;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.*;

/**
 * 
 * @author francesco
 *
 */
public class RandomJSONFactoryTest extends TestCase {

	GameMap map;
	int numberOfPlayers;
	private static final int CITY_PER_REGION = 5;
	private static final int NUMBER_OF_REGIONS = 3;
	private static final int VICTORY_POINTS_REGION_BONUS = 5;

	public void setUp() throws CannotCreateMapException {

		final String path = "./src/test/resources/CompleteMap1.txt";
		numberOfPlayers = 4;
		FileToJson reader = new FileToJson();
		try {
			RandomStandardMapFactory factory = new RandomStandardMapFactory();
			JSONObject mapConfig = reader.jsonObjectFromFile(path);
			map = factory.createMap(mapConfig, numberOfPlayers, null);
		} catch (FileNotFoundException e) {
			throw new CannotCreateMapException();
		}

	}

	public void testRegions() throws CannotCreateMapException{
		setUp();
		// controls if the names are the expected ones
		assertEquals(map.getRegions().get(0).getName(), "Sea");
		assertEquals(map.getRegions().get(1).getName(), "Hills");
		assertEquals(map.getRegions().get(2).getName(), "Mountains");
		// controls the bonus value
		assertEquals(map.getRegions().get(0).getRegionBonus().getVictoryPointsAmount(), VICTORY_POINTS_REGION_BONUS);
		assertEquals(map.getRegions().get(1).getRegionBonus().getVictoryPointsAmount(), VICTORY_POINTS_REGION_BONUS);
		assertEquals(map.getRegions().get(2).getRegionBonus().getVictoryPointsAmount(), VICTORY_POINTS_REGION_BONUS);
		// controls if the list of cities is the expected one
		int i;

		for (i = 0; i < CITY_PER_REGION; i++)
			assertEquals(map.getRegions().get(0).getCities().get(i).getRegion().getName(), "Sea");
		for (i = 0; i < CITY_PER_REGION; i++)
			assertEquals(map.getRegions().get(1).getCities().get(i).getRegion().getName(), "Hills");
		for (i = 0; i < CITY_PER_REGION; i++)
			assertEquals(map.getRegions().get(2).getCities().get(i).getRegion().getName(), "Mountains");

		// controls if the bonuses are the expected ones
		for (i = 0; i < NUMBER_OF_REGIONS; i++)
			assertEquals(map.getRegions().get(i).getRegionBonus().toString(),
					"\nVictoryPointsAmount = 5");
		// control permits are in place
		for (i = 0; i < NUMBER_OF_REGIONS; i++) {
			FaceUpPermits faceUpPermits = map.getRegions().get(i).getFaceUpPermitCards();
			assertEquals(faceUpPermits.getListSize(), faceUpPermits.getMaxPermitCards());
		}

		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
		map.getTurnManager().changeTurn();
	}
}
