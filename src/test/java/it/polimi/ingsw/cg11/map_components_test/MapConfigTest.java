package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import org.json.JSONObject;
import org.junit.Test;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.StandardMapFactory;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.TestCase;

public class MapConfigTest extends TestCase {
	protected GameMap map;
	protected GameMap map2;
	protected int numberOfPlayers;
	protected TurnManager turnManager;
	protected TurnManager turnManager2;
	public JSONObject mapConfig;

	public void setUp() throws CannotCreateMapException, FileNotFoundException {

		final String path = "./src/test/resources/CompleteMap1.txt";
		numberOfPlayers = 4;
		FileToJson reader = new FileToJson();
		StandardMapFactory factory = new StandardMapFactory();
		mapConfig = reader.jsonObjectFromFile(path);
		map = factory.createMap(mapConfig, numberOfPlayers, null);
		turnManager = map.getTurnManager();
		
		map2 = factory.createMap(mapConfig, numberOfPlayers-2, null);
		turnManager2 = map2.getTurnManager();
	}

	@Test
	public void test() {
		assertTrue(true);
	}
	

}
