package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.CityFactory;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class CityTest extends MapConfigTest {
	private City city1;
	private City city2;
	
	public CityTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		city1 = map.getCities().get(0);
		city2 = map.getCities().get(0);
	}

	public void testGetColor() throws ColorNotFoundException {
		assertEquals(ColorMap.getColor("CYAN"), city1.getCOLOR());
	}

	public void testNoSuchRegionException() {

		JSONObject cityConfig = new JSONObject();
		JSONObject bonus = new JSONObject();
		JSONArray cityBonuses = new JSONArray();

		cityBonuses.put(bonus);
		bonus = cityBonuses.getJSONObject(0);

		bonus.put("Assistants", 1);
		bonus.put("Money", 2);
		bonus.put("Nobility", 3);
		bonus.put("VictoryPoints", 4);
		bonus.put("PoliticCards", 5);
		cityConfig.put("City", "Juvelar");
		cityConfig.put("color", "PURPLE");
		cityConfig.put("Region", "Hogwarts");
		cityBonuses.put(bonus);

		CityFactory cityFactory = new CityFactory();
		boolean doesException = false;
		try {
			cityFactory.createCity(cityConfig, cityBonuses, "kingCity", 0, map);
		} catch (Exception e) {
			doesException = true;
		}
		assertTrue(doesException);
	}

	public void testCitiesFromRegion() {

		String string1 = new String("[Cities of Mountains region]"
				+ "City = Kultos emporiums: empty VictoryPointsAmount = 1 PoliticCardsAmount = 1 - adjacent cities - Indur Lyram"+

				"City = Lyram emporiums: empty VictoryPointsAmount = 3 - adjacent cities - Juvelar Kultos Naris" +

				"City = Merkatim emporiums: empty AssistantsAmount = 2 - adjacent cities - Juvelar Osium" +

				"City = Naris emporiums: empty AssistantsAmount = 1 PoliticCardsAmount = 1 - adjacent cities - Lyram" +

				"City = Osium emporiums: empty AssistantsAmount = 1 - adjacent cities - Merkatim");
		assertEquals(string1.replaceAll("\\s", ""), map.getRegions().get(2).citiesToString().replaceAll("\n", "").replaceAll("\\s", ""));
	}
	
	public void testHashCodeAndEquals(){
		assertFalse(city1.equals(null));
		assertFalse(city1.equals(map));
		assertFalse(new City(null, null, null, null).equals(city1));
		assertFalse(new City(null, null, null, city1.getCityBonus()).equals(city1));
		assertFalse(new City(null, city1.getCOLOR(), null, city1.getCityBonus()).equals(city1));
		assertFalse(new City(city1.getName(), city1.getCOLOR(), null, city1.getCityBonus()).equals(city1));
		assertFalse(new City(city1.getName(), city1.getCOLOR(), map.getCities().get(10).getRegion(), city1.getCityBonus()).equals(city1));
		assertTrue(new City(null, null, null, null).equals(new City(null, null, null, null)));
		assertTrue(city1.equals(city2));
		
		assertFalse(city1.hashCode()==(new City(null, null, null, null)).hashCode());
				
	}
}
