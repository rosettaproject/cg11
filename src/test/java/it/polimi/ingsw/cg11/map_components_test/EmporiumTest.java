package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.EmporiumPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

public class EmporiumTest extends MapConfigTest {

	private EmporiumPoolManager emporiumPoolManager;
	private Emporium emporium1;
	private Emporium emporium2;
	private Emporium emporium3;
	private static final int player1ID = 0;
	private static final int player2ID = 1;
	private static final int GRADEN_INDEX = 6;
	private PlayerState player1;
	private City graden;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		graden = map.getCities().get(GRADEN_INDEX);
		try {
			emporiumPoolManager = new EmporiumPoolManager(map, numberOfPlayers);
		} catch (NoSuchPlayerException e1) {
			fail();
		}
		player1 = map.getPlayers().get(0);
		try {
			emporium1 = emporiumPoolManager.obtainPieceFromPool(player1ID);
		} catch (NoSuchEmporiumException e) {
			fail();
		}
		try {
			emporium2 = emporiumPoolManager.obtainPieceFromPool(player1ID);
		} catch (NoSuchEmporiumException e) {
			fail();
		}
		try {
			emporium3 = emporiumPoolManager.obtainPieceFromPool(player2ID);
		} catch (NoSuchEmporiumException e) {
			fail();
		}

	}

	public void testEmporium() {
		try {
			assertEquals(player1, emporiumPoolManager.getPlayerFromId(player1ID, map));
		} catch (NoSuchPlayerException e) {
			fail();
		}

		try {
			emporiumPoolManager.getPlayerFromId(6, map);
			fail();
		} catch (NoSuchPlayerException e) {

		}
	}

	public void testHashCodeAndEquals() {
		assertTrue(emporium1.equals(emporium1));
		assertFalse(emporium1.equals(emporium3));
		assertFalse(emporium1.equals(null));
		assertFalse(emporium1.equals(player1));
		assertTrue(emporium1.equals(emporium2));

		assertFalse(emporium1.hashCode() == emporium3.hashCode());
	}

	public void testEmporiumOnCity() {
		try {
			graden.addEmporium(emporium3);
		} catch (ExistEmporiumException e1) {
			fail();
		}
		assertFalse(graden.existsPlayerEmporium(player1));
		try {
			graden.addEmporium(emporium1);
		} catch (ExistEmporiumException e) {
			fail();
		}
		try {
			graden.addEmporium(emporium1);
			fail();
		} catch (ExistEmporiumException e) {
		}
		
		assertFalse(graden.toString().contains("emporiums: empty"));
	}
}
