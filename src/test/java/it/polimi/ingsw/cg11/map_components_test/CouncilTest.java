package it.polimi.ingsw.cg11.map_components_test;

import org.json.JSONArray;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.RandomCouncillorPoolManager;
import junit.framework.TestCase;

public class CouncilTest extends TestCase {

	private CouncillorPoolManager councillorPoolManager;
	@SuppressWarnings("unused")
	private RandomCouncillorPoolManager randomCouncillorPoolManager;
	private JSONArray councillorPool;
	private Councillor councillor1;
	private Councillor councillor2;
	private String string1;


	public void setUp() {
		councillorPool = new JSONArray("[{\"color\":\"PINK\"},{\"color\":\"BLACK\"}]");
		try {
			councillorPoolManager = new CouncillorPoolManager(councillorPool);
		} catch (ColorNotFoundException e) {

		}
		try {
			randomCouncillorPoolManager = new RandomCouncillorPoolManager(councillorPool);
		} catch (ColorNotFoundException e) {
			fail();
		}
		string1 = "[PINK, BLACK]";
	}

	public void testCouncil() {
		assertEquals(string1, councillorPoolManager.getCouncillors().toString());
		
		try {
			councillor1 = councillorPoolManager.obtainPieceFromPool();
		} catch (NoSuchCouncillorException e) {
		}
		try {
			councillor2 = councillorPoolManager.obtainPieceFromPool();
		} catch (NoSuchCouncillorException e) {
		}
		try {
			councillorPoolManager.obtainPieceFromPool();
			fail();
		} catch (NoSuchCouncillorException e) {
		}
		assertFalse(councillor1.hashCode()==(councillor2.hashCode()));
		assertTrue(councillor1.equals(councillor1));
		assertFalse(councillor1.equals(null));
		assertFalse(councillor1.equals(string1));

	}
}
