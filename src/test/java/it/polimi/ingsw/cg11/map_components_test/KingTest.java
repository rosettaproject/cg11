package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import org.json.JSONArray;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;

public class KingTest extends MapConfigTest {
	private King king;
	JSONArray cities = new JSONArray();
	private final static int MOVING_COST = 2;

	public KingTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		king = new King(map.getCities().get(0), MOVING_COST);
	}

	public void testGetName() {
		assertEquals("King", king.getName());
	}

	public void testGetCurrentCity() {
		assertEquals(map.getCities().get(0), king.getCurrentCity());
	}

	public void testGetMovingCost() {
		assertEquals(MOVING_COST, king.getMovingCost());
	}

	public void testMove() {
		king.move(map.getCities().get(1));
		assertEquals(map.getCities().get(1), king.getCurrentCity());
	}
	
	public void testToString(){
		String string = "KingCurrent city: City = Arkonemporiums: emptyAssistantsAmount = 1MoneyAmount = 1- adjacent cities -BurgenCastrum";
		assertEquals(string, king.toString().replaceAll("\n", ""));
	}
	
	
}
