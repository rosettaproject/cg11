package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import org.json.JSONObject;
import org.junit.Test;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.RandomStandardMapFactory;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.TestCase;

public class RandomMapConfigTest extends TestCase {
	protected GameMap map;
	protected int numberOfPlayers;
	protected TurnManager turnManager;
	JSONObject mapConfig;

	public void setUp() throws CannotCreateMapException, FileNotFoundException {

		final String path = "./src/test/resources/CompleteMap1.txt";
		numberOfPlayers = 2;
		FileToJson reader = new FileToJson();
		RandomStandardMapFactory factory = new RandomStandardMapFactory();
		mapConfig = reader.jsonObjectFromFile(path);
		map = factory.createMap(mapConfig, numberOfPlayers, null);
		turnManager = map.getTurnManager();
	}

	@Test
	public void test() {
		assertTrue(true);
	}
}
