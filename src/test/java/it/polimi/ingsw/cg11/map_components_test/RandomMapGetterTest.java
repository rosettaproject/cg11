package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import org.json.JSONException;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;

public class RandomMapGetterTest extends RandomMapConfigTest{
	
	private final static int NOBILITY_PATH_SIZE=21;

	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		try {
			super.setUp();
		} catch (JSONException e) {
		}
	}
	
	@Override
	public void test(){
		
		assertEquals(map.getKingBalcony().getNAME(),"King");
		assertEquals(map.getKingBalcony().getCouncil().size(),4);
		
		assertEquals(map.getNobilityPath().getBonusList().size(), NOBILITY_PATH_SIZE);
		
		assertEquals(map.getKingBonuses().size(),5);
		
		assertEquals(map.getKing().getName(),"King");
		
		assertNotNull(map.getEmporiumPool());
	}

}
