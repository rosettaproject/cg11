package it.polimi.ingsw.cg11.map_components_test;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhaseManager;

public class GamePhaseTest extends MapConfigTest {
	private GamePhase gamePhase1;
	private GamePhase gamePhase2;
	private GamePhase gamePhase3;
	private GamePhase endPhase;
	
	private String gamePhaseName;
	private Boolean inRandomOrder;
	
	private GamePhaseManager gamePhaseManager;
	
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		gamePhaseName = "game phase";
		inRandomOrder = false;
		gamePhase1 = new GamePhase(gamePhaseName, inRandomOrder);
		gamePhase2 = new GamePhase(null, false);
		gamePhase3 = new GamePhase(null, false);
		
		gamePhaseManager = new GamePhaseManager(gamePhaseName, map.getMarketModel());
		
	}
	
	public void tests(){
		assertEquals(gamePhase1.hashCode(), gamePhase1.hashCode());
		assertEquals(new GamePhase(null, true).hashCode(), 39122);
		
		assertFalse(gamePhase1.equals(null));
		assertFalse(gamePhase1.equals(inRandomOrder));
		assertFalse(gamePhase2.equals(gamePhase1));
		assertTrue(gamePhase2.equals(gamePhase3));
		
	}
	
	public void testGamePhaseManager(){
		gamePhaseManager.endTheGame();
		endPhase = gamePhaseManager.getCurrentGamePhase();
		gamePhaseManager.changeGamePhase();
		assertEquals(endPhase, gamePhaseManager.getCurrentGamePhase());
	}
}
