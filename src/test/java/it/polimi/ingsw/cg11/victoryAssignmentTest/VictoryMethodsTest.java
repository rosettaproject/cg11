package it.polimi.ingsw.cg11.victoryAssignmentTest;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

public class VictoryMethodsTest extends MapConfigTest{
	
	private List<PlayerState> players;

	private static final Logger LOG = Logger.getLogger(VictoryMethodsTest.class.getName());

	public void setUp(){
		try {
			super.setUp();
		} catch (FileNotFoundException | CannotCreateMapException e) {
			LOG.log(Level.SEVERE,"Configuration error",e);
		}
		
		players = map.getPlayers();
	}
	
	public void test(){
		
		PermitCard permit;
		PermitsDeckFactory permitFactory = new PermitsDeckFactory();
		JSONArray permitConfig =
				new JSONArray("[[\"Burgen\",\"Castrum\",\"Dorful\"],{\"Assistants\": 1,\"VictoryPoints\": 1}]");
		
		permit = permitFactory.createPermitCardFromConfig(permitConfig, map.getRegions().get(0), map);
		
		ModelChanges modelChanges = new ModelChanges();
		JSONObject changes = new JSONObject();
		JSONObject resources;
		
		try {
			players.get(2).getPlayerData().getNobilityPoints().gain(5);
	
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Set up error",e);
		}
		
		try {
			players.get(0).getPlayerActionExecutor().attributeVictoryResources(map, modelChanges);
			changes = new JSONObject(modelChanges.getJsonConfigAsString());
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Method error",e);
		}
		
		resources = changes.getJSONObject("resources");
		assertEquals(resources.get("0"),2);
		assertEquals(resources.get("1"),2);
		assertEquals(resources.get("2"),5);
		assertEquals(resources.get("3"),2);
		assertEquals(changes.getInt("WinnerID"),2);
		
		try {
			players.get(0).getPlayerData().getNobilityPoints().gain(2);
			players.get(3).getPlayerData().getNobilityPoints().gain(2);
			
	
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Set up error",e);
		}
		
		modelChanges = new ModelChanges();
		
		try {
			players.get(0).getPlayerActionExecutor().attributeVictoryResources(map, modelChanges);
			changes = new JSONObject(modelChanges.getJsonConfigAsString());
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Method error",e);
		}
		
		resources = changes.getJSONObject("resources");
		assertEquals(resources.get("0"),2);
		assertEquals(resources.get("2"),5);
		assertEquals(resources.get("3"),2);
		assertEquals(changes.getInt("WinnerID"),2);
		
		modelChanges = new ModelChanges();
		
		try {
			players.get(0).getPlayerData().getNobilityPoints().gain(3);
	
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Set up error",e);
		}
		
		try {
			players.get(0).getPlayerActionExecutor().attributeVictoryResources(map, modelChanges);
			changes = new JSONObject(modelChanges.getJsonConfigAsString());
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Method error",e);
		}
		
		resources = changes.getJSONObject("resources");
		assertEquals(resources.get("0"),5);
		assertEquals(resources.get("2"),5);
		assertEquals(changes.getInt("WinnerID"),2);
		
		players = map2.getPlayers();
		players.get(0).getPlayerData().getPermitsHand().addToHand(permit);
	
		modelChanges = new ModelChanges();
		
		try {
			players.get(0).getPlayerActionExecutor().attributeVictoryResources(map2, modelChanges);
			changes = new JSONObject(modelChanges.getJsonConfigAsString());
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Method error",e);
		}

		resources = changes.getJSONObject("resources");
		assertEquals(resources.get("0"),8);
		assertEquals(resources.get("1"),5);
		assertEquals(changes.getInt("WinnerID"),0);
	}
}
