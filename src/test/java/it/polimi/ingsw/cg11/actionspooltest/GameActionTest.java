package it.polimi.ingsw.cg11.actionspooltest;

import it.polimi.ingsw.cg11.server.model.player.actionspool.GameAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.QuickAction;
import junit.framework.TestCase;

public class GameActionTest extends TestCase {

	private GameAction action = new MainAction();
	private GameAction action_2 = new MainAction();
	private GameAction action_3 = new QuickAction();
	private MainAction action_4 = new MainAction();

	public void test(){
		
		assertTrue(action.equals(action_2));
		assertFalse(action.equals(action_3));
		assertFalse(action_4.equals(action_3));
		assertTrue(action_4.equals(action));
	}
}
