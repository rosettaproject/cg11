package it.polimi.ingsw.cg11.cards_test;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;

import org.json.JSONArray;
import org.json.JSONException;

import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticsDeckFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.*;

public class DeckTest extends TestCase {
	private Queue<PoliticCard> cards1;
	private Queue<PoliticCard> cards2;
	private Deck<PoliticCard> deck1;
	private Deck<PoliticCard> deck2;
	private Deck<PoliticCard> deck3;
	
	private PoliticCard card1;
	private String string1;
	

	public DeckTest(String name) {
		super(name);
	}

	public void setUpDeck() throws FileNotFoundException, JSONException, ColorNotFoundException {
		String filePath = "./src/test/resources/DeckTest.txt";

		FileToJson fileReader = new FileToJson();
		JSONArray jsonArray = fileReader.jsonArrayFromFile(filePath);
		
		card1 = new PoliticCard(ColorMap.getColor("PINK"));
		
		cards1 = new LinkedList<>();
		cards2 = new LinkedList<>();

		deck1 = new PoliticsDeckFactory().createDeck(jsonArray);
		deck2 = new PoliticsDeckFactory().createDeck(jsonArray);
		deck3 = new Deck<>(cards1);
		
		string1 = new String("DECK: 1 cards\n\nPoliticCard [color= PINK ]\n\n--------------");
		
	}

	public void setUp() throws FileNotFoundException, EmptyDeckException, JSONException, ColorNotFoundException {
		setUpDeck();
		deck2.draw();
		deck2.draw();
		deck2.addToDeck(card1);
		deck1.draw();
		deck1.draw();
		cards2.add(card1);
		deck3.addAllToDeck(cards2);
	}

	public void testIsEmpty() {
		assertTrue(deck1.isEmpty());
		assertFalse(deck2.isEmpty());
		assertEquals(string1, deck3.toString());
		
	}

}
