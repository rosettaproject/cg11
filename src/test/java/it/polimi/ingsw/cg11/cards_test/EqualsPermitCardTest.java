/**
 * 
 */
package it.polimi.ingsw.cg11.cards_test;

import java.io.FileNotFoundException;

import org.junit.Test;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * @author Federico
 *
 */
public class EqualsPermitCardTest extends MapConfigTest {

	private PlayerState player;
	private PermitCard permitCard;
	private PermitCard playerPermitCard;
	private static final int FIRST = 0;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		player = map.getPlayers().get(FIRST);
		try {
			permitCard = map.getRegions().get(FIRST).getFaceUpPermitCards().obtainPermit(FIRST);
		} catch (EmptyFaceUpPermitsException e) {
		}
		player.getPlayerData().getPermitsHand().addToHand(permitCard);
		playerPermitCard = player.getPlayerData().getPermitsHand().getCards().get(FIRST);

	}

	@Test
	public void testName() throws Exception {
		permitCard = new PermitCard(playerPermitCard.getPermitBonus(), playerPermitCard.getCities());
		assertTrue(playerPermitCard.equals(permitCard));
		permitCard = new PermitCard(null, playerPermitCard.getCities());
		assertFalse(playerPermitCard.equals(permitCard));

	}

}
