package it.polimi.ingsw.cg11.cards_test;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.StandardMapFactory;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.*;

/**
 * @author Federico
 *
 */
public class PermitCardsDeckTest extends TestCase {

	GameMap map;
	private int numberOfPlayers;
	JSONArray deckConfig;

	/**
	 * @param name
	 */
	public PermitCardsDeckTest(String name) {
		super(name);
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		final String path = "./src/test/resources/CompleteMap1.txt";
		numberOfPlayers = 1;

		FileToJson reader = new FileToJson();

		StandardMapFactory factory = new StandardMapFactory();

		JSONObject mapConfig = reader.jsonObjectFromFile(path);

		map = factory.createMap(mapConfig, numberOfPlayers, null);

	}

	public void testGetBonus() throws EmptyDeckException {
		assertTrue(true);
	}

}
