/**
 * 
 */
package it.polimi.ingsw.cg11.cards_test;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * @author Federico
 *
 */
public class PoliticAndPermitDrawDiscardTest extends MapConfigTest {

	private static final int POLITIC_CARDS = 90;
	private static final int STARTING_POLITIC_CARDS_PER_PLAYER = 6;
	private static final int PLAYERS = 4;
	private static final int FIRST = 0;
	private static final int SECOND = 1;
	private static final int THIRD = 2;
	private static final int FOURTH = 3;

	private Deck<PoliticCard> firstDeck;
	private Deck<PoliticCard> secondDeck;

	private PlayerState player1;
	private PlayerState player2;
	private PlayerState player3;
	private PlayerState player4;
	private Region region1;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		firstDeck = map.getPoliticsDeck();
		secondDeck = map.getSecondPoliticsDeck();
		playerSetUp();
		setUpRegions();
	}

	@Override
	public void test() {
		super.test();
		politicDeckDrawAndDiscardTest();
		permitDrawTest();
	}

	public void permitDrawTest() {
		PermitCard permitCard1;
		PermitCard permitCard2;
		PermitCard permitCard3;
		int permitCards;
		int usedCards;

		try {
			permitCard1 = region1.getFaceUpPermitCards().obtainPermit(FIRST);
			player1.getPlayerData().addToPermitsHand(permitCard1);

			permitCard2 = region1.getFaceUpPermitCards().obtainPermit(FIRST);
			player1.getPlayerData().addToPermitsHand(permitCard2);

			permitCard3 = region1.getFaceUpPermitCards().obtainPermit(FIRST);
			player1.getPlayerData().addToPermitsHand(permitCard3);

			player1.getPlayerData().getUsedPermits()
					.addToHand(player1.getPlayerData().getPermitsHand().removeCard(permitCard1));
			player1.getPlayerData().getUsedPermits()
					.addToHand(player1.getPlayerData().getPermitsHand().removeCard(permitCard2));
			player1.getPlayerData().getUsedPermits()
					.addToHand(player1.getPlayerData().getPermitsHand().removeCard(permitCard3));

			permitCards = player1.getPlayerData().getPermitsHand().getCards().size();
			assertEquals(0, permitCards);

			usedCards = player1.getPlayerData().getUsedPermits().getCards().size();
			assertEquals(3, usedCards);

		} catch (EmptyFaceUpPermitsException e) {
		} catch (NoSuchPermitCardException e) {
		}
	}

	public void politicDeckDrawAndDiscardTest() {

		try {
			assertEquals(POLITIC_CARDS - (STARTING_POLITIC_CARDS_PER_PLAYER * PLAYERS + 1), firstDeck.numberOfCards());

			drawPoliticCards(player1, 65);
			assertEquals(0, firstDeck.numberOfCards());

			discardAllPoliticCards(player1);
			discardAllPoliticCards(player2);
			discardAllPoliticCards(player3);
			discardAllPoliticCards(player4);

			assertEquals(0, firstDeck.numberOfCards());
			assertEquals(POLITIC_CARDS, secondDeck.numberOfCards());
			politiCardCloneTest();

			drawPoliticCards(player1, 2);

			assertEquals(88, firstDeck.numberOfCards());
			assertEquals(0, secondDeck.numberOfCards());

			discardAllPoliticCards(player1);

			assertEquals(88, firstDeck.numberOfCards());
			assertEquals(2, secondDeck.numberOfCards());

			drawPoliticCards(player1, 50);
			discardAllPoliticCards(player1);

			assertEquals(38, firstDeck.numberOfCards());
			assertEquals(52, secondDeck.numberOfCards());

			drawPoliticCards(player1, 50);

			assertEquals(40, firstDeck.numberOfCards());
			assertEquals(0, secondDeck.numberOfCards());

			drawPoliticCards(player2, 10);
			drawPoliticCards(player3, 10);
			drawPoliticCards(player4, 20);

			assertEquals(0, firstDeck.numberOfCards());
			assertEquals(0, secondDeck.numberOfCards());

			discardAllPoliticCards(player1);
			discardAllPoliticCards(player2);
			discardAllPoliticCards(player3);
			discardAllPoliticCards(player4);

			assertEquals(0, firstDeck.numberOfCards());
			assertEquals(90, secondDeck.numberOfCards());

		} catch (NoSuchPoliticCardException e) {
		}
	}

	public void drawPoliticCards(PlayerState player, int numberOfCards) {
		for (int i = 0; i < numberOfCards; i++) {
			try {
				player.getPlayerData().getPoliticsHand().draw();
			} catch (EmptyPoliticDeckException e) {
			}
		}

	}

	public void discardAllPoliticCards(PlayerState player) throws NoSuchPoliticCardException {
		List<PoliticCard> politicCardToDiscard = new ArrayList<>();
		List<PoliticCard> playerPoliticHand = player.getPlayerData().getPoliticsHand().getCards();
		Iterator<PoliticCard> iterator = playerPoliticHand.iterator();
		PoliticCard cardToAdd;

		while (iterator.hasNext()) {
			cardToAdd = iterator.next();
			politicCardToDiscard.add(cardToAdd.getCopy());
		}
		player.getPlayerData().getPoliticsHand().discard(politicCardToDiscard);
	}

	public void setUpRegions() {
		region1 = map.getRegions().get(FIRST);
	}

	public void playerSetUp() {
		player1 = map.getPlayers().get(FIRST);
		player2 = map.getPlayers().get(SECOND);
		player3 = map.getPlayers().get(THIRD);
		player4 = map.getPlayers().get(FOURTH);
	}

	public void permitDraw() throws EmptyPermitDeckException, EmptyFaceUpPermitsException {
		PermitCard permitCard = region1.getFaceUpPermitCards().obtainPermit(FIRST);
		player1.getPlayerData().getPermitsHand().addToHand(permitCard);
	}

	public void politiCardCloneTest() {
		PoliticCard card = new PoliticCard(Color.CYAN);
		PoliticCard cloneCard = card.getCopy();
		assertTrue(card.equals(cloneCard));
	}

}
