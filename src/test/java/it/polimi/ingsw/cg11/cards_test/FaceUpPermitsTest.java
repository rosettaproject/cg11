package it.polimi.ingsw.cg11.cards_test;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.resources.PermitsHand;

public class FaceUpPermitsTest extends MapConfigTest {

	private Deck<PermitCard> emptyDeck;
	private Region region;
	@SuppressWarnings("unused")
	private FaceUpPermits emptyFaceUpPermits;
	private FaceUpPermits faceUpPermits;
	private PermitsHand permitHand;
	private PermitCard permitCardToRemove;
	private static final int PERMIT_DECK_SIZE = 15;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		Queue<PermitCard> cards = new LinkedList<>();
		emptyDeck = new Deck<PermitCard>(cards);
		region = map.getRegions().get(1);
		permitCardToRemove = map.getRegions().get(0).getFaceUpPermitCards().getPermit(0);
		faceUpPermits = map.getRegions().get(1).getFaceUpPermitCards();
		permitHand = map.getPlayers().get(0).getPlayerData().getPermitsHand();
	}

	public void testVarious() {
		try {
			emptyFaceUpPermits = new FaceUpPermits(emptyDeck, region);
			fail();
		} catch (EmptyPermitDeckException e) {
		}
		try {
			permitHand.removeCard(permitCardToRemove);
			fail();
		} catch (NoSuchPermitCardException e2) {
			
		}
		try {
			faceUpPermits.removeCard(permitCardToRemove);
			fail();
		} catch (NoSuchPermitCardException e) {

		}
		for (int i = 0; i < PERMIT_DECK_SIZE -2; i++) {
			try {
				faceUpPermits.removeCard(map.getRegions().get(1).getFaceUpPermitCards().getPermit(0));
				
			} catch (NoSuchPermitCardException e) {
				
			}
		}
		try {
			faceUpPermits.shuffleBack();
			
		} catch (NoSuchFaceUpPermitsException  e) {
		}
		
		for (@SuppressWarnings("unused") int i = 0; faceUpPermits.getListSize() != 0; i++) {
			try {
				faceUpPermits.removeCard(map.getRegions().get(1).getFaceUpPermitCards().getPermit(0));
			} catch (NoSuchPermitCardException e1) {
				fail();
			}
		}
		try {
			faceUpPermits.shuffleBack();
			fail();
		} catch (NoSuchFaceUpPermitsException e) {
		}
		
		try {
			faceUpPermits.obtainPermit(0);
			fail();
		} catch (EmptyFaceUpPermitsException e) {
			
		}
		assertTrue(faceUpPermits.toString().contains("empty"));
		
	}
}
