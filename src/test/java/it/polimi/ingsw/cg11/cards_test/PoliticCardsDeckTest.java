package it.polimi.ingsw.cg11.cards_test;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticsDeckFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.player.resources.PoliticsHand;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import junit.framework.TestCase;
/**
 * 
 * @author paolasanfilippo
 *
 */
public class PoliticCardsDeckTest extends TestCase{
	
	private final List<String> colors = new ArrayList<>();
	private Deck<PoliticCard> politicDeck, politicDeck1, emptyDeck;
	private List<PoliticCard> cardsToBeDiscarded= new ArrayList<PoliticCard>();
	private PoliticsHand politicHand;
	public PoliticCardsDeckTest(String name) {
		super(name);
	}
	
	public void setUp() throws FileNotFoundException, JSONException, ColorNotFoundException, EmptyDeckException{
		setUpColor();
		setUpPoliticsDeck();
		
	}
	
	public void setUpColor(){
		colors.add("RED");
		colors.add("BLUE");
		colors.add("GREEN");
		colors.add("ORANGE");
		colors.add("PINK");
		colors.add("BLACK");
		colors.add("WHITE");
		colors.add("CYAN");
		colors.add("PURPLE");
	}
	public void setUpPoliticsDeck() throws FileNotFoundException, JSONException, ColorNotFoundException, EmptyDeckException{
		String filePath = "./src/test/resources/politicCardDeckTest.txt";
		
		FileToJson fileReader = new FileToJson();
		JSONArray jsonArray =  fileReader.jsonArrayFromFile(filePath);
		
		politicDeck = new  PoliticsDeckFactory().createDeck(jsonArray);
		politicDeck1=new PoliticsDeckFactory().createDeck(jsonArray);
		emptyDeck = new PoliticsDeckFactory().createDeck(new JSONArray("[]"));
		
	}
	
	public void testDiscard() throws EmptyDeckException{
		PoliticCard politicCard3= new PoliticCard(Color.PINK);
		PoliticCard politicCard4= new PoliticCard(Color.BLUE);
		PoliticCard politicCard5= new PoliticCard(Color.BLACK);
		
		politicHand = new PoliticsHand(emptyDeck, emptyDeck, 0);
		
		politicHand = new PoliticsHand(politicDeck,politicDeck1,1);
		politicHand.addToHand(politicCard3);
		politicHand.addToHand(politicCard4);
		politicHand.addToHand(politicCard5);
		
		cardsToBeDiscarded.add(politicCard4);
		cardsToBeDiscarded.add(politicCard3);

		try {
			politicHand.discard(cardsToBeDiscarded);
		} catch (NoSuchPoliticCardException e) {
			fail();
		}
		
		assertEquals(politicCard5.getColor().toString(), politicHand.getCards().get(0).getColor().toString());
		
		
	}
	
	
	
	
}
