package it.polimi.ingsw.cg11.cards_test;


import java.awt.Color;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import junit.framework.TestCase;
/**
 * 
 * @author paolasanfilippo
 *
 */
public class PoliticCardTest extends TestCase{
	
	private PoliticCard politicCard;
	private Color color = new Color(22, 22, 22);
	
	public PoliticCardTest(String name) {
		super(name);
	}
	
	public void setUp(){
		politicCard=new PoliticCard(color);
		
	}
	public void testGetColor() {
		assertEquals(new Color(22,22,22), politicCard.getColor());
	}
	
	public void testEqualsAndHashCode(){
		PoliticCard politicCard3= new PoliticCard(Color.PINK);
		PoliticCard politicCard4= new PoliticCard(Color.BLUE);
		PoliticCard politicCard5= new PoliticCard(null);
		PoliticCard politicCard6= new PoliticCard(Color.PINK);
		PoliticCard politicCard7= new PoliticCard(null);
		assertTrue(politicCard3.equals(politicCard3));
		assertFalse(politicCard3.equals(politicCard4));
		assertFalse(politicCard3.equals(null));
		assertFalse(politicCard3.equals(color));
		assertFalse(politicCard3.equals(politicCard5));
		assertFalse(politicCard5.equals(politicCard3));
		assertTrue(politicCard5.equals(politicCard5));
		assertTrue(politicCard3.equals(politicCard6));
		assertTrue(politicCard5.equals(politicCard7));
		
		assertTrue(politicCard3.hashCode()==politicCard3.hashCode());
		assertTrue(politicCard5.hashCode()==politicCard5.hashCode());
		
		assertEquals("", politicCard.toString());
	}
	
}
