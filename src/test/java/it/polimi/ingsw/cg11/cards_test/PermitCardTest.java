package it.polimi.ingsw.cg11.cards_test;

import java.io.FileNotFoundException;
import java.util.List;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;

public class PermitCardTest extends MapConfigTest {

	private PermitCard permitCard1, permitCard2, permitCard3;
	private Bonus bonus;
	private List<City> cities;
	private FaceUpPermits faceUpPermits;
	private Region region;

	private final static int MAX_PERMIT_CARDS = 2;

	public PermitCardTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		faceUpPermits = map.getRegions().get(0).getFaceUpPermitCards();
		permitCard1 = faceUpPermits.getPermit(0);
		permitCard2 = faceUpPermits.getPermit(0);
		permitCard3 = faceUpPermits.getPermit(1);

		region = map.getRegions().get(0);
		cities = map.getCities();
		bonus = faceUpPermits.getPermit(0).getPermitBonus();

	}

	public void testGetPermitBonus() {
		assertEquals(bonus.toString(), permitCard1.getPermitBonus().toString());

	}

	public void testgetMaxPermitCards() {
		assertTrue(faceUpPermits.getMaxPermitCards() == MAX_PERMIT_CARDS);
	}

	public void testGetRegion() {
		assertEquals(faceUpPermits.getREGION().getName(), region.getName());
	}

	public void testGetCities() {
		for (int i = 0; i < permitCard1.getCities().size(); i++) {
			for (int j = 0; j < cities.size(); j++) {
				if (permitCard1.getCities().get(i).getName() == cities.get(j).getName()) {
					assertTrue(true);
				}

			}
		}
	}

	public void testgetFaceUpPermitsList() {
		for (int i = 0; i < faceUpPermits.getListSize(); i++) {
			assertTrue(true);
		}
	}

	public void testEqualsAndHashCode() {
		PermitCard permitCardCitiesNull1 = new PermitCard(null, null);
		PermitCard permitCardCitiesNull2 = new PermitCard(null, null);
		PermitCard permitCardBonusNull1 = new PermitCard(null, permitCard1.getCities());
		PermitCard permitCardBonusNull2 = new PermitCard(null, permitCard1.getCities());

		PermitCard permitCard5 = new PermitCard(permitCard1.getPermitBonus(), permitCard3.getCities());
		PermitCard permitCard6 = new PermitCard(permitCard3.getPermitBonus(), permitCard1.getCities());

		assertFalse(permitCard1.equals(permitCard3));
		assertTrue(permitCard1.equals(permitCard2));
		assertFalse(permitCard1.equals(region));
		assertFalse(permitCard1.equals(null));
		assertFalse(permitCardCitiesNull1.equals(permitCard1));
		assertTrue(permitCardCitiesNull1.equals(permitCardCitiesNull2));
		assertFalse(permitCard1.equals(permitCard5));
		if (!permitCard1.getPermitBonus().equals(permitCard2.getPermitBonus())) {
			assertFalse(permitCard1.equals(permitCard6));
		}
		assertFalse(permitCardBonusNull1.equals(permitCard1));
		assertTrue(permitCardBonusNull1.equals(permitCardBonusNull2));

		assertTrue(permitCard1.hashCode() == permitCard2.hashCode());
		assertTrue(permitCardCitiesNull1.hashCode() == permitCardCitiesNull2.hashCode());

	}

}
