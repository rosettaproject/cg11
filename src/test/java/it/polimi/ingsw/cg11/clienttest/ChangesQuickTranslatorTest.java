package it.polimi.ingsw.cg11.clienttest;

import java.awt.Color;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class ChangesQuickTranslatorTest extends ClientMapFactoryTest {

	private ClientMap gameMap;
	private ChangesTranslator translator;
	private JSONObject changesAsJson;
	private ModelChanges changes;
	private ChangesMessage message;
	private CouncillorPoolManager councillorPoolManager;

	public void setUp() {
		super.setUp();
		gameMap = getGameMap();
		translator = new ChangesTranslator(getUser(), new ChangesMessage("Test message"));
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		councillorPoolManager = getServerMap().getCouncillorsPool();
	}

	public void test() {

		hireOneAssistantTest();
		electCouncillorWithOneAssistantTest();
		changePermitCardTest();
		performAnAdditionalMainActionTest();
	}

	private void performAnAdditionalMainActionTest() {

		int assistantsCost = 3;
		try {
			gameMap.getUserResources().getPlayerAssistants().gain(4);
		} catch (IllegalResourceException e) {
		}
		int startingAssitants = gameMap.getUserResources().getPlayerAssistants().getAmount();

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "PerformAnAdditionalMainAction");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("AssistantsCost", assistantsCost);

		message = translator.translateChange(gameMap, changes);

		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(), startingAssitants - assistantsCost);

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "PerformAnAdditionalMainAction");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("AssistantsCost", -assistantsCost);

		message = translator.translateChange(gameMap, changes);

		assertNull(message);
	}

	private void changePermitCardTest() {

		int assistantsCost = 3;

		try {
			gameMap.getUserResources().getPlayerAssistants().gain(assistantsCost + 1);
		} catch (IllegalResourceException e) {
		}
		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();

		PermitCard firstPermit = addTestPermitCard("Arkon");
		PermitCard secondPermit = addTestPermitCard("Dorful");

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "ChangePermitCard");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("AssistantsCost", assistantsCost);
		changes.addToChanges("RegionName", "sea");
		changes.addToChanges("FirstPermit", firstPermit);
		changes.addToChanges("SecondPermit", secondPermit);

		message = translator.translateChange(gameMap, changes);

		String firstPermitCity = gameMap.getRegions().get(0).getFaceUpPermit(0).getCityNames().get(0);
		String secondPermitCity = gameMap.getRegions().get(0).getFaceUpPermit(1).getCityNames().get(0);

		assertEquals(firstPermit.getCities().get(0).getName(), firstPermitCity);
		assertEquals(secondPermit.getCities().get(0).getName(), secondPermitCity);
		assertEquals(startingAssistants - assistantsCost, gameMap.getUserResources().getPlayerAssistants().getAmount());

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "ChangePermitCard");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("AssistantsCost", -assistantsCost);
		changes.addToChanges("RegionName", "sea");
		changes.addToChanges("FirstPermit", firstPermit);
		changes.addToChanges("SecondPermit", secondPermit);

		message = translator.translateChange(gameMap, changes);

		assertNull(message);
	}

	private void electCouncillorWithOneAssistantTest() {

		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();
		int index;
		boolean colorIsPresent = false;
		String colors[] = { "CYAN", "PINK", "PURPLE", "WHITE", "BLACK" };
		Color councillorColor = null;
		String colorString = null;

		index = 0;
		do {
			colorString = colors[index];
			try {
				councillorColor = ColorMap.getColor(colorString);
			} catch (ColorNotFoundException e) {
			}
			colorIsPresent = councillorPoolManager.hasCouncillor(councillorColor);
			index++;
		} while (!colorIsPresent);

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "ElectCouncillorWithOneAssistant");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("BalconyName", "sea");
		changes.addToChanges("Councillor", colorString.toLowerCase());

		message = translator.translateChange(gameMap, changes);

		try {
			assertEquals(gameMap.getRegions().get(0).getRegionBalcony().getCouncillorColor(3),
					ColorMap.getColor(colorString));
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(), startingAssistants - 1);

		councillorColor = Color.BLACK;
		try {
			colorString = ColorMap.getString(councillorColor);
		} catch (ColorNotFoundException e1) {
		}

		while (councillorPoolManager.hasCouncillor(councillorColor)) {
			try {
				councillorPoolManager.obtainPieceFromPool(councillorColor);
				gameMap.getCouncillorPoolManager().obtainPieceFromPool(councillorColor);
			} catch (NoSuchCouncillorException e) {
			}
		}
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "ElectCouncillorWithOneAssistant");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("BalconyName", "sea");
		changes.addToChanges("Councillor", "not a color");

		message = translator.translateChange(gameMap, changes);

		assertNull(message);
	}

	private void hireOneAssistantTest() {

		int gainedAssistants = 1;
		int moneyCost = 3;

		int startingMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "HireOneAssistant");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("Assistant", gainedAssistants);
		changes.addToChanges("MoneyCost", moneyCost);

		message = translator.translateChange(gameMap, changes);

		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(), startingMoney - moneyCost);
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(),
				startingAssistants + gainedAssistants);

		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action", "HireOneAssistant");
		changes.addToChanges("ActorID", 0);
		changes.addToChanges("Assistant", gainedAssistants);
		changes.addToChanges("MoneyCost", -moneyCost);

		message = translator.translateChange(gameMap, changes);

		assertNull(message);
	}

	private PermitCard addTestPermitCard(String cityName) {

		JSONArray permitConfig = new JSONArray("[[\"" + cityName + "\"],{\"VictoryPoints\":7}]");
		PermitsDeckFactory permitFactory = new PermitsDeckFactory();
		PermitCard testCard = permitFactory.createPermitCardFromConfig(permitConfig, getServerMap().getRegions().get(0),
				getServerMap());

		return testCard;
	}
}
