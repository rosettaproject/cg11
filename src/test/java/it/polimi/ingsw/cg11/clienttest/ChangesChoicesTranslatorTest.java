package it.polimi.ingsw.cg11.clienttest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;

public class ChangesChoicesTranslatorTest extends ClientMapFactoryTest{

	private ClientMap gameMap;
	private ChangesTranslator translator;
	private JSONObject changesAsJson;
	private ModelChanges changes;
	private ChangesMessage message;
	
	public void setUp(){
		super.setUp();
		gameMap = getGameMap();
		translator = new ChangesTranslator(getUser(), new ChangesMessage("Test message"));
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
	}
	
	public void test(){
		
		translateChooseAnExtraPermitCardTest();
		translateChooseAnExtraPermitBonusTest();
		translateChooseAnExtraBonusTest();
	}

	private void translateChooseAnExtraBonusTest() {
		
		int startingMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		int moneyGained = 4;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraBonus");
		changes.addToChanges("ActorID",0);
		changes.addAsResource("Money", moneyGained);
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),startingMoney+moneyGained);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitBonus");
		changes.addToChanges("ActorID",0);
		changes.addAsResource("Money", -moneyGained);
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
	}

	private void translateChooseAnExtraPermitBonusTest() {
		
		int startingMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		int moneyGained = 4;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitBonus");
		changes.addToChanges("ActorID",0);
		changes.addAsResource("Money", moneyGained);
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),startingMoney+moneyGained);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitBonus");
		changes.addToChanges("ActorID",0);
		changes.addAsResource("Money", -moneyGained);
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
	}

	private void translateChooseAnExtraPermitCardTest() {

		PermitCard testCard =  addTestPermitCard("Arkon");
		List<PoliticCard> politicList = new ArrayList<>();
		politicList.add(new PoliticCard(Color.BLACK));
		int assistantsAmount = 2;
		int politicCardsNumber = 1;
		
		int startingPoliticHandNumber = gameMap.getUserHand().numberOfPoliticCards();
		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitCard");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("Region","sea");
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addAsResource("Assistants", assistantsAmount);
		changes.addAsResource("PoliticCards", politicCardsNumber);
		try {
			changes.addToChanges("ObtainedCards", politicList);
		} catch (ColorNotFoundException e) {
		}

		List<ClientPermitCard> newPermits = new ArrayList<>();
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		
		ClientPermitCard clientPermitCard = permitFactory.createPermitCardFromChanges(
				new JSONObject(changes.getJsonConfigAsString()).getJSONObject("AcquiredPermit"));
		
		newPermits.add(clientPermitCard);
		gameMap.getRegions().get(0).changeFaceUpPermits(newPermits);
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserHand().getPermitCard(0).getCityNames().get(0),
				testCard.getCities().get(0).getName());
				
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(),startingAssistants+assistantsAmount);
		assertEquals(gameMap.getUserHand().numberOfPoliticCards(),startingPoliticHandNumber+politicCardsNumber);
		
		startingPoliticHandNumber = gameMap.getOpponentsHands().get(0).getPoliticCards();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("Region","sea");
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addAsResource("PoliticCards", politicCardsNumber);
		
		newPermits = new ArrayList<>();
		
		newPermits.add(clientPermitCard);
		gameMap.getRegions().get(0).changeFaceUpPermits(newPermits);
		
		translator.translateChange(gameMap, changes);
		assertEquals(gameMap.getOpponentsHands().get(0).getPoliticCards(),startingPoliticHandNumber+politicCardsNumber);
		
		translator.translateChange(gameMap, changes);
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("Region","sea");
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addAsResource("Assistants", -assistantsAmount);
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","ChooseAnExtraPermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("Region","not a region");
		changes.addToChanges("AcquiredPermit",testCard);
		
		try{
		message = translator.translateChange(gameMap, changes);
		fail();
		}catch(NullPointerException e){
			assertTrue(true);
		}
		
	
	}
	
	private PermitCard addTestPermitCard(String cityName) {
		
		JSONArray permitConfig = new JSONArray("[[\""+cityName+"\"],{\"VictoryPoints\":7}]");
		PermitsDeckFactory permitFactory = new PermitsDeckFactory();
		PermitCard testCard = 
				permitFactory.createPermitCardFromConfig(permitConfig, getServerMap().getRegions().get(0), getServerMap());
		
		return testCard;
	}
}
