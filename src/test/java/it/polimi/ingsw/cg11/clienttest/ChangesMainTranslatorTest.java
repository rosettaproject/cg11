package it.polimi.ingsw.cg11.clienttest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class ChangesMainTranslatorTest extends ClientMapFactoryTest {

	private ClientMap gameMap;
	private ChangesTranslator translator;
	private JSONObject changesAsJson;
	private ModelChanges changes;
	private ChangesMessage message;
	private CouncillorPoolManager councillorPoolManager;
	
	public void setUp(){
		super.setUp();
		gameMap = getGameMap();
		translator = new ChangesTranslator(getUser(), new ChangesMessage("Test message"));
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		councillorPoolManager = getServerMap().getCouncillorsPool();
	}
	
	@Test
	public void test(){
		
		setUp();
		try {
			finishTest();
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		electCouncillorTest();
		takePermitCardTest();
		buildEmporiumTest();
		buildEmporiumWithKingTest();
		notAnActionTest();
		translator.setIgnoreNextChange(true);
		assertTrue(translator.isIgnoreNextChange());
		assertEquals(message,getMessage());
	}

	
	private void notAnActionTest() {
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","not an action");
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals("",message.getMessage());
	}


	private void takePermitCardTest(){
		
		PermitCard testCard = addTestPermitCard();
		List<PoliticCard> discardedCards = new ArrayList<>();
		discardedCards.add(new PoliticCard(Color.ORANGE));
		
		gameMap.getUserHand().addPoliticCard(new ClientPoliticCard(Color.ORANGE));
		
		int startingVictoryPoints = gameMap.getUserResources().getPlayerVictoryPoints().getAmount();
		int numberOfCardsInHand = gameMap.getUserHand().numberOfPoliticCards();
		int playerStartingMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		int neededMoney = 3;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","TakePermitCard");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("NeededMoney",neededMoney);
		changes.addAsResource("VictoryPoints", 7);
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addToChanges("NewPermit",testCard);
		
		List<ClientPermitCard> newPermits = new ArrayList<>();
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		
		newPermits.add(
				permitFactory.createPermitCardFromChanges(
						new JSONObject(changes.getJsonConfigAsString()).getJSONObject("AcquiredPermit")));
		
		gameMap.getRegions().get(0).changeFaceUpPermits(newPermits);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(numberOfCardsInHand-1,gameMap.getUserHand().numberOfPoliticCards());
		
		assertEquals(testCard.getPermitBonus().getVictoryPointsAmount(),
				gameMap.getUserHand().getPermitCard(0).getBonus().getBonusValue("VictoryPoints"));
		assertEquals(testCard.getCities().get(0).getName(),
				gameMap.getUserHand().getPermitCard(0).getCityNames().get(0));
		
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),playerStartingMoney-neededMoney);
		assertEquals(gameMap.getUserResources().getPlayerVictoryPoints().getAmount(),startingVictoryPoints+7);
		
		gameMap.getUserHand().addPoliticCard(new ClientPoliticCard(Color.ORANGE));
		
		startingVictoryPoints = gameMap.getOpponentsResources().get(0).getPlayerVictoryPoints().getAmount();
		numberOfCardsInHand = gameMap.getOpponentsHands().get(0).getPoliticCards();
		playerStartingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
		neededMoney = 2;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","TakePermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("NeededMoney",neededMoney);
		changes.addAsResource("VictoryPoints", 7);
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addToChanges("NewPermit",testCard);
				
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(numberOfCardsInHand-1,gameMap.getOpponentsHands().get(0).getPoliticCards());
		
		assertEquals(testCard.getPermitBonus().getVictoryPointsAmount(),
				gameMap.getOpponentsHands().get(0).getPermitCard(0).getBonus().getBonusValue("VictoryPoints"));
		assertEquals(testCard.getCities().get(0).getName(),
				gameMap.getOpponentsHands().get(0).getPermitCard(0).getCityNames().get(0));
		
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),playerStartingMoney-neededMoney);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerVictoryPoints().getAmount(),startingVictoryPoints+7);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","TakePermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("NeededMoney",-neededMoney);
		changes.addAsResource("VictoryPoints", 7);
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		changes.addToChanges("AcquiredPermit",testCard);
		changes.addToChanges("NewPermit",testCard);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertNull(message);
	}
	
	private void buildEmporiumWithKingTest() {
		
		String destinationCity = "Hellar";
		int neededAssistants = 3;
		int neededMoney = 5;
		int startingEmporiums = gameMap.getUserResources().getRemainingEmporiums();
		int startingMoney = gameMap.getUserResources().getPlayerMoney().getAmount();

		try {
			gameMap.getUserResources().getPlayerAssistants().gain(neededAssistants);
		} catch (IllegalResourceException e1) {
			fail(e1.toString());
		}
		
		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();
		
		List<PoliticCard> discardedCards = new ArrayList<>();
		discardedCards.add(new PoliticCard(Color.BLACK));
		discardedCards.add(new PoliticCard(Color.ORANGE));
		
		gameMap.getUserHand().addPoliticCard(new ClientPoliticCard(Color.BLACK));
		gameMap.getUserHand().addPoliticCard(new ClientPoliticCard(Color.ORANGE));
		
		int startingHand = gameMap.getUserHand().numberOfPoliticCards();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithKing");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("NeededMoney", neededMoney);
		changes.addToChanges("NeededAssistants",neededAssistants);
		
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		changes.addAsResource("Money",2);
		changes.addAsResource("Money",3);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),startingMoney-neededMoney+5);
		assertEquals(gameMap.getUserResources().getRemainingEmporiums(),startingEmporiums-1);
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(),startingAssistants-neededAssistants);
		assertEquals(startingHand-2,gameMap.getUserHand().numberOfPoliticCards());
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithKing");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("NeededMoney", -neededMoney);
		changes.addToChanges("NeededAssistants",neededAssistants);
		
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		
		setMessage(translator.translateChange(gameMap, changes));
		assertNull(message);
		
		destinationCity = "Hellar";
		neededAssistants = 2;
		neededMoney = 3;
		startingEmporiums = gameMap.getOpponentsResources().get(0).getRemainingEmporiums();
		startingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();

		try {
			gameMap.getOpponentsResources().get(0).getPlayerAssistants().gain(neededAssistants);
		} catch (IllegalResourceException e1) {
			fail(e1.toString());
		}
		
		startingAssistants = gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount();
		
		discardedCards = new ArrayList<>();
		discardedCards.add(new PoliticCard(Color.BLACK));
		discardedCards.add(new PoliticCard(Color.ORANGE));
		
		gameMap.getOpponentsHands().get(0).addPoliticCard();
		gameMap.getOpponentsHands().get(0).addPoliticCard();
		
		startingHand = gameMap.getOpponentsHands().get(0).getPoliticCards();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithKing");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("NeededMoney", neededMoney);
		changes.addToChanges("NeededAssistants",neededAssistants);
		
		try {
			changes.addToChanges("DiscardedCards",discardedCards);
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		changes.addAsResource("Money",2);
		changes.addAsResource("Money",3);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),startingMoney-neededMoney+5);
		assertEquals(gameMap.getOpponentsResources().get(0).getRemainingEmporiums(),startingEmporiums-1);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount(),startingAssistants-neededAssistants);
		assertEquals(startingHand-2,gameMap.getOpponentsHands().get(0).getPoliticCards());
	}
	
	private void buildEmporiumTest(){
		
		int neededAssistants = 1;
		String destinationCity = "Dorful";
		PermitCard testCard = addTestPermitCard();
		ClientPermitCard testClientPermit = addClientPermit();
		
		int startingEmporiums = gameMap.getUserResources().getRemainingEmporiums();
		int startingAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();
		int nobilityBonusAmount = 2;
		gameMap.getUserHand().addPermitCard(testClientPermit);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithPermitCard");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("NeededAssistants", neededAssistants);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("UsedPermit",testCard);
		changes.addAsResource("Nobility", nobilityBonusAmount);
		
		setMessage(translator.translateChange(gameMap, changes));

		assertEquals(gameMap.getUserHand().getUsedPermitCard(0),testClientPermit);
		assertEquals(gameMap.getUserResources().getRemainingEmporiums(),startingEmporiums-1);
		assertEquals(gameMap.getUserResources().getPlayerNobilityPoints().getAmount(),nobilityBonusAmount);
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(),startingAssistants-neededAssistants);
		
		for(ClientCity city : gameMap.getCities())
			if(city.hasEmporiumBuilt(gameMap.getUserResources()))
				assertEquals(destinationCity,city.getName());
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithPermitCard");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("NeededAssistants", -neededAssistants);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("UsedPermit",testCard);
		changes.addAsResource("Nobility", nobilityBonusAmount);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertNull(message);
		
		startingEmporiums = gameMap.getOpponentsResources().get(0).getRemainingEmporiums();
		startingAssistants = gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount();
		int startingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
		int moneyBonusAmount = 2;
		destinationCity = "Arkon";
		gameMap.getOpponentsHands().get(0).addPermitCard(testClientPermit);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","BuildEmporiumWithPermitCard");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("NeededAssistants", neededAssistants);
		changes.addToChanges("DestinationCity",destinationCity);
		changes.addToChanges("UsedPermit",testCard);
		changes.addAsResource("Money", moneyBonusAmount);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		ClientPermitCard permit = gameMap.getOpponentsHands().get(0).getUsedPermitCard(0);

		assertEquals(permit,testClientPermit);
		assertEquals(gameMap.getOpponentsResources().get(0).getRemainingEmporiums(),startingEmporiums-1);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),moneyBonusAmount+startingMoney);
		assertEquals(gameMap.getOpponentsResources().get(0).getRemainingEmporiums(),startingEmporiums-1);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount(),startingAssistants-neededAssistants);
		
		for(ClientCity city : gameMap.getCities())
			if(city.hasEmporiumBuilt(gameMap.getUserResources()) && !city.getName().equalsIgnoreCase("Dorful"))
				assertEquals(destinationCity,city.getName());
	}			


	private void electCouncillorTest(){
		
		int index = 0;
		int staringMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		int moneyGained = 4;
		boolean colorIsPresent = false;
		String colors[] = {"CYAN","PINK","PURPLE","WHITE","BLACK"};
		Color councillorColor = null;
		String colorString = null;
		
		do{
			colorString = colors[index];
			try {
				councillorColor = ColorMap.getColor(colorString);
			} catch (ColorNotFoundException e) {
			}
			colorIsPresent = councillorPoolManager.hasCouncillor(councillorColor);
			index++;
		}while(!colorIsPresent);
			
			changesAsJson = new JSONObject();
			changes = new ModelChanges(changesAsJson);
			changes.addToChanges("Action","ElectCouncillor");
			changes.addToChanges("Councillor",colorString.toLowerCase());
			changes.addToChanges("ActorID",0);
			changes.addToChanges("BalconyName","king");
			
			setMessage(translator.translateChange(gameMap, changes));
			
			assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),staringMoney+moneyGained);
			try {
				assertEquals(gameMap.getKingBalcony().getCouncillorColor(3),ColorMap.getColor(colorString));
				assertNotNull(gameMap.getKingBalcony().toString());
			} catch (ColorNotFoundException e) {
				fail(e.toString());
			}
			
			index = 0;
			staringMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
			colorIsPresent = false;
			councillorColor = null;
			colorString = null;
			
			do{
				colorString = colors[index];
				try {
					councillorColor = ColorMap.getColor(colorString);
				} catch (ColorNotFoundException e) {
				}
				colorIsPresent = gameMap.getCouncillorPoolManager().hasCouncillor(councillorColor);
				index++;
			}while(!colorIsPresent);
				
				changesAsJson = new JSONObject();
				changes = new ModelChanges(changesAsJson);
				changes.addToChanges("Action","ElectCouncillor");
				changes.addToChanges("Councillor",colorString.toLowerCase());
				changes.addToChanges("ActorID",1);
				changes.addToChanges("BalconyName","sea");
				
				setMessage(translator.translateChange(gameMap, changes));
				
				assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),staringMoney+moneyGained);
				try {
					assertEquals(gameMap.getRegions().get(0).getRegionBalcony().getCouncillorColor(3),ColorMap.getColor(colorString));
				} catch (ColorNotFoundException e) {
					fail(e.toString());
				}
				
				changesAsJson = new JSONObject();
				changes = new ModelChanges(changesAsJson);
				changes.addToChanges("Action","ElectCouncillor");
				changes.addToChanges("Councillor","not a color");
				changes.addToChanges("ActorID",1);
				changes.addToChanges("BalconyName","sea");
	
				setMessage(translator.translateChange(gameMap, changes));
				
				assertNull(message);
	}
	
	private void finishTest() throws ColorNotFoundException{
	
		int cardsInHand = gameMap.getUserHand().numberOfPoliticCards();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("PoliticCard",new PoliticCard(ColorMap.getColor("ORANGE")));
		changes.addToChanges("DrawingID",0);
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getUserHand().numberOfPoliticCards(),cardsInHand+1);
		
		cardsInHand = gameMap.getOpponentsHands().get(0).getPoliticCards();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("BlindDrawID",1);
		changes.setTopic("Test_Game_CHANGES");
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getOpponentsHands().get(0).getPoliticCards(),cardsInHand+1);
		
		changes = new ModelChanges(new JSONObject());
		
		changes.addToChanges("BlindDrawID",1);
		changes.setTopic("Test_Game_CHANGES");
		changes.addToChanges("WinnerID",1);

		changes.addAsResource("0", 2);
		changes.addAsResource("1", 3);
		changes.addAsResource("2", 4);
		changes.addAsResource("3", 5);
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getUserResources().getPlayerVictoryPoints().getAmount(), 2);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerVictoryPoints().getAmount(), 3);
		assertEquals(gameMap.getOpponentsResources().get(1).getPlayerVictoryPoints().getAmount(), 4);
		assertEquals(gameMap.getOpponentsResources().get(2).getPlayerVictoryPoints().getAmount(), 5);
		
		assertNotNull(translator.getMessage().getMessage());
		
		cardsInHand = gameMap.getOpponentsHands().get(0).getPoliticCards();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("PoliticCard",new PoliticCard(ColorMap.getColor("ORANGE")));
		changes.addToChanges("DrawingID",1);
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getUserHand().numberOfPoliticCards(),cardsInHand+1);
		
		cardsInHand = gameMap.getOpponentsHands().get(1).getPoliticCards();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("BlindDrawID",2);
		changes.setTopic("Test_Game_CHANGES");
		
		setMessage(translator.translateChange(gameMap, changes));
		
		assertEquals(gameMap.getOpponentsHands().get(1).getPoliticCards(),cardsInHand+1);
	}

	private PermitCard addTestPermitCard() {
		
		JSONArray permitConfig = new JSONArray("[[\"Dorful\"],{\"VictoryPoints\":7}]");
		PermitsDeckFactory permitFactory = new PermitsDeckFactory();
		PermitCard testCard = 
				permitFactory.createPermitCardFromConfig(permitConfig, getServerMap().getRegions().get(0), getServerMap());
		
		return testCard;
	}
	
	private ClientPermitCard addClientPermit() {
		
		JSONArray permitConfig = new JSONArray("[[\"Dorful\"],{\"VictoryPoints\":7}]");
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		
		ClientPermitCard testPermit = permitFactory.createPermitCardFromConfig(permitConfig);
		return testPermit;
	}
	
	public ChangesMessage getMessage() {
		return message;
	}

	public void setMessage(ChangesMessage message) {
		this.message = message;
	}
}
