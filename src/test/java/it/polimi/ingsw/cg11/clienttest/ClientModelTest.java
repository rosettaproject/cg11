package it.polimi.ingsw.cg11.clienttest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChartFactory;
import it.polimi.ingsw.cg11.client.model.bonus.ColorBonusChart;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientKing;
import it.polimi.ingsw.cg11.client.model.player.ClientEmporium;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerEmporiums;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class ClientModelTest extends ClientMapFactoryTest {

	
	
	@Override
	public void setUp() {
		super.setUp();
	}
	
	@Override
	public void test() {
		king();
		playerHand();
		clientEmporium();
		colorBonusChart();
		
		PlayerResourcesChart playerResourcesChart = getGameMap().getUserResources();
		String playerResources = "Username: User"+"Assistants [amount=1]"+"Money [amount=10]"+"VictoryPoints [amount=0]"+"NobilityPoints [amount=0]"+"Emporiums [amount=10]";
		assertEquals(playerResources, playerResourcesChart.toString().replaceAll("\n", ""));
		
	}
	
	private void colorBonusChart() {
		JSONObject json = new JSONObject();
		json.put("color", "BLACK");
		
		BonusChartFactory bonusFactory = new BonusChartFactory();
		try {
			ColorBonusChart colorBonus = bonusFactory.createColorBonusChart(json);
			ColorBonusChart colorBonus2 = bonusFactory.createColorBonusChart(json);
			
			assertEquals(Color.BLACK, colorBonus.getColor());
			assertEquals(colorBonus2, colorBonus);
			assertNotNull(colorBonus.toString());
			assertNotNull(colorBonus.hashCode());
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
	}

	private void clientEmporium() {
		ClientEmporium clientEmporium = new ClientEmporium(Color.BLACK, "username");
		
		assertEquals(Color.BLACK, clientEmporium.getColor());
		try {
			assertEquals("BLACK", ColorMap.getString(clientEmporium.getColor()));
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		assertNotNull(clientEmporium.hashCode());
		assertNotNull(clientEmporium.toString());
		assertTrue(clientEmporium.hashCode()!= new ClientEmporium(null, null).hashCode());
		assertTrue(clientEmporium.equals(clientEmporium));
		assertFalse(clientEmporium.equals(null));
		assertFalse(clientEmporium.equals(clientEmporium.getPlayerUsername()));
		assertFalse(new ClientEmporium(null, null).equals(clientEmporium));
		assertFalse(new ClientEmporium(Color.BLACK, null).equals(clientEmporium));
		assertFalse(new ClientEmporium(Color.BLACK, "username2").equals(clientEmporium));
		assertTrue(new ClientEmporium(null, null).equals(new ClientEmporium(null, null)));
		List<ClientEmporium> emporiums = new ArrayList<>();
		emporiums.add(clientEmporium);
		PlayerEmporiums playerEmporiums = new PlayerEmporiums(emporiums);
		assertTrue(playerEmporiums.toString().equals("Emporiums [amount=1]"));
	}

	private void playerHand() {
		List<ClientPoliticCard> politicCards;
		List<ClientPermitCard> permitCards;
		UserHand userHand;
		OpponentHand opponentHand;
		
		ClientPoliticCard tmpPoliticCard;
		
		politicCards = new ArrayList<>();
		permitCards = new ArrayList<>();
		politicCards.add(new ClientPoliticCard(Color.BLACK));
		
		userHand = new UserHand(politicCards, permitCards);
		
		
		tmpPoliticCard = new ClientPoliticCard(Color.BLACK); 
		assertEquals(tmpPoliticCard, userHand.getPoliticCard(Color.BLACK));
		assertEquals(null, userHand.getPoliticCard(Color.CYAN));
		
		assertEquals(1, userHand.numberOfPoliticCards());
		assertEquals(0, userHand.numberOfPermitCards());
		assertEquals(0, userHand.numberOfUsedPermit());
		
		userHand.addAllPoliticCard(politicCards);
		assertEquals(2, userHand.numberOfPoliticCards());
		
		assertNotNull(userHand.toString());
		assertTrue(userHand.toString().contains("Politic cards:"));
		
		opponentHand = new OpponentHand(0, permitCards, 10);
		assertNotNull(opponentHand.toString());
	}

	public void king(){
		ClientKing king;
		ClientCity city;
		
		city = new ClientCity("cityTest", "regionName", null, Color.DARK_GRAY);
		assertEquals(null, city.getColorAsString());
		
		king = new ClientKing(city);
		assertNotNull(king.toString());
		assertNotNull(king.getColor());
		assertNotNull(king.getCurrentCity());
		
		city = new ClientCity("cityTest", "regionName", null, Color.BLACK);
		assertEquals("BLACK", city.getColorAsString());
		
	}
}

