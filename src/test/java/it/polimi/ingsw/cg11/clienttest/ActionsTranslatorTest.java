package it.polimi.ingsw.cg11.clienttest;


import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.messages.ModelChanges;

public class ActionsTranslatorTest extends ClientMapFactoryTest {

	private ChangesTranslator translator;
	private JSONObject changesAsJson;
	private ModelChanges changes;
	private ChangesMessage message;
	
	public void setUp(){
		super.setUp();
		translator = new ChangesTranslator(getUser(), new ChangesMessage("Test message"));
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
	}
	
	public void test(){
		
		//testing "ChangesMessage"
		
		message = new ChangesMessage("Test");
		message.setResponse(true);
	
		assertTrue(message.isResponse());
		assertEquals(message.getPlayerID(),-1);
		
		message.setPlayerID(0);
		
		assertEquals(message.getPlayerID(),0);
		
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("ActorID",0);
		
		assertEquals(translator.getActorIDFromChanges(changes),0);
		
	}
}
