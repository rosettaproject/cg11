package it.polimi.ingsw.cg11.clienttest;

import java.util.ArrayList;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class ClientCardsTest extends ClientMapFactoryTest {

	private ClientPoliticCard politicCard;
	private ClientPermitCard permitCard;
	private ClientPermitCard nullPermitCard;
	
	
	public void setUp(){
		super.setUp();
		try {
			politicCard = new ClientPoliticCard(ColorMap.getColor("BLACK"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		permitCard = getGameMap().getRegions().get(0).getFaceUpPermit(0);
		nullPermitCard = new ClientPermitCard(null, null);
	}
	
	public void test(){
		assertTrue(politicCard.equals(politicCard));
		assertFalse(politicCard.equals(null));
		assertFalse(politicCard.equals(getGameMap()));
		assertFalse(new ClientPoliticCard(null).equals(politicCard));
		assertTrue(new ClientPoliticCard(null).equals(new ClientPoliticCard(null)));
		assertFalse(politicCard.hashCode()==new ClientPoliticCard(null).hashCode());
		
		assertTrue(permitCard.equals(permitCard));
		assertFalse(permitCard.equals(null));
		assertFalse(permitCard.equals(politicCard));
		assertFalse(nullPermitCard.equals(permitCard));
		assertTrue(nullPermitCard.equals(new ClientPermitCard(null, null)));
		assertFalse(permitCard.equals(new ClientPermitCard(getGameMap().getColorBonus().get(0), null)));
		assertFalse(new ClientPermitCard(permitCard.getBonus(), null).equals(permitCard));
		assertFalse(permitCard.equals(new ClientPermitCard(permitCard.getBonus(), new ArrayList<>())));
		assertTrue(permitCard.hashCode()!=nullPermitCard.hashCode());
		
	}
}
