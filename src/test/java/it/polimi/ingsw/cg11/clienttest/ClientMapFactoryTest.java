package it.polimi.ingsw.cg11.clienttest;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Test;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientMapFactory;
import it.polimi.ingsw.cg11.client.model.map.ClientRegion;
import it.polimi.ingsw.cg11.client.model.map.VisibleBalcony;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.RandomStandardMapFactory;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import it.polimi.ingsw.cg11.server.view.Token;
import junit.framework.TestCase;

public class ClientMapFactoryTest extends TestCase {

	private static final int STARTING_POLITIC_CARDS_NUMBER = 6;
	private static final int PLAYERS = 4;
	private static final int NUMBER_OF_OPPONENTS = PLAYERS - 1;
	private static final int STARTING_EMPORIUMS = 10;
	private static final int STARTING_MONEY = 10;
	private ClientMap gameMap;
	private ClientMapFactory mapFactory;
	private Token user;
	private List<Token> opponents;
	private GameMap serverMap;
	private RandomStandardMapFactory serverFactory;
	private JSONObject mapConfig;
	private JSONObject startupConfigAsJson;
	private List<ModelChanges> startingHands;

	public void setUp() {

		final String path = "./src/test/resources/CompleteMap1.txt";
		FileToJson reader = new FileToJson();
		try {
			mapConfig = reader.jsonObjectFromFile(path);
		} catch (FileNotFoundException e1) {
		}

		try {
			serverFactory = new RandomStandardMapFactory();
			serverMap = serverFactory.createMap(mapConfig, PLAYERS, "Test_Game");
		} catch (CannotCreateMapException e) {
		}
		startupConfigAsJson = serverFactory.getMapStartupConfig();
		startingHands = serverFactory.getStartingHands();

		SerializableMapStartupConfig mapConfig = new SerializableMapStartupConfig();
		mapConfig.setJsonConfigAsString(startupConfigAsJson);
		serverFactory.getStartingHands();

		user = new Token("User");
		getUser().setCurrentGame("Test_Game");
		getUser().setPlayerID(0);

		opponents = new ArrayList<>();

		for (int index = 0, id = 1; index < NUMBER_OF_OPPONENTS; index++, id++) {
			Token opponent;
			opponent = new Token("Opponent_" + String.valueOf(id));
			opponent.setCurrentGame("Test_Game");
			opponent.setPlayerID(id);
			opponents.add(opponent);
		}
		mapFactory = new ClientMapFactory();
		gameMap = mapFactory.createClientMap(mapConfig, startingHands.get(0), getUser(), opponents);
	}

	@Test
	public void test() {
		setUp();

		List<ClientCity> cities = getGameMap().getCities();

		ClientCity tmpCity = cities.get(0);
		BonusChart bonus = tmpCity.getBonus();
		String colorAsString = tmpCity.getColorAsString();

		assertNotNull(bonus);
		assertNotNull(colorAsString);
		assertNotNull(tmpCity.toString());
		assertNotNull(tmpCity.getAdjacentCityNames());

		int index = 0;
		for (ClientCity city : cities) {
			assertTrue(city.getName().equals(getServerMap().getCities().get(index).getName()));
			assertTrue(city.getColor().equals(getServerMap().getCities().get(index).getCOLOR()));
			assertEquals(city.getRegionName(), getServerMap().getCities().get(index).getRegion().getName());
			index++;
		}

		assertEquals("User", getGameMap().getUserResources().getPlayerUsername());
		assertEquals(STARTING_EMPORIUMS, getGameMap().getUserResources().getRemainingEmporiums());
		assertEquals(1, getGameMap().getUserResources().getPlayerAssistants().getAmount());
		assertEquals(STARTING_MONEY, getGameMap().getUserResources().getPlayerMoney().getAmount());
		assertEquals(0, getGameMap().getUserResources().getPlayerNobilityPoints().getAmount());
		assertEquals(0, getGameMap().getUserResources().getPlayerVictoryPoints().getAmount());

		for (index = 0; index < NUMBER_OF_OPPONENTS; index++) {
			assertEquals(STARTING_POLITIC_CARDS_NUMBER, getGameMap().getOpponentsHands().get(index).getPoliticCards());
		}
		
		assertEquals(getGameMap().getRegions().get(0).getRegionBonus(), getGameMap().getRegions().get(1).getRegionBonus());
		String region = "<<Region>> null"+"<<Bonus>>"+bonus+"<<Face up permits>>empty<<Balcony>>empty";
		assertEquals(region.replaceAll("\n", ""), new ClientRegion(null, bonus, new ArrayList<>(), new VisibleBalcony(new ArrayList<>())).toString().replaceAll("\n", ""));
	}

	public ClientMap getGameMap() {
		return gameMap;
	}

	public Token getUser() {
		return user;
	}

	public GameMap getServerMap() {
		return serverMap;
	}

}
