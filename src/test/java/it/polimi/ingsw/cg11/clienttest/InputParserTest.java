package it.polimi.ingsw.cg11.clienttest;


import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.exceptions.NotWellFormedInputCommandException;
import it.polimi.ingsw.cg11.client.model.map.OfferChart;
import it.polimi.ingsw.cg11.client.view.cli.InputParser;
import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;


public class InputParserTest extends ClientMapFactoryTest{
	
	private InputParser testParser;
	private SerializableAction serializableAction;

	public void setUp(){

		super.setUp();
		testParser = new InputParser();
		testParser.setGameMap(getGameMap());
	}
	
	public void test(){
		
		electCouncillorTest();
		buildEmporiumWithKingTest();
		buildEmporiumWithPermitCardTest();
		changePermitCardsTest();
		chooseExtraBonusTest();
		chooseExtraPermitBonusTest();
		chooseExtraPermitTest();
		electCouncillorWithOneAssistantTest();
		engageAnAssistantTest();
		finishTest();
		oneMoreMainActionTest();
		offerAssistantsTest();
		offerPermitCardTest();
		offerPoliticCardTest();
		acceptOfferTest();
		sendChatMessageTest();
		takePermitCardTest();
	}
	
	private void electCouncillorTest(){
		
		String electCouncillor = "electcouncillor: color orange,balcony sea";
		
		JSONObject actionData = parseAction(electCouncillor);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"electcouncillor");
		assertEquals(actionData.get("color"),"orange");
		assertEquals(actionData.get("balcony"),"sea");
	}
	
	private void buildEmporiumWithKingTest(){
		
		String buildEmporiumWithKing = "buildEmporiumWithKing: city testCity, politicCards blue orange purple";
		
		JSONObject actionData = parseAction(buildEmporiumWithKing);
		
		JSONArray cards = (JSONArray)actionData.get("politiccards");
		JSONObject card;
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"buildemporiumwithking");
		assertEquals(actionData.get("city"),"testcity");
		card = cards.getJSONObject(0);
		assertEquals(card.getString("color"),"blue");
		card = cards.getJSONObject(1);
		assertEquals(card.getString("color"),"orange");
		card = cards.getJSONObject(2);
		assertEquals(card.getString("color"),"purple");
	}
	
	private void buildEmporiumWithPermitCardTest(){
		
		String buildEmporiumWithPermitCard = 
				"buildEmporium: city testcity, permitCard 0";
		
		ClientPermitCard testCard;
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		testCard = permitFactory.createPermitCardFromConfig(
				new JSONArray("[[\"Arkon\",\"testcity2\"],{\"VictoryPoints\":\"1\"}]"));
		
		getGameMap().getUserHand().addPermitCard(testCard);
		JSONObject actionData = parseAction(buildEmporiumWithPermitCard);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"buildemporium");
		assertEquals(actionData.get("city"),"testcity");
		assertEquals(actionData.get("region"),"sea");
		assertEquals(actionData.get("permitcard").toString()
				,"[[\"Arkon\",\"testcity2\"],{\"VictoryPoints\":1}]");

	}
	
	private void changePermitCardsTest(){
		
		String buildEmporiumWithKing = "changeFaceUpPermits: region regionName";
		
		JSONObject actionData = parseAction(buildEmporiumWithKing);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"changefaceuppermits");
		assertEquals(actionData.get("region"),"regionname");
	}
	
	private void chooseExtraBonusTest(){
		
		String chooseExtraBonus = "extraBonus: city cityName";
		
		JSONObject actionData = parseAction(chooseExtraBonus);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"extrabonus");
		assertEquals(actionData.get("city"),"cityname");
	}
	
	private void chooseExtraPermitBonusTest(){
		
		String chooseExtraBonus = "extraPermitBonus: region sea, permitCard 0";
		
		ClientPermitCard testCard;
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		testCard = permitFactory.createPermitCardFromConfig(
				new JSONArray("[[\"testcity1\",\"testcity2\"],{\"VictoryPoints\":\"1\"}]"));
		
		getGameMap().getUserHand().addPermitCard(testCard);
		
		JSONObject actionData = parseAction(chooseExtraBonus);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"extrapermitbonus");
		assertEquals(actionData.get("region"),"sea");
		assertEquals(actionData.get("permitcard").toString()
				,"[[\"Arkon\",\"testcity2\"],{\"VictoryPoints\":1}]");
	}
	
	private void chooseExtraPermitTest(){
		
		String chooseExtraPermit = "extraPermit: region sea, permitCard 0";

		JSONObject actionData = parseAction(chooseExtraPermit);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"extrapermit");
		assertEquals(actionData.get("region"),"sea");
		
		String outOfBoundsPermit = "extraPermit: region sea, permitCard 999";
		
		try{
			serializableAction = testParser.getActionFromInputCommand(outOfBoundsPermit, 0);
			fail();
		}catch(NotWellFormedInputCommandException e){
			assertTrue(true);
		}
	}
	
	public void electCouncillorWithOneAssistantTest(){
		
		String chooseExtraPermit = "electCouncillorWithAssistant: color black, balcony sea";

		JSONObject actionData = parseAction(chooseExtraPermit);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"electcouncillorwithassistant");
		assertEquals(actionData.get("color"),"black");
		assertEquals(actionData.get("balcony"),"sea");
	}
	
	private void engageAnAssistantTest(){
		
		String chooseExtraPermit = "engageAssistant";

		JSONObject actionData = parseAction(chooseExtraPermit);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"engageassistant");
	}
	
	private void finishTest(){
		
		String chooseExtraPermit = "finish";

		JSONObject actionData = parseAction(chooseExtraPermit);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"finish");
	}
	
	private void oneMoreMainActionTest(){
		
		String chooseExtraPermit = "oneMoreMainAction";

		JSONObject actionData = parseAction(chooseExtraPermit);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"onemoremainaction");
	}
	
	private void offerAssistantsTest(){
		
		String offerAssistants = "assistantsOffer: assistants 1, cost 2";
		
		JSONObject actionData = parseAction(offerAssistants);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"assistantsoffer");
		assertEquals(actionData.getInt("assistants"),1);
		assertEquals(actionData.getInt("cost"),2);
	}
	
	private void offerPermitCardTest(){
		
		String offerAssistants = "permitCardOffer: permitCard 0, cost 2";
		
		JSONObject actionData = parseAction(offerAssistants);
		
		ClientPermitCard testCard;
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		testCard = permitFactory.createPermitCardFromConfig(
				new JSONArray("[[\"Arkon\",\"testcity2\"],{\"VictoryPoints\":\"1\"}]"));
		
		getGameMap().getUserHand().addPermitCard(testCard);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"permitcardoffer");
		assertEquals(actionData.getInt("cost"),2);
		assertEquals(actionData.get("permitcard").toString()
				,"[[\"Arkon\",\"testcity2\"],{\"VictoryPoints\":1}]");
	}
	
	private void offerPoliticCardTest(){
		
		String offerAssistants = "politicCardOffer: politicCard 7, cost 2";
		
		try {
			getGameMap().getUserHand().addPoliticCard(new ClientPoliticCard(ColorMap.getColor("orange")));
		} catch (ColorNotFoundException e) {
		}
		
		
		JSONObject actionData = parseAction(offerAssistants);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"politiccardoffer");
		assertEquals(actionData.getInt("cost"),2);
		assertEquals(actionData.get("politiccard").toString(),"{\"color\":\"ORANGE\"}");
	}
	
	private void acceptOfferTest(){
		
		String offerAssistants = "acceptOffer: offer 0";
		
		OfferChart offer = new OfferChart("Opponent_1", "AssistantsOffer", 1, 2);
		
		getGameMap().getMarketChart().addOffer(offer);
		
		JSONObject actionData = parseAction(offerAssistants);

		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"acceptoffer");
		assertEquals(actionData.getInt("OfferingPlayer"),1);
		assertEquals(actionData.get("OfferType"),"AssistantsOffer");
		assertEquals(actionData.getInt("OfferCost"),2);
		assertEquals(actionData.getInt("assistants"),1);

	}
	
	private void sendChatMessageTest(){
		
		String offerAssistants = "chat: player Opponent_1, message messageTest";
		
		JSONObject actionData = parseAction(offerAssistants);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"chat");
		assertEquals(actionData.get("message"),"messageTest ");
		assertEquals(actionData.get("player").toString().toLowerCase(),"opponent_1");
		
		offerAssistants = "chatAll: message messageTest";
		
		actionData = parseAction(offerAssistants);
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"chatall");
		assertEquals(actionData.get("message"),"messageTest ");
		assertEquals(actionData.get("player"),"allPlayers");
	}
	
	private void takePermitCardTest(){
	
		String offerAssistants =
				"takePermitCard: region sea, politicCards blue orange purple, permitCard 0";
		
		JSONObject actionData = parseAction(offerAssistants);
		
		JSONArray cards = (JSONArray)actionData.get("politiccards");
		JSONObject card;
		
		assertEquals(actionData.get("PlayerID"),0);
		assertEquals(actionData.get("ActionName"),"takepermitcard");
		assertEquals(actionData.get("region"),"sea");
		card = cards.getJSONObject(0);
		assertEquals(card.getString("color"),"blue");
		card = cards.getJSONObject(1);
		assertEquals(card.getString("color"),"orange");
		card = cards.getJSONObject(2);
		assertEquals(card.getString("color"),"purple");
	}
	
	private JSONObject parseAction(String input){
		
		try {
			serializableAction = testParser.getActionFromInputCommand(input, 0);
		} catch (NotWellFormedInputCommandException e) {
			fail();
		}
		JSONObject actionData = new JSONObject(serializableAction.getJsonConfigAsString());
		
		return actionData;
	}
}
