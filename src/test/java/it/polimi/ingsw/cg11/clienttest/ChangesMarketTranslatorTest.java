package it.polimi.ingsw.cg11.clienttest;

import java.awt.Color;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.OfferChart;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;

public class ChangesMarketTranslatorTest extends ClientMapFactoryTest {

	private ClientMap gameMap;
	private ChangesTranslator translator;
	private JSONObject changesAsJson;
	private ModelChanges changes;
	private ChangesMessage message;
	
	public void setUp(){
		super.setUp();
		gameMap = getGameMap();
		gameMap.getMarketChart().resetChart();
		translator = new ChangesTranslator(getUser(), new ChangesMessage("Test message"));
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
	}
	
	public void test(){
		
		addAssistantsOfferTest();
		addPermitCardOfferTest(0);
		addPermitCardOfferTest(1);
		addPermitCardOfferTest(2);
		addPoliticCardOfferTest(0,4);
		acceptAssistantsOfferTest();
		acceptPermitCardOfferTest();
		addPoliticCardOfferTest(1,1);
		addPoliticCardOfferTest(2,2);
		acceptPoliticCardOfferTest();
	}

	private void acceptPoliticCardOfferTest() {
		
		PoliticCard testCard = new PoliticCard(Color.BLACK);
		int offerCost = 4;
		int acceptingPoliticCards = gameMap.getOpponentsHands().get(0).getPoliticCards();
		int startingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
		int offererMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPoliticCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",0);
		changes.addToChanges("OfferCost", offerCost);
		try {
			changes.addToChanges("PoliticCard",testCard);
		} catch (ColorNotFoundException e) {
		}
		
		gameMap.getUserHand().addPoliticCard(new ClientPoliticCard(Color.BLACK));
		int offererPoliticCards = gameMap.getUserHand().numberOfPoliticCards();
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserHand().numberOfPoliticCards(),offererPoliticCards-1);
		assertEquals(gameMap.getOpponentsHands().get(0).getPoliticCards(),acceptingPoliticCards+1);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),startingMoney-offerCost);
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),offererMoney+offerCost);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPoliticCardOffer");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("OffererID",1);
		changes.addToChanges("OfferCost", offerCost);
		try {
			changes.addToChanges("PoliticCard",testCard);
		} catch (ColorNotFoundException e) {
		}
		
		gameMap.getOpponentsHands().get(0).addPoliticCard();
		
		translator.translateChange(gameMap, changes);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPoliticCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",2);
		changes.addToChanges("OfferCost", offerCost);
		try {
			changes.addToChanges("PoliticCard",testCard);
		} catch (ColorNotFoundException e) {
		}
		
		gameMap.getOpponentsHands().get(1).addPoliticCard();
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserHand().numberOfPoliticCards(),8);
		assertEquals(gameMap.getOpponentsHands().get(0).getPoliticCards(),8);
		assertEquals(gameMap.getOpponentsHands().get(1).getPoliticCards(),6);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPoliticCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",2);
		changes.addToChanges("OfferCost", -offerCost);
		try {
			changes.addToChanges("PoliticCard",testCard);
		} catch (ColorNotFoundException e) {
		}
		
		gameMap.getOpponentsHands().get(1).addPoliticCard();
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
	}

	private void acceptPermitCardOfferTest() {
		
		
		int offerCost = 3;
		int startingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
		int offererMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		PermitCard testCard = addTestPermitCard("Arkon");
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPermitCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",0);
		changes.addToChanges("OfferCost", offerCost);
		changes.addToChanges("PermitCard",testCard);
		
		JSONObject changesAsJson = new JSONObject(changes.getJsonConfigAsString());
		JSONObject permitAsJson = changesAsJson.getJSONObject("PermitCard");
		
		ClientPermitCard offeredCard = permitFactory.createPermitCardFromChanges(permitAsJson);
		gameMap.getUserHand().addPermitCard(offeredCard);
		int offererPermitsHandSize = gameMap.getUserHand().numberOfPermitCards();
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getOpponentsHands().get(0).getPermitCard(0).getCityNames().get(0),
				testCard.getCities().get(0).getName());
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),startingMoney-offerCost);
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),offererMoney+offerCost);
		assertEquals(gameMap.getUserHand().numberOfPermitCards(),offererPermitsHandSize-1);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPermitCardOffer");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("OffererID",1);
		changes.addToChanges("OfferCost", offerCost);
		changes.addToChanges("PermitCard",testCard);
		
		changesAsJson = new JSONObject(changes.getJsonConfigAsString());
		permitAsJson = changesAsJson.getJSONObject("PermitCard");
		
		offeredCard = permitFactory.createPermitCardFromChanges(permitAsJson);
		gameMap.getOpponentsHands().get(0).addPermitCard(offeredCard);
		
		translator.translateChange(gameMap, changes);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPermitCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",2);
		changes.addToChanges("OfferCost", offerCost);
		changes.addToChanges("PermitCard",testCard);
		
		changesAsJson = new JSONObject(changes.getJsonConfigAsString());
		permitAsJson = changesAsJson.getJSONObject("PermitCard");
		
		offeredCard = permitFactory.createPermitCardFromChanges(permitAsJson);
		gameMap.getOpponentsHands().get(1).addPermitCard(offeredCard);
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getUserHand().numberOfPermitCards(),1);
		assertEquals(gameMap.getOpponentsHands().get(0).numberOfPermitCards(),2);
		assertEquals(gameMap.getOpponentsHands().get(1).numberOfPermitCards(),0);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptPermitCardOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",2);
		changes.addToChanges("OfferCost", -offerCost);
		changes.addToChanges("PermitCard",testCard);
		
		changesAsJson = new JSONObject(changes.getJsonConfigAsString());
		permitAsJson = changesAsJson.getJSONObject("PermitCard");
		
		offeredCard = permitFactory.createPermitCardFromChanges(permitAsJson);
		gameMap.getOpponentsHands().get(1).addPermitCard(offeredCard);
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
	}

	private void acceptAssistantsOfferTest() {
		
		int offerCost = 2;
		int assistants = 1;
		int offersNumber = gameMap.getMarketChart().numberOfOffers();
		int offererAssistants = gameMap.getUserResources().getPlayerAssistants().getAmount();
		int offererMoney = gameMap.getUserResources().getPlayerMoney().getAmount();
		try {
			gameMap.getOpponentsResources().get(0).getPlayerMoney().gain(offerCost);
		} catch (IllegalResourceException e) {
		}
		int startingMoney = gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount();
		int startingAssistants = gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount();
				
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptAssistantsOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",0);
		changes.addToChanges("OfferCost", offerCost);
		changes.addToChanges("Assistants",assistants);
		
		translator.translateChange(gameMap, changes);
		
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerMoney().getAmount(),startingMoney-offerCost);
		assertEquals(gameMap.getOpponentsResources().get(0).getPlayerAssistants().getAmount(),
				startingAssistants+assistants);
		assertEquals(gameMap.getUserResources().getPlayerAssistants().getAmount(),offererAssistants-assistants);
		assertEquals(gameMap.getUserResources().getPlayerMoney().getAmount(),offererMoney+offerCost);
		assertEquals(gameMap.getMarketChart().numberOfOffers(),offersNumber-1);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AcceptAssistantsOffer");
		changes.addToChanges("ActorID",1);
		changes.addToChanges("OffererID",0);
		changes.addToChanges("OfferCost", offerCost);
		changes.addToChanges("Assistants",assistants);
		
		message = translator.translateChange(gameMap, changes);
		
		assertNull(message);
	}

	private void addPoliticCardOfferTest(int offererID,int offerIndex) {

		int offerCost = 4;
		PoliticCard card = new PoliticCard(Color.BLACK);
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AddPoliticCardOffer");
		changes.addToChanges("ActorID",offererID);
		changes.addToChanges("OfferCost",offerCost);
		try {
			changes.addToChanges("OfferedItem",card);
		} catch (ColorNotFoundException e) {
		}
		
		message = translator.translateChange(gameMap, changes);
		
		OfferChart offer = gameMap.getMarketChart().getOffer(offerIndex);
		
		assertEquals(offer.getOfferCost(),offerCost);
		assertEquals(offer.getPoliticCardOffered().getColor(),Color.BLACK);
		if(offererID == 0)
			assertEquals(offer.getOfferingPlayerUsername(),"User");
		else
			assertEquals(offer.getOfferingPlayerUsername(),"Opponent_"+String.valueOf(offererID));
		
	}

	private void addPermitCardOfferTest(int offererID) {
		
		int offerCost = 3;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AddPermitCardOffer");
		changes.addToChanges("ActorID",offererID);
		changes.addToChanges("OfferCost",offerCost);
		changes.addToChanges("OfferedItem",addTestPermitCard("Arkon"));
		
		message = translator.translateChange(gameMap, changes);
		
		OfferChart offer = gameMap.getMarketChart().getOffer(1+offererID);
		
		assertEquals(offer.getOfferCost(),offerCost);
		assertEquals(offer.getPermitOffered().getCityNames().get(0),"Arkon");
		if(offererID == 0)
			assertEquals(offer.getOfferingPlayerUsername(),"User");
		else
			assertEquals(offer.getOfferingPlayerUsername(),"Opponent_"+String.valueOf(offererID));
	}

	private void addAssistantsOfferTest() {

		int offerCost = 2;
		int offeredAssistants = 1;
		
		changesAsJson = new JSONObject();
		changes = new ModelChanges(changesAsJson);
		changes.addToChanges("Action","AddAssistantsOffer");
		changes.addToChanges("ActorID",0);
		changes.addToChanges("OfferCost",offerCost);
		changes.addToChanges("OfferedItem",offeredAssistants);
		
		translator.translateChange(gameMap, changes);
		
		OfferChart offer = gameMap.getMarketChart().getOffer(0);
		
		assertEquals(offer.getAssistantsOfferedAmount(),offeredAssistants);
		assertEquals(offer.getOfferCost(),offerCost);
		assertEquals(offer.getOfferingPlayerUsername(),"User");
	}
	
	private PermitCard addTestPermitCard(String cityName) {
		
		JSONArray permitConfig = new JSONArray("[[\""+cityName+"\"],{\"VictoryPoints\":7}]");
		PermitsDeckFactory permitFactory = new PermitsDeckFactory();
		PermitCard testCard = 
				permitFactory.createPermitCardFromConfig(permitConfig, getServerMap().getRegions().get(0), getServerMap());
		
		return testCard;
	}
}
