package it.polimi.ingsw.cg11.clienttest;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.map.OfferChart;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class OfferChartTest extends ClientMapFactoryTest {
	
	private OfferChart assistantsOffer;
	private OfferChart nullOffer;
	private ClientPermitCard permitCard;
	private ClientPoliticCard politicCard;
	private static final String PLAYER1 = "player1";
	private static final String PLAYER2 = "player2";
	private static final String ASSISTANTS_OFFER = "assistantsOffer";
	private static final String PERMIT_OFFER = "permitOffer";
	private static final String REGION = "sea";
	private List<String> cities;
	private OfferChart permitOffer;  
	private static final int ASSISTANTS_AMOUNT = 2;
	private static final int COST = 2;
	
	public void setUp(){
		super.setUp();
		nullOffer = new OfferChart(null, null, null, 0);
		setUpAssistantsOffer();
		setUpPermitOffer();
	}

	private void setUpPermitOffer() {
		cities = new ArrayList<>();
		cities.add("Arkon");
		cities.add("Burgen");
		permitCard = new ClientPermitCard(getGameMap().getColorBonus().get(0), cities);
		permitOffer = new OfferChart(PLAYER1, PERMIT_OFFER, permitCard, REGION, COST);
		try {
			politicCard = new ClientPoliticCard(ColorMap.getColor("PINK"));
		} catch (ColorNotFoundException e) {
			
		}
		assertNotNull(permitOffer);
	}


	public void setUpAssistantsOffer() {
		assistantsOffer = new OfferChart(PLAYER1, ASSISTANTS_OFFER, ASSISTANTS_AMOUNT, COST);
		assertNull(assistantsOffer.getPermitRegionName());
	}
	
	
	public void testEquals(){
		assertTrue(assistantsOffer.equals(assistantsOffer));
		assertFalse(assistantsOffer.equals(null));
		assertFalse(assistantsOffer.equals(ASSISTANTS_OFFER));
		assertFalse(new OfferChart(PLAYER1, null, ASSISTANTS_AMOUNT, COST).equals(assistantsOffer));
		assertFalse(new OfferChart(null, ASSISTANTS_OFFER, ASSISTANTS_AMOUNT, COST).equals(assistantsOffer));
		assertFalse(new OfferChart(PLAYER2, null, ASSISTANTS_AMOUNT, COST).equals(assistantsOffer));
		assertFalse(new OfferChart(PLAYER2, null, ASSISTANTS_AMOUNT, COST).equals(nullOffer));
		assertFalse(new OfferChart(PLAYER2, null, ASSISTANTS_AMOUNT, COST).equals(null));
		assertFalse(assistantsOffer.equals(new OfferChart(PLAYER2, ASSISTANTS_OFFER, ASSISTANTS_AMOUNT, COST)));
		assertTrue(assistantsOffer.hashCode()!=nullOffer.hashCode());
		assertTrue(permitOffer.hashCode()!=new OfferChart( PLAYER1, null, politicCard, COST).hashCode());
		
	}
}
