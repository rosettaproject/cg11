/**
 * 
 */
package it.polimi.ingsw.cg11.graph_Test;

import java.io.FileNotFoundException;
import java.util.List;

import org.json.JSONException;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ActionFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.FinishFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NotLinkedCitiesException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.CityEdge;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GraphedMap;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;

/**
 * @author Federico
 *
 */
public class GraphMapTest extends MapConfigTest {

	private static final int FIRST_PLAYER = 0;
	// private static final int SECOND_PLAYER = 1;
	// private static final int THIRD_PLAYER = 2;
	// private static final int FOURTH_PLAYER = 3;

	private GraphedMap graph;
	private City Arkon;
	private City Burgen;
	private City Castrum;
	private City testCity;
	private List<PlayerState> players;
	// private List<City> cityOfMap;

	private void setUpCities() throws NoSuchCityException {
		List<City> cities = map.getCities();
		ActionFactory builder = new FinishFactory();

		Arkon = builder.getCityFromName(cities, "Arkon");
		Burgen = builder.getCityFromName(cities, "Burgen");
		Castrum = builder.getCityFromName(cities, "Castrum");
		testCity = new City("name", null, null, null);
	}

	public void setGraphedMap() throws NoSuchCityException {
		graph = GraphedMap.createGraphedMap(map);
		graph.addVertex(testCity);
	}

	private void setEmporiums() throws ExistEmporiumException {
		players = map.getPlayers();
		PlayerData playerData1 = players.get(FIRST_PLAYER).getPlayerData();
		Arkon.addEmporium(playerData1.getOneEmporium());
		Burgen.addEmporium(playerData1.getOneEmporium());
		Castrum.addEmporium(playerData1.getOneEmporium());
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		try {
			super.setUp();
			setUpCities();
			setGraphedMap();
			setEmporiums();
		} catch (JSONException | NoSuchCityException | ExistEmporiumException e) {

		}
	}

	public void test() {
		adiacentT();
		linkedCityT();
	}

	public void linkedCityT() {
		List<City> linkedCity = graph.getLinkedCity(Arkon, players.get(FIRST_PLAYER));

		assertTrue(linkedCity.contains(Arkon));
		assertTrue(linkedCity.contains(Burgen));
		assertTrue(linkedCity.contains(Castrum));
		assertEquals(3, linkedCity.size());

	}

	public void adiacentT() {
		CityEdge edge;
		int distance;

		distance = graph.getDistance(Arkon, Arkon);
		assertEquals(0, distance);

		distance = graph.getDistance(Arkon, Burgen);
		assertEquals(1, distance);

		distance = graph.getDistance(Burgen, Castrum);
		assertEquals(2, distance);

		edge = graph.getEdge(Arkon, Arkon);
		assertEquals(null, edge);

		edge = graph.getEdge(Burgen, Castrum);
		assertEquals(null, edge);

		edge = graph.getEdge(Arkon, Burgen);
		assertEquals("Arkon", edge.getSource().getName());
		assertEquals("Burgen", edge.getTarget().getName());
		try {
			graph.linkedCitiesControls(Arkon, Burgen);
			assertTrue(true);
		} catch (NotLinkedCitiesException e) {
			assertTrue(false);
		}
		try {
			graph.linkedCitiesControls(Arkon, testCity);
			assertTrue(false);
		} catch (NotLinkedCitiesException e) {
			assertTrue(true);
		}

	}

}
