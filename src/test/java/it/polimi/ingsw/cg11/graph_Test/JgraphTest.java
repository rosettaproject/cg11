
/**
 * 
 */
package it.polimi.ingsw.cg11.graph_Test;

import junit.framework.TestCase;
import org.jgrapht.*;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.CityEdge;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GraphedMap;

/**
 * @author Federico
 *
 */
public class JgraphTest extends TestCase {

	private CityEdge cityEdgeFactory = new CityEdge();
	private SimpleGraph<City, CityEdge> graph;
	GraphedMap graphedMap;
	private City city1 = new City("City1", null, null, null);
	private City city2 = new City("City2", null, null, null);
	private City city3 = new City("City3", null, null, null);
	private City city4 = new City("City4", null, null, null);
	private City city5 = new City("City5", null, null, null);
	private City city6 = new City("City6", null, null, null);
	private City city7 = new City("City7", null, null, null);

	public JgraphTest() {
		graph = new SimpleGraph<>(cityEdgeFactory);
	}

	public void setUpEdge() {
		graph.addVertex(city1);
		graph.addVertex(city2);
		graph.addVertex(city3);
		graph.addVertex(city4);
		graph.addVertex(city5);
		graph.addVertex(city6);
		graph.addVertex(city7);

		graph.addEdge(city1, city2);
		graph.addEdge(city2, city3);
		graph.addEdge(city3, city4);
		graph.addEdge(city2, city4);
		graph.addEdge(city2, city5);
		graph.addEdge(city5, city6);
	}

	public void setUp(){
		setUpEdge();
	}

	public void testEdge() {
		CityEdge edge1 = graph.getEdge(city1, city2);
		CityEdge edge2 = graph.getEdge(city1, city5);

		assertEquals("City1", edge1.getSource().getName());
		assertEquals("City2", edge1.getTarget().getName());
		assertEquals("CityEdge [sourceCity=City1, targetCity=City2]", edge1.toString());

		assertEquals(null, edge2);

	}

	public void testDijkstra() {

		int numberOfCity;
		DijkstraShortestPath<City, CityEdge> dijkstra;
		GraphPath<City, CityEdge> shortestPath;

		dijkstra = new DijkstraShortestPath<>(graph, city1, city2);
		shortestPath = dijkstra.getPath();
		numberOfCity = Graphs.getPathVertexList(shortestPath).size() - 1;
		assertEquals(1, numberOfCity);

		dijkstra = new DijkstraShortestPath<>(graph, city1, city4);
		shortestPath = dijkstra.getPath();
		numberOfCity = Graphs.getPathVertexList(shortestPath).size() - 1;
		assertEquals(2, numberOfCity);

		dijkstra = new DijkstraShortestPath<>(graph, city1, city7);
		shortestPath = dijkstra.getPath();
		assertEquals(null, shortestPath);
	}
}
