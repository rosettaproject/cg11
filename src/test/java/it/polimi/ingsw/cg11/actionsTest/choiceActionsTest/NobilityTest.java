package it.polimi.ingsw.cg11.actionsTest.choiceActionsTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.List;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ActionServiceMethods;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;
import it.polimi.ingsw.cg11.server.model.player.actionspool.GameAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.BonusFactory;

public class NobilityTest extends ActionInitializer {

	public static final String EXTRA_BONUS_TYPE = "ExtraBonus";
	public static final String EXTRA_PERMIT_TYPE = "ExtraPermit";
	public static final String EXTRA_PERMIT_BONUS_TYPE = "ExtraPermitBonus";

	private ActionServiceMethods actionServiceMethods;
	private Bonus nobilityBonus;

	private Bonus createNobilityBonus() {
		BonusFactory bonusFactory;
		JSONObject jsonObjectNobility;
		jsonObjectNobility = new JSONObject();
		bonusFactory = new BonusFactory();
		jsonObjectNobility.put("Nobility", 1);
		return bonusFactory.createBonus(jsonObjectNobility, map);
	}

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setVariablesResources(player1);
		nobilityBonus = createNobilityBonus();
		actionServiceMethods = new ActionServiceMethods();

	}

	@Override
	public void test() {
		printNobilityPath();

		electCouncillorInRegion3(player1);

		try {
			position2(player1);
			position3(player1);
			position4(player1);
			position5(player1);
			position6(player1);
			position7(player1);
			position8(player1);
			position9(player1);
			position10(player1);
			position11(player1);
			position12(player1);
			position13(player1);
			position14(player1);
			position15(player1);
			position16(player1);
			position17(player1);
			position18(player1);
			position19(player1);
			position20(player1);
			position21(player1);
		} catch (TransactionException | NotAllowedActionException e) {
			fail(e.toString());
		}

	}

	private void position2(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		checkResources(player);
	}

	private void position3(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		moneyAmount += 2;
		victoryAmount += 2;
		checkResources(player);
	}

	private void position4(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position5(PlayerState player) throws TransactionException, NotAllowedActionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		actionServiceMethods.canChooseExtraBonusControl(player, map);
		assertTrue(!availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		checkResources(player);
	}

	private void position6(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position7(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position8(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position9(PlayerState player) throws TransactionException {
		int numberOfPolitic;
		int currentPolitics;

		numberOfPolitic = player.getPlayerData().getPoliticsHand().getNumberOfCards();

		takeNobilityBonus(player);

		victoryAmount += 3;
		currentPolitics = player.getPlayerData().getPoliticsHand().getNumberOfCards();

		assertEquals(numberOfPolitic + 1, currentPolitics);
		checkResources(player);
	}

	private void position10(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position11(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_PERMIT_TYPE)));

		checkResources(player);
	}

	private void position12(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position13(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		assistantAmount += 1;
		victoryAmount += 5;
		checkResources(player);
	}

	private void position14(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position15(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE)));
		actionServiceMethods.canChoosePermitCardBonusControl(player);
		assertTrue(!availableActions.contains(new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE)));
		checkResources(player);
	}

	private void position16(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position17(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		actionServiceMethods.canChooseExtraBonusControl(player, map);
		assertTrue(!availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		checkResources(player);
	}

	private void position18(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position19(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 8;
		checkResources(player);
	}

	private void position20(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 2;
		checkResources(player);
	}

	private void position21(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 3;
		checkResources(player);
	}

	private void takeNobilityBonus(PlayerState player) throws TransactionException {
		nobilityBonus.obtain(player);
		nobilityAmount += 1;
	}

	private void checkResources(PlayerState player) {
		resourcesCheck(player, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}

	public void finish(PlayerState player) {
		try {
			Finish finishAction = new Finish(player, turnManager, null, map);
			finishAction.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}

	public void electCouncillorInRegion3(PlayerState player) {
		CouncillorPoolManager councillorsPool = map.getCouncillorsPool();
		Color councillorColor = councillorsPool.getCouncillors().get(FIRST).getColor();
		Balcony balcony = region3.getBalcony();
		try {
			ElectCouncillor electCouncillor = new ElectCouncillor(councillorsPool, player, turnManager, councillorColor,
					balcony, null);
			electCouncillor.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
		moneyAmount += 4;
	}
}
