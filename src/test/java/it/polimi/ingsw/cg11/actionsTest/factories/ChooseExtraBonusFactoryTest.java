package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraBonusFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * @author Paola
 *
 */
public class ChooseExtraBonusFactoryTest extends ActionInitializer {
	
	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "extraBonus";
	private static final String CITY = "Dorful";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private static final String WRONG_CITY = "Milano";
	
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	
	private SerializableChooseExtraBonus serializableChooseExtraBonus1;
	private SerializableChooseExtraBonus serializableChooseExtraBonus2;
	private SerializableChooseExtraBonus serializableChooseExtraBonus3;
	
	private ChooseExtraBonusFactory chooseExtraBonusFactory1;
	private ChooseExtraBonusFactory chooseExtraBonusFactory2;
	private Token playerToken;
	
	private boolean caughtException;
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
		
	}
	public void setUpAction(){
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"city\": \"" + CITY + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		
		serializableChooseExtraBonus1 = new SerializableChooseExtraBonus();
		serializableChooseExtraBonus1.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraBonus1.setPlayerToken(pToken);
		
		chooseExtraBonusFactory1 = new ChooseExtraBonusFactory();
		
		
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"city\": \"" + WRONG_CITY + "\"}");
		
		actionConfig2 = new JSONObject(actionData2);
		
		serializableChooseExtraBonus2 = new SerializableChooseExtraBonus();
		serializableChooseExtraBonus2.setJsonConfigAsString(actionConfig2);
		
		chooseExtraBonusFactory2 = new ChooseExtraBonusFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableChooseExtraBonus3 = new SerializableChooseExtraBonus();
		serializableChooseExtraBonus3.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraBonus3.setPlayerToken(playerToken);
		serializableChooseExtraBonus3.setTopic(TOPIC);
		
	}
	
	
	public void testChooseExtraBonusFactory() {
		try {
			chooseExtraBonusFactory1.createAction(serializableChooseExtraBonus1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableChooseExtraBonus1.accept(clientController);
		serializableChooseExtraBonus1.accept(controller);
		try {
			chooseExtraBonusFactory2.createAction(serializableChooseExtraBonus2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableChooseExtraBonus3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableChooseExtraBonus3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), 
				serializableChooseExtraBonus3.toString().replaceAll("\n|\\s", ""));
	}
}
