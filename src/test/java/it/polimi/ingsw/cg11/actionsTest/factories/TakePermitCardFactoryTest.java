package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableTakePermitCard;
import it.polimi.ingsw.cg11.server.model.actionsfactory.TakePermitCardFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

public class TakePermitCardFactoryTest extends ActionInitializer {

	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "takePermitCard";
	private static final String REGION = "sea";
	private static final String POLITICCARDS = "multicolor multicolor";
	private static final String POLITIC_CARD = "politiccards";
	private static final String COLOR = "color";
	private static final String PERMIT_CARD = "permitcard";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";

	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private JSONArray politicArray;
	private SerializableTakePermitCard serializableTakePermitCard1;
	private SerializableTakePermitCard serializableTakePermitCard2;
	private SerializableTakePermitCard serializableTakePermitCard3;
	private TakePermitCardFactory takePermitCardFactory1;
	private TakePermitCardFactory takePermitCardFactory2;

	private boolean caughtException;

	private PoliticCard multicolorCard;
	private String permitCardToString;
	private String wrongPermitCardToString;
	private Token playerToken;

	public TakePermitCardFactoryTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setUpAction();
		setUpOthers();
		
	}

	public void setUpAction() {

		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		actionConfig1 = new JSONObject(actionData1);

		// permit card to offer
		permitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		actionConfig1.put(PERMIT_CARD, new JSONArray(permitCardToString));

		politicArray = setUpPoliticCards();
		
		actionConfig1.put(POLITIC_CARD, politicArray);
		serializableTakePermitCard1 = new SerializableTakePermitCard();
		serializableTakePermitCard1.setJsonConfigAsString(actionConfig1);
		serializableTakePermitCard1.setPlayerToken(pToken);
		
		takePermitCardFactory1 = new TakePermitCardFactory();

		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		actionConfig2 = new JSONObject(actionData2);
		//wrong permit card to offer
		wrongPermitCardToString = new String("[[\"Milano\"],{\"VictoryPoints\": 7}]");
		actionConfig2.put(PERMIT_CARD, new JSONArray(wrongPermitCardToString));

		
		actionConfig2.put(POLITIC_CARD, politicArray);

		serializableTakePermitCard2 = new SerializableTakePermitCard();
		serializableTakePermitCard2.setJsonConfigAsString(actionConfig2);

		takePermitCardFactory2 = new TakePermitCardFactory();

		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableTakePermitCard3 = new SerializableTakePermitCard();
		serializableTakePermitCard3.setJsonConfigAsString(actionConfig1);
		serializableTakePermitCard3.setPlayerToken(playerToken);
		serializableTakePermitCard3.setTopic(TOPIC);
		//serializableTakePermitCard3.accept(controller);
	}
	
	public JSONArray setUpPoliticCards() {
		// politic cards used for the council
		try {
			multicolorCard = new PoliticCard(ColorMap.getColor("MULTICOLOR"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		map.getTurnManager().getCurrentlyPlaying().getPlayerData().getPoliticsHand().addToHand(multicolorCard);
		map.getTurnManager().getCurrentlyPlaying().getPlayerData().getPoliticsHand().addToHand(multicolorCard);

		// politic cards in a jsonarray
		JSONObject singleValue;
		JSONArray arrayOfValue;
		Map<String, String> valuesMap;
		valuesMap = new HashMap<>();
		valuesMap.put(POLITIC_CARD, COLOR);
		StringTokenizer stringTokenizer = new StringTokenizer(POLITICCARDS);
		arrayOfValue = new JSONArray();
		while (stringTokenizer.hasMoreTokens()) {
			singleValue = new JSONObject();
			singleValue.put(COLOR, stringTokenizer.nextToken());
			arrayOfValue.put(singleValue);
		}
		return arrayOfValue;
	}

	public void testTakePermitCardFactory() {
		try {
			takePermitCardFactory1.createAction(serializableTakePermitCard1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableTakePermitCard1.accept(clientController);
		serializableTakePermitCard1.accept(controller);
		try {
			takePermitCardFactory2.createAction(serializableTakePermitCard2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException = true;
		}

		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableTakePermitCard3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableTakePermitCard3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableTakePermitCard3.toString().replaceAll("\n|\\s", ""));
	}
}
