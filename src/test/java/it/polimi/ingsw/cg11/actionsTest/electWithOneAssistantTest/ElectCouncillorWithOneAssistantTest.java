package it.polimi.ingsw.cg11.actionsTest.electWithOneAssistantTest;

import java.awt.Color;
import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;

public class ElectCouncillorWithOneAssistantTest extends ActionInitializer {

	ElectCouncillorWithOneAssistant electCouncillor;
	CouncillorPoolManager councillorPool;
	Color councillorColor;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		councillorPool = map.getCouncillorsPool();
		moneyAmount = 10;
		assistantAmount = 1;
		victoryAmount = 0;
		nobilityAmount = 0;
	}

	public void test() {

		electInRegionWithAssistant();
		electInRegionWithNoAssistant();
		
		addAssistants(player1, 4);
		assistantAmount += 4;
		
		electWrongCouncillorColor();
		electInRegionWhenPlayerHasToMakeChoice();
	}

	public void electInRegionWhenPlayerHasToMakeChoice() {
		councillorColor = Color.PINK;

		try {
			electCouncillor = new ElectCouncillorWithOneAssistant(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			
			player1.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
			
			electCouncillor.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
		}
	}

	public void electWrongCouncillorColor() {
		councillorColor = Color.GREEN;
		try {
			electCouncillor = new ElectCouncillorWithOneAssistant(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			fail();
			electCouncillor.execute();
			fail();
		} catch (NotAllowedActionException e) {
			fail();
		} catch (IllegalCreationCommandException e) {
			assertTrue(true);
		}
	}

	public void electInRegionWithAssistant() {
		councillorColor = Color.PINK;

		try {
			electCouncillor = new ElectCouncillorWithOneAssistant(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		assistantAmount -= 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);


	}

	public void electInRegionWithNoAssistant() {
		councillorColor = Color.PINK;

		try {
			electCouncillor = new ElectCouncillorWithOneAssistant(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}
}
