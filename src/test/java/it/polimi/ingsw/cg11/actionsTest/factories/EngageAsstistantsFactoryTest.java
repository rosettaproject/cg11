package it.polimi.ingsw.cg11.actionsTest.factories;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableEngageAssistant;
import it.polimi.ingsw.cg11.server.model.actionsfactory.EngageAnAssistantFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class EngageAsstistantsFactoryTest extends ActionInitializer {

	private static final String ACTIONNAME = "engageAssistant";
	private static final int PLAYERID = 0;
	private static final int WRONG_PLAYERID = 8;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private SerializableEngageAssistant serEngageAssistant1;
	private SerializableEngageAssistant serEngageAssistant2;
	private SerializableEngageAssistant serEngageAssistant3;
	private EngageAnAssistantFactory engageAssistantFactory1;
	private EngageAnAssistantFactory engageAssistantFactory2;
	private boolean caughtException;
	private Token playerToken;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setUpActions();
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serEngageAssistant3 = new SerializableEngageAssistant();
		serEngageAssistant3.setJsonConfigAsString(actionConfig1);
		serEngageAssistant3.setPlayerToken(playerToken);
		serEngageAssistant3.setTopic(TOPIC);
	}

	public void setUpActions() {
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		serEngageAssistant1 = new SerializableEngageAssistant();
		serEngageAssistant1.setJsonConfigAsString(actionConfig1);
		engageAssistantFactory1 = new EngageAnAssistantFactory();

		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME + "\"}");
		actionConfig2 = new JSONObject(actionData2);
		serEngageAssistant2 = new SerializableEngageAssistant();
		serEngageAssistant2.setJsonConfigAsString(actionConfig2);
		engageAssistantFactory2 = new EngageAnAssistantFactory();

		caughtException = false;
	}

	public void testEngageAssistant() {
		try {
			engageAssistantFactory1.createAction(serEngageAssistant1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serEngageAssistant1.accept(clientController);
		serEngageAssistant1.accept(controller);
		try {
			engageAssistantFactory2.createAction(serEngageAssistant2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException = true;
		}

		assertTrue(caughtException);
	}

	public void testOthers() {
		assertEquals(PLAYER, serEngageAssistant3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serEngageAssistant3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), serEngageAssistant3.toString().replaceAll("\n|\\s", ""));
	}

}
