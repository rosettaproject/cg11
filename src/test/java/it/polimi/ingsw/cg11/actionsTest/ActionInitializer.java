package it.polimi.ingsw.cg11.actionsTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.control.Controller;
import it.polimi.ingsw.cg11.server.model.cards.Card;
import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.actionspool.QuickAction;
import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * @author Federico
 *
 */
public class ActionInitializer extends MapConfigTest {
	

	protected static final int NUMBER_OF_PLAYER = 4;
	protected static final int MOVE_KING_COST = 2;
	protected int numberOfPoliticCard;
	protected int numberOfPermitCard;
	protected int startingMoney;
	protected int costToCorrupt;
	protected int costToMoveKing;
	protected int startingAssistant;
	protected int startingNobility;
	protected int startingVictory;
	protected int moneyAmount;
	protected int assistantAmount;
	protected int nobilityAmount;
	protected int victoryAmount;
	protected static final int FIRST = 0;
	protected static final int SECOND = 1;
	protected static final int THIRD = 2;
	protected static final int FOURTH = 3;
	protected PlayerData playerData1;
	protected PlayerData playerData2;
	protected PlayerData playerData3;
	protected PlayerData playerData4;
	protected PlayerState player1;
	protected PlayerState player2;
	protected PlayerState player3;
	protected PlayerState player4;
	protected Region region1;
	protected Region region2;
	protected Region region3;
	protected King king;
	protected Controller controller;
	protected ClientController clientController;
	protected Token pToken;

	private String clientUsername = "clientUsername";

	public void printKing() {
		System.out.println(king.toString());
		System.out.println(map.getKingBalcony().toString());
	}

	public <E extends Card> void printdDeck(Deck<E> deck) {
		System.out.println(deck.toString());
	}

	public void addAssistants(PlayerState player, int n) {
		try {
			player.getPlayerData().getAssistants().gain(n);
		} catch (IllegalResourceException e) {
		}
	}

	public void changeTurn() {
		map.getTurnManager().changeTurn();
	}

	public void addCoins(PlayerState player, int n) {
		try {
			player.getPlayerData().getMoney().gain(n);
		} catch (IllegalResourceException e) {
		}
	}

	public void drawCards(PlayerState player, int n) {
		while (n > 0) {
			player.getPlayerActionExecutor().drawPoliticCard();
			n--;
		}
	}

	public void regionsSetUp() {
		region1 = map.getRegions().get(FIRST);
		region2 = map.getRegions().get(SECOND);
		region3 = map.getRegions().get(THIRD);
	}

	public void playerSetUp() {
		player1 = map.getPlayers().get(FIRST);
		playerData1 = player1.getPlayerData();

		player2 = map.getPlayers().get(SECOND);
		playerData2 = player2.getPlayerData();

		player3 = map.getPlayers().get(THIRD);
		playerData3 = player3.getPlayerData();

		player4 = map.getPlayers().get(FOURTH);
		playerData4 = player4.getPlayerData();
	}

	public void printPlayer(PlayerState player) {
		System.out.println(player.getPlayerData().toString() + "\n\n--------------------------------------\n");
	}

	public void printRegion(Region region) {
		System.out.println(region.toString() + "\n\n--------------------------------------\n");
	}

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		tokenSetUp();
		playerSetUp();
		regionsSetUp();
		kingSetUp();
		controllerSetUp();
	}

	private void tokenSetUp() {
		pToken = new Token(clientUsername );
		pToken.setPlayerID(FIRST);
	}

	public void controllerSetUp(){
		controller = new Controller(map);
		clientController = new ClientController(pToken);
	}
	
	public void kingSetUp() {
		king = map.getKing();
		costToMoveKing = 2;
	}

	public void addMain(PlayerState player, int n) {
		while (n > 0) {
			player.getPlayerActionExecutor().addToAvailableActions(new MainAction());
			n--;
		}
	}

	public void addQuick(PlayerState player, int n) {
		while (n > 0) {
			player.getPlayerActionExecutor().addToAvailableActions(new QuickAction());
			n--;
		}
	}

	public <E extends Card> void drawAllPoliticCard(PlayerState player, Deck<E> politicDeck)
			throws EmptyPoliticDeckException {
		while (!politicDeck.isEmpty())
			player.getPlayerData().getPoliticsHand().draw();

	}
	
	public void drawAllPoliticAndPermitCard(PlayerState player, Region region){
		PermitCard permitToTake;
		FaceUpPermits faceUpPermits = region.getFaceUpPermitCards();
		TakePermitCard takePermit;
		List<PoliticCard> politicCards = new ArrayList<>();
		politicCards.add(new PoliticCard(Color.BLACK));
		while(faceUpPermits.getListSize() != 0){
			try {
				player.getPlayerData().addToPoliticsHand(new PoliticCard(Color.BLACK));
				permitToTake = faceUpPermits.getPermit(FIRST);
				takePermit = new TakePermitCard(null,player, turnManager, region1, permitToTake, politicCards, null);
				takePermit.execute();
			} catch (IllegalCreationCommandException | NotAllowedActionException e) {
				fail(e.toString());
			}		
		}
	
		try {
			drawAllPoliticCard(player1, map.getPoliticsDeck());
		} catch (EmptyPoliticDeckException e) {
			fail(e.toString());
		}
		
		assertTrue(region.getPermitCardsDeck().getCards().isEmpty());
		assertTrue(region.getFaceUpPermitCards().getListSize() == 0);
		assertTrue(map.getPoliticsDeck().isEmpty());
	}

	public void resourcesCheck(PlayerState player, int money, int assistant, int nobility, int victory){
		assertEquals(money, player.getPlayerData().getMoney().getAmount());
		assertEquals(assistant, player.getPlayerData().getAssistants().getAmount());
		assertEquals(nobility, player.getPlayerData().getNobilityPoints().getAmount());
		assertEquals(victory, player.getPlayerData().getVictoryPoints().getAmount());
	}
	
	public void printPlayerAndRegions() {

		printPlayer(player1);
		printPlayer(player2);
		printPlayer(player3);
		printPlayer(player4);

		printRegion(region1);
		printRegion(region2);
		printRegion(region3);

	}
	
	public void printGame(){
		System.out.println(map.toString());
	}
	
	public void printNobilityPath(){
		System.out.println(map.getNobilityPath().toString());
	}
	
	public void removeMainAction(PlayerState player, int mainsToRemove){
		int index = mainsToRemove;
		while(index > 0){
			player.getPlayerActionExecutor().getAvailableActions().remove(new MainAction());
			index -=1;
		}
	}
	
	public void setVariablesResources(PlayerState player){
		PlayerData pData = player.getPlayerData();
		
		moneyAmount = pData.getMoney().getAmount();
		assistantAmount = pData.getAssistants().getAmount();
		nobilityAmount = pData.getNobilityPoints().getAmount();
		victoryAmount = pData.getVictoryPoints().getAmount();
		
	}
	
}
