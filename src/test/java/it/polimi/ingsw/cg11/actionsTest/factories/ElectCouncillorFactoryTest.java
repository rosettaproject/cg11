package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillor;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ElectCouncillorFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ElectCouncillorWithOneAssistantFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class ElectCouncillorFactoryTest extends ActionInitializer {

	private String actionData1;
	private String actionData2;
	private String actionData4;
	private String actionData5;

	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private JSONObject actionConfig4;
	private JSONObject actionConfig5;

	private SerializableElectCouncillor serElectCouncillor1;
	private SerializableElectCouncillor serElectCouncillor2;
	private SerializableElectCouncillor serElectCouncillor3;
	private SerializableElectCouncillor serElectCouncillor4;
	private SerializableElectCouncillor serElectCouncillor5;
	private SerializableElectCouncillorWithOneAssistant serElectCouncillorWithAssistant1;
	private SerializableElectCouncillorWithOneAssistant serElectCouncillorWithAssistant2;
	private SerializableElectCouncillorWithOneAssistant serElectCouncillorWithAssistant3;

	private ElectCouncillorFactory electCouncillorFactory1;
	private ElectCouncillorFactory electCouncillorFactory2;
	private ElectCouncillorFactory electCouncillorFactory4;
	private ElectCouncillorFactory electCouncillorFactory5;

	private ElectCouncillorWithOneAssistantFactory electCouncillorWithAssistantFactory1;
	private ElectCouncillorWithOneAssistantFactory electCouncillorWithAssistantFactory2;

	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "electcouncillor";
	private static final String COLOR = "pink";
	private static final String BALCONY = "sea";
	private static final String WRONG_BALCONY = "sky";
	private static final String KING_BALCONY = "king";
	private static final int WRONG_PLAYERID = 7;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";

	private boolean caughtException1;
	private boolean caughtException2;
	private boolean caughtException3;
	private Token playerToken;

	public ElectCouncillorFactoryTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setUpActions();
		setUpOthers();

	}

	private void setUpOthers() {
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);

		serElectCouncillor3 = new SerializableElectCouncillor();
		serElectCouncillor3.setJsonConfigAsString(actionConfig1);
		serElectCouncillor3.setPlayerToken(playerToken);
		serElectCouncillor3.setTopic(TOPIC);

		serElectCouncillorWithAssistant3 = new SerializableElectCouncillorWithOneAssistant();
		serElectCouncillorWithAssistant3.setJsonConfigAsString(actionConfig1);
		serElectCouncillorWithAssistant3.setPlayerToken(playerToken);
		serElectCouncillorWithAssistant3.setTopic(TOPIC);
	}

	public void setUpActions() {
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"color\": \"" + COLOR + "\",\"balcony\": \"" + BALCONY + "\"}");
		actionConfig1 = new JSONObject(actionData1);

		serElectCouncillor1 = new SerializableElectCouncillor();
		serElectCouncillor1.setJsonConfigAsString(actionConfig1);
		serElectCouncillorWithAssistant1 = new SerializableElectCouncillorWithOneAssistant();
		serElectCouncillorWithAssistant1.setJsonConfigAsString(actionConfig1);
		electCouncillorFactory1 = new ElectCouncillorFactory();
		electCouncillorWithAssistantFactory1 = new ElectCouncillorWithOneAssistantFactory();

		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"color\": \"" + COLOR + "\",\"balcony\": \"" + BALCONY + "\"}");
		actionConfig2 = new JSONObject(actionData2);
		serElectCouncillor2 = new SerializableElectCouncillor();
		serElectCouncillor2.setJsonConfigAsString(actionConfig2);
		serElectCouncillorWithAssistant2 = new SerializableElectCouncillorWithOneAssistant();
		serElectCouncillorWithAssistant2.setJsonConfigAsString(actionConfig2);
		electCouncillorFactory2 = new ElectCouncillorFactory();
		electCouncillorWithAssistantFactory2 = new ElectCouncillorWithOneAssistantFactory();

		actionData4 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"color\": \"" + COLOR + "\",\"balcony\": \"" + WRONG_BALCONY + "\"}");
		actionConfig4 = new JSONObject(actionData4);
		serElectCouncillor4 = new SerializableElectCouncillor();
		serElectCouncillor4.setJsonConfigAsString(actionConfig4);
		electCouncillorFactory4 = new ElectCouncillorFactory();

		actionData5 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"color\": \"" + COLOR + "\",\"balcony\": \"" + KING_BALCONY + "\"}");
		actionConfig5 = new JSONObject(actionData5);
		serElectCouncillor5 = new SerializableElectCouncillor();
		serElectCouncillor5.setJsonConfigAsString(actionConfig5);
		electCouncillorFactory5 = new ElectCouncillorFactory();

		caughtException1 = false;
		caughtException2 = false;
		caughtException3 = false;
	}

	public void testElectCouncillorFactory() {
		try {
			electCouncillorFactory1.createAction(serElectCouncillor1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serElectCouncillor1.accept(clientController);
		serElectCouncillor1.accept(controller);
		try {
			electCouncillorFactory2.createAction(serElectCouncillor2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException1 = true;
		}
		assertTrue(caughtException1);

		try {
			electCouncillorFactory4.createAction(serElectCouncillor4, map);
		} catch (Exception e) {
			caughtException3 = true;
		}
		assertTrue(caughtException3);

		try {
			electCouncillorFactory5.createAction(serElectCouncillor5, map);
		} catch (Exception e) {
			fail();
		}
	}

	public void testElectCouncillorWithAssistantFactory() {
		try {
			electCouncillorWithAssistantFactory1.createAction(serElectCouncillorWithAssistant1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serElectCouncillorWithAssistant1.accept(clientController);
		serElectCouncillorWithAssistant1.accept(controller);
		try {
			electCouncillorWithAssistantFactory2.createAction(serElectCouncillorWithAssistant2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException2 = true;
		}
		assertTrue(caughtException2);
	}

	public void testOthers() {
		assertEquals(PLAYER, serElectCouncillor3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serElectCouncillor3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""),
				serElectCouncillor3.toString().replaceAll("\n|\\s", ""));
		assertEquals(PLAYER, serElectCouncillorWithAssistant3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serElectCouncillorWithAssistant3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""),
				serElectCouncillorWithAssistant3.toString().replaceAll("\n|\\s", ""));
	}

}
