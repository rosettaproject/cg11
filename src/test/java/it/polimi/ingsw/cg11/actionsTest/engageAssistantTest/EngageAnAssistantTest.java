/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.engageAssistantTest;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.player.actions.EngageAnAssistant;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;

/**
 * @author Federico
 *
 */
public class EngageAnAssistantTest extends ActionInitializer {

	EngageAnAssistant engageAssistant;
	
	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		
		moneyAmount = 10;
		assistantAmount = 1;
		nobilityAmount = 0;
		victoryAmount = 0;
		
		addQuick(player1, 10);
	}

	
	@Override
	public void test() {
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		
		engageAnAssistant();
		engageAnAssistant();
		engageAnAssistant();
		engageAnAssistantWithoutMoney();
		engageAnAssistantWhilePlayerHasChoiceToPerform();
	}
	
	public void engageAnAssistantWhilePlayerHasChoiceToPerform(){
		engageAssistant = new EngageAnAssistant(player1, turnManager, null);
		addCoins(player1, 3);
		moneyAmount += 3;
		try {
			player1.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
			engageAssistant.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
		
	}
	
	public void engageAnAssistant(){
		
		engageAssistant = new EngageAnAssistant(player1, turnManager, null);
		try {
			engageAssistant.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		
		moneyAmount -= 3;
		assistantAmount += 1;
		nobilityAmount += 0;
		victoryAmount += 0;
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		
	}
	
	public void engageAnAssistantWithoutMoney(){
		engageAssistant = new EngageAnAssistant(player1, turnManager, null);
		try {
			engageAssistant.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		}
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}
}
