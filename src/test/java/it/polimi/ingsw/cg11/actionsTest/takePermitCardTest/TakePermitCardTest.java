/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.takePermitCardTest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;

/**
 * 
 * @author Federico
 *
 */
public class TakePermitCardTest extends ActionInitializer {

	
	public void test() {
		drawCards(player1, 10);
		takeCard1();
		try {
			playerData1.getMoney().gain(10);
		} catch (IllegalResourceException e) {
		}
		takeCard2();
	}

	public void takeCard1() {
		
		
		resourcesCheck(player1, 10, 1, 0, 0);


		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		numberOfPoliticCard = playerData1.getPoliticsHand().getCards().size(); 
		numberOfPermitCard = playerData1.getPermitsHand().getCards().size();
		try {
			TakePermitCard takePermitCard = new TakePermitCard(map,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		
		try {
			TakePermitCard takePermitCard = new TakePermitCard(map,player2, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
			fail();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		}
		
		try {
			List<PoliticCard> politicCard2 = new ArrayList<>();
			politicCard2.add(new PoliticCard(Color.GREEN));
			TakePermitCard takePermitCard = new TakePermitCard(map,player2, map.getTurnManager(), region1, permitCard,
					politicCard2, null);
			fail();
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			assertTrue(true);
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

		resourcesCheck(player1, 0, 1, 0, 7);

		assertEquals(numberOfPoliticCard-1,playerData1.getPoliticsHand().getCards().size());
		assertEquals(numberOfPermitCard+1, playerData1.getPermitsHand().getCards().size());
	}

	public void takeCard2() {
		moneyAmount = 10;
		assistantAmount = 1;
		nobilityAmount = 0;
		victoryAmount = 7;
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		try {
			TakePermitCard takePermitCard = new TakePermitCard(map,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
			fail("Actions executed");
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		}

	}

}
