/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.oneMoreMainAction;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.player.actions.OneMoreMainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * @author Federico
 *
 */
public class OneMoreMainActionTest extends ActionInitializer {
	
	OneMoreMainAction oneMoreMainAction;
	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		
		moneyAmount = 10;
		assistantAmount = 1;
		nobilityAmount = 0;
		victoryAmount = 0;
		
	}
	
	@Override
	public void test() {

		oneMoreMainActionWithoutAssistants(player1);
		
		addAssistants(player1, 3);
		assistantAmount += 3;
		oneMoreMainAction(player1);
		
		oneMoreMainActionWithoutQuick(player1);
		
		addAssistants(player1, 3);
		assistantAmount += 3;
		addQuick(player1, 1);
		oneMoreMainAction(player1);
		oneMoreMainWithChoiceToMake(player1);
		
	}
	
	public void oneMoreMainWithChoiceToMake(PlayerState player) {
		try {
			oneMoreMainAction = new OneMoreMainAction(player, turnManager, null);
			
			player.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
			
			oneMoreMainAction.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
		}
	}

	public void oneMoreMainAction(PlayerState player){
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		try {
			oneMoreMainAction = new OneMoreMainAction(player1, turnManager, null);
			oneMoreMainAction.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
		
		assistantAmount -= 3;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		
	}
	
	
	public void oneMoreMainActionWithoutQuick(PlayerState player){
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		addAssistants(player1, 4);
		assistantAmount += 4;
		try {
			oneMoreMainAction = new OneMoreMainAction(player1, turnManager, null);
			oneMoreMainAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		}
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}
	
	public void oneMoreMainActionWithoutAssistants(PlayerState player){
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		try {
			oneMoreMainAction = new OneMoreMainAction(player1, turnManager, null);
			oneMoreMainAction.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}
	
}
