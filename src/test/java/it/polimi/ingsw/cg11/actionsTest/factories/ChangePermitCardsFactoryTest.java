package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableChangePermitCards;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChangePermitCardsFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class ChangePermitCardsFactoryTest extends ActionInitializer {

	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "changeFaceUpPermits";
	private static final String REGION = "sea";
	private static final String WRONG_REGION = "sky";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private SerializableChangePermitCards serializableChangePermitCards1;
	private SerializableChangePermitCards serializableChangePermitCards2;
	private SerializableChangePermitCards serializableChangePermitCards3;
	private ChangePermitCardsFactory changePermitCardsFactory1;
	private ChangePermitCardsFactory changePermitCardsFactory2;
	
	private boolean caughtException;
	private Token playerToken;
	
	public ChangePermitCardsFactoryTest(){
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
		
	}
	public void setUpAction(){
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		
		serializableChangePermitCards1 = new SerializableChangePermitCards();
		serializableChangePermitCards1.setJsonConfigAsString(actionConfig1);
		changePermitCardsFactory1 = new ChangePermitCardsFactory();
		
		
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + WRONG_REGION + "\"}");
		
		actionConfig2 = new JSONObject(actionData2);
		
		serializableChangePermitCards2 = new SerializableChangePermitCards();
		serializableChangePermitCards2.setJsonConfigAsString(actionConfig2);
		
		changePermitCardsFactory2 = new ChangePermitCardsFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableChangePermitCards3 = new SerializableChangePermitCards();
		serializableChangePermitCards3.setJsonConfigAsString(actionConfig1);
		serializableChangePermitCards3.setPlayerToken(playerToken);
		serializableChangePermitCards3.setTopic(TOPIC);
		
	}
	
	
	public void testChangePermitCardsFactory() {
		try {
			changePermitCardsFactory1.createAction(serializableChangePermitCards1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableChangePermitCards1.accept(clientController);
		serializableChangePermitCards1.accept(controller);
		try {
			changePermitCardsFactory2.createAction(serializableChangePermitCards2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableChangePermitCards3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableChangePermitCards3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), 
				serializableChangePermitCards3.toString().replaceAll("\n|\\s", ""));
	}
	
}
