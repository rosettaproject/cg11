package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.view.Token;

public class SerializableMapStartUpConfigTest extends ActionInitializer {
	private SerializableMapStartupConfig serializableMapStartupConfig;
	private String topic;
	private List<Token> tokenList;
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		serializableMapStartupConfig = new SerializableMapStartupConfig();
		serializableMapStartupConfig.setJsonConfigAsString(mapConfig);
		topic = "topic";
		tokenList = new ArrayList<>();
		tokenList.add(pToken);
		tokenList.add(pToken);
	}
	
	public void test(){
		serializableMapStartupConfig.setTopic(topic);
		assertEquals(topic, serializableMapStartupConfig.getTopic());
		
		
		serializableMapStartupConfig.setPlayerToken(pToken);
		assertEquals(pToken, serializableMapStartupConfig.getPlayerToken());
		
		serializableMapStartupConfig.accept(clientController);
		serializableMapStartupConfig.accept(controller);
		serializableMapStartupConfig.addTokenList(tokenList);
		assertFalse(mapConfig.toString(1).equals(serializableMapStartupConfig.toString()));
	}
}
