/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.marketTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffer;
import it.polimi.ingsw.cg11.server.model.market.MarketOfferFactory;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.AcceptMarketOffer;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferAssistants;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPoliticCard;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * @author Federico
 *
 */
public class MarketTest extends ActionInitializer {


	private static final String FIRST_GAME_PHASE = "StandardPlayPhase";
	private static final String SECOND_GAME_PHASE = "MarketOfferingPhase";
	private static final String THIRD_GAME_PHASE = "MarketAcceptingPhase";

	private CouncillorPoolManager councillorPool;

	private OfferAssistants offerAssistantAction;
	private OfferPermitCard offerPermitCardAction;
	private OfferPoliticCard offerPoliticCardAction;

	private AcceptMarketOffer acceptOffer;

	private MarketOffer permitCardOffer;
	private MarketOffer assistantOffer;
	private MarketOffer politicCardOffer;

	private MarketOffers marketOffers;
	private MarketOfferFactory offerFactory;

	private PermitCard permitCardToTake;

	private final int permitCardOfferCost = 6;
	private final int assistantOfferCost = 5;
	private final int politicCardOfferCost = 7;

	private final int assistantsToOffer = 2;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();

		addCoins(player1, 20);

		startingMoney = 10 + 20;
		startingAssistant = 1;
		startingNobility = 0;
		startingVictory = 0;

		moneyAmount = startingMoney;
		assistantAmount = startingAssistant;
		nobilityAmount = startingNobility;
		victoryAmount = startingNobility;

		councillorPool = map.getCouncillorsPool();
		marketOffers = new MarketOffers();
		offerFactory = new MarketOfferFactory();
	}

	@Override
	public void test() {
		drawCards(player1, 50);

		player1OfferInWrongPhase();
		player1AcceptInWrongPhase();
		
		player1TakeFirstPermitCardOfRegion1();
		goToMarketPhase();
		makeOffers();
		takeOffers();

	}
	
	public void player1TryToAcceptAJustTakenOffer(PlayerState player){
		acceptOffer = new AcceptMarketOffer(null, player, turnManager, politicCardOffer, marketOffers, "");
		try {
			acceptOffer.execute();
			fail();
		} catch (NotAllowedActionException e) {
			
		}
	}
	
	public void player1AcceptInWrongPhase(){
		acceptOffer = new AcceptMarketOffer(null, player1, turnManager, politicCardOffer, marketOffers, "");
		try {
			acceptOffer.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertNotNull("Exception is null", e);
			
		}
	}

	public void player1OfferInWrongPhase() {
		PoliticCard politicCardToOffer = new PoliticCard(Color.ORANGE);

		offerPoliticCardAction = new OfferPoliticCard(player1, turnManager, marketOffers, politicCardToOffer,
				politicCardOfferCost, "");
		try {
			offerPoliticCardAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
	}

	public void player4MakePoliticCardOffertWithoutPoliticCard(PlayerState player) {
		PoliticCard politicCardToOffer = new PoliticCard(Color.ORANGE);

		offerPoliticCardAction = new OfferPoliticCard(player, turnManager, marketOffers, politicCardToOffer,
				politicCardOfferCost, "");
		try {
			offerPoliticCardAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
	}

	public void player4MakePoliticCardOffertWithWrongColor(PlayerState player) {
		PoliticCard politicCardToOffer = new PoliticCard(Color.DARK_GRAY);
		
		offerPoliticCardAction = new OfferPoliticCard(player, turnManager, marketOffers, politicCardToOffer,
				politicCardOfferCost, "");
		try {
			offerPoliticCardAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
	}

	public void player4MakeOfferWithoutAssistants(PlayerState player) {
		int numberOfAssistants = 100;
		offerAssistantAction = new OfferAssistants(player, turnManager, marketOffers, numberOfAssistants,
				assistantOfferCost, "");
		try {
			offerAssistantAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}

	}

	public void takeOffers() {
		PlayerState tmpPlayer = null;

		int playerIndex = 0;
		while (playerIndex < NUMBER_OF_PLAYER) {

			tmpPlayer = turnManager.getCurrentlyPlaying();

			switch (tmpPlayer.getPlayerData().getPlayerID()) {

			case 0:
				player1TakePoliticCardOffer(tmpPlayer);
				player1TakePoliticCardOffer(tmpPlayer);
				player1TryToAcceptAJustTakenOffer(tmpPlayer);
				finish(tmpPlayer);
				break;
			case 1:
				finish(tmpPlayer);
				break;
			case 2:
				player3TakeAssistantOffer(tmpPlayer);
				finish(tmpPlayer);
				break;
			case 3:
				player4TakePermitCardOffer(tmpPlayer);
				finish(tmpPlayer);
				break;
			default:
				fail();
				break;
			}

			playerIndex += 1;
		}

		if (tmpPlayer == null)
			fail();

		assertEquals(FIRST_GAME_PHASE, turnManager.getCurrentGamePhase().getPhaseName());
	}

	public void player1TakePoliticCardOffer(PlayerState player) {
		moneyAmount = player.getPlayerData().getMoney().getAmount();
		assistantAmount = player.getPlayerData().getAssistants().getAmount();
		nobilityAmount = player.getPlayerData().getNobilityPoints().getAmount();
		victoryAmount = player.getPlayerData().getVictoryPoints().getAmount();

		acceptOffer = new AcceptMarketOffer(null, player, turnManager, politicCardOffer, marketOffers, "");
		try {
			acceptOffer.execute();
		} catch (NotAllowedActionException e) {
			fail();
		}

		resourcesCheck(player, moneyAmount - politicCardOfferCost, assistantAmount, nobilityAmount, victoryAmount);
	}

	public void player3TakeAssistantOffer(PlayerState player) {
		moneyAmount = player.getPlayerData().getMoney().getAmount();
		assistantAmount = player.getPlayerData().getAssistants().getAmount();
		nobilityAmount = player.getPlayerData().getNobilityPoints().getAmount();
		victoryAmount = player.getPlayerData().getVictoryPoints().getAmount();

		acceptOffer = new AcceptMarketOffer(null, player, turnManager, assistantOffer, marketOffers, "");
		try {
			acceptOffer.execute();
		} catch (NotAllowedActionException e) {
			fail();
		}

		resourcesCheck(player, moneyAmount - assistantOfferCost, assistantAmount + assistantsToOffer, nobilityAmount,
				victoryAmount);
	}

	public void player4TakePermitCardOffer(PlayerState player) {

		moneyAmount = player.getPlayerData().getMoney().getAmount();
		assistantAmount = player.getPlayerData().getAssistants().getAmount();
		nobilityAmount = player.getPlayerData().getNobilityPoints().getAmount();
		victoryAmount = player.getPlayerData().getVictoryPoints().getAmount();

		acceptOffer = new AcceptMarketOffer(null, player, turnManager, permitCardOffer, marketOffers, "");
		try {
			acceptOffer.execute();
		} catch (NotAllowedActionException e) {
			fail();
		}

		moneyAmount = moneyAmount - permitCardOfferCost + permitCardToTake.getPermitBonus().getMoneyAmount();
		assistantAmount += permitCardToTake.getPermitBonus().getAssistantsAmount();
		nobilityAmount += permitCardToTake.getPermitBonus().getNobilityPointsAmount();
		victoryAmount += +permitCardToTake.getPermitBonus().getVictoryPointsAmount();

		resourcesCheck(player, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void makeOffers() {
		PlayerState tmpPlayer = null;

		int playerIndex = 0;
		while (playerIndex < NUMBER_OF_PLAYER) {

			tmpPlayer = turnManager.getCurrentlyPlaying();

			switch (tmpPlayer.getPlayerData().getPlayerID()) {

			case 0:
				player1offerPermitCardWithoutPermitCard(tmpPlayer);
				player1offerPermitCard(tmpPlayer);
				finish(tmpPlayer);
				break;
			case 1:
				player2offerTwoAssistants(tmpPlayer);
				finish(tmpPlayer);
				break;
			case 2:
				player3offerPoliticCard(tmpPlayer);
				player3offerPoliticCard(tmpPlayer);
				player3offerWrongPoliticCard(tmpPlayer);
				finish(tmpPlayer);
				break;
			case 3:
				player4MakeOfferWithoutAssistants(tmpPlayer);
				player4MakePoliticCardOffertWithWrongColor(tmpPlayer);
				player4MakePoliticCardOffertWithoutPoliticCard(tmpPlayer);
				finish(tmpPlayer);
				break;
			default:
				fail();
				break;
			}
			playerIndex += 1;
		}

		if (tmpPlayer == null)
			fail();

		assertEquals(THIRD_GAME_PHASE, turnManager.getCurrentGamePhase().getPhaseName());
	}

	public void player1offerPermitCardWithoutPermitCard(PlayerState player) {
		PermitCard permitCardPlayer1HasNot = region1.getFaceUpPermitCards().getPermit(FIRST);

		offerPermitCardAction = new OfferPermitCard(player, turnManager, marketOffers, permitCardPlayer1HasNot,
				permitCardOfferCost, "");
		try {
			offerPermitCardAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
	}

	public void player3offerPoliticCard(PlayerState player) {

		// Player3 is performing this action

		PoliticCard politicCardToOffer = new PoliticCard(Color.CYAN);

		offerPoliticCardAction = new OfferPoliticCard(player, turnManager, marketOffers, politicCardToOffer,
				politicCardOfferCost, "");
		try {
			offerPoliticCardAction.execute();
			politicCardOffer = offerFactory.createMarketOffer(player, politicCardToOffer, politicCardOfferCost);
		} catch (NotAllowedActionException e) {
			fail();
		}

	}

	public void player3offerWrongPoliticCard(PlayerState player) {

		// Player3 is performing this action

		PoliticCard politicCardToOffer = new PoliticCard(Color.BLUE);

		offerPoliticCardAction = new OfferPoliticCard(player, turnManager, marketOffers, politicCardToOffer,
				politicCardOfferCost, "");
		try {
			offerPoliticCardAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertNotNull(e);
		}

	}
	
	public void player2offerTwoAssistants(PlayerState player) {

		// Player2 can perform this action

		offerAssistantAction = new OfferAssistants(player, turnManager, marketOffers, assistantsToOffer,
				assistantOfferCost, "");
		try {
			offerAssistantAction.execute();
			assistantOffer = offerFactory.createMarketOffer(player, assistantsToOffer, assistantOfferCost);
		} catch (NotAllowedActionException e) {
			fail();
		}
	}

	public void player1offerPermitCard(PlayerState player) {
		// Player1 can perform this action

		offerPermitCardAction = new OfferPermitCard(player, turnManager, marketOffers, permitCardToTake,
				permitCardOfferCost, "");
		try {
			offerPermitCardAction.execute();
			permitCardOffer = offerFactory.createMarketOffer(player, permitCardToTake, permitCardOfferCost);
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

	}

	public void player1TakeFirstPermitCardOfRegion1() {

		TakePermitCard takePermitCardAction;
		List<PoliticCard> playerPoliticCards;

		resourcesCheck(player1, startingMoney, startingAssistant, startingNobility, victoryAmount);

		permitCardToTake = region3.getFaceUpPermitCards().getPermit(SECOND);

		playerPoliticCards = new ArrayList<>();
		playerPoliticCards.add(new PoliticCard(Color.ORANGE));
		playerPoliticCards.add(new PoliticCard(Color.ORANGE));
		playerPoliticCards.add(new PoliticCard(Color.ORANGE));
		playerPoliticCards.add(new PoliticCard(Color.ORANGE));

		try {
			takePermitCardAction = new TakePermitCard(null, player1, turnManager, region3, permitCardToTake,
					playerPoliticCards, "");

			takePermitCardAction.execute();

		} catch (IllegalCreationCommandException e) {
			fail();
		} catch (NotAllowedActionException e) {
			fail();
		}

		victoryAmount = startingVictory + 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

		assertTrue(!region3.getFaceUpPermitCards().contains(permitCardToTake));
		assertTrue(playerData1.getPermitsHand().getCards().contains(permitCardToTake));

		finish(player1);

		assertTrue(!turnManager.isPlaying(player1));
		assertTrue(turnManager.isPlaying(player2));

	}

	public void electCouncillor(PlayerState player) {
		try {
			if (!turnManager.isPlaying(player)) {
				fail();
			}

			ElectCouncillor electCouncillor = new ElectCouncillor(councillorPool, player, turnManager, Color.CYAN,
					region1.getBalcony(), null);
			electCouncillor.execute();

			finish(player);
		} catch (NotAllowedActionException e) {
			fail();
		} catch (IllegalCreationCommandException e) {
			fail();
		}
	}

	public void goToMarketPhase() {

		assertEquals(FIRST_GAME_PHASE, turnManager.getCurrentGamePhase().getPhaseName());

		electCouncillor(player2); // From p2 to p3
		electCouncillor(player3); // From p3 to p4
		electCouncillor(player4); // Starting market phase...

		assertEquals(SECOND_GAME_PHASE, turnManager.getCurrentGamePhase().getPhaseName());
	}

	public void finish(PlayerState player) {

		try {
			Finish finish = new Finish(player, turnManager, "", map);
			finish.execute();
		} catch (NotAllowedActionException e) {
			fail();
		} catch (IllegalCreationCommandException e) {
			fail();
		}
	}

}
