package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.actionsfactory.BuildEmporiumWithPermitCardFactory;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;
/**
 * 
 * @author Paola
 *
 */
public class BuildEmporiumFactoryTest extends ActionInitializer {
		
	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "buildEmporium";
	private static final String REGION = "sea";
	private static final String PERMIT_CARD = "permitcard";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private static final String CITY = "Dorful";
	
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private String permitCardToString;
	
	private SerializableBuildEmporiumWithPermitCard serializableBuildEmporiumWithPermitCard1;
	private SerializableBuildEmporiumWithPermitCard serializableBuildEmporiumWithPermitCard2;
	private SerializableBuildEmporiumWithPermitCard serializableBuildEmporiumWithPermitCard3;
	
	private BuildEmporiumWithPermitCardFactory buildEmporiumWithPermitCardFactory1;
	private BuildEmporiumWithPermitCardFactory buildEmporiumWithPermitCardFactory2;
	
	private boolean caughtException;
	private String wrongPermitCardToString;
	private Token playerToken;
	private PermitCard permitCard;
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setUpAction();
		setUpOthers();

	}

	public void setUpAction() {

		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\",\"city\": \"" + CITY + "\"}");
		actionConfig1 = new JSONObject(actionData1);

		// permit card to offer
		permitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		actionConfig1.put(PERMIT_CARD, new JSONArray(permitCardToString));

		serializableBuildEmporiumWithPermitCard1 = new SerializableBuildEmporiumWithPermitCard();
		serializableBuildEmporiumWithPermitCard1.setJsonConfigAsString(actionConfig1);
		serializableBuildEmporiumWithPermitCard1.setPlayerToken(pToken);
		
		buildEmporiumWithPermitCardFactory1 = new BuildEmporiumWithPermitCardFactory();
		
		//add the permit card to the player's permit hand
		permitCard = map.getRegions().get(0).getFaceUpPermitCards().getPermit(0);
		player1.getPlayerData().getPermitsHand().addToHand(permitCard);

		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\",\"city\": \"" + CITY + "\"}");
		actionConfig2 = new JSONObject(actionData2);
		
		// permit card to offer
		wrongPermitCardToString = new String("[[\"Milano\"],{\"VictoryPoints\": 7}]");
		actionConfig2.put(PERMIT_CARD, new JSONArray(wrongPermitCardToString));


		serializableBuildEmporiumWithPermitCard2 = new SerializableBuildEmporiumWithPermitCard();
		serializableBuildEmporiumWithPermitCard2.setJsonConfigAsString(actionConfig2);
		buildEmporiumWithPermitCardFactory2 = new BuildEmporiumWithPermitCardFactory();

		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableBuildEmporiumWithPermitCard3 = new SerializableBuildEmporiumWithPermitCard();
		serializableBuildEmporiumWithPermitCard3.setJsonConfigAsString(actionConfig1);
		serializableBuildEmporiumWithPermitCard3.setPlayerToken(playerToken);
		serializableBuildEmporiumWithPermitCard3.setTopic(TOPIC);
	}
	

	public void testTakePermitCardFactory() {
		try {
			buildEmporiumWithPermitCardFactory1.createAction(serializableBuildEmporiumWithPermitCard1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableBuildEmporiumWithPermitCard1.accept(clientController);
		serializableBuildEmporiumWithPermitCard1.accept(controller);
		try {
			buildEmporiumWithPermitCardFactory2.createAction(serializableBuildEmporiumWithPermitCard2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException = true;
		}

		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableBuildEmporiumWithPermitCard3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableBuildEmporiumWithPermitCard3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableBuildEmporiumWithPermitCard3.toString().replaceAll("\n|\\s", ""));
	}
}
