package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraPermitBonusFactory;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * @author paolasanfilippo
 *
 */
public class ChooseExtraPermitBonusFactoryTest extends ActionInitializer {
	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "extraPermit";
	private static final String REGION = "sea";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private static final String PERMIT_CARD = "permitcard";
	
	private String actionData1;
	private String actionData2;
	private String actionData3;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private JSONObject actionConfig3;
	
	private SerializableChooseExtraPermitBonus serializableChooseExtraPermitBonus1;
	private SerializableChooseExtraPermitBonus serializableChooseExtraPermitBonus2;
	private SerializableChooseExtraPermitBonus serializableChooseExtraPermitBonus3;
	private SerializableChooseExtraPermitBonus serializableChooseExtraPermitBonus4;
	
	private ChooseExtraPermitBonusFactory chooseExtraPermitBonusFactory1;
	private ChooseExtraPermitBonusFactory chooseExtraPermitBonusFactory2;
	private ChooseExtraPermitBonusFactory chooseExtraPermitBonusFactory3;
	
	private boolean caughtException;
	private Token playerToken;
	private String usedPermitCardToString;
	private String wrongPermitCardToString;
	private PermitCard usedPermitCard;
	
	private String unusedPermitCardToString;
	private PermitCard unusedPermitCard;
	
	
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
		
	}
	public void setUpAction(){
		//USED PERMIT
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		
		usedPermitCard = map.getRegions().get(0).getFaceUpPermitCards().getPermit(0);
		player1.getPlayerData().addToUsedPermits(usedPermitCard);
		
		usedPermitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		actionConfig1.put(PERMIT_CARD, new JSONArray(usedPermitCardToString));
		
		serializableChooseExtraPermitBonus1 = new SerializableChooseExtraPermitBonus();
		serializableChooseExtraPermitBonus1.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraPermitBonus1.setPlayerToken(pToken);
		
		chooseExtraPermitBonusFactory1 = new ChooseExtraPermitBonusFactory();
		
		
		//NOT EXISTING PERMIT
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		wrongPermitCardToString = new String("[[\"Milano\"],{\"VictoryPoints\": 7}]");
		actionConfig2 = new JSONObject(actionData2);
		actionConfig2.put(PERMIT_CARD, new JSONArray(wrongPermitCardToString));
		serializableChooseExtraPermitBonus2 = new SerializableChooseExtraPermitBonus();
		serializableChooseExtraPermitBonus2.setJsonConfigAsString(actionConfig2);
		
		chooseExtraPermitBonusFactory2 = new ChooseExtraPermitBonusFactory();
		
		//UNUSED PERMIT
		actionData3 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		
		unusedPermitCardToString = new String("[[\"Arkon\"],{\"VictoryPoints\": 4,\"Money\": 3}]");
		actionConfig3 = new JSONObject(actionData3);
		actionConfig3.put(PERMIT_CARD, new JSONArray(unusedPermitCardToString));
		serializableChooseExtraPermitBonus3 = new SerializableChooseExtraPermitBonus();
		serializableChooseExtraPermitBonus3.setJsonConfigAsString(actionConfig3);
		chooseExtraPermitBonusFactory3 = new ChooseExtraPermitBonusFactory();
		unusedPermitCard = map.getRegions().get(0).getFaceUpPermitCards().getPermit(1);
		player1.getPlayerData().addToPermitsHand(unusedPermitCard);
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableChooseExtraPermitBonus4 = new SerializableChooseExtraPermitBonus();
		serializableChooseExtraPermitBonus4.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraPermitBonus4.setPlayerToken(playerToken);
		serializableChooseExtraPermitBonus4.setTopic(TOPIC);
		
	}
	
	
	public void testChooseExtraPermitBonusFactory() {
		try {
			chooseExtraPermitBonusFactory1.createAction(serializableChooseExtraPermitBonus1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableChooseExtraPermitBonus1.accept(clientController);
		serializableChooseExtraPermitBonus1.accept(controller);
		try {
			chooseExtraPermitBonusFactory2.createAction(serializableChooseExtraPermitBonus2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
		
		try {
			chooseExtraPermitBonusFactory3.createAction(serializableChooseExtraPermitBonus3, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableChooseExtraPermitBonus4.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableChooseExtraPermitBonus4.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableChooseExtraPermitBonus4.toString().replaceAll("\n|\\s", ""));
	}
}
