package it.polimi.ingsw.cg11.actionsTest.finish;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;

public class FinishTest extends ActionInitializer{

	private Finish finishAction;
		
	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		
		try {
			finishAction = new Finish(player1, turnManager, null, map);
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}
	
	@Override
	public void test() {
		
		finishWhenPlayerCannotFinish(); //Player has a main action to do
		
		removeMainAction(player1, 1);
		
		finishWhenPlayerCanFinish();	//Player finish his turn
		finishWhenPlayerCannotFinish(); //It's not player turn
	}
	
	public void finishWhenPlayerCanFinish(){
		try {
			finishAction.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		assertTrue(true);
	}
	
	public void finishWhenPlayerCannotFinish(){
		try {
			finishAction.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
		assertTrue(true);
	}
	
	
}
