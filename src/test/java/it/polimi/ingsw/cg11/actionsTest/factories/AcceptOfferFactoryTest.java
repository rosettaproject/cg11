package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableAcceptMarketOffer;
import it.polimi.ingsw.cg11.server.model.actionsfactory.AcceptMarketOfferFactory;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.market.NoSuchOfferException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.market.AssistantsOffer;
import it.polimi.ingsw.cg11.server.model.market.PermitCardOffer;
import it.polimi.ingsw.cg11.server.model.market.PoliticCardOffer;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

public class AcceptOfferFactoryTest extends ActionInitializer {
	
	private static final int PLAYERID = 0;
	private static final String OFFER_ASSISTANTS = "AssistantsOffer";
	private static final String OFFER_POLITIC = "PoliticCardOffer";
	private static final String OFFER_PERMIT = "PermitCardOffer";
	private static final int OFFER_COST = 2;
	private static final int ASSISTANTS_AMOUNT = 1;
	private static final int NUMBER = 10;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private static final String POLITIC_CARD = "politiccard";
	private static final String PERMIT_CARD = "permitcard";
	private static final String REGION = "sea";
	
	private String actionData1;
	private String actionData2;
	private String actionData3;
	private String actionData4;
	private String actionData5;
	
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private JSONObject actionConfig3;
	private JSONObject actionConfig4;
	private JSONObject actionConfig5;

	private SerializableAcceptMarketOffer serializableAcceptMarketOffer1;
	private SerializableAcceptMarketOffer serializableAcceptMarketOffer2;
	private SerializableAcceptMarketOffer serializableAcceptMarketOffer3;
	private SerializableAcceptMarketOffer serializableAcceptMarketOffer4;
	private SerializableAcceptMarketOffer serializableAcceptMarketOffer5;
	private SerializableAcceptMarketOffer serializableAcceptMarketOffer6;
	
	private AcceptMarketOfferFactory acceptMarketOfferFactory1;
	private AcceptMarketOfferFactory acceptMarketOfferFactory2;
	private AcceptMarketOfferFactory acceptMarketOfferFactory3;
	private AcceptMarketOfferFactory acceptMarketOfferFactory4;
	private AcceptMarketOfferFactory acceptMarketOfferFactory5;
	
	
	private boolean caughtException;
	private boolean caughtException2;
	
	private AssistantsOffer assistantsOffer1;
	private AssistantsOffer assistantsOffer2;
	private PermitCardOffer permitCardOffer1;
	private PermitCardOffer permitCardOffer2;
	private PoliticCardOffer politicCardOffer1;
	private PoliticCardOffer politicCardOffer2;
	
	private Token playerToken;
	private PermitCard permitCard;
	private PoliticCard politicCard;
	private String politicCardToString;
	private String permitCardToString;
	private AssistantsOffer assistantsOffer3;
	private AssistantsOffer assistantsOffer4;
	private PermitCardOffer permitCardOffer3;
	private PoliticCardOffer politicCardOffer3;
	private PoliticCard politicCard1;
	
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpResources();
		setUpOffers();
		setUpAction();
		setUpOthers();
	}
	
	private void setUpResources(){
		permitCard = map.getRegions().get(0).getFaceUpPermitCards().getPermit(0);
		player1.getPlayerData().addToPermitsHand(permitCard);
		try {
			politicCard = new PoliticCard(ColorMap.getColor("PINK"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		try {
			politicCard1 = new PoliticCard(ColorMap.getColor("BLACK"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		player1.getPlayerData().addToPoliticsHand(politicCard);
		
	}
	private void setUpOffers() {
		
		assistantsOffer1 = new AssistantsOffer(player1, ASSISTANTS_AMOUNT, OFFER_COST);
		assistantsOffer2 = new AssistantsOffer(player1, ASSISTANTS_AMOUNT, OFFER_COST);
		assistantsOffer3 = new AssistantsOffer(null, ASSISTANTS_AMOUNT, OFFER_COST);
		assistantsOffer4 = new AssistantsOffer(player1, NUMBER, OFFER_COST);
		
		permitCardOffer1 = new PermitCardOffer(player1, permitCard, OFFER_COST);
		permitCardOffer2 = new PermitCardOffer(player1, permitCard, OFFER_COST);
		permitCardOffer3 = new PermitCardOffer(null, null, OFFER_COST);

		politicCardOffer1 = new PoliticCardOffer(player1, politicCard, OFFER_COST);
		politicCardOffer2 = new PoliticCardOffer(player1, politicCard, OFFER_COST);
		politicCardOffer3 = new PoliticCardOffer(null, null, OFFER_COST);
		
		map.getMarketModel().addOffer(assistantsOffer1);
		map.getMarketModel().addOffer(permitCardOffer1);
		map.getMarketModel().addOffer(politicCardOffer1);
		map.getMarketModel().addOffer(assistantsOffer4);
		
	}
	
	public void setUpAction() {
		//assistants offer
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + OFFER_ASSISTANTS
				 + "\",\"OfferType\": \"" + OFFER_ASSISTANTS
				+ "\",\"OfferingPlayer\": \"" + player1.getPlayerData().getPlayerID()
				+ "\",\"OfferCost\": \"" + OFFER_COST + "\",\n\"assistants\": \"" + ASSISTANTS_AMOUNT + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		serializableAcceptMarketOffer1 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer1.setJsonConfigAsString(actionConfig1);
		serializableAcceptMarketOffer1.setPlayerToken(pToken);
		
		acceptMarketOfferFactory1 = new AcceptMarketOfferFactory();
		
		//politic card offer
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + OFFER_POLITIC
				 + "\",\"OfferType\": \"" + OFFER_POLITIC
				+ "\",\"OfferingPlayer\": \"" + player1.getPlayerData().getPlayerID()
				+ "\",\"OfferCost\": \"" + OFFER_COST + "\"}");
		actionConfig2 = new JSONObject(actionData2);
		politicCardToString = new String("{\"color\": \"PINK\"}");
		actionConfig2.put(POLITIC_CARD, new JSONObject(politicCardToString));
		serializableAcceptMarketOffer2 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer2.setJsonConfigAsString(actionConfig2);
		acceptMarketOfferFactory2 = new AcceptMarketOfferFactory();
		
		//permitOffer
		actionData3 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + OFFER_PERMIT
				 + "\",\"OfferType\": \"" + OFFER_PERMIT
				+ "\",\"OfferingPlayer\": \"" + player1.getPlayerData().getPlayerID()
				+ "\",\"OfferCost\": \"" + OFFER_COST + "\",\"region\": \"" + REGION + "\"}");
		actionConfig3 = new JSONObject(actionData3);
		permitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		actionConfig3.put(PERMIT_CARD, new JSONArray(permitCardToString));
		serializableAcceptMarketOffer3 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer3.setJsonConfigAsString(actionConfig3);
		acceptMarketOfferFactory3 = new AcceptMarketOfferFactory();
		
		//wrong offer 1
		actionData4 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + OFFER_PERMIT
				 + "\",\"OfferType\": \"" + "NobilityOffer"
				+ "\",\"OfferingPlayer\": \"" + player1.getPlayerData().getPlayerID()
				+ "\",\"OfferCost\": \"" + OFFER_COST + "\"}");
		actionConfig4 = new JSONObject(actionData4);
		serializableAcceptMarketOffer4 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer4.setJsonConfigAsString(actionConfig4);
		acceptMarketOfferFactory4 = new AcceptMarketOfferFactory();
		
		//wrong offer 2
		actionData5 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + OFFER_ASSISTANTS
				 + "\",\"OfferType\": \"" + OFFER_ASSISTANTS
				+ "\",\"OfferingPlayer\": \"" + player1.getPlayerData().getPlayerID()
				+ "\",\"OfferCost\": \"" + OFFER_COST +  "\",\n\"assistants\": \"" + -2 +"\"}");
		actionConfig5 = new JSONObject(actionData5);
		serializableAcceptMarketOffer5 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer5.setJsonConfigAsString(actionConfig5);
		acceptMarketOfferFactory5 = new AcceptMarketOfferFactory();
		
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableAcceptMarketOffer6 = new SerializableAcceptMarketOffer();
		serializableAcceptMarketOffer6.setJsonConfigAsString(actionConfig1);
		serializableAcceptMarketOffer6.setPlayerToken(playerToken);
		serializableAcceptMarketOffer6.setTopic(TOPIC);
		
	}
	
	public void testAcceptOffer(){
		try {
			acceptMarketOfferFactory1.createAction(serializableAcceptMarketOffer1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableAcceptMarketOffer1.accept(clientController);
		serializableAcceptMarketOffer1.accept(controller);
		try {
			acceptMarketOfferFactory2.createAction(serializableAcceptMarketOffer2, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		
		try {
			acceptMarketOfferFactory3.createAction(serializableAcceptMarketOffer3, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		
		try {
			acceptMarketOfferFactory4.createAction(serializableAcceptMarketOffer4, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
		//per lanciare eccezione illegal resource in assistantsOffer, TODO
		try {
			acceptMarketOfferFactory5.createAction(serializableAcceptMarketOffer5, map);
		} catch (IllegalCreationCommandException e) {
			caughtException2 = true;
		}
		assertTrue(caughtException2);
		
	}
	
	public void testRemoveOffert(){
		try {
			map.getMarketModel().removeOffer(assistantsOffer3);
			fail();
		} catch (NoSuchOfferException e) {
		}
	}
	public void testOthers(){
		assertEquals(PLAYER, serializableAcceptMarketOffer6.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableAcceptMarketOffer6.getTopic());
		assertEquals(serializableAcceptMarketOffer6.getJsonConfigAsString(), 
				serializableAcceptMarketOffer6.toString().replaceAll("\n|\\s", ""));
		assertEquals(player1, permitCardOffer1.getOfferingPlayer());
		assertEquals(player1, politicCardOffer1.getOfferingPlayer());
		assertEquals(player1, assistantsOffer1.getOfferingPlayer());
	}
	
	public void testEqualsAndHashCode(){
		//assistants
		assertEquals(assistantsOffer1.hashCode(), assistantsOffer2.hashCode());
		assertEquals(assistantsOffer3.hashCode(), 30814);
		assertTrue(assistantsOffer1.equals(assistantsOffer1));
		assertFalse(assistantsOffer1.equals(null));
		assertFalse(assistantsOffer1.equals(permitCard));
		assertFalse(assistantsOffer1.equals(new AssistantsOffer(player1, NUMBER, OFFER_COST)));
		assertFalse(assistantsOffer1.equals(new AssistantsOffer(player1, ASSISTANTS_AMOUNT, NUMBER)));
		assertFalse(assistantsOffer3.equals(assistantsOffer1));
		assertFalse(assistantsOffer1.equals(new AssistantsOffer(player2, ASSISTANTS_AMOUNT, OFFER_COST)));
		assertTrue(assistantsOffer3.equals(assistantsOffer3));
		
		//permit
		assertEquals(permitCardOffer1.hashCode(), permitCardOffer2.hashCode());
		assertEquals(permitCardOffer3.hashCode(), 31713);
		assertTrue(permitCardOffer1.equals(permitCardOffer1));
		assertFalse(permitCardOffer1.equals(null));
		assertFalse(permitCardOffer1.equals(new PermitCardOffer(player1, permitCard, NUMBER)));
		assertFalse(permitCardOffer3.equals(permitCardOffer1));
		assertFalse(permitCardOffer1.equals(
				new PermitCardOffer(player1, map.getRegions().get(0).getFaceUpPermitCards().getPermit(1), OFFER_COST)));
		assertFalse(permitCardOffer3.equals(new PermitCardOffer(player1, null, OFFER_COST)));
		assertTrue(permitCardOffer3.equals(permitCardOffer3));
		assertFalse(permitCardOffer1.equals(new PermitCardOffer(player2, permitCard, OFFER_COST)));
		
		//politic
		assertEquals(politicCardOffer1.hashCode(), politicCardOffer2.hashCode());
		assertEquals(politicCardOffer3.hashCode(), 31713);
		assertTrue(politicCardOffer1.equals(politicCardOffer2));
		assertFalse(politicCardOffer1.equals(null));
		assertFalse(politicCardOffer1.equals(new PoliticCardOffer(player1, politicCard, NUMBER)));
		assertFalse(politicCardOffer3.equals(politicCardOffer1));
		assertFalse(politicCardOffer1.equals(
				new PoliticCardOffer(player1, politicCard1, OFFER_COST)));
		assertFalse(politicCardOffer3.equals(new PoliticCardOffer(player1, null, OFFER_COST)));
		assertTrue(politicCardOffer3.equals(politicCardOffer3));
		assertFalse(politicCardOffer1.equals(new PoliticCardOffer(player2, politicCard, OFFER_COST)));
		
	}
}
