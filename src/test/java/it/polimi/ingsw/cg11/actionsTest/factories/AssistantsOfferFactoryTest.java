package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableOfferAssistants;
import it.polimi.ingsw.cg11.server.model.actionsfactory.OfferAssistantsFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class AssistantsOfferFactoryTest extends ActionInitializer {
	private static final String ACTIONNAME = "assistantsOffer";
	private static final int WRONG_PLAYERID = 8;
	private static final int ASSISTANTS_AMOUNT = 3;
	private static final int COST = 2;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private JSONObject actionConfig1;
	private SerializableOfferAssistants serOfferAssistants1;
	private OfferAssistantsFactory offerAssistantsFactory1;
	private String actionData2;
	private JSONObject actionConfig2;
	private SerializableOfferAssistants serOfferAssistants2;
	private OfferAssistantsFactory offerAssistantsFactory2;
	private SerializableOfferAssistants serializableOfferAssistants3;
	private boolean caughtException;
	private Token playerToken;

	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
	}
	public void setUpAction(){
		actionData1 = new String("{" + "\n\"PlayerID\": " + player1.getPlayerData().getPlayerID() + ",\n\"ActionName\": \"" + ACTIONNAME
				+ "\",\n\"assistants\": \"" + ASSISTANTS_AMOUNT + "\",\n\"cost\": \"" + COST  +"\"}");
		actionConfig1 = new JSONObject(actionData1);
		serOfferAssistants1 = new SerializableOfferAssistants();
		serOfferAssistants1.setJsonConfigAsString(actionConfig1);
		serOfferAssistants1.setPlayerToken(pToken);
		
		offerAssistantsFactory1 = new OfferAssistantsFactory();
		
		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+  "\",\"assistants\": \"" + ASSISTANTS_AMOUNT + "\",\"cost\": \"" + COST+"\"}");
		actionConfig2 = new JSONObject(actionData2);
		serOfferAssistants2 = new SerializableOfferAssistants();
		serOfferAssistants2.setJsonConfigAsString(actionConfig2);
		offerAssistantsFactory2 = new OfferAssistantsFactory();
		
		
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableOfferAssistants3 = new SerializableOfferAssistants();
		serializableOfferAssistants3.setJsonConfigAsString(actionConfig1);
		serializableOfferAssistants3.setPlayerToken(playerToken);
		serializableOfferAssistants3.setTopic(TOPIC);
		
	}
	
	public void testAssistantOffer(){
		try {
			offerAssistantsFactory1.createAction(serOfferAssistants1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serOfferAssistants1.accept(clientController);
		serOfferAssistants1.accept(controller);
		try {
			offerAssistantsFactory2.createAction(serOfferAssistants2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableOfferAssistants3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableOfferAssistants3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), 
				serializableOfferAssistants3.toString().replaceAll("\n|\\s", ""));
	}
	
	
}
