/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.takePermitCardTest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * @author Federico
 *
 */
public class TakePermitCardTest3 extends ActionInitializer {


	private static final String MULTICOLOR = "MULTICOLOR";

	@Override
	public void test() {
		drawMulticolor();
		takeCard1();
	}

	public void takeCard1() {

		resourcesCheck(player1, 10, 1, 0, 0);


		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor(MULTICOLOR)));
		} catch (ColorNotFoundException e1) {
		}

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);;

		try {
			TakePermitCard takePermitCard = new TakePermitCard(null,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

		resourcesCheck(player1, 10 - (7 + 1) + 3, 1, 0, 4);

		
	}

	public void drawMulticolor() {
		PoliticCard multicolorCard;
		try {
			multicolorCard = new PoliticCard(ColorMap.getColor(MULTICOLOR));
			List<PoliticCard> playerPoiliticCards = player1.getPlayerData().getPoliticsHand().getCards();
			do {
				player1.getPlayerActionExecutor().drawPoliticCard();
			} while (!playerPoiliticCards.contains(multicolorCard));
		} catch (ColorNotFoundException e) {
		}
	}
}
