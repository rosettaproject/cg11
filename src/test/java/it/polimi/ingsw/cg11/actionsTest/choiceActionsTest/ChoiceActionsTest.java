package it.polimi.ingsw.cg11.actionsTest.choiceActionsTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermit;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class ChoiceActionsTest extends ActionInitializer {

	public static final String EXTRA_BONUS_TYPE = "ExtraBonus";
	public static final String EXTRA_PERMIT_TYPE = "ExtraPermit";
	public static final String EXTRA_PERMIT_BONUS_TYPE = "ExtraPermitBonus";

	private ChooseExtraBonus chooseExtraBonus;
	private ChooseExtraPermit chooseExtraPermit;
	private ChooseExtraPermitBonus chooseExtraPermitBonus;

	private PlayerChoice extraBonusChoice;
	private PlayerChoice extraPermitChoice;
	private PlayerChoice extraPermitBonus;

	private City destinationCity;
	private PermitCard permitCard;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();

		addMain(player1, 10);
		addCoins(player1, 50);

		try {
			drawAllPoliticCard(player1, map.getPoliticsDeck());
		} catch (EmptyPoliticDeckException e) {
		}

		extraBonusChoice = new PlayerChoice(EXTRA_BONUS_TYPE);
		extraPermitChoice = new PlayerChoice(EXTRA_PERMIT_TYPE);
		extraPermitBonus = new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE);

	}

	@Override
	public void test() {

		buildOnArkon();
		// Corrupting a council to discard some cards (needed in
		// takeAnOtherExtraPermitFromMountainRegion() )
		takePermitCardFromSeaRegion();

		//Add possibility to make "extra choices"
		player1.getPlayerActionExecutor().addToAvailableActions(extraBonusChoice);
		
		player1.getPlayerActionExecutor().addToAvailableActions(extraPermitChoice);
		player1.getPlayerActionExecutor().addToAvailableActions(extraPermitChoice);
		
		player1.getPlayerActionExecutor().addToAvailableActions(extraPermitBonus);

		// Extra bonus choice
		takeAnExtraBonusOnBurgen();
		takeAnExtraBonusOnArkon();
		takeAnExtraBonusOnArkonWithoutExtraBonus();

		// Extra permit choice
		takeAnExtraPermitFromSeaRegion();
		takeAnOtherExtraPermitFromMountainRegion();
		takeAnExtraPermitWithoutExtraPermit();

		// Extra permit bonus choice
		takeAnExtraPermitBonusTakingPoliticCard();
		takeAnExtraPermitBonusWithoutExtraPermitBonus();
		takeAnExtraPermitBonusWithoutExtraPermitCard();

	}

	public void takePermitCardFromSeaRegion() {
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		try {
			TakePermitCard takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

	}

	public void buildOnArkon() {
		BuildEmporiumWithKing buildWithKing;
		destinationCity = region1.getCities().get(FIRST);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player1, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

	}

	public void takeAnExtraBonusOnArkonWithoutExtraBonus() {

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		chooseExtraBonus = new ChooseExtraBonus(player1, turnManager, destinationCity, null);
		try {
			chooseExtraBonus.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnExtraBonusOnArkon() {

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		chooseExtraBonus = new ChooseExtraBonus(player1, turnManager, destinationCity, null);
		try {
			chooseExtraBonus.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

		moneyAmount += 1;
		assistantAmount += 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnExtraBonusOnBurgen() {

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		chooseExtraBonus = new ChooseExtraBonus(player1, turnManager, region1.getCities().get(SECOND), null);
		try {
			chooseExtraBonus.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnExtraPermitFromSeaRegion() {
		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		chooseExtraPermit = new ChooseExtraPermit(player1, turnManager, region1, permitCard, null);

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		try {
			chooseExtraPermit.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		
		nobilityAmount += 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnOtherExtraPermitFromMountainRegion() {
		int numberOfPoliticCards;

		permitCard = region3.getFaceUpPermitCards().getPermit(SECOND);
		chooseExtraPermit = new ChooseExtraPermit(player1, turnManager, region3, permitCard, null);

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		numberOfPoliticCards = playerData1.getPoliticsHand().getNumberOfCards();

		try {
			chooseExtraPermit.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		victoryAmount += 1;
		numberOfPoliticCards += 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(numberOfPoliticCards, playerData1.getPoliticsHand().getNumberOfCards());

	}

	public void takeAnExtraPermitWithoutExtraPermit() {
		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		chooseExtraPermit = new ChooseExtraPermit(player1, turnManager, region2, permitCard, null);

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		try {
			chooseExtraPermit.execute();
			fail();
		} catch (NotAllowedActionException e) {
		}

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnExtraPermitBonusTakingPoliticCard() {
		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		permitCard = player1.getPlayerData().getPermitsHand().getCard(SECOND);

		chooseExtraPermitBonus = new ChooseExtraPermitBonus(player1, turnManager, permitCard, null);

		try {
			chooseExtraPermitBonus.execute();
		} catch (NotAllowedActionException e) {
			fail();
		}

		nobilityAmount += 1;
		moneyAmount += 2;		//Nobility track
		victoryAmount += 2;
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void takeAnExtraPermitBonusWithoutExtraPermitBonus() {
		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		permitCard = player1.getPlayerData().getPermitsHand().getCard(FIRST);

		chooseExtraPermitBonus = new ChooseExtraPermitBonus(player1, turnManager, permitCard, null);

		try {
			chooseExtraPermitBonus.execute();
			fail();
		} catch (NotAllowedActionException e) {

		}

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}

	public void takeAnExtraPermitBonusWithoutExtraPermitCard() {
		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);

		chooseExtraPermitBonus = new ChooseExtraPermitBonus(player1, turnManager, permitCard, null);

		try {
			chooseExtraPermitBonus.execute();
			fail();
		} catch (NotAllowedActionException e) {

		}

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}

}
