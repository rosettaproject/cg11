/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.changePermitCardTest;

import java.io.FileNotFoundException;
import java.util.Iterator;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.player.actions.ChangePermitCards;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.QuickAction;

/**
 * @author Federico
 *
 */
public class ChangePermitCardTest extends ActionInitializer {

	ChangePermitCards changePermitCards;
	Deck<PermitCard> permitDeck;
	PermitCard permitCard1;
	PermitCard permitCard2;
	FaceUpPermits faceUpPermits;
	Iterator<PermitCard> iterator;
	TakePermitCard takePermit;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		assistantAmount = 2;
		addAssistants(player1, assistantAmount);
		addQuick(player1, 2);
		addMain(player1, 50);
		addCoins(player1, 1000);
		permitDeck = region1.getPermitCardsDeck();
		faceUpPermits = region1.getFaceUpPermitCards();
	}

	@Override
	public void test() {
		changePermitRegion1();
		changePermitRegion1();
		changePermitRegion1();
		changePermitRegion1NoAssistant();
		changePermitRegion1NoQuickAction();
		
		drawAllPoliticAndPermitCard(player1, region1);
		changePermitRegion1NoPermitCards();
	}

	public void changePermitRegion1NoPermitCards() {
		try {
			changePermitCards = new ChangePermitCards(player1, turnManager, region1, null);
			changePermitCards.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(faceUpPermits.getListSize() == 0);
		}
	}

	public void changePermitRegion1NoQuickAction() {
		try {
			changePermitCards = new ChangePermitCards(player1, turnManager, region1, null);
			changePermitCards.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertFalse(player1.getPlayerActionExecutor().getAvailableActions().contains(new QuickAction()));
		}

	}

	public void changePermitRegion1NoAssistant() {
		try {
			changePermitCards = new ChangePermitCards(player1, turnManager, region1, null);
			changePermitCards.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertEquals(0, playerData1.getAssistants().getAmount());
		}
	}

	public void changePermitRegion1() {
		iterator = permitDeck.getCards().iterator();
		permitCard1 = iterator.next();
		permitCard2 = iterator.next();

		try {
			changePermitCards = new ChangePermitCards(player1, turnManager, region1, null);
			changePermitCards.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		assistantAmount -= 1;

		assertEquals(assistantAmount+1, playerData1.getAssistants().getAmount());
		assertEquals(faceUpPermits.getPermit(FIRST), permitCard1);
		assertEquals(faceUpPermits.getPermit(SECOND), permitCard2);

	}
}
