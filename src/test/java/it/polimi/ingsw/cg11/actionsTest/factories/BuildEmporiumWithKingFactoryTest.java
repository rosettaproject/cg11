package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.actionsfactory.BuildEmporiumWithKingFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

public class BuildEmporiumWithKingFactoryTest extends ActionInitializer{
	
	private static final int WRONG_PLAYERID = 6;
	private static final String ACTIONNAME = "buildEmporiumWithKing";
	private static final String CITY = "Graden";
	private static final String POLITICCARDS = "multicolor multicolor";
	private static final String POLITIC_CARD = "politiccards";
	private static final String COLOR = "color";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	
	
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private SerializableBuildEmporiumWithKing serializableBuildEmporiumWithKing1;
	private SerializableBuildEmporiumWithKing serializableBuildEmporiumWithKing2;
	private SerializableBuildEmporiumWithKing serializableBuildEmporiumWithKing3;
	private BuildEmporiumWithKingFactory buildEmporiumWithKingFactory1;
	private BuildEmporiumWithKingFactory buildEmporiumWithKingFactory2;
	
	private boolean caughtException;
	
	private PoliticCard multicolorCard ;
	public Token playerToken;
	
	public BuildEmporiumWithKingFactoryTest() {
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
	}
	public void setUpAction(){
		try {
			multicolorCard= new PoliticCard(ColorMap.getColor("MULTICOLOR"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		map.getTurnManager().getCurrentlyPlaying().getPlayerData().getPoliticsHand().addToHand(multicolorCard);
		map.getTurnManager().getCurrentlyPlaying().getPlayerData().getPoliticsHand().addToHand(multicolorCard);
		
		actionData1 = new String("{" + "\"PlayerID\": " + player1.getPlayerData().getPlayerID() + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"city\": \"" + CITY  +"\"}");
		
		actionConfig1 = new JSONObject(actionData1);
		
		JSONObject singleValue;
		JSONArray arrayOfValue;
		Map<String, String> valuesMap;
		valuesMap = new HashMap<>();
		valuesMap.put(POLITIC_CARD, COLOR);
		StringTokenizer stringTokenizer = new StringTokenizer(POLITICCARDS);
		arrayOfValue = new JSONArray();
		while (stringTokenizer.hasMoreTokens()) {
			singleValue = new JSONObject();
			singleValue.put(COLOR, stringTokenizer.nextToken());
			arrayOfValue.put(singleValue);
		}

		actionConfig1.put(POLITIC_CARD, arrayOfValue);
		
		serializableBuildEmporiumWithKing1 = new SerializableBuildEmporiumWithKing();
		serializableBuildEmporiumWithKing1.setJsonConfigAsString(actionConfig1);
		
		buildEmporiumWithKingFactory1 = new BuildEmporiumWithKingFactory();
		
		
		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"city\": \"" + CITY +"\"}");
		
		
		actionConfig2 = new JSONObject(actionData2);
		actionConfig2.put(POLITIC_CARD, arrayOfValue);
		
		serializableBuildEmporiumWithKing2 = new SerializableBuildEmporiumWithKing();
		serializableBuildEmporiumWithKing2.setJsonConfigAsString(actionConfig2);
		
		buildEmporiumWithKingFactory2 = new BuildEmporiumWithKingFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableBuildEmporiumWithKing3 = new SerializableBuildEmporiumWithKing();
		serializableBuildEmporiumWithKing3.setJsonConfigAsString(actionConfig1);
		serializableBuildEmporiumWithKing3.setPlayerToken(playerToken);
		serializableBuildEmporiumWithKing3.setTopic(TOPIC);
		
	}
	
	public void testChangePermitCardsFactory() {
		try {
			buildEmporiumWithKingFactory1.createAction(serializableBuildEmporiumWithKing1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableBuildEmporiumWithKing1.accept(clientController);
		serializableBuildEmporiumWithKing1.accept(controller);
		try {
			buildEmporiumWithKingFactory2.createAction(serializableBuildEmporiumWithKing2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableBuildEmporiumWithKing3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableBuildEmporiumWithKing3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableBuildEmporiumWithKing3.toString().replaceAll("\n|\\s", ""));
		
	}
}
