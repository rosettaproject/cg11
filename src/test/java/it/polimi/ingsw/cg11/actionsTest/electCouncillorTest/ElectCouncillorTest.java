/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.electCouncillorTest;

import java.awt.Color;
import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;

/**
 * 
 * @author Federico
 *
 */
public class ElectCouncillorTest extends ActionInitializer {
	CouncillorPoolManager councillorPool;
	ElectCouncillor electCouncillor;
	Color councillorColor;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();

		councillorPool = map.getCouncillorsPool();
		addMain(player1, 10);
	}

	@Override
	public void test() {
		
		electBlackInRegion1WithNoBlackCouncillor();
		electPinkInRegion1();
		
		electBlackInRegion1SecondTime();
		electBlackInRegion1SecondTime();
		electBlackInRegion1SecondTime();
		electBlackInRegion1SecondTime();
		
		electBlackInRegion1WithNoBlackCouncillor();

		electWrongCouncillorColorInRegion1();
		
		electPinkInRegion1WithExtraChoiceToPerform();
	}


	public void electPinkInRegion1WithExtraChoiceToPerform() {
		player1.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
		
		councillorColor = Color.PINK;
		try {
			electCouncillor = new ElectCouncillor(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
		}
	}

	public void electWrongCouncillorColorInRegion1() {
		councillorColor = Color.GREEN;
		try {
			electCouncillor = new ElectCouncillor(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			fail();
			electCouncillor.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}
	
	public void electBlackInRegion1WithNoBlackCouncillor() {
		councillorColor = Color.BLACK;
		//There are no more black councillors
		try {
			electCouncillor = new ElectCouncillor(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}
		
		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		
	}
	
	public void electPinkInRegion1(){
		councillorColor = Color.PINK;
		try {
			electCouncillor = new ElectCouncillor(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
		
		moneyAmount += 4;
		assistantAmount += 0;
		nobilityAmount += 0;
		victoryAmount += 0;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}
	
	public void electBlackInRegion1SecondTime() {
		councillorColor = Color.BLACK;
		try {
			electCouncillor = new ElectCouncillor(councillorPool, player1, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
		
		moneyAmount += 4;
		assistantAmount += 0;
		nobilityAmount += 0;
		victoryAmount += 0;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}
		
	

}
