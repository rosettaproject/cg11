/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.takePermitCardTest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;

/**
 * @author Federico
 *
 */
public class TakePermitCardTest4 extends ActionInitializer {

	@Override
	public void test() {
		takeCard1();
		takeCard2();
		takeCard3();
	}

	public void takeCard1() {

		resourcesCheck(player1, 10, 1, 0, 0);

		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.PINK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);

		try {
			TakePermitCard takePermitCard = new TakePermitCard(null,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			fail("TakePermitCard built");
			takePermitCard.execute();
			fail("TakePermitCard executed");
		} catch (IllegalCreationCommandException e) {
			assertTrue(true);
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
	}

	public void takeCard2() {
		resourcesCheck(player1, 10, 1, 0, 0);

		try {
			playerData1.getMoney().spend(5);
		} catch (IllegalResourceException e1) {
			fail(e1.toString());
		}

		resourcesCheck(player1, 5, 1, 0, 0);

		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);;

		try {
			TakePermitCard takePermitCard = new TakePermitCard(null,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
			fail("TakePermitCard executed");
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		}

		resourcesCheck(player1, 5, 1, 0, 0);

	}

	public void takeCard3() {
		Region region2 = map.getRegions().get(1);

		resourcesCheck(player1, 5, 1, 0, 0);

		try {
			playerData1.getMoney().spend(5);
		} catch ( IllegalResourceException e1) {
			fail(e1.toString());
		}

		resourcesCheck(player1, 0, 1, 0, 0);

		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region2.getFaceUpPermitCards().getPermit(SECOND);

		try {
			TakePermitCard takePermitCard = new TakePermitCard(null,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
			fail("TakePermitCard executed");
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		}

		resourcesCheck(player1, 0, 1, 0, 0);
	}

}
