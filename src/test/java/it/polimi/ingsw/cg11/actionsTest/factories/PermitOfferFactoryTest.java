package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableOfferPermitCard;
import it.polimi.ingsw.cg11.server.model.actionsfactory.OfferPermitCardFactory;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class PermitOfferFactoryTest extends ActionInitializer {
	private static final String ACTIONNAME = "permitCardOffer";
	private static final int PLAYERID = 0;
	private static final String PERMITCARD = "permitcard";
	private static final int COST = 2;
	private static final String REGION = "sea";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig2;
	private JSONObject actionConfig1;
	private PermitCard permitCard;
	private JSONArray permitArray;
	private JSONArray permitArray2;
	private SerializableOfferPermitCard serializableOfferPermitCard1;
	private SerializableOfferPermitCard serializableOfferPermitCard2;
	private SerializableOfferPermitCard serializableOfferPermitCard3;
	private OfferPermitCardFactory offerPermitCardFactory1;
	
	private OfferPermitCardFactory offerPermitCardFactory2;
	private boolean caughtException;
	private String permitCardToString;
	private String wrongPermitCardToString;
	private Token playerToken;
	

	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		actions();
		others();
	}
	
	public void test(){
		testPermitOffer();
		checkOthers();
	}
	
	public void actions(){
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"cost\": \"" + COST  + "\",\"region\": \"" + REGION +"\"}");
		permitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		
		actionConfig1 = new JSONObject(actionData1);
		permitArray = new JSONArray(permitCardToString);
		actionConfig1.put(PERMITCARD, permitArray);
		permitCard = map.getRegions().get(0).getFaceUpPermitCards().getPermit(0);
		player1.getPlayerData().addToPermitsHand(permitCard);
		serializableOfferPermitCard1 = new SerializableOfferPermitCard();
		serializableOfferPermitCard1.setJsonConfigAsString(actionConfig1);
		serializableOfferPermitCard1.setPlayerToken(pToken);
		offerPermitCardFactory1 = new OfferPermitCardFactory();
		
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"cost\": \"" + COST+"\",\"region\": \"" + REGION+"\"}");
		wrongPermitCardToString = new String("[[\"Milano\"],{\"VictoryPoints\": 7}]");
		actionConfig2 = new JSONObject(actionData2);
		permitArray2 = new JSONArray(wrongPermitCardToString);
		actionConfig2.put(PERMITCARD, permitArray2);
		serializableOfferPermitCard2 = new SerializableOfferPermitCard();
		serializableOfferPermitCard2.setJsonConfigAsString(actionConfig2);
		offerPermitCardFactory2 = new OfferPermitCardFactory();
		
		
		
		caughtException = false;
	}
	
	public void others(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableOfferPermitCard3 = new SerializableOfferPermitCard();
		serializableOfferPermitCard3.setJsonConfigAsString(actionConfig1);
		serializableOfferPermitCard3.setPlayerToken(playerToken);
		serializableOfferPermitCard3.setTopic(TOPIC);
	}
	
	public void testPermitOffer(){
		try {
			offerPermitCardFactory1.createAction(serializableOfferPermitCard1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableOfferPermitCard1.accept(clientController);
		serializableOfferPermitCard1.accept(controller);
		try {
			offerPermitCardFactory2.createAction(serializableOfferPermitCard2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		
		assertTrue(caughtException);
	}
	
	public void checkOthers(){
		assertEquals(PLAYER, serializableOfferPermitCard3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableOfferPermitCard3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableOfferPermitCard3.toString().replaceAll("\n|\\s", ""));
	}
}
