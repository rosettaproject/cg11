package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.server.model.actionsfactory.SendChatMessageFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class SendChatMessageFactoryTest extends ActionInitializer {
	private String actionData1;

	private JSONObject actionConfig1;
	
	private SerializableChatMessage serializableChatMessage1;
	
	private SendChatMessageFactory sendChatMessageFactory1;
	private SerializableChatMessage serializableChatMessage2;
	
	private static final String ACTIONNAME = "chat";
	private static final String SENDER_PLAYER = "Paola";
	private static final String RECEIVER_PLAYER = "Costi";
	private static final String MESSAGE = "Ciao Costi";

	private Token playerToken;


	public SendChatMessageFactoryTest() {
		super();
	}

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		player1.getPlayerData().setUsername(SENDER_PLAYER);
		player2.getPlayerData().setUsername("Peve");
		player3.getPlayerData().setUsername("Fede");
		player4.getPlayerData().setUsername("Costi");
		playerToken = new Token(SENDER_PLAYER);
		
		actionData1 = new String("{" + "\"PlayerID\": " + player1.getPlayerData().getPlayerID() + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"player\": \"" + RECEIVER_PLAYER + "\",\"message\": \"" + MESSAGE + "\"}");
		actionConfig1 = new JSONObject(actionData1);
		
		serializableChatMessage1 = new SerializableChatMessage();
		serializableChatMessage1.setJsonConfigAsString(actionConfig1);
		serializableChatMessage1.setPlayerToken(pToken);
		serializableChatMessage1.setPlayerToken(playerToken);
		
		sendChatMessageFactory1 = new SendChatMessageFactory();
		
		serializableChatMessage2 = new SerializableChatMessage(SENDER_PLAYER, MESSAGE); 
	}

	public void testElectCouncillorFactory() {
		try {
			sendChatMessageFactory1.createAction(serializableChatMessage1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableChatMessage1.accept(clientController);
		serializableChatMessage1.accept(controller);
		assertEquals(map.getGameTopic(), serializableChatMessage1.getTopic());
		assertEquals(SENDER_PLAYER, serializableChatMessage2.getSender());
		assertEquals(MESSAGE, serializableChatMessage2.getMessage());

	}
}
