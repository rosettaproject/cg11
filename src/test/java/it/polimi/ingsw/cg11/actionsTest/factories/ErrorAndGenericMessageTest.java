package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.ErrorMessage;
import it.polimi.ingsw.cg11.messages.GenericMessage;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;

public class ErrorAndGenericMessageTest extends ActionInitializer {
	private ErrorMessage errorMessage;
	private GenericMessage genericMessage;
	private String message;
	private String topic;
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		message = "message";
		topic = "topic";
		errorMessage = new ErrorMessage(message, null);
		genericMessage = new GenericMessage(message, null);
	}
	
	public void testErrorMessage(){
		errorMessage.setTopic(topic);
		assertEquals(topic, errorMessage.getTopic());
		
		assertEquals(message, errorMessage.getMessage());
		errorMessage.setPlayerToken(pToken);
		assertEquals(pToken, errorMessage.getPlayerToken());
		assertEquals("Topic: topicMessage: message", errorMessage.toString().replaceAll("\n", ""));
		
		errorMessage.accept(clientController);
		errorMessage.accept(controller);
	}
	
	public void testGenericMessage(){
		genericMessage.setTopic(topic);
		assertEquals(topic, genericMessage.getTopic());
		
		assertEquals(message, genericMessage.getMessage());
		genericMessage.setPlayerToken(pToken);
		assertEquals(pToken, genericMessage.getPlayerToken());
		
		genericMessage.accept(clientController);
		genericMessage.accept(controller);
	}
}
