package it.polimi.ingsw.cg11.actionsTest.gameTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;
import it.polimi.ingsw.cg11.server.model.player.actionspool.GameAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class FinishGameTest extends ActionInitializer {

	CouncillorPoolManager councillorsPool;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		int coinsToAdd = 1000;

		super.setUp();
		councillorsPool = map.getCouncillorsPool();

		addCoins(player1, coinsToAdd);
		addCoins(player2, coinsToAdd);
		addCoins(player3, coinsToAdd);
		addCoins(player4, coinsToAdd);
	}

	@Override
	public void test() {
		City tmpCity;
		List<City> citiesOfMap = map.getCities();
		int citiesBuilt;
		int maxEmporiums;
		GamePhase expectedGamePhase;
		GamePhase currentGamePhase;

		citiesBuilt = 0;
		maxEmporiums = 10;
		while (citiesBuilt < maxEmporiums) {
			tmpCity = citiesOfMap.get(citiesBuilt);
			buildEmporiumWithKing(player1, tmpCity);

			makeRamainedMainActions(player1);
			makeAllExtraBonus(player1);

			finish(player1);

			citiesBuilt += 1;

			completeTurn();
		}
		
		expectedGamePhase = new GamePhase("GameIsOver", true);
		currentGamePhase = turnManager.getCurrentGamePhase();
		
		assertEquals(expectedGamePhase, currentGamePhase);
		
	}

	private void completeTurn() {
		completeStandardPhaseForOtherPlayers();
		completeOfferingPhase();
		completeAcceptingPhase();
	}

	private void completeStandardPhaseForOtherPlayers() {
		electCouncillorInRegion3(player2);
		finish(player2);

		electCouncillorInRegion3(player3);
		finish(player3);

		electCouncillorInRegion3(player4);
		finish(player4);
	}

	private void completeOfferingPhase() {
		PlayerState tmpPlayer = null;
		int playerIndex = 0;
		while (playerIndex < NUMBER_OF_PLAYER) {
			tmpPlayer = turnManager.getCurrentlyPlaying();
			finish(tmpPlayer);
			playerIndex += 1;
		}
	}

	private void completeAcceptingPhase() {
		completeOfferingPhase();
	}

	public void makeRamainedMainActions(PlayerState player) {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		if (availableActions.contains(new MainAction()))

		while (availableActions.contains(new MainAction())) {
			electCouncillorInRegion3(player);
		}
	}

	public void makeAllExtraBonus(PlayerState player) {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();
		ChooseExtraBonus extraBonus;

		if (availableActions.contains(new PlayerChoice("ExtraBonus")))

		while (availableActions.contains(new PlayerChoice("ExtraBonus"))) {
			City city = map.getCities().get(FIRST);
			extraBonus = new ChooseExtraBonus(player, turnManager, city, null);
			try {
				extraBonus.execute();
			} catch (NotAllowedActionException e) {
				fail(e.toString());
			}
		}
	}

	public void electCouncillorInRegion3(PlayerState player) {

		Color councillorColor = councillorsPool.getCouncillors().get(FIRST).getColor();
		Balcony balcony = region3.getBalcony();
		try {
			ElectCouncillor electCouncillor = new ElectCouncillor(councillorsPool, player, turnManager, councillorColor,
					balcony, null);
			electCouncillor.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}

	public void buildEmporiumWithKing(PlayerState player, City city) {

		List<PoliticCard> politicCard = new ArrayList<>();
		PoliticCard purplePoliticCard = null;
		try {
			purplePoliticCard = new PoliticCard(ColorMap.getColor("PURPLE"));
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		politicCard.add(purplePoliticCard);

		player.getPlayerData().getPoliticsHand().addToHand(purplePoliticCard);
		BuildEmporiumWithKing buildWithKing;
		try {
			buildWithKing = new BuildEmporiumWithKing(map, player, city, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
	}

	public void finish(PlayerState player) {
		try {
			Finish finishAction = new Finish(player, turnManager, null, map);
			finishAction.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}
}
