package it.polimi.ingsw.cg11.actionsTest.choiceActionsTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.client.view.cli.ScreenFormat;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ActionServiceMethods;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermit;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.GameAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.BonusFactory;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

public class NobilityTest2 extends ActionInitializer {

	public static final String EXTRA_BONUS_TYPE = "ExtraBonus";
	public static final String EXTRA_PERMIT_TYPE = "ExtraPermit";
	public static final String EXTRA_PERMIT_BONUS_TYPE = "ExtraPermitBonus";

	private ActionServiceMethods actionServiceMethods;
	private Bonus nobilityBonus;
	private PermitCard permitCard;

	private Bonus createNobilityBonus() {
		BonusFactory bonusFactory;
		JSONObject jsonObjectNobility;
		jsonObjectNobility = new JSONObject();
		bonusFactory = new BonusFactory();
		jsonObjectNobility.put("Nobility", 1);
		return bonusFactory.createBonus(jsonObjectNobility, map);
	}

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setVariablesResources(player1);
		nobilityBonus = createNobilityBonus();
		actionServiceMethods = new ActionServiceMethods();
		

	}

	@Override
	public void test() {
		printNobilityPath();
		printRegion(region1);
		
		takePermitCard1InRegion1(player1);
		addMain(player1, 1);
		buildEmporiumOnArkon(player1);

		try {
			position2(player1);
			position3(player1);
			position4(player1);
			position5(player1);
			position6(player1);
			position7(player1);
			position8(player1);
			position9(player1);
			position10(player1);
			position11(player1);
			position12(player1);
			position13(player1);
			position14(player1);
			position15(player1);
			position16(player1);
			position17(player1);
			position18(player1);
			position19(player1);
			position20(player1);
			position21(player1);
		} catch (TransactionException | NotAllowedActionException e) {
			fail(e.toString());
		}
		
		ScreenFormat.displayOnScreen("\n\nTEST TERMINATED\n\n");

	}

	private void position2(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		checkResources(player);
	}

	private void position3(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		moneyAmount += 2;
		victoryAmount += 2;
		checkResources(player);
	}

	private void position4(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position5(PlayerState player) throws TransactionException, NotAllowedActionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		actionServiceMethods.canChooseExtraBonusControl(player, map);

		performExtraBonusOnArkon(player);
		
		checkResources(player);
	}

	private void position6(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position7(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position8(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position9(PlayerState player) throws TransactionException {
		int numberOfPolitic;
		int currentPolitics;

		numberOfPolitic = player.getPlayerData().getPoliticsHand().getNumberOfCards();

		takeNobilityBonus(player);

		victoryAmount += 3;
		currentPolitics = player.getPlayerData().getPoliticsHand().getNumberOfCards();

		assertEquals(numberOfPolitic + 1, currentPolitics);
		checkResources(player);
	}

	private void position10(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position11(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_PERMIT_TYPE)));
		
		takeAnExtraPermitFromSeaRegion();
		checkResources(player);
	}

	private void position12(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position13(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		assistantAmount += 1;
		victoryAmount += 5;
		checkResources(player);
	}

	private void position14(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position15(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE)));
		actionServiceMethods.canChoosePermitCardBonusControl(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE)));
		
		takeAnExtraPermitBonus();
		
		checkResources(player);
	}

	private void position16(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position17(PlayerState player) throws TransactionException {
		List<GameAction> availableActions = player.getPlayerActionExecutor().getAvailableActions();

		takeNobilityBonus(player);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		actionServiceMethods.canChooseExtraBonusControl(player, map);
		assertTrue(availableActions.contains(new PlayerChoice(EXTRA_BONUS_TYPE)));
		
		checkResources(player);
	}

	private void position18(PlayerState player) throws TransactionException {
		position2(player);
	}

	private void position19(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 8;
		checkResources(player);
	}

	private void position20(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 2;
		checkResources(player);
	}

	private void position21(PlayerState player) throws TransactionException {
		takeNobilityBonus(player);
		victoryAmount += 3;
		checkResources(player);
	}

	private void takeNobilityBonus(PlayerState player) throws TransactionException {
		nobilityBonus.obtain(player);
		nobilityAmount += 1;
	}

	private void checkResources(PlayerState player) {
		resourcesCheck(player, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
	}

	public void finish(PlayerState player) {
		try {
			Finish finishAction = new Finish(player, turnManager, null, map);
			finishAction.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}

	public void electCouncillorInRegion3(PlayerState player) {
		CouncillorPoolManager councillorsPool = map.getCouncillorsPool();
		Color councillorColor = councillorsPool.getCouncillors().get(FIRST).getColor();
		Balcony balcony = region3.getBalcony();
		try {
			ElectCouncillor electCouncillor = new ElectCouncillor(councillorsPool, player, turnManager, councillorColor,
					balcony, null);
			electCouncillor.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
		moneyAmount += 4;
	}

	private void performExtraBonusOnArkon(PlayerState player) throws NotAllowedActionException, TransactionException {
		City city = map.getCities().get(FIRST);
		ChooseExtraBonus extraBonus = new ChooseExtraBonus(player, turnManager, city, null);
		extraBonus.execute();

		// Bonus of Arkon
		assistantAmount += 1;
		moneyAmount += 1;

	}

	public void buildEmporiumOnArkon(PlayerState player) {

		City city = region1.getCities().get(FIRST);
		List<PoliticCard> politicCard = new ArrayList<>();
		PoliticCard purplePoliticCard = null;
		try {
			purplePoliticCard = new PoliticCard(ColorMap.getColor("PURPLE"));
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		politicCard.add(purplePoliticCard);

		player.getPlayerData().getPoliticsHand().addToHand(purplePoliticCard);
		BuildEmporiumWithPermitCard buildWithCard;
		try {
			buildWithCard = new BuildEmporiumWithPermitCard(map, player, city, permitCard, null);
			buildWithCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
		assistantAmount += 1;
		moneyAmount += 1;
		checkResources(player);
	}

	private void takePermitCard1InRegion1(PlayerState player) {
		List<PoliticCard> politicCard = new ArrayList<>();
		
		politicCard.add(new PoliticCard(Color.BLACK));
		permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);
		
		try {
			TakePermitCard takePermitCard = new TakePermitCard(map, player, turnManager, region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		moneyAmount -= 10;
		moneyAmount += 3;
		victoryAmount += 4;
		checkResources(player);
	}
	public void takeAnExtraPermitFromSeaRegion() {
		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		ChooseExtraPermit chooseExtraPermit = new ChooseExtraPermit(player1, turnManager, region1, permitCard, null);

		moneyAmount = playerData1.getMoney().getAmount();
		assistantAmount = playerData1.getAssistants().getAmount();
		nobilityAmount = playerData1.getNobilityPoints().getAmount();
		victoryAmount = playerData1.getVictoryPoints().getAmount();

		try {
			chooseExtraPermit.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		
		victoryAmount += 7;
		checkResources(player1);
	}
	
	public void takeAnExtraPermitBonus() {
		permitCard = player1.getPlayerData().getUsedPermits().getCard(FIRST);

		ChooseExtraPermitBonus chooseExtraPermitBonus = new ChooseExtraPermitBonus(player1, turnManager, permitCard, null);

		try {
			chooseExtraPermitBonus.execute();
		} catch (NotAllowedActionException e) {
			fail();
		}
		printPlayer(player1);
		
		//victoryAmount += 7;
		moneyAmount += 3;
		victoryAmount += 4;

		checkResources(player1);

	}
}
