package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermit;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraPermitFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * 
 * @author paolasanfilippo
 *
 */
public class ChooseExtraPermitFactoryTest extends ActionInitializer {

	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "extraPermit";
	private static final String REGION = "sea";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private static final String PERMIT_CARD = "permitcard";

	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;

	private SerializableChooseExtraPermit serializableChooseExtraPermit1;
	private SerializableChooseExtraPermit serializableChooseExtraPermit2;
	private SerializableChooseExtraPermit serializableChooseExtraPermit3;

	private ChooseExtraPermitFactory chooseExtraPermitFactory1;
	private ChooseExtraPermitFactory chooseExtraPermitFactory2;

	private boolean caughtException;
	private Token playerToken;
	private String permitCardToString;
	private String wrongPermitCardToString;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		setUpAction();
		setUpOthers();

	}

	public void setUpAction() {
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		actionConfig1 = new JSONObject(actionData1);

		permitCardToString = new String("[[\"Dorful\"],{\"VictoryPoints\": 7}]");
		actionConfig1.put(PERMIT_CARD, new JSONArray(permitCardToString));

		serializableChooseExtraPermit1 = new SerializableChooseExtraPermit();
		serializableChooseExtraPermit1.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraPermit1.setPlayerToken(pToken);

		chooseExtraPermitFactory1 = new ChooseExtraPermitFactory();

		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\",\"region\": \"" + REGION + "\"}");
		wrongPermitCardToString = new String("[[\"Milano\"],{\"VictoryPoints\": 7}]");
		actionConfig2 = new JSONObject(actionData2);
		actionConfig2.put(PERMIT_CARD, new JSONArray(wrongPermitCardToString));
		serializableChooseExtraPermit2 = new SerializableChooseExtraPermit();
		serializableChooseExtraPermit2.setJsonConfigAsString(actionConfig2);

		chooseExtraPermitFactory2 = new ChooseExtraPermitFactory();

		caughtException = false;
	}

	public void setUpOthers() {
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableChooseExtraPermit3 = new SerializableChooseExtraPermit();
		serializableChooseExtraPermit3.setJsonConfigAsString(actionConfig1);
		serializableChooseExtraPermit3.setPlayerToken(playerToken);
		serializableChooseExtraPermit3.setTopic(TOPIC);

	}

	public void testChooseExtraPermitFactory() {
		try {
			chooseExtraPermitFactory1.createAction(serializableChooseExtraPermit1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableChooseExtraPermit1.accept(clientController);
		serializableChooseExtraPermit1.accept(controller);
		try {
			chooseExtraPermitFactory2.createAction(serializableChooseExtraPermit2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException = true;
		}
		assertTrue(caughtException);
	}

	public void testOthers() {
		assertEquals(PLAYER, serializableChooseExtraPermit3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableChooseExtraPermit3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""),
				serializableChooseExtraPermit3.toString().replaceAll("\n|\\s", ""));
	}
}
