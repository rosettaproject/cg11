package it.polimi.ingsw.cg11.actionsTest;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.view.Token;

public class TokenTest extends ActionInitializer {
	
	private Token token1;
	private Token token2;
	
	private static final String username1 = "Player1";
	private static final String username2 = "Player2";
	private static final String currentGame1 = "currentGame1";
	private static final String currentGame2 = "currentGame2";
	
	private final static int player1ID = 1;
	private final static int player2ID = 2;
	private Token token3;
	private Token token4;
	private Token token5;
	private Token token6;
	private Token token7;
	private Token token8;
	
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		token1 = new Token(username1);
		token1.setPlayerID(player1ID);
		token1.setCurrentGame(currentGame1);

		token2 = new Token(username2);
		token2.setCurrentGame(currentGame2);
		
		token3 = new Token(username1);
		token3.setCurrentGame(null);
		
		token4 = new Token(username1);
		token4.setCurrentGame(currentGame2);
		
		token5 = new Token (username1);
		token5.setCurrentGame(currentGame1);
		token5.setPlayerID(player2ID);
		
		token6 = new Token(username1);
		token6.setCurrentGame(currentGame1);
		token6.setPlayerID(player1ID);
		
		token7 = new Token(null);
		token7.setCurrentGame(null);
		token8 = new Token(null);
		token8.setCurrentGame(null);
		
	}
	
	public void testToken(){
		assertEquals(currentGame1, token1.getCurrentGame());
		assertTrue(token1.equals(token1));
		assertFalse(token1.equals(null));
		assertFalse(token1.equals(username1));
		assertFalse(new Token(null).equals(token1));
		assertFalse(token1.equals(token2));
		assertFalse(token3.equals(token1));
		assertFalse(token1.equals(token4));
		assertFalse(token1.equals(token5));
		assertFalse(token1.equals(token6));
		assertFalse(token7.equals(token8));
		
		assertEquals(token1.hashCode(), token1.hashCode());
		assertEquals(token7.hashCode(), token7.hashCode());
	}
}
