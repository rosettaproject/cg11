package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableOfferPoliticCard;
import it.polimi.ingsw.cg11.server.model.actionsfactory.OfferPoliticCardFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

public class PoliticOfferFactoryTest extends ActionInitializer {
	private static final String ACTIONNAME = "politicCardOffer";
	private static final int PLAYERID = 0;
	private static final int COST = 2;
	private static final String POLITIC_CARD = "politiccard";
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	private SerializableOfferPoliticCard serializableOfferPoliticCard2;
	private SerializableOfferPoliticCard serializableOfferPoliticCard1;
	private SerializableOfferPoliticCard serializableOfferPoliticCard3;
	private OfferPoliticCardFactory offerPoliticCardFactory1;
	
	private OfferPoliticCardFactory offerPoliticCardFactory2;
	private boolean caughtException;
	
	private PoliticCard politicCard;
	private String politicCardToString;
	private String politicCardToString2;
	private Token playerToken;
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpActions();
		setUpOthers();
	}
	
	public void setUpActions(){
		try {
			politicCard = new PoliticCard(ColorMap.getColor("PINK"));
		} catch (ColorNotFoundException e) {
			fail();
		}
		politicCardToString = new String("{\"color\": \"PINK\"}");
		
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				 + "\",\"cost\": \"" + COST  +"\"}");
		actionConfig1 = new JSONObject(actionData1);
		actionConfig1.put(POLITIC_CARD, new JSONObject(politicCardToString));
		serializableOfferPoliticCard1 = new SerializableOfferPoliticCard();
		serializableOfferPoliticCard1.setJsonConfigAsString(actionConfig1);
		serializableOfferPoliticCard1.setPlayerToken(pToken);
		offerPoliticCardFactory1 = new OfferPoliticCardFactory();
		player1.getPlayerData().getPoliticsHand().addToHand(politicCard);
		
		politicCardToString2 = new String("{\"color\": \"GREEN\"}");
		actionData2 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				 + "\",\"cost\": \"" + COST+"\"}");
		actionConfig2 = new JSONObject(actionData2);
		actionConfig2.put(POLITIC_CARD, new JSONObject(politicCardToString2));
		serializableOfferPoliticCard2 = new SerializableOfferPoliticCard();
		serializableOfferPoliticCard2.setJsonConfigAsString(actionConfig2);
		offerPoliticCardFactory2 = new OfferPoliticCardFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableOfferPoliticCard3 = new SerializableOfferPoliticCard();
		serializableOfferPoliticCard3.setJsonConfigAsString(actionConfig1);
		serializableOfferPoliticCard3.setPlayerToken(playerToken);
		serializableOfferPoliticCard3.setTopic(TOPIC);
	}
	
	public void testEngageAssistant(){
		try {
			offerPoliticCardFactory1.createAction(serializableOfferPoliticCard1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableOfferPoliticCard1.accept(clientController);
		serializableOfferPoliticCard1.accept(controller);
		try {
			offerPoliticCardFactory2.createAction(serializableOfferPoliticCard2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableOfferPoliticCard3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableOfferPoliticCard3.getTopic());
		assertEquals(actionConfig1.toString().replaceAll("\n|\\s", ""), 
				serializableOfferPoliticCard3.toString().replaceAll("\n|\\s", ""));
	}
	
}
