package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableFinish;
import it.polimi.ingsw.cg11.server.model.actionsfactory.FinishFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.view.Token;

public class FinishFactoryTest extends ActionInitializer {
	private static final String ACTIONNAME = "finish";
	private static final int PLAYERID = 0;
	private static final int WRONG_PLAYERID = 8;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig2;
	private JSONObject actionConfig1;
	private SerializableFinish serializableFinish1;
	private SerializableFinish serializableFinish2;
	private SerializableFinish serializableFinish3;
	private FinishFactory finishFactory1;
	private FinishFactory finishFactory2;
	private boolean caughtException;
	private Token playerToken;

	public FinishFactoryTest() {
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
	}
	
	public void setUpAction(){
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\"}");
		actionConfig1 = new JSONObject(actionData1);
		serializableFinish1 = new SerializableFinish();
		serializableFinish1.setJsonConfigAsString(actionConfig1);
		serializableFinish1.setPlayerToken(pToken);
		finishFactory1 = new FinishFactory();
		
		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\"}");
		actionConfig2 = new JSONObject(actionData2);
		serializableFinish2 = new SerializableFinish();
		serializableFinish2.setJsonConfigAsString(actionConfig2);
		finishFactory2 = new FinishFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serializableFinish3 = new SerializableFinish();
		serializableFinish3.setJsonConfigAsString(actionConfig1);
		serializableFinish3.setPlayerToken(playerToken);
		serializableFinish3.setTopic(TOPIC);
		
	}
	
	public void testEngageAssistant(){
		try {
			finishFactory1.createAction(serializableFinish1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serializableFinish1.accept(clientController);
		serializableFinish1.accept(controller);
		try {
			finishFactory2.createAction(serializableFinish2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serializableFinish3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serializableFinish3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), 
				serializableFinish3.toString().replaceAll("\n|\\s", ""));
	}
}
