/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.takePermitCardTest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;

/**
 * @author Federico
 *
 */
public class TakeParmitCardTest2 extends ActionInitializer {

	protected static final int SECOND = 1;

	@Override
	public void test() {
		drawCards(player1, 10);
		takeCard1();
	}

	public void takeCard1() {

		resourcesCheck(player1, 10, 1, 0, 0);


		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));
		politicCard.add(new PoliticCard(Color.BLACK));

		PermitCard permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);

		try {
			TakePermitCard takePermitCard = new TakePermitCard(null,player1, map.getTurnManager(), region1, permitCard,
					politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}

		resourcesCheck(player1, 10-4+3, 1, 0, 4);


	}

}
