/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.BuildEmporiumWithPermitCardTest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;

/**
 * @author Federico
 *
 */
public class BuildEmporiumWithPermitCardTest extends ActionInitializer {

	private int moneyAmount;
	private int assistantAmount;
	private int nobilityAmount;
	private int victoryAmount;

	BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;

	public void test() {
		int numberOfMain = 25;
		int numberOfCard = 30;
		int coins = 50;
		addMain(player1, numberOfMain);
		drawCards(player1, numberOfCard);
		addCoins(player1, coins);

		buildEmporiumOnDorful();
		buildEmporiumOnArkon();
		buildEmporiumOnBurgen();
		buildEmporiumOnEsti();
		buildEmporiumOnEsti2();
		wrongCityAndPoliticCardException();
		buildEmporiumOnKultos();
		buildEmporiumOnNaris();
	}

	public void buildEmporiumOnNaris() {

		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.ORANGE));

		permitCard = region3.getFaceUpPermitCards().getPermit(FIRST);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region3, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			
			//Add a choice action to do
			player1.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
			buildEmporiumWithPermitCard.execute();
			
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			
		}

	}
	
	public void buildEmporiumOnKultos() {

		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.ORANGE));

		permitCard = region3.getFaceUpPermitCards().getPermit(SECOND);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region3, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			buildEmporiumWithPermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

	}

	public void wrongCityAndPoliticCardException() {
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;
		int index;

		// Player has to make a choice before this action
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		if (player1.getPlayerActionExecutor().getAvailableActions().contains(new PlayerChoice("Generic")))
			try {
				playerData1.addToPoliticsHand(new PoliticCard(Color.BLACK));
				takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
				takePermitCard.execute();
				fail();
			} catch (IllegalCreationCommandException | NotAllowedActionException e) {
				assertTrue(true);
			}

		// Player has a wrong politic hand
		politicCard.add(new PoliticCard(Color.GREEN));
		try {
			index = player1.getPlayerActionExecutor().getAvailableActions().indexOf(new PlayerChoice("Generic"));
			if (index > 0)
				player1.getPlayerActionExecutor().getAvailableActions().remove(index);

			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			fail();
			takePermitCard.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}

		// Player wants to build on a wrong city
		City wrongCity = new City("hi", Color.GREEN, region1, null);
		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, wrongCity, permitCard, null);
			assertTrue(true);
			buildEmporiumWithPermitCard.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}
		costToCorrupt = 10;
		moneyAmount += -costToCorrupt + permitCard.getPermitBonus().getMoneyAmount();
		assistantAmount += permitCard.getPermitBonus().getAssistantsAmount();
		nobilityAmount += permitCard.getPermitBonus().getNobilityPointsAmount();
		victoryAmount += permitCard.getPermitBonus().getVictoryPointsAmount();

	}

	public void buildEmporiumOnEsti2() {
		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			assertTrue(true);
			buildEmporiumWithPermitCard.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			assertTrue(true);
		}

	}

	public void buildEmporiumOnEsti() {
		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;
		costToCorrupt = 10;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		permitCard = player1.getPlayerData().getPermitsHand().getCards().get(FIRST);
		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			buildEmporiumWithPermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt + permitCard.getPermitBonus().getMoneyAmount()
				+ destinationCity.getCityBonus().getMoneyAmount() + 1 + 2;
		assistantAmount += permitCard.getPermitBonus().getAssistantsAmount()
				+ destinationCity.getCityBonus().getAssistantsAmount() + 1;
		nobilityAmount += permitCard.getPermitBonus().getNobilityPointsAmount()
				+ destinationCity.getCityBonus().getNobilityPointsAmount() + 1;
		victoryAmount += permitCard.getPermitBonus().getVictoryPointsAmount()
				+ destinationCity.getCityBonus().getVictoryPointsAmount() + 1 + 2;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void buildEmporiumOnBurgen() {
		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;
		costToCorrupt = 10;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		permitCard = player1.getPlayerData().getPermitsHand().getCards().get(FIRST);
		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			buildEmporiumWithPermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt + permitCard.getPermitBonus().getMoneyAmount()
				+ destinationCity.getCityBonus().getMoneyAmount() + 1;
		assistantAmount += permitCard.getPermitBonus().getAssistantsAmount()
				+ destinationCity.getCityBonus().getAssistantsAmount() + 1;
		nobilityAmount += permitCard.getPermitBonus().getNobilityPointsAmount()
				+ destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount += permitCard.getPermitBonus().getVictoryPointsAmount()
				+ destinationCity.getCityBonus().getVictoryPointsAmount() + 1;

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void buildEmporiumOnArkon() {
		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;
		costToCorrupt = 10;

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(SECOND);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		permitCard = player1.getPlayerData().getPermitsHand().getCards().get(FIRST);
		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			buildEmporiumWithPermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt + permitCard.getPermitBonus().getMoneyAmount()
				+ destinationCity.getCityBonus().getMoneyAmount();
		assistantAmount += permitCard.getPermitBonus().getAssistantsAmount()
				+ destinationCity.getCityBonus().getAssistantsAmount();
		nobilityAmount += permitCard.getPermitBonus().getNobilityPointsAmount()
				+ destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount += permitCard.getPermitBonus().getVictoryPointsAmount()
				+ destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}

	public void buildEmporiumOnDorful() {
		City destinationCity;
		TakePermitCard takePermitCard;
		PermitCard permitCard;
		BuildEmporiumWithPermitCard buildEmporiumWithPermitCard;
		startingMoney = 60;
		costToCorrupt = 10;
		startingAssistant = 1;
		startingNobility = 0;
		startingVictory = 0;

		resourcesCheck(player1, startingMoney, startingAssistant, startingNobility, startingVictory);

		// Draw a permit card
		List<PoliticCard> politicCard = new ArrayList<>();
		politicCard.add(new PoliticCard(Color.BLACK));

		permitCard = region1.getFaceUpPermitCards().getPermit(FIRST);
		try {
			takePermitCard = new TakePermitCard(map, player1, turnManager, region1, permitCard, politicCard, null);
			takePermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		permitCard = player1.getPlayerData().getPermitsHand().getCards().get(FIRST);
		destinationCity = permitCard.getCities().get(FIRST);

		try {
			buildEmporiumWithPermitCard = new BuildEmporiumWithPermitCard(map, player1, destinationCity, permitCard,
					null);
			buildEmporiumWithPermitCard.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}

		moneyAmount = startingMoney - costToCorrupt + permitCard.getPermitBonus().getMoneyAmount()
				+ destinationCity.getCityBonus().getMoneyAmount();
		assistantAmount = startingAssistant + permitCard.getPermitBonus().getAssistantsAmount()
				+ destinationCity.getCityBonus().getAssistantsAmount();
		nobilityAmount = startingNobility + permitCard.getPermitBonus().getNobilityPointsAmount()
				+ destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount = startingVictory + permitCard.getPermitBonus().getVictoryPointsAmount()
				+ destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player1, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

	}
}
