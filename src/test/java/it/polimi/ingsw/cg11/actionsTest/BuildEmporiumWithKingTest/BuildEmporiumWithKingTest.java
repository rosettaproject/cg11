/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.BuildEmporiumWithKingTest;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * @author Federico
 *
 */
public class BuildEmporiumWithKingTest extends ActionInitializer {

	private int coinsToAdd;
	private int mainActionToAdd;
	private int cardsToDraw;

	private BuildEmporiumWithKing buildWithKing;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		coinsToAdd = 60;
		mainActionToAdd = 20;
		cardsToDraw = 35;

		super.setUp();
		addCoins(player2, coinsToAdd);
		addMain(player2, mainActionToAdd);
		drawCards(player2, cardsToDraw);
	}

	public void test() {
		changeTurn();

		buildEmporiumWithoutPoliticCard();
		
		buildEmporiumOnArkon();
		buildEmporiumOnBurgen();
		buildEmporiumOnCastrum();
		buildEmporiumOnIndur();
		buildEmporiumOnIndurSecondTime();
		
		buildEmporiumOnCityWithPoliticCardBonus();
		
		buildEmporiumWithoutMoney();
		buildEmporiumWithAChoiceToPerform();
		
	}

	private void buildEmporiumWithAChoiceToPerform() {
		player2.getPlayerActionExecutor().addToAvailableActions(new PlayerChoice(""));
		addCoins(player2, 50);
		
		City destinationCity;

		destinationCity = region3.getCities().get(THIRD);

		List<PoliticCard> politicCard = new ArrayList<>();
		
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
		
	}

	public void buildEmporiumWithoutMoney() {
		City destinationCity;

		destinationCity = region2.getCities().get(SECOND);

		List<PoliticCard> politicCard = new ArrayList<>();
		
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
			fail();
		} catch (NotAllowedActionException e) {
			assertTrue(true);
		} catch (IllegalCreationCommandException e) {
			fail(e.toString());
		} catch (ColorNotFoundException e) {
			fail(e.toString());
		}
	}

	public void buildEmporiumOnCityWithPoliticCardBonus() {
		City destinationCity;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST * 3;

		destinationCity = region3.getCities().get(FOURTH);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt - costToMoveKing + destinationCity.getCityBonus().getMoneyAmount();
		assistantAmount += destinationCity.getCityBonus().getAssistantsAmount();
		nobilityAmount += destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount += destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(destinationCity, king.getCurrentCity());
		assertEquals("Naris", king.getCurrentCity().getName());
		
	}

	public void buildEmporiumWithoutPoliticCard() {
		City destinationCity;

		destinationCity = region2.getCities().get(SECOND);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			fail();
			buildWithKing.execute();
			fail();
		} catch (NotAllowedActionException e) {
			fail();
		} catch (IllegalCreationCommandException e) {
			assertTrue(true);
		}
	}

	public void buildEmporiumOnIndurSecondTime() {
		City destinationCity;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST * 1;
		City oldCity = region2.getCities().get(FOURTH);
		

		destinationCity = region2.getCities().get(FOURTH);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
			fail();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			assertTrue(true);
		}

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(oldCity, king.getCurrentCity());
		assertEquals("Indur", king.getCurrentCity().getName());

	}

	public void buildEmporiumOnIndur() {
		City destinationCity;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST * 2;

		destinationCity = region2.getCities().get(FOURTH);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt - costToMoveKing + destinationCity.getCityBonus().getMoneyAmount();
		assistantAmount += destinationCity.getCityBonus().getAssistantsAmount();
		nobilityAmount += destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount += destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(destinationCity, king.getCurrentCity());
		assertEquals("Indur", king.getCurrentCity().getName());

	}

	public void buildEmporiumOnCastrum() {
		City destinationCity;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST * 2;

		destinationCity = region1.getCities().get(THIRD);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt - costToMoveKing + destinationCity.getCityBonus().getMoneyAmount() + 1 + 2;
		assistantAmount += destinationCity.getCityBonus().getAssistantsAmount() + 1;
		nobilityAmount += destinationCity.getCityBonus().getNobilityPointsAmount() + 1;
		victoryAmount += destinationCity.getCityBonus().getVictoryPointsAmount() + 2;

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(destinationCity, king.getCurrentCity());
		assertEquals("Castrum", king.getCurrentCity().getName());
	}

	public void buildEmporiumOnBurgen() {
		City destinationCity;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST * 1;

		destinationCity = region1.getCities().get(SECOND);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

		moneyAmount += -costToCorrupt - costToMoveKing + destinationCity.getCityBonus().getMoneyAmount() + 1;
		assistantAmount += destinationCity.getCityBonus().getAssistantsAmount() + 1;
		nobilityAmount += destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount += destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);
		assertEquals(destinationCity, king.getCurrentCity());
		assertEquals("Burgen", king.getCurrentCity().getName());
	}

	public void buildEmporiumOnArkon() {
		City destinationCity;
		startingMoney = 11;
		costToCorrupt = 10;
		costToMoveKing = MOVE_KING_COST;
		startingAssistant = 2;
		startingNobility = 0;
		startingVictory = 0;

		resourcesCheck(player2, startingMoney + coinsToAdd, startingAssistant, startingNobility, startingVictory);

		destinationCity = region1.getCities().get(FIRST);
		
		costToMoveKing *= map.getGraphedMap().getDistance(king.getCurrentCity(), destinationCity);

		List<PoliticCard> politicCard = new ArrayList<>();
		try {
			politicCard.add(new PoliticCard(ColorMap.getColor("PURPLE")));
			buildWithKing = new BuildEmporiumWithKing(map, player2, destinationCity, politicCard, null);
			buildWithKing.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException | ColorNotFoundException e) {
			fail(e.toString());
		}

		moneyAmount = startingMoney + coinsToAdd - costToCorrupt - costToMoveKing
				+ destinationCity.getCityBonus().getMoneyAmount();
		assistantAmount = startingAssistant + destinationCity.getCityBonus().getAssistantsAmount();
		nobilityAmount = startingNobility + destinationCity.getCityBonus().getNobilityPointsAmount();
		victoryAmount = startingVictory + destinationCity.getCityBonus().getVictoryPointsAmount();

		resourcesCheck(player2, moneyAmount, assistantAmount, nobilityAmount, victoryAmount);

		assertEquals(destinationCity, king.getCurrentCity());
		assertEquals("Arkon", king.getCurrentCity().getName());
	}
}
