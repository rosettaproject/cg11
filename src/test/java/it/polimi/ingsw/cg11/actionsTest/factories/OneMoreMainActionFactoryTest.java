package it.polimi.ingsw.cg11.actionsTest.factories;

import java.io.FileNotFoundException;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.SerializableOneMoreMainAction;
import it.polimi.ingsw.cg11.server.model.actionsfactory.OneMoreMainActionFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.view.Token;

public class OneMoreMainActionFactoryTest extends ActionInitializer{
	
	private static final int ASSISTANTS= 3;
	private static final int PLAYERID = 0;
	private static final String ACTIONNAME = "oneMoreMainAction";
	private static final int WRONG_PLAYERID = 8;
	private static final String PLAYER = "Player1";
	private static final String TOPIC = "topic";
	private String actionData1;
	private String actionData2;
	private JSONObject actionConfig1;
	private JSONObject actionConfig2;
	
	private SerializableOneMoreMainAction serOneMoreMainAction1;
	private SerializableOneMoreMainAction serOneMoreMainAction2;
	private SerializableOneMoreMainAction serOneMoreMainAction3;
	private OneMoreMainActionFactory oneMoreMainActionFactory1;
	private OneMoreMainActionFactory oneMoreMainActionFactory2;
	private boolean caughtException;
	private Token playerToken;
	
	
	public OneMoreMainActionFactoryTest(){
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		super.setUp();
		setUpAction();
		setUpOthers();
	}
	
	public void setUpAction(){
		try {
			map.getTurnManager().getCurrentlyPlaying().getPlayerData().getAssistants().gain(ASSISTANTS);
		} catch (IllegalResourceException e) {
			fail();
		}
		
		actionData1 = new String("{" + "\"PlayerID\": " + PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\"}");
		actionConfig1 = new JSONObject(actionData1);
		serOneMoreMainAction1 = new SerializableOneMoreMainAction();
		serOneMoreMainAction1.setJsonConfigAsString(actionConfig1);
		oneMoreMainActionFactory1 = new OneMoreMainActionFactory();
		
		
		actionData2 = new String("{" + "\"PlayerID\": " + WRONG_PLAYERID + ",\"ActionName\": \"" + ACTIONNAME
				+ "\"}");
		actionConfig2 = new JSONObject(actionData2);
		serOneMoreMainAction2 = new SerializableOneMoreMainAction();
		serOneMoreMainAction2.setJsonConfigAsString(actionConfig2);
		oneMoreMainActionFactory2 = new OneMoreMainActionFactory();
		
		caughtException = false;
	}
	
	public void setUpOthers(){
		player1.getPlayerData().setUsername(PLAYER);
		player2.getPlayerData().setUsername("Player2");
		player3.getPlayerData().setUsername("Player3");
		player4.getPlayerData().setUsername("Player4");
		playerToken = new Token(PLAYER);
		serOneMoreMainAction3 = new SerializableOneMoreMainAction();
		serOneMoreMainAction3.setJsonConfigAsString(actionConfig1);
		serOneMoreMainAction3.setPlayerToken(playerToken);
		serOneMoreMainAction3.setTopic(TOPIC);
		
	}
	
	public void testOneMoreMainAction(){
		try {
			oneMoreMainActionFactory1.createAction(serOneMoreMainAction1, map);
		} catch (IllegalCreationCommandException e) {
			fail();
		}
		serOneMoreMainAction1.accept(clientController);
		serOneMoreMainAction1.accept(controller);
		try {
			oneMoreMainActionFactory2.createAction(serOneMoreMainAction2, map);
		} catch (IllegalCreationCommandException e) {
			caughtException=true;
		}
		assertTrue(caughtException);
	}
	
	public void testOthers(){
		assertEquals(PLAYER, serOneMoreMainAction3.getPlayerToken().getClientUsername());
		assertEquals(TOPIC, serOneMoreMainAction3.getTopic());
		assertEquals(actionData1.replaceAll("\n|\\s", ""), 
				serOneMoreMainAction3.toString().replaceAll("\n|\\s", ""));
	}
}
