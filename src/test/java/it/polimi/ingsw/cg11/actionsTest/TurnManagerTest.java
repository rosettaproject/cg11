/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.EngageAnAssistant;
import it.polimi.ingsw.cg11.server.model.player.actions.Finish;
import it.polimi.ingsw.cg11.server.model.player.actionspool.GameAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.actionspool.QuickAction;

/**
 * @author Federico
 */
public class TurnManagerTest extends ActionInitializer {

	CouncillorPoolManager councillorPool;
	GamePhase gamePhase;

	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		councillorPool = map.getCouncillorsPool();
	}
	
	@Override
	public void test() {
		
		electCouncillor(player1, Color.PINK, region1);
		engageAssistant(player1);
		finish(player1, map);		
		assertFalse(player1.isAllowedToPlay());
		gamePhase = map.getGamePhaseManager().getCurrentGamePhase();
		assertEquals(gamePhase.getPhaseName(), "StandardPlayPhase");
		electCouncillor(player2, Color.BLACK, region1);
		engageAssistant(player2);
		finish(player2, map);
		assertFalse(player2.isAllowedToPlay());
		gamePhase = map.getGamePhaseManager().getCurrentGamePhase();
		assertEquals(gamePhase.getPhaseName(), "StandardPlayPhase");

		electCouncillor(player3, Color.PINK, region1);
		engageAssistant(player3);
		finish(player3, map);
		assertFalse(player3.isAllowedToPlay());
		gamePhase = map.getGamePhaseManager().getCurrentGamePhase();
		assertEquals(gamePhase.getPhaseName(), "StandardPlayPhase");
		
		electCouncillor(player4, Color.BLACK, region1);
		engageAssistant(player4);
		finish(player4, map);
		gamePhase = map.getGamePhaseManager().getCurrentGamePhase();
		assertEquals("MarketOfferingPhase", gamePhase.getPhaseName());
	}
	
	public void electCouncillor(PlayerState player, Color councillorColor, Region region){
		try {
			ElectCouncillor electCouncillor = new ElectCouncillor(councillorPool, player, turnManager, councillorColor,
					region1.getBalcony(), null);
			electCouncillor.execute();
		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			fail(e.toString());
		}
		List<GameAction> availableAction = player.getPlayerActionExecutor().getAvailableActions();
		assertFalse(availableAction.contains(new MainAction()));
	}
	
	public void engageAssistant(PlayerState player){
		EngageAnAssistant engage = new EngageAnAssistant(player, turnManager, null);
		try {
			engage.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		List<GameAction> availableAction = player.getPlayerActionExecutor().getAvailableActions();
		assertFalse(availableAction.contains(new QuickAction()));
	}
	
	public void finish(PlayerState player, GameMap gameMap){
		try {		
			Finish finish = new Finish(player,turnManager, gameMap.getGameTopic(), gameMap);
			finish.execute();
		} catch (NotAllowedActionException | IllegalCreationCommandException e) {
			fail(e.toString());
		}
	}
	
}
