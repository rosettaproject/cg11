/**
 * 
 */
package it.polimi.ingsw.cg11.actionsTest.messageTest;

import java.io.FileNotFoundException;

import it.polimi.ingsw.cg11.actionsTest.ActionInitializer;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.player.actions.SendChatMessage;

/**
 * @author Federico
 *
 */
public class SendMessageTest extends ActionInitializer {

	private static final String ALL_PLAYERS = "allPlayers";
	
	private SendChatMessage sendChatMessage;
	
	@Override
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
	}
	
	@Override
	public void test() {
		sendMessage();
		publishMethod();
	}
	
	public void sendMessage(){
		String sender = "sender";
		String receiver = ALL_PLAYERS;
		String messageText = "messageText";
		String topic = "topic";
		
		sendChatMessage = new SendChatMessage(sender, receiver, messageText, topic);
		
		try {
			sendChatMessage.execute();
		} catch (NotAllowedActionException e) {
			fail(e.toString());
		}
		
		assertTrue(true);
	}
	
	public void publishMethod(){
		sendChatMessage.publish(new ModelChanges(), "");
		assertTrue(true);
	}
	
}
