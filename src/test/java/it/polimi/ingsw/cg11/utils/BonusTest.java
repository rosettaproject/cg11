package it.polimi.ingsw.cg11.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.model.utils.ColoredBonus;

public class BonusTest extends MapConfigTest {
	
	private List<Bonus> kingbonuses= new ArrayList<>();
	private List<City> cities= new ArrayList<>();
	private Bonus bonus1, bonus2;
	private ColoredBonus redColoredBonus;
	private ColoredBonus yellowColoredBonus;
	private ColoredBonus emptyColoredBonus;
	
	public BonusTest() {
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException {
		super.setUp();
		kingbonuses= map.getKingBonuses();
		bonus1=kingbonuses.get(0);
		cities= map.getCities();
		try {
			redColoredBonus = map.getCityColorBonus(ColorMap.getColor("RED"));
		} catch (ColorNotFoundException e) {

		}
		try {
			yellowColoredBonus = map.getCityColorBonus(ColorMap.getColor("YELLOW"));
		} catch (ColorNotFoundException e) {

		}
		emptyColoredBonus = new ColoredBonus(null, null);
	}
	/*
	 *  Tests the if's second branch in the amount getters, when the bonus has not the kind of bonus required
	 */
	public void testGetters(){
		/*
		 * Kingbonuses have only victoryPoints.
		 */
		assertTrue(bonus1.getAssistantsAmount()==0);
		assertTrue(bonus1.getMoneyAmount()==0);
		assertTrue(bonus1.getNobilityPointsAmount()==0);
		assertTrue(bonus1.getPoliticCardsAmount()==0);
		assertTrue(bonus1.getMainActionsAmount()==0);
		assertTrue(bonus1.getExtraBonusAmount()==0);
		assertTrue(bonus1.getExtraPermitAmount()==0);
		assertTrue(bonus1.getExtraPermitBonusAmount()==0);
		
		for(int i=0; i<cities.size(); i++){
			if(!cities.get(i).getCityBonus().toString().contains("VictoryPoints")){
				bonus2=cities.get(i).getCityBonus();
				assertTrue(bonus2.getVictoryPointsAmount()==0);
			}
		}
		
		try {
			map.getCityColorBonus(ColorMap.getColor("BLACK"));
			fail();
		} catch (ColorNotFoundException e) {

		}
	}
	
	public void testColoredBonus(){
		assertTrue(redColoredBonus.equals(redColoredBonus));
		assertFalse(redColoredBonus.equals(null));
		assertFalse(redColoredBonus.equals(bonus1));
		assertFalse(redColoredBonus.equals(yellowColoredBonus));
		
		assertFalse(emptyColoredBonus.equals(redColoredBonus));
		assertTrue(emptyColoredBonus.equals(new ColoredBonus(null, null)));
		assertTrue(emptyColoredBonus.hashCode()!=redColoredBonus.hashCode());
	}
	
}
