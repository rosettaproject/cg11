package it.polimi.ingsw.cg11.utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.utils.AsString;
import junit.framework.TestCase;

public class AsStringTest extends TestCase {

	private static final String CARD = "CARD ";
	private static final String POSITION = "POSITION ";
	private static final String OFFER = "OFFER ";
	private static final String EMPTY = "empty\n";
	
	List<PoliticCard> listOfPoliticCards;
	Map<Integer, Integer> generalMap;
	
	
	public void setUp() {
		listOfPoliticCards = new ArrayList<>();
		listOfPoliticCards.add(new PoliticCard(Color.BLACK));
		listOfPoliticCards.add(new PoliticCard(Color.BLACK));
		listOfPoliticCards.add(new PoliticCard(Color.PINK));
		listOfPoliticCards.add(new PoliticCard(Color.PINK));
		
		generalMap = new HashMap<>();
		generalMap.put(10, 10);
		generalMap.put(20, 20);
	}
	
	public void test(){
		String stringToTest;
		
		stringToTest = AsString.cardsAsString(listOfPoliticCards);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains(CARD));
		
		stringToTest = AsString.pathAsString(listOfPoliticCards);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains(POSITION));
		
		stringToTest = AsString.marketAsString(listOfPoliticCards);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains(OFFER));
		
		stringToTest = AsString.asString(generalMap, false);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains("10"));
		
		stringToTest = AsString.asString(generalMap, true);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains("10"));
		assertTrue(stringToTest.contains(")"));
		
		stringToTest = AsString.marketAsString(new ArrayList<>());
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains(EMPTY));
		
		stringToTest = AsString.asString(new HashMap<Object, Object>(), false);
		assertNotNull(stringToTest);
		assertTrue(stringToTest.contains(EMPTY));
		
	}
	
	
}
