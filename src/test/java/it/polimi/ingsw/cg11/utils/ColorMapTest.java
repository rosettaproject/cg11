/**
 * 
 */
package it.polimi.ingsw.cg11.utils;

import java.awt.Color;

import org.junit.Test;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import junit.framework.TestCase;

public class ColorMapTest extends TestCase {

	public ColorMapTest(String name) {
		super(name);
	}

	@Test
	public void testName() {
		try {
			ColorMap.getColor("NOT A COLOR");
			assertTrue(false);
		} catch (ColorNotFoundException e) {
			assertTrue(true);
		}
		try {
			ColorMap.getString(new Color(1, 2, 3));
			assertTrue(false);
		} catch (ColorNotFoundException e) {
			assertTrue(true);
		}
		try {
			ColorMap.getColor("BLUE");
			assertTrue(true);
		} catch (ColorNotFoundException e) {
			assertTrue(false);
		}
		try {
			ColorMap.getString(Color.BLUE);
			assertTrue(true);
		} catch (ColorNotFoundException e) {
			assertTrue(false);
		}
		
		try {
			ColorMap.getColor("GRAY");
			assertTrue(true);
		} catch (ColorNotFoundException e) {
			assertTrue(false);
		}
		
		
	}
}
