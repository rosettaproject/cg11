package it.polimi.ingsw.cg11.player_resources_test;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NegativeValueException;
import it.polimi.ingsw.cg11.server.model.player.resources.*;
import junit.framework.*;

public class MoneyTest extends TestCase {
	
	private Money money;

	public MoneyTest(String name) {
		super(name);
	}

	public void setUp() throws NegativeValueException {
		money = new Money();
	}

	public void testGainAndSpend() throws NegativeValueException {
		boolean throwsException=false;
		try {
			money.gain(3);
			money.spend(2);
		} catch (IllegalResourceException e1) {
			throwsException=true;
		}

		assertTrue(money.getAmount() == 1);
		try {
			money.spend(3);
		} catch (IllegalResourceException e) {
			throwsException=true;
		}
		assertTrue(throwsException);
		
		throwsException=false;
		try {
			money.spend(-1);
		} catch ( IllegalResourceException e) {
			throwsException=true;
		}
		assertTrue(throwsException);
		
		try {
			money.gain(-3);
			fail();
		} catch (IllegalResourceException e) {
			
		}
	}

}
