package it.polimi.ingsw.cg11.player_resources_test;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.player.resources.NobilityPoints;
import junit.framework.TestCase;

public class NobilityPointsTest extends TestCase{
	
	private NobilityPoints nobilityPoints;
	private int newPoints=3;
	private int amount=0;
	private int negativeAmount = -3;

	public NobilityPointsTest(String name) {
		super(name);
	}

	public void setUp() {
		nobilityPoints = new NobilityPoints();
		try {
			nobilityPoints.gain(newPoints);
		} catch (IllegalResourceException e) {
			fail();
		}
		try {
			nobilityPoints.gain(negativeAmount );
			fail();
		} catch (IllegalResourceException e) {
			
		}
		amount=nobilityPoints.getAmount();
	}

	public void testGainAndGetAmount() {
		assertTrue(nobilityPoints.getAmount() == newPoints);
		assertTrue(amount==nobilityPoints.getAmount());
		
		try {
			nobilityPoints.gain(NobilityPoints.getMaximumAmount()+1);
		} catch (IllegalResourceException e) {
			fail();
		}
		assertTrue(nobilityPoints.getAmount()==NobilityPoints.getMaximumAmount());
	}

}
