package it.polimi.ingsw.cg11.player_resources_test;

import java.awt.Color;
import java.util.List;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.json.JSONException;


import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.player.resources.PoliticsHand;
import it.polimi.ingsw.cg11.server.model.player.resources.StandardPlayerDataFactory;


public class PlayerDataTest extends MapConfigTest {

	private String username;
	private String currentGame;
	private StandardPlayerDataFactory standardPlayerDataFactory;
	private PoliticsHand politicsHand;
	private List<PoliticCard> politicCards;
	private PoliticCard politicCard;
	private Color color;

	public void setUp() throws FileNotFoundException, CannotCreateMapException {

		super.setUp();
		color = new Color(127, 255, 212);
		username = "player1";
		currentGame = "current Game";
		standardPlayerDataFactory = new StandardPlayerDataFactory();
		politicCards = new ArrayList<>();
		politicCards = map.getPlayers().get(numberOfPlayers - 1).getPlayerData().getPoliticsHand().getCards();
		politicCard = new PoliticCard(color);
	}

	public void test() {

		try {
			setUp();

			assertNotNull(map.getPlayers().get(0).getPlayerData().getPoliticsHand());
			assertNotNull(map.getPlayers().get(0).getPlayerData().getPermitsHand());
			assertNotNull(map.getPlayers().get(0).getPlayerData().getUsedPermits());

			assertEquals(map.getPlayers().get(0).getPlayerData().getAssistants().getAmount(), 1);
			assertEquals(map.getPlayers().get(0).getPlayerData().getMoney().getAmount(), 10);
			assertEquals(map.getPlayers().get(0).getPlayerData().getNobilityPoints().getAmount(), 0);
			assertEquals(map.getPlayers().get(0).getPlayerData().getVictoryPoints().getAmount(), 0);
			assertEquals(map.getPlayers().get(1).getPlayerData().getRemainingEmporiums(), 10);

			Color color = Color.BLUE;
			PoliticCard card = new PoliticCard(color);
			PermitCard card2 = new PermitCard(null, null);
			PermitCard card3 = new PermitCard(null, null);

			map.getPlayers().get(0).getPlayerData().addToPoliticsHand(card);
			map.getPlayers().get(0).getPlayerData().addToPermitsHand(card2);
			map.getPlayers().get(0).getPlayerData().addToUsedPermits(card3);
			map.getPlayers().get(0).getPlayerData().toString();

		} catch (JSONException e) {
		} catch (FileNotFoundException e) {
		} catch (CannotCreateMapException e) {
		}

		assertTrue(map.getPlayers().get(0).getPlayerData().setUsername(username));
		assertFalse(map.getPlayers().get(0).getPlayerData().setUsername(username));
		assertTrue(map.getPlayers().get(0).getPlayerData().setCurrentGame(currentGame));
		assertFalse(map.getPlayers().get(0).getPlayerData().setCurrentGame(currentGame));
		try {
			standardPlayerDataFactory.initializePlayerData(-3, map.getPoliticsDeck(), map.getSecondPoliticsDeck());
			fail();
		} catch (Exception e) {

		}
	}

	public void testHand() {
		politicsHand = map.getPlayers().get(0).getPlayerData().getPoliticsHand();
		try {
			politicsHand.discard(politicCards);
			fail();
		} catch (NoSuchPoliticCardException e) {

		}
		try {
			politicsHand.removeCard(politicCards.get(0));
			fail();
		} catch (NoSuchPoliticCardException e) {

		}
		politicsHand.addToHand(politicCard);
	}
}
