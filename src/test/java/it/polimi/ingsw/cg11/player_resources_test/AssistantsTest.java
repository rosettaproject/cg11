package it.polimi.ingsw.cg11.player_resources_test;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
import junit.framework.*;

public class AssistantsTest extends TestCase {

	private Assistants assistants;

	public AssistantsTest(String name) {
		super(name);
	}

	public void setUp() throws IllegalResourceException {
		assistants = new Assistants();
		assistants.gain(3);
		assistants.use(2);
	}

	public void testGainAndUse() {
		assertTrue(assistants.getAmount() == 1);
		
		boolean throwsException=false;

			try {
				assistants.use(assistants.getAmount()+1);
			} catch (IllegalResourceException e) {
				throwsException = true;
			}

		assertTrue(throwsException);
		throwsException=false;
		try {
			assistants.use(-2);
		} catch (IllegalResourceException e) {
			throwsException = true;
		}
		assertTrue(throwsException);
	}
}
