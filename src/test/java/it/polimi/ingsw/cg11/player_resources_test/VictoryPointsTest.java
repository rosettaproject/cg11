package it.polimi.ingsw.cg11.player_resources_test;

import it.polimi.ingsw.cg11.server.model.player.resources.*;
import junit.framework.*;

public class VictoryPointsTest extends TestCase {

	private VictoryPoints victoryPoints;
	private int newPoints = 3;
	private int amount = 0;

	public VictoryPointsTest(String name) {
		super(name);
	}

	public void setUp() {
		victoryPoints = new VictoryPoints();
		victoryPoints.gain(newPoints);
		amount = victoryPoints.getAmount();
	}

	public void testGain() {
		boolean throwsException=false;
		assertTrue(victoryPoints.getAmount() == newPoints);
		assertTrue(amount == victoryPoints.getAmount());
		
		try {
			victoryPoints.gain(-2);
		} catch (IllegalArgumentException e) {
			throwsException=true;
		}
		assertTrue(throwsException);
		victoryPoints.gain(VictoryPoints.getMaxPoint());
		assertTrue(victoryPoints.getAmount()==VictoryPoints.getMaxPoint());
	}

}
