package it.polimi.ingsw.cg11.turn_manager_test;
import java.io.FileNotFoundException;

import org.json.JSONException;

import it.polimi.ingsw.cg11.map_components_test.MapConfigTest;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;

public class TurnManagerTest extends MapConfigTest {
		
	public TurnManagerTest(){
		super();
	}
	
	public void setUp() throws FileNotFoundException, CannotCreateMapException{
		try {
			super.setUp();

		} catch (JSONException e) {
		}
		
	}
	
	public void test(){
		
		assertEquals(map.getTurnManager().getStartingGamePhase(),"StandardPlayPhase");
		
		assertTrue(map.getTurnManager().getCurrentlyPlaying().isAllowedToPlay());
		assertTrue(map.getTurnManager().isPlaying(map.getPlayers().get(0)));
		assertFalse(map.getTurnManager().isPlaying(map.getPlayers().get(1)));
		assertEquals(map.getTurnManager().getCurrentlyPlaying(),map.getPlayers().get(0));
		
	
		map.getTurnManager().changeTurn();
		
		assertTrue(map.getTurnManager().getCurrentlyPlaying().isAllowedToPlay());
		assertFalse(map.getPlayers().get(0).isAllowedToPlay());
		assertEquals(map.getTurnManager().getCurrentlyPlaying(),map.getPlayers().get(1));
		
		map.getTurnManager().changeTurn();
		
		assertTrue(map.getTurnManager().getCurrentlyPlaying().isAllowedToPlay());
		assertFalse(map.getPlayers().get(1).isAllowedToPlay());
		assertEquals(map.getTurnManager().getCurrentlyPlaying(),map.getPlayers().get(2));
		
		map.getTurnManager().changeTurn();
		
		assertTrue(map.getTurnManager().getCurrentlyPlaying().isAllowedToPlay());
		assertFalse(map.getPlayers().get(2).isAllowedToPlay());
		assertEquals(map.getTurnManager().getCurrentlyPlaying(),map.getPlayers().get(3));

	}
}
