/**
 * 
 */
package it.polimi.ingsw.cg11.other_components_test;

import junit.framework.*;

/**
 * @author Federico
 *
 */
public class OtherComponentTestRunner {

	/**
	 * @param args
	 */
	public static Test suite(){
		TestSuite suite = new TestSuite();
		suite.addTest(new BalconyTest("testBalconies_3"));
		return suite;
	}
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());
		
	}

}
