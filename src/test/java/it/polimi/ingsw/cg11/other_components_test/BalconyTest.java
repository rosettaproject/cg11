package it.polimi.ingsw.cg11.other_components_test;

import java.io.FileNotFoundException;
import java.util.*;
import org.json.JSONArray;
import org.json.JSONException;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.utils.*;
import junit.framework.*;
/**
 * @author Federico
 *
 */
public class BalconyTest extends TestCase{

	private final int NUMBER_OF_BALCONY=1;
	private final int NUMBER_OF_COUNCILLORS_PER_BALCONY=4;
	private final int NUMBER_OF_REGIONS=3;
	
	private  int numberOfCouncillors;
	private int numberOfColors;
	
	private List<String> regionNames = new ArrayList<>();
	private List<Balcony> balconies = new ArrayList<>();
	private List<Queue<Councillor>> listOfCouncils = new ArrayList<>();
	private CouncillorPoolManager poolCouncillors1;

	/**
	 * 
	 */
	public BalconyTest(String name) {
		super(name);
	}

	public void setUpRegionName(){
		String namePattern = "REGION_";
		String singleRegionName;
		
		for (int regionIndex = 0; regionIndex<NUMBER_OF_REGIONS; regionIndex++){
			singleRegionName = namePattern + String.valueOf(regionIndex+1);
			regionNames.add(singleRegionName);
		}
	}

	public void setUpCouncillors() throws FileNotFoundException, JSONException, ColorNotFoundException{
		String filePath = "./src/test/resources/poolOfCouncillorTest.txt";
		
		FileToJson fileReader = new FileToJson();
		JSONArray jsonArray =  fileReader.jsonArrayFromFile(filePath);
		
		poolCouncillors1 = new CouncillorPoolManager(jsonArray);
		
		numberOfCouncillors=12;
	}

	public void setUpCouncils() throws NoSuchCouncillorException{
		Queue<Councillor> council;
		Councillor newCouncillor;
		
		poolCouncillors1.shuffleCouncillors();
		
		for(int balconyIndex=0; balconyIndex<NUMBER_OF_BALCONY; balconyIndex++ ){
			council = new LinkedList<>();
			for(int councilIndex=0; councilIndex<NUMBER_OF_COUNCILLORS_PER_BALCONY; councilIndex++){
				newCouncillor = poolCouncillors1.obtainPieceFromPool();
				council.add(newCouncillor);
			}
			listOfCouncils.add(balconyIndex, council);
		}
	}

	public void setUpBalconies(){
		Balcony singleBalcony;
		String regionName;
		Queue<Councillor> council;
		
		
		for(int indexBalcony=0; indexBalcony<NUMBER_OF_BALCONY; indexBalcony++){
			regionName=regionNames.get(indexBalcony);
			council = listOfCouncils.get(indexBalcony);
			
			singleBalcony = new Balcony(regionName, council);
			
			balconies.add(indexBalcony, singleBalcony);	
		}
		
		
	}
	
	public void setUp() throws FileNotFoundException, NoSuchCouncillorException, JSONException, ColorNotFoundException{
		setUpRegionName();
		setUpCouncillors();
		setUpCouncils();
		setUpBalconies();
		
	}
	
	
	public void testBalconies_3() throws NoSuchCouncillorException{
		assertEquals("REGION_1", balconies.get(0).getNAME());
	}
	
	/**
	 * @return the nUMBER_OF_BALCONY
	 */
	public int getNUMBER_OF_BALCONY() {
		return NUMBER_OF_BALCONY;
	}

	/**
	 * @return the nUMBER_OF_COUNCILLORS_PER_BALCONY
	 */
	public int getNUMBER_OF_COUNCILLORS_PER_BALCONY() {
		return NUMBER_OF_COUNCILLORS_PER_BALCONY;
	}

	/**
	 * @return the nUMBER_OF_COUNCILLORS
	 */
	public int getNUMBER_OF_COUNCILLORS() {
		return numberOfCouncillors;
	}

	/**
	 * @return the nUMBER_OF_REGIONS
	 */
	public int getNUMBER_OF_REGIONS() {
		return NUMBER_OF_REGIONS;
	}

	/**
	 * @return the numberOfColors
	 */
	public int getNumberOfColors() {
		return numberOfColors;
	}


	/**
	 * @return the regionNames
	 */
	public List<String> getRegionNames() {
		return regionNames;
	}

	/**
	 * @return the balconies
	 */
	public List<Balcony> getBalconies() {
		return balconies;
	}

	/**
	 * @return the poolCouncillors
	 */
	public CouncillorPoolManager getPoolCouncillors() {
		return poolCouncillors1;
	}

	/**
	 * @return the listOfCouncils
	 */
	public List<Queue<Councillor>> getListOfCouncils() {
		return listOfCouncils;
	}

}
