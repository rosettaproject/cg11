package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link ElectCouncillor} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableElectCouncillor extends SerializableAction {

	private static final long serialVersionUID = -5231193510648849926L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}

}
