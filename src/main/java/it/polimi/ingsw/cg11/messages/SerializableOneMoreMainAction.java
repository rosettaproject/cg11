package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.OneMoreMainAction;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link OneMoreMainAction} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableOneMoreMainAction extends SerializableAction {

	private static final long serialVersionUID = -8349058117551812894L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
