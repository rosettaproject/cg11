package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link BuildEmporiumWithKing} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableBuildEmporiumWithKing extends SerializableAction {

	private static final long serialVersionUID = 5868025889603728662L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
