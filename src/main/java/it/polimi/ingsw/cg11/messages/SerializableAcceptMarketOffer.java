package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.AcceptMarketOffer;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link AcceptMarketOffer} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableAcceptMarketOffer extends SerializableAction {

	private static final long serialVersionUID = -5231193510648849926L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
