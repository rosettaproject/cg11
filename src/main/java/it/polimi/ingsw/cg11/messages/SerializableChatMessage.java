package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link } action.
 * 
 * @author Federico
 *
 */
public class SerializableChatMessage extends SerializableAction {

	private String message;
	private String sender;

	private static final long serialVersionUID = 6947812928114129645L;

	public SerializableChatMessage() {
		//No attributes to set up
	}
	
	/**
	 * Builds the object setting the sender and the message
	 * @param sender
	 * @param message
	 */
	public SerializableChatMessage(String sender, String message) {
		this.sender = sender;
		this.message = message;
	}

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);

	}

	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the sender of message
	 */
	public String getSender() {
		return sender;
	}

}
