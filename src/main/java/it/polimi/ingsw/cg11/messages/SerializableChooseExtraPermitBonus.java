package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 *  
 * @author paolasanfilippo
 *
 */
public class SerializableChooseExtraPermitBonus extends SerializableAction {

	private static final long serialVersionUID = -5642451851287210162L;
	
	@Override
	public void accept(ServerVisitor visitor) {
			visitor.visit(this);	
	}
}
