/**
 * 
 */
package it.polimi.ingsw.cg11.messages;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * A generic message to send to the client
 * @author Federico
 */
public class GenericMessage implements Visitable, Serializable {

	private static final Logger LOG = Logger.getLogger(GenericMessage.class.getName());

	private static final long serialVersionUID = 6646911175841769195L;

	private String message;
	private String topic;
	private Token playerToken;

	/**
	 * @param message this object's message as a string
	 * @param topic
	 */
	public GenericMessage(String message, String topic) {
		this.message = message;
		this.topic = topic;
	}

	@Override
	public String getTopic() {
		return topic;
	}
	
	/**
	 * @return this object message as a string
	 */
	public String getMessage(){
		return message;
	}

	@Override
	public void accept(ServerVisitor visitor) {
		LOG.log(Level.SEVERE, "Attempted visit from unsupported visitor");
	}

	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public Token getPlayerToken() {
		return playerToken;
	}

	@Override
	public void setPlayerToken(Token playerToken) {
		this.playerToken = playerToken;
	}

}
