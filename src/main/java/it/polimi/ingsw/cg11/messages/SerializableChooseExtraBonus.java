package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link ChooseExtraBonus} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableChooseExtraBonus extends SerializableAction {

	private static final long serialVersionUID = 5157829054328191478L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
