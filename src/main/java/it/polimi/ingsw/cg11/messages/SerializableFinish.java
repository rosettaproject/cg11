package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link ElectCouncillorWithOneAssistant} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableFinish extends SerializableAction {

	private static final long serialVersionUID = 8764938487020311995L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
