package it.polimi.ingsw.cg11.messages;

import java.awt.Color;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This object contains an encoding of the changes occurred in the game state after any of the players performed an action
 * @author francesco
 *
 */
public class ModelChanges implements Serializable, Visitable {

	private static final long serialVersionUID = -4450665760727939096L;
	private static final Logger LOG = Logger.getLogger(ModelChanges.class.getName());
	public static final String RESOURCES = "resources";
	private String jsonConfigAsString;
	private String topic;
	private Token playerToken;

	public ModelChanges() {
		this.jsonConfigAsString = "{}";
	}

	/**
	 * This constructor takes a JSONObject as a parameter containing an encoding of some model changes
	 * @param change
	 */
	public ModelChanges(JSONObject change) {
		jsonConfigAsString = change.toString();
	}

	/**
	 * @return the changes encoded in this object as a string
	 */
	public String getJsonConfigAsString() {
		return jsonConfigAsString;
	}

	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;

	}
	
	/**
	 * @param changeKey
	 * @return true if a change exist identified by the key specified as a parameter
	 */
	public boolean containsChange(String changeKey){
		return jsonConfigAsString.contains(changeKey);
	}

	/**
	 * Adds an integer to the changes, with the string taken as a parameter as its key
	 * @param changeKey
	 * @param changeValue
	 */
	public void addToChanges(String changeKey, int changeValue) {
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		oldChanges.put(changeKey, changeValue);
		jsonConfigAsString = oldChanges.toString();
	}


	/**
	 * Adds a string to the changes, with the string taken as a parameter as its key
	 * @param changeKey
	 * @param changeValue
	 */
	public void addToChanges(String changeKey, String changeValue) {
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		oldChanges.put(changeKey, changeValue);
		jsonConfigAsString = oldChanges.toString();
	}


	/**
	 * Adds a JSONObject to the changes, with the string taken as a parameter as its key. Since this
	 * method relies on a specific encoding for model changes, it shall be kept as a private method for 
	 * this specific implementation
	 * @param changeKey
	 * @param changeValue
	 */
	private void addToChanges(String changeKey, JSONObject object) {
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		oldChanges.put(changeKey, object);
		jsonConfigAsString = oldChanges.toString();
	}

	/**
	 * Adds a JSONArray to the changes, with the string taken as a parameter as its key. Since this
	 * method relies on a specific encoding for model changes, it shall be kept as a private method for 
	 * this specific implementation
	 * @param changeKey
	 * @param changeValue
	 */
	private void addToChanges(String changeKey, JSONArray array) {
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		oldChanges.put(changeKey, array);
		jsonConfigAsString = oldChanges.toString();
	}

	/**
	 * Adds an integer as a resource, meaning that if a key with the same name is already present, the value
	 * passed as a parameter will be added to the current value
	 * @param resourceKey
	 * @param amount
	 */
	public void addAsResource(String resourceKey, int amount) {
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		JSONObject resources = new JSONObject();
		int resourceTotal;

		if (oldChanges.has(RESOURCES)) {
			resources = oldChanges.getJSONObject(RESOURCES);
			oldChanges.remove(RESOURCES);
		}

		if (!resources.has(resourceKey))
			resources.put(resourceKey, amount);
		else {
			resourceTotal = amount + resources.getInt(resourceKey);
			resources.remove(resourceKey);
			resources.put(resourceKey, resourceTotal);
		}
		addToChanges(RESOURCES, resources);
	}

	/**
	 * Adds the content of the ModelChanges passed as a parameter to this object
	 * @param changesToAdd
	 */
	public void addToChanges(ModelChanges changesToAdd) {

		JSONObject changesAsJson = new JSONObject(changesToAdd.getJsonConfigAsString());
		Iterator<String> keysIterator = changesAsJson.keys();

		if (changesAsJson.has(RESOURCES)) {
			JSONObject resources = changesAsJson.getJSONObject(RESOURCES);

			Iterator<String> resourcesIterator = resources.keys();
			
			while (resourcesIterator.hasNext()) {
				String resourceKey = resourcesIterator.next();
				addAsResource(resourceKey, resources.getInt(resourceKey));
			}

		}

		JSONObject oldChanges = new JSONObject(jsonConfigAsString);

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			if (!key.equalsIgnoreCase(RESOURCES))
				oldChanges.put(key, changesAsJson.get(key));
		}
		jsonConfigAsString = oldChanges.toString();
	}

	/**
	 * Adds an encoding of the {@link PoliticCard} passed as a parameter to the model changes in this object
	 * @param changeKey 
	 * @param card
	 * @throws ColorNotFoundException
	 */
	public void addToChanges(String changeKey, PoliticCard card) throws ColorNotFoundException {

		JSONObject cardAsJson = new JSONObject();

		cardAsJson.put("color", ColorMap.getString(card.getColor()));
		addToChanges(changeKey, cardAsJson);
	}

	/**
	 * Adds an encoding of the list of {@link PoliticCard}s passed as a parameter to the model changes in this object
	 * @param changeKey 
	 * @param politicCards
	 * @throws ColorNotFoundException
	 */
	public void addToChanges(String changeKey, List<PoliticCard> politicCards) throws ColorNotFoundException {

		JSONArray cards = new JSONArray();

		for (PoliticCard card : politicCards) {
			JSONObject cardAsJson = new JSONObject();
			cardAsJson.put("color", ColorMap.getString(card.getColor()));
			cards.put(cardAsJson);
		}
		addToChanges(changeKey, cards);
	}

	/**
	 * Adds an encoding of the {@link Color} passed as a parameter to the changes
	 * @param changeKey
	 * @param color
	 * @throws ColorNotFoundException
	 */
	public void addToChanges(String changeKey, Color color) throws ColorNotFoundException {
		addToChanges(changeKey, ColorMap.getString(color));
	}

	/**
	 * Adds an encoding of the {@link PermitCard} passed as a parameter to the model changes in this object
	 * @param changeKey 
	 * @param permitToAdd
	 */
	public void addToChanges(String changeKey, PermitCard permitToAdd) {
		
		ModelChanges permitCard = new ModelChanges();
		
		permitCard.addToChanges("PermitBonus",permitToAdd.getPermitBonus());
		permitCard.addCityListToChanges("PermitCities", permitToAdd.getCities());
				
		JSONObject permitAsJson = new JSONObject(permitCard.jsonConfigAsString);
		
		addToChanges(changeKey,permitAsJson);
		
	}
	
	/**
	 * Adds the names of the cities passed as a parameter to the changes
	 * @param changeKey
	 * @param cities
	 */
	public void addCityListToChanges(String changeKey,List<City> cities){
		
		JSONArray citiesAsJson = new JSONArray();
		
		for(City city: cities){
			citiesAsJson.put(city.getName());
		}
		addToChanges(changeKey,citiesAsJson);
	}
	
	/**
	 * Adds an encoding of the {@link Bonus} passed as a parameter to the changes
	 * @param changeKey
	 * @param bonusToAdd
	 */
	public void addToChanges(String changeKey, Bonus bonusToAdd) {

		JSONObject bonus = new JSONObject();

		Iterator<String> transactionIterator = bonusToAdd.getChainOfCommands().keySet().iterator();

		while (transactionIterator.hasNext()) {
			String key = transactionIterator.next();
			if (bonusToAdd.getChainOfCommands().containsKey(key)) {
				bonus.put(key, bonusToAdd.getChainOfCommands().get(key).getAmount());
			}
		}
		addToChanges(changeKey,bonus);
	}
	
	/**
	 * Removes the model change associated with the specified key from this object
	 * @param key
	 */
	public void removeFromChanges(String key){
		JSONObject oldChanges = new JSONObject(jsonConfigAsString);
		oldChanges.remove(key);
		jsonConfigAsString = oldChanges.toString();
	}

	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visit(this);

	}

	@Override
	public void accept(ServerVisitor visitor) {
		LOG.log(Level.SEVERE, "Attempted visit from unsupported visitor");
	}
	
	@Override
	public String toString() {
		return new JSONObject(jsonConfigAsString).toString(1);
	}

	@Override
	public Token getPlayerToken() {
		return playerToken;
	}

	@Override
	public void setPlayerToken(Token playerToken) {
		this.playerToken = playerToken;
	}
}
