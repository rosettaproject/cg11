package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.ChangePermitCards;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link ChangePermitCards} action.
 * @author paolasanfilippo
 *
 */
public class SerializableChangePermitCards extends SerializableAction {

	private static final long serialVersionUID = -7524759048430032822L;

	@Override
	public void accept(ServerVisitor visitor) {
			visitor.visit(this);	
	}
}
