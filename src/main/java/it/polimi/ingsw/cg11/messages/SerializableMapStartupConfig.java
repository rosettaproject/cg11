package it.polimi.ingsw.cg11.messages;

import java.util.List;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * 
 * This object is to be sent to all clients participating in a game so they can set up the initial game state
 * @author francesco
 *
 */
public class SerializableMapStartupConfig extends SerializableAction {

	private static final long serialVersionUID = -4437483915925474900L;
	
	/**
	 * Adds a list of token to the {@link JSONObject} jsonConfig
	 * @param tokenList
	 */
	public void addTokenList(List<Token> tokenList){
		JSONObject jsonConfig = new JSONObject(getJsonConfigAsString());
		JSONArray tokens = new JSONArray();
		ListIterator<Token> tokenIterator = tokenList.listIterator();
		
		while(tokenIterator.hasNext()){
			Token token = tokenIterator.next();
			JSONObject tokenAsJson = new JSONObject();
			
			tokenAsJson.put("PlayerID", token.getPlayerID());
			tokenAsJson.put("Username", token.getClientUsername());
			tokens.put(tokenAsJson);
		}
		jsonConfig.put("Tokens", tokens);
		setJsonConfigAsString(jsonConfig);
	}

	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visit(this);			
	}

}
