package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This object is a chat message
 * @author Federico
 */
public final class Messages {

	private static final String CHANGE_SUFFIX = "_CHANGES";

	private Messages() {}
	
	/**
	 * Sends the message passed as a parameter to all the players in the game identified with the gameName parameter
	 * @param message
	 * @param gameName
	 */
	public static void sendToAllPlayers(String message, String gameName) {
		GenericMessage notice;
		String topic;
		
		topic = gameName + CHANGE_SUFFIX;
		notice = new GenericMessage(message, topic);
		
		Broker.getBroker().publish(notice, topic);
	}
	
	/**
	 * Sends the message passed as a parameter to the player in the game identified with the gameName parameter who's id
	 * matches the one passed as a parameter
	 * @param message
	 * @param gameName
	 * @param playerID
	 */
	public static void sendToPlayer(String message, String gameName, int playerID) {
		GenericMessage notice;
		String topic;
		
		topic = gameName + "_" + playerID;
		notice = new GenericMessage(message, topic);
		
		Broker.getBroker().publish(notice, topic);
	}
	
	/**
	 * Sends the message passed as a parameter to the player in the game identified with the gameName parameter who's
	 * username matches the one passed as a parameter
	 * @param message
	 * @param gameName
	 * @param username
	 */
	public static void sendToPlayer(String message, String gameName, String username) {
		GenericMessage notice;
		String topic;
		
		topic = gameName + "_" + username;
		notice = new GenericMessage(message, topic);
		
		Broker.getBroker().publish(notice, topic);
	}
}
