package it.polimi.ingsw.cg11.messages;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This serializable object is sent to signals the disconnection of a player to the server
 * @author francesco
 *
 */
public class DisconnectedPlayerMessage implements Visitable, Serializable{

	private static final Logger LOG = Logger.getLogger(DisconnectedPlayerMessage.class.getName());
	private static final long serialVersionUID = -8350415033668669475L;

	private String topic;
	private Token playerToken;
	
	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void accept(ClientVisitor visitor) {
		LOG.log(Level.WARNING, "Client cannot visit this message");
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;
		
	}

	@Override
	public Token getPlayerToken() {
		return playerToken;
	}

	@Override
	public void setPlayerToken(Token playerToken) {
		this.playerToken = playerToken;
	}


}
