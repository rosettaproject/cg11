package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.OfferAssistants;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link OfferAssistants} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableOfferPoliticCard extends SerializableAction {

	private static final long serialVersionUID = 1675938466950669696L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
