package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link TakePermitCard} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableTakePermitCard extends SerializableAction {

	private static final long serialVersionUID = 5821963243081386657L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
