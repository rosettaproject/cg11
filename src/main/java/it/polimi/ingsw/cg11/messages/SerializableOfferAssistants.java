package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.OfferAssistants;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link OfferAssistants} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableOfferAssistants extends SerializableAction {
	private static final long serialVersionUID = -7803702683245972198L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
