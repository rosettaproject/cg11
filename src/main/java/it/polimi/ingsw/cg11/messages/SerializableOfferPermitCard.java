package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.OfferPermitCard;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link OfferPermitCard} action.
 * 
 * @author paolasanfilippo
 *
 */
public class SerializableOfferPermitCard extends SerializableAction {

	private static final long serialVersionUID = 3908451739553236584L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
