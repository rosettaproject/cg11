package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.EngageAnAssistant;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link EngageAnAssistant} action.
 * 
 * @author Federico
 *
 */
public class SerializableEngageAssistant extends SerializableAction {

	private static final long serialVersionUID = 4113958010461271042L;

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
	}
}
