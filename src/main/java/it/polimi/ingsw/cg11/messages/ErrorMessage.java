package it.polimi.ingsw.cg11.messages;

import java.io.Serializable;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

public class ErrorMessage implements Visitable,Serializable {

	private String message;
	private String topic;
	private Token playerToken;

	private static final long serialVersionUID = 6947812928114129645L;
	
	/**
	 * @param message this object's message as a string
	 * @param topic
	 */
	public ErrorMessage(String message, String topic) {
		this.message = message;
		this.topic = topic;
		
		playerToken = new Token("");
	}


	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;
		
	}

	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visit(this);
		
	}
	
	/**
	 * @return this object message as a string
	 */
	public String getMessage(){
		return message;
	}


	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visit(this);
	}
	

	@Override
	public String toString() {
		return "Topic: " + topic + "\nMessage: " + message;
	}


	@Override
	public Token getPlayerToken() {
		return playerToken;
	}

	@Override
	public void setPlayerToken(Token playerToken) {
		this.playerToken = playerToken;
	}

}
