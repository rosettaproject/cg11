package it.polimi.ingsw.cg11.messages;

import java.io.Serializable;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This abstract class defines a generic action tha can be sent from client to
 * server
 * 
 * @author Federico
 *
 */
public abstract class SerializableAction implements Serializable, Visitable {

	private static final long serialVersionUID = 7485729335607717910L;
	protected static final Logger LOG = Logger.getLogger(SerializableChangePermitCards.class.getName());
	private String jsonConfigAsString;
	private String topic;
	private Token playerToken;

	@Override
	public Token getPlayerToken() {
		return playerToken;
	}

	@Override
	public void setPlayerToken(Token playerToken) {
		this.playerToken = playerToken;
	}

	/**
	 * Set the configuration of action
	 * 
	 * @param actionConfig
	 */
	public void setJsonConfigAsString(JSONObject actionConfig){
		jsonConfigAsString = actionConfig.toString();
	}
	
	/**
	 * Returns the configuration of action as string
	 * 
	 * @return a string that represent the configuration of action
	 */
	public String getJsonConfigAsString() {
		return jsonConfigAsString;
	}

	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public String toString() {
		if(jsonConfigAsString != null)
			return new JSONObject(jsonConfigAsString).toString(1);
		else 
			return "";
	}
}
