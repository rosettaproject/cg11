package it.polimi.ingsw.cg11.messages;

import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;

/**
 * Serializable for the {@link BuildEmporiumWithPermitCard} action.
 * @author paolasanfilippo
 *
 */
public class SerializableBuildEmporiumWithPermitCard extends SerializableAction {

	private static final long serialVersionUID = 7472102413105026291L;
	
	@Override
	public void accept(ServerVisitor visitor) {
			visitor.visit(this);	
	}

}
