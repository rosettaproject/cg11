package it.polimi.ingsw.cg11.server.model.player.actionspool;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.AvailableMandatoryMainActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.AvailableMandatoryPlayerChoiceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.resources.PoliticsHand;

/**
 * This class contains a reference to the player to whom it belongs and also
 * methods to call the handler objects to perform any of the available actions
 * 
 * @author francesco
 *
 */
public class PlayerActionExecutor {

	private static final Logger LOG = Logger.getLogger(PlayerActionExecutor.class.getName());

	private static final int SECOND_PLACE_NOBILITY = 2;
	private static final int FIRST_PLACE_NOBILITY = 5;
	private static final int MOST_PERMIT_CARDS = 3;

	public static final String ACTOR_ID = "ActorID";
	public static final String DRAWING_PLAYER_ID_KEY = "DrawingID";
	public static final String BLIND_DRAW_ID = "BlindDrawID";
	public static final String POLITIC_CARD = "PoliticCard";
	public static final String EMPTY_DECK_MESSAGE = "EmptyDeck";
	private static final String NOBILITY_POINTS = "Nobility";
	private static final String PERMIT_CARDS = "PermitCards";
	private static final String POLITIC_CARDS_AND_ASSISTANTS = "PoliticCards+Assistants";
	private static final String VICTORY_POINTS = "VictoryPoints";

	private List<GameAction> availableActions;
	private PlayerState player;
	private MainActionsHandler mainActionsHandler;
	private QuickActionsHandler quickActionsHandler;
	private MarketActionsHandler marketActionsHandler;
	private PlayerChoicesHandler playerChoicesHandler;
	/**
	 * 
	 * @param player
	 */
	public PlayerActionExecutor(PlayerState player) {
		availableActions = new ArrayList<>();
		this.resetTurnActions();
		this.player = player;
	}

	/**
	 * Draw a card from Politic Deck This action is executed at the start of
	 * each turn
	 * 
	 * @throws IllegalStateChangeException
	 */
	public void drawPoliticCard() {
		try {
			player.getPlayerData().getPoliticsHand().draw();
		} catch (EmptyPoliticDeckException e) {
			LOG.log(Level.INFO, "Empty politic deck", e);
		}
	}

	/**
	 * Ends the player's turn
	 * 
	 * @param turnManager
	 * @param gameMap
	 * @return {@link ModelChanges}
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges finish(TurnManager turnManager, GameMap gameMap) throws IllegalStateChangeException {
		PoliticsHand playerHand;
		ModelChanges modelChanges = new ModelChanges();

		int actorID;

		if (availableActions.contains(new MainAction())) {
			throw new IllegalStateChangeException(new AvailableMandatoryMainActionException());
		}
		if (availableActions.contains(new PlayerChoice("Generic")))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		actorID = turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID();
		turnManager.changeTurn();
		playerHand = turnManager.getCurrentlyPlaying().getPlayerData().getPoliticsHand();

		try {
			if (!turnManager.getLastTurnManager().gameIsOver()) {
				if (turnManager.getCurrentGamePhase().equals(turnManager.getFirstGamePhase())) {
					modelChanges = playerHand.draw();
					modelChanges.addToChanges(DRAWING_PLAYER_ID_KEY,
							turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID());
				}
				modelChanges.addToChanges(PlayerActionExecutor.ACTOR_ID, actorID);
			} else {
				attributeVictoryResources(gameMap, modelChanges);
			}
		} catch (EmptyPoliticDeckException e) {
			modelChanges.addToChanges(DRAWING_PLAYER_ID_KEY,
					turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID());
			LOG.log(Level.INFO, "Empty politic deck", e);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, "Unexpected exception while assigning victory resources", e);
		}

		
		return modelChanges;
	}
	/**
	 * When a new turn starts, it adds a main and a quick action to be performed by the player
	 */
	public void resetTurnActions() {

		while (!availableActions.isEmpty())
			availableActions.remove(0);

		availableActions.add(new MainAction());
		availableActions.add(new QuickAction());
	}
	/**
	 * Adds the action passed as parameter to available ones
	 * @param action
	 */
	public void addToAvailableActions(GameAction action) {
		availableActions.add(action);
	}

	/**
	 * Assigns the final points of the game
	 * @param gameMap
	 * @param modelChanges
	 * @throws IllegalResourceException
	 */
	public void attributeVictoryResources(GameMap gameMap, ModelChanges modelChanges) throws IllegalResourceException {

		List<PlayerState> players = gameMap.getPlayers();

		assignPointsForMostNobility(players, modelChanges);
		assignPointsForMostPermitCards(players, modelChanges);
		settleWinner(players, modelChanges);
	}
	/**
	 * At the end of the game, assigns the extra victory points to he player that has more nobility points
	 * @param players
	 * @param modelChanges
	 * @throws IllegalResourceException
	 */
	private void assignPointsForMostNobility(List<PlayerState> players, ModelChanges modelChanges)
			throws IllegalResourceException {

		List<PlayerState> temporaryPlayersList = new ArrayList<>(players);

		List<PlayerState> playersWithMostNobility = extractPlayersWithMostResource(temporaryPlayersList,
				NOBILITY_POINTS);

		if (playersWithMostNobility.size() == 1) {
			List<PlayerState> secondPlacePlayers = extractPlayersWithMostResource(temporaryPlayersList,
					NOBILITY_POINTS);

			playersWithMostNobility.get(0).getPlayerData().getVictoryPoints().gain(FIRST_PLACE_NOBILITY);
			modelChanges.addAsResource(Integer.toString(playersWithMostNobility.get(0).getPlayerData().getPlayerID()),
					FIRST_PLACE_NOBILITY);

			for (PlayerState selectedPlayer : secondPlacePlayers) {
				selectedPlayer.getPlayerData().getVictoryPoints().gain(SECOND_PLACE_NOBILITY);
				modelChanges.addAsResource(
						Integer.toString(selectedPlayer.getPlayerData().getPlayerID()),
						SECOND_PLACE_NOBILITY);
			}

		} else {
			for (PlayerState selectedPlayer : playersWithMostNobility) {
				selectedPlayer.getPlayerData().getVictoryPoints().gain(FIRST_PLACE_NOBILITY);
				modelChanges.addAsResource(
						Integer.toString(selectedPlayer.getPlayerData().getPlayerID()),
						FIRST_PLACE_NOBILITY);
			}
		}
	}

	/**
	 * At the end of the game, assigns the extra victory points to the player that has more {@link PermitCard}
	 * @param players
	 * @param modelChanges
	 */
	public void assignPointsForMostPermitCards(List<PlayerState> players, ModelChanges modelChanges) {

		List<PlayerState> temporaryPlayersList = new ArrayList<>(players);

		List<PlayerState> playersWithMostPermits = extractPlayersWithMostResource(temporaryPlayersList, PERMIT_CARDS);

		if(playersWithMostPermits.size() == 1)
			for (PlayerState selectedPlayer : playersWithMostPermits) {
				selectedPlayer.getPlayerData().getVictoryPoints().gain(MOST_PERMIT_CARDS);
				modelChanges.addAsResource(Integer.toString(selectedPlayer.getPlayerData().getPlayerID()),
						MOST_PERMIT_CARDS);
		}
	}

	/**
	 * Tells which player is the winner of the game
	 * @param players
	 * @param modelChanges
	 */
	public void settleWinner(List<PlayerState> players, ModelChanges modelChanges) {

		List<PlayerState> temporaryPlayersList = new ArrayList<>(players);

		List<PlayerState> playersWithMostVictoryPoints = extractPlayersWithMostResource(temporaryPlayersList,
				VICTORY_POINTS);

		if (playersWithMostVictoryPoints.size() == 1) {
			int winnerID = playersWithMostVictoryPoints.get(0).getPlayerData().getPlayerID();
			modelChanges.addToChanges("WinnerID", winnerID);
		} else {
			List<PlayerState> playersWithMostAssistantsAndPoliticCards = extractPlayersWithMostResource(
					playersWithMostVictoryPoints, VICTORY_POINTS);
			
			int index = 0;
			for (PlayerState selectedPlayer : playersWithMostAssistantsAndPoliticCards) {
				int winnerID = selectedPlayer.getPlayerData().getPlayerID();
				modelChanges.addToChanges("WinnerID"+"_"+index, winnerID);
				index ++;
			}
		}
	}

	/**
	 * Removes from the list passed as a parameter the players with the highest
	 * (tied) nobility and returns them as a new ArrayList
	 * 
	 * @param players
	 * @param resource : string with the kind of resource looked for
	 * @return the list of players with the highest (tied) nobility
	 */
	private List<PlayerState> extractPlayersWithMostResource(List<PlayerState> players, String resource) {

		List<PlayerState> playersWithMostResource = new ArrayList<>();
		PlayerState playerWithMostResource = null;
		PlayerState tmpPlayer;
		int maxResource = 0;
		int playerIndex;

		for (PlayerState selectedPlayer : players) {
			int playerResource = getPlayerResource(resource, selectedPlayer);

			if (playerResource >= maxResource) {
				maxResource = playerResource;
				playerWithMostResource = selectedPlayer;
			}
		}
		playersWithMostResource.add(playerWithMostResource);
		players.remove(playerWithMostResource);

		playerIndex = 0;
		while (playerIndex < players.size()) {
			tmpPlayer = players.get(playerIndex);
			if (getPlayerResource(resource, tmpPlayer) == maxResource) {
				playersWithMostResource.add(tmpPlayer);
				players.remove(tmpPlayer);
				playerIndex -= 1;
			}
			playerIndex += 1;
		}

		return playersWithMostResource;
	}

	/**
	 * Returns the amount of the selected resource, passed as parameter, that the player owns
	 * 
	 * @param resource
	 * @param selectedPlayer
	 * @return the amount
	 */
	private int getPlayerResource(String resource, PlayerState selectedPlayer) {

		int resourceAmount;

		switch (resource) {

		case NOBILITY_POINTS:
			resourceAmount = selectedPlayer.getPlayerData().getNobilityPoints().getAmount();
			break;

		case PERMIT_CARDS:
			resourceAmount = selectedPlayer.getPlayerData().getPermitsHand().getNumberOfCards()
					+ selectedPlayer.getPlayerData().getUsedPermits().getNumberOfCards();
			break;

		case POLITIC_CARDS_AND_ASSISTANTS:
			resourceAmount = selectedPlayer.getPlayerData().getPoliticsHand().getNumberOfCards()
					+ selectedPlayer.getPlayerData().getAssistants().getAmount();
			break;

		case VICTORY_POINTS:
			resourceAmount = selectedPlayer.getPlayerData().getVictoryPoints().getAmount();
			break;

		default:
			resourceAmount = 0;
		}
		return resourceAmount;
	}
	/**
	 * Returns the {@link MainActionsHandler}
	 * @return main action handler
	 */
	public MainActionsHandler getMainActionsHandler() {
		return mainActionsHandler;
	}
	/**
	 * Returns the {@link QuickActionsHandler}
	 * @return quick action handler
	 */
	public QuickActionsHandler getQuickActionsHandler() {
		return quickActionsHandler;
	}
	/**
	 * Returns the {@link MarketActionsHandler}
	 * @return market actions handler
	 */
	public MarketActionsHandler getMarketActionsHandler() {
		return marketActionsHandler;
	}
	/**
	 * Return the {@link PlayerChoicesHandler}
	 * @return player's choices handler
	 */
	public PlayerChoicesHandler getPlayerChoicesHandler() {
		return playerChoicesHandler;
	}
	/**
	 * Returns a list of available actions
	 * @return list of game's actions
	 */
	public List<GameAction> getAvailableActions() {
		return this.availableActions;
	}
	/**
	 * Sets up {@link MainActionsHandler}, {@link QuickActionsHandler},
	 * {@link MarketActionsHandler} and {@link PlayerChoicesHandler}
	 */
	public void setActionHandlers() {
		this.mainActionsHandler = new MainActionsHandler(player);
		this.quickActionsHandler = new QuickActionsHandler(player);
		this.marketActionsHandler = new MarketActionsHandler(player);
		this.playerChoicesHandler = new PlayerChoicesHandler(player);
	}

}
