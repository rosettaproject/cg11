package it.polimi.ingsw.cg11.server.model.exceptions.market;

import it.polimi.ingsw.cg11.server.model.market.MarketOffer;

/**
 * Exception thrown when the requested {@link MarketOffer} doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchOfferException extends Exception {

	private static final long serialVersionUID = -8671439407679004742L;

	/**
	 * Exception thrown when the requested {@link MarketOffer} doesn't exist.
	 */
	public NoSuchOfferException() {
		super();
	}
	/**
	 * Exception thrown when the requested {@link MarketOffer} doesn't exist.
	 * @param cause
	 */
	public NoSuchOfferException(Throwable cause) {
		super(cause);
	}
}
