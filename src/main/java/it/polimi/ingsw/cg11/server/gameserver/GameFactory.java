package it.polimi.ingsw.cg11.server.gameserver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.Messages;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.RandomStandardMapFactory;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;
import it.polimi.ingsw.cg11.server.model.player.resources.PoliticsHand;
import it.polimi.ingsw.cg11.server.model.utils.FileToJson;
import it.polimi.ingsw.cg11.server.model.utils.ResourcesPath;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.server.view.View;

/**
 * This class manages the waiting queue and creates a new game
 * 
 * @author Federico
 *
 */
public class GameFactory {

	private static final Logger LOG = Logger.getLogger(GameFactory.class.getName());

	private static final String CHANGE_SUFFIX = "_CHANGES";
	private static final String CHAT_TO_ALL = "_CHAT_TO_ALL";

	private static final String ALL_MAPS_PATH = "/src/main/resources/Maps.txt";
	private static final String DEFAULT_MAP_PATH = "/src/main/resources/CompleteMap1.txt";

	private static final int MIN_PLAYERS = 2;
	private static final int TIMER = 20000;

	private String randomMap;

	private int numberOfPlayers;
	private int suffixName;

	private List<Game> games;
	private List<View> playersView;

	private FileToJson fileToJson;
	private JSONObject mapConfig;
	private ExecutorService executor;

	private static GameFactory gameFactory;
	/**
	 * Constructor, invoked by getGameFactory method.
	 */
	private GameFactory() {
		games = new ArrayList<>();
		playersView = new ArrayList<>();
		fileToJson = new FileToJson();

		suffixName = 1;
		executor = Executors.newCachedThreadPool();
	}

	
	/**
	 * Returns the {@link GameFactory}. If it doesn't exist yet, it creates it.
	 * It's a singleton.
	 * @return {@link GameFactory}
	 */
	public static GameFactory getGameFactory() {
		if (gameFactory == null) {
			gameFactory = new GameFactory();
		}
		return gameFactory;
	}

	/**
	 * Add view to list of connected views
	 * 
	 * @param playerView
	 */
	public void addPlayerView(View playerView) {
		usernameCheck(playerView);
		if (playersView.size() == 1) {
			startTimer();
		}
		LOG.log(Level.INFO, "Adding player " + playerView.getClientIdentifier().getClientUsername() + " to queue\n");
		playersView.add(playerView);

	}
	/**
	 * This method is called when the first player is ready to play.
	 * It sets a timer. Before the timer expires other players will be able to connect to the same game.
	 * After that, it will be initialized a different game.
	 */
	private void startTimer() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					LOG.log(Level.INFO, "\nTimer started...\n");
					Thread.sleep(TIMER);
					LOG.log(Level.INFO, "\nTimer finished...\n");

					LOG.log(Level.INFO, "\nCreating game...\n");
					createAGame();
					LOG.log(Level.INFO, "\nReset queue...\n");
					playersView = new ArrayList<>();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					LOG.log(Level.SEVERE, "\n\nError during timern\n", e);
				}
			}
		});
	}
	/**
	 * Creates a game, with various operations:
	 * <ul><li> Sets player's username and gameName to playerData </li>
	 * <li> Sets playerID and game topic into player view</li>
	 * <li> Sends ID to player (client side)</li>
	 * <li> Sends updated username to player (useful in case of ambiguity)</li>
	 * <li> Creates topic for view </li>
	 * <li> Subscribes view to broker</li>
	 * <li> Subscribes game</li>
	 * </ul>
	 */
	private void createAGame() {
		Game game;
		GameMap gameMap;
		RandomStandardMapFactory mapFactory;
		Broker broker;
		PlayerData playerData;

		String gameTopic;
		String gameIdentifier;
		String gameChangeTopic;
		String gameChatTopic;
		String playerIDTopic;
		String playerUsernameTopic;

		String playerUsername;

		List<Token> playerTokens;
		View playerView;
		int playerID;
		int playerIndex;

		List<PlayerState> players;
		ListIterator<View> viewIterator;

		LOG.log(Level.INFO, "Creating game\n");
		numberOfPlayers = playersView.size();
		if (numberOfPlayers >= MIN_PLAYERS) {
			try {
				broker = Broker.getBroker();
				randomMap = chooseRandomMap();
				mapConfig = fileToJson.jsonObjectFromFile(randomMap);
				mapFactory = new RandomStandardMapFactory();
				gameIdentifier = Game.PREFIX + String.valueOf(Game.getCounter());

				gameMap = mapFactory.createMap(mapConfig, numberOfPlayers, gameIdentifier);
				game = new Game(gameMap);
				games.add(game);

				players = game.getGameMap().getPlayers();

				playerTokens = new ArrayList<>();

				viewIterator = playersView.listIterator();
				gameTopic = gameIdentifier;
				gameChangeTopic = gameTopic + CHANGE_SUFFIX;
				gameChatTopic = gameTopic + CHAT_TO_ALL;

				while (viewIterator.hasNext()) {
					playerIndex = viewIterator.nextIndex();
					playerData = players.get(playerIndex).getPlayerData();
					playerView = viewIterator.next();

					playerID = playerData.getPlayerID();
					playerUsername = playerView.getClientIdentifier().getClientUsername();

					// Sets player username and gameName to playerData
					playerData.setUsername(playerUsername);
					playerData.setCurrentGame(gameTopic);

					// Sets playerID and game topic into player view
					playerView.getClientIdentifier().setPlayerID(playerID);
					playerView.getClientIdentifier().setCurrentGame(gameTopic);
					playerTokens.add(playerView.getClientIdentifier());

					// Sends ID to player (client side)
					playerView.sendIDToPlayer(playerID);
					
					// Sends updated username to player (useful in case of ambiguity)
					playerView.setUpdatedUsername(playerUsername);

					// Creates topic for view
					playerIDTopic = gameTopic + "_" + playerView.getClientIdentifier().getPlayerID();
					playerUsernameTopic = gameTopic + "_" + playerView.getClientIdentifier().getClientUsername();

					// Subscribes view to broker
					broker.clientSubscribe(playerView, gameChatTopic);
					broker.clientSubscribe(playerView, gameChangeTopic);
					broker.clientSubscribe(playerView, playerIDTopic);
					broker.clientSubscribe(playerView, playerUsernameTopic);

					String log = "View subscribed to: \n" + playerIDTopic + ", " + gameChangeTopic + " and "
							+ playerUsernameTopic + " and " + gameChangeTopic + "\n\n";
					LOG.log(Level.INFO, log);
				}

				// Subscribes game
				broker.clientSubscribe(game, gameTopic);
				LOG.log(Level.INFO, "Game subscribed.\nTopic: " + gameTopic);

				sendInitialMessageToPlayers(playersView, gameTopic);
				sendMapToPlayers(mapFactory, gameIdentifier, playerTokens, gameChangeTopic);
				sendPoliticHandToPlayers(players, mapFactory, gameTopic);
				startFirstPlayerTimer(gameMap);

			} catch (FileNotFoundException e) {
				LOG.log(Level.SEVERE, "File not found", e);
			} catch (CannotCreateMapException e) {
				LOG.log(Level.SEVERE, "Can not create a game", e);
			}
		} else {
			LOG.log(Level.WARNING, "Can not create a game\n");
		}
	}
	/**
	 * Chooses a random map from a file with all the available maps
	 * @return file path of a random map
	 */
	private String chooseRandomMap() {
		String allMaps;
		String selectedMapPath;
		String defaultPath;
		String pomPath;
		
		FileReader fileReader = null;
		BufferedReader bufferedReader;

		int maxRange = 127;
		int randomNumber;
		int numberOfMaps;
		int lineCounter;

		pomPath = ResourcesPath.getPomPath();
		defaultPath = pomPath + DEFAULT_MAP_PATH;
		allMaps = pomPath + ALL_MAPS_PATH;
		try {
			fileReader = new FileReader(allMaps);
			bufferedReader = new BufferedReader(fileReader);
			numberOfMaps = Integer.parseInt(bufferedReader.readLine());
			randomNumber = new Random().nextInt(maxRange) % numberOfMaps;
			
			lineCounter = 0;
			while ((selectedMapPath = bufferedReader.readLine()) != null) {
				if (lineCounter == randomNumber) {
					fileReader.close();
					bufferedReader.close();
					LOG.log(Level.INFO, "Loading map "+lineCounter);
					selectedMapPath = pomPath + selectedMapPath;
					return selectedMapPath;
				}
				lineCounter += 1;
			}
			fileReader.close();
			bufferedReader.close();
		} catch (NumberFormatException | IOException e) {
			LOG.log(Level.FINE, "Can not choose that map. Loading the default one\n", e);
			if(fileReader != null)
				try {
					fileReader.close();
				} catch (IOException e1) {
					LOG.log(Level.SEVERE, "\n\nError during closing fileReader", e1);
				}
			return defaultPath;
		} 
		LOG.log(Level.INFO, "Loading map 1");
		return defaultPath;
	}
	/**
	 * Starts the turn's timer for the first player
	 * @param gameMap
	 */
	private void startFirstPlayerTimer(GameMap gameMap) {
		TurnManager turnManager = gameMap.getTurnManager();
		turnManager.startTimer();
	}
	/**
	 * Sends to everyone a message with the list of all players' usernames 
	 * @param players
	 * @param gameName
	 */
	private void sendInitialMessageToPlayers(List<View> players, String gameName) {
		Iterator<View> playerIterator = players.iterator();
		View tmpPlayer;

		String message = "\nPlayers:\n\n";

		while (playerIterator.hasNext()) {
			tmpPlayer = playerIterator.next();
			message += "- " + tmpPlayer.getClientIdentifier().getClientUsername() + "\n";
		}

		Messages.sendToAllPlayers(message, gameName);
	}
	/**
	 * Sends {@link PoliticsHand} to the players
	 * @param players
	 * @param mapFactory
	 * @param gameTopic
	 */
	private void sendPoliticHandToPlayers(List<PlayerState> players, RandomStandardMapFactory mapFactory,
			String gameTopic) {
		ListIterator<View> viewIterator;
		Broker broker;
		ModelChanges playerHand;
		int index;
		int playerID;

		viewIterator = playersView.listIterator();
		broker = Broker.getBroker();
		while (viewIterator.hasNext()) {
			index = viewIterator.nextIndex();
			playerID = players.get(index).getPlayerData().getPlayerID();
			playerHand = getInitialHand(playerID, mapFactory);
			broker.publish(playerHand, gameTopic + '_' + playerID);
			viewIterator.next();
		}
	}
	/**
	 * Sends the initial {@link GameMap} to the player
	 * @param mapFactory
	 * @param gameIdentifier
	 * @param playerTokens
	 * @param gameChangeTopic
	 */
	private void sendMapToPlayers(RandomStandardMapFactory mapFactory, String gameIdentifier, List<Token> playerTokens,
			String gameChangeTopic) {
		SerializableMapStartupConfig initialMap;
		Broker broker;

		broker = Broker.getBroker();
		initialMap = getInitialMap(mapFactory, gameIdentifier);
		initialMap.addTokenList(playerTokens);
		broker.publish(initialMap, gameChangeTopic);
		LOG.log(Level.INFO, "Initial map sent");
	}
	/**
	 * Returns the initial map as {@link SerializableMapStartupConfig}
	 * @param mapFactory
	 * @param gameIdentifier
	 * @return {@link SerializableMapStartupConfig}
	 */
	private SerializableMapStartupConfig getInitialMap(RandomStandardMapFactory mapFactory, String gameIdentifier) {
		SerializableMapStartupConfig initialMap = new SerializableMapStartupConfig();

		initialMap.setJsonConfigAsString(mapFactory.getMapStartupConfig());
		initialMap.setTopic(gameIdentifier);
		return initialMap;
	}
	/**
	 * Returns the initial hand of a player, passed as parameter
	 * @param playerID
	 * @param mapFactory
	 * @return model changes
	 */
	private ModelChanges getInitialHand(int playerID, RandomStandardMapFactory mapFactory) {
		ModelChanges initialHand;
		initialHand = mapFactory.getStartingHands().get(playerID);
		return initialHand;
	}
	/**
	 * Checks and manages the presence of players with the same username
	 * @param playerView
	 */
	private void usernameCheck(View playerView) {
		Iterator<View> viewIterator = playersView.iterator();
		View tmpView;
		String playerUsername = playerView.getClientIdentifier().getClientUsername();
		String tmpUsername;

		while (viewIterator.hasNext()) {
			tmpView = viewIterator.next();
			tmpUsername = tmpView.getClientIdentifier().getClientUsername();
			if (playerUsername.equals(tmpUsername)) {
				playerView.getClientIdentifier().setClientUsername(playerUsername + "_" + suffixName);
				suffixName += 1;
				return;
			}
		}
	}

}
