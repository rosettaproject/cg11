package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.NobilityPath;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;

/**
 * Implements a transaction in which the player gets {@link NobilityPoints}
 * 
 * @author francesco
 *
 */
public class NobilityTransaction implements Transaction {


	private int amount;
	private NobilityPath nobilityPath;
	/**
	 * 
	 * @param amount
	 * @param gameMap
	 */
	public NobilityTransaction(int amount, GameMap gameMap) {

		this.amount = amount;
		this.nobilityPath = gameMap.getNobilityPath();

		if (this.amount < 0)
			throw new IllegalArgumentException();
	}

	@Override
	public int getAmount() {
		return this.amount;
	}

	/**
	 * Lets the player gain the set amount of nobility points and acquire the
	 * corresponding bonus
	 * 
	 * @param player
	 * @throws TransactionException
	 */
	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {
	
		try {
			transactionSubject.getPlayerData().getNobilityPoints().gain(amount);
		} catch (IllegalResourceException e) {
			throw new TransactionException(e);
		}

	}
	/**
	 * Returns the nobility {@link Bonus} at the specified index
	 * @param nobilityIndex
	 * @return bonus at the specified index
	 */
	public Bonus getNobilityBonus(int nobilityIndex){
		return nobilityPath.getBonusList().get(nobilityIndex);
	}
}