package it.polimi.ingsw.cg11.server.view.rmi;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.view.connections.ClientView;
import it.polimi.ingsw.cg11.messages.DisconnectedPlayerMessage;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.server.view.View;

/**
 * This object allows client-server communication using a client view stub to
 * call the dispatch() remote method in order to deliver a message to the
 * client, and allows the client's own view to call the publish() method using a
 * {@link RemoteFactoryImpl}
 * 
 * @author francesco
 *
 */
public class RMIView implements View {

	private static final Logger LOG = Logger.getLogger(RMIView.class.getName());

	private static final String IN_RMI_VIEW = " in RMI client\n";

	private ClientView clientRMIView;
	private final Token token;
	private RemoteFactoryImpl remoteFactory;
	private boolean errorOccurred;

	public RMIView(ClientView clientViewStub, Token clientIdentifier, RemoteFactoryImpl remoteFactoryImpl)
			throws RemoteException {
		this.clientRMIView = clientViewStub;
		this.token = clientIdentifier;
		this.remoteFactory = remoteFactoryImpl;

		errorOccurred = false;

	}

	/**
	 * This method send a message to the client. The message will be received
	 * from {@link ClientRMIView}
	 * 
	 * @param message
	 * @throws RemoteException
	 */
	public void sendToClient(Visitable message) {
		String playerName = token.getClientUsername();
		try {
			if (!errorOccurred) {
				clientRMIView.readMessageFromServer(message);
				LOG.log(Level.INFO, "Sent message from server to " + playerName + "\n" + message.toString() + "\n");
			}
		} catch (RemoteException e) { // NOSONAR
			errorOccurred = true;
			LOG.info("\nCan not send message to RMI client\nRemoving player...\n\n");
			disconnectPlayer();
			remoteFactory.deleteRMIView(token);
		}
	}

	@Override
	public void dispatchMessage(Visitable message) {
		sendToClient(message);
	}

	@Override
	public Token getClientIdentifier() {
		return token;
	}

	@Override
	public void publish(Visitable message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

	@Override
	public String getSubscriberIdentifier() {
		return token.getClientUsername();
	}

	@Override
	public void sendIDToPlayer(int playerID) {
		try {
			clientRMIView.setPlayerID(playerID);
			LOG.log(Level.INFO, "\nSetup correctly player ID " + playerID + IN_RMI_VIEW);
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, "\nCan not set player ID " + playerID + IN_RMI_VIEW, e);
		}
	}

	/**
	 * This method is used when it is going to interrupt the communication with
	 * the client
	 */
	private void disconnectPlayer() {
		DisconnectedPlayerMessage disconnectedPlayerMessage;
		String topic;

		topic = token.getCurrentGame();

		disconnectedPlayerMessage = new DisconnectedPlayerMessage();
		disconnectedPlayerMessage.setPlayerToken(token);
		disconnectedPlayerMessage.setTopic(topic);

		publish(disconnectedPlayerMessage, topic);

	}

	@Override
	public Token getToken() {
		return token;
	}

	@Override
	public void setUpdatedUsername(String username) {
		try {
			clientRMIView.setUpdatedUsername(username);
			LOG.log(Level.INFO, "\nSetup correctly updated username " + username + IN_RMI_VIEW);
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, "\nCan not set updated username " + username + IN_RMI_VIEW, e);
		}
	}
}
