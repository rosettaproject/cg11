package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link ChooseExtraPermitBonus}
 * @author paolasanfilippo
 *
 */
public class ChooseExtraPermitBonusFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		
		PlayerState player;
		TurnManager turnManager;
		PermitCard permitCard;
		int permitIndex;
		int usedPermitIndex;
		
		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			turnManager = gameMap.getTurnManager();
			permitCard = createPermitCardFromActionData(actionData,gameMap,"region","permitcard");
			
			permitIndex = player.getPlayerData().getPermitsHand().indexOf(permitCard);
			usedPermitIndex = player.getPlayerData().getUsedPermits().indexOf(permitCard);
			
			if(permitIndex<0 && usedPermitIndex<0)
				throw new NoSuchPermitCardException();
			else{
				if(usedPermitIndex>=0)
					permitCard = player.getPlayerData().getUsedPermits().getCard(usedPermitIndex);
				else
					permitCard = player.getPlayerData().getPermitsHand().getCard(permitIndex);
			}
			return new ChooseExtraPermitBonus(player,turnManager,permitCard, gameMap.getGameTopic());
				
		} catch (NoSuchPlayerException | NoSuchRegionException | NoSuchPermitCardException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
