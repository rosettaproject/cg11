package it.polimi.ingsw.cg11.server.model.controls.bonus;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.IllegalBonusTypeException;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
/**
 * Contains some controls on bonuses
 * @author paolasanfilippo
 *
 */
public class BonusControls {
	/**
	 * Controls if the {@link Bonus}, passed as a parameter, increases the nobility points.
	 * If the nobility points of the bonus are more than 0, it throws a {@link IllegalBonusTypeException}
	 * @param bonus : bonus that has to be checked. Cannot be null
	 * @throws IllegalBonusTypeException
	 */
	public void isNotNobilityBonus(Bonus bonus) throws IllegalBonusTypeException{
		if(bonus.getNobilityPointsAmount() != 0)
			throw new IllegalBonusTypeException();
	}
}
