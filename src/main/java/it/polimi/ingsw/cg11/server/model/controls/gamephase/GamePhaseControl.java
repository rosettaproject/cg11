package it.polimi.ingsw.cg11.server.model.controls.gamephase;

import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;

/**
 * Controls on {@link GamePhase}.
 * 
 * @author paolasanfilippo
 *
 */
public class GamePhaseControl {
	/**
	 * Checks if the actual {@link GamePhase} is the expected one. If it's not,
	 * it throws a {@link NotCorrectGamePhaseException}.
	 * 
	 * @param turnManager
	 * @param expectedGamePhase
	 *            :the expected game phase
	 * @throws NotCorrectGamePhaseException
	 */
	public void controlGamePhase(TurnManager turnManager, GamePhase expectedGamePhase)
			throws NotCorrectGamePhaseException {
		String currentPhase;
		currentPhase = turnManager.getCurrentGamePhase().getPhaseName();
		if (!turnManager.getCurrentGamePhase().equals(expectedGamePhase)) {
			throw new NotCorrectGamePhaseException("." + currentPhase + "Exception");
		}
	}
}
