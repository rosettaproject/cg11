package it.polimi.ingsw.cg11.server.model.mapcomponents.other;

import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * Represents the "nobility path" of the game, as a list of bonuses
 * 
 * @author francesco
 *
 */

public class NobilityPath {

	private List<Bonus> bonusList;

	/**
	 * Nobility's path constructor
	 * 
	 * @param bonusList
	 */
	public NobilityPath(List<Bonus> bonusList) {
		this.bonusList = bonusList;
	}

	/**
	 * Returns the bonuses of the nobility path
	 * 
	 * @return list of bonuses
	 */
	public List<Bonus> getBonusList() {
		return bonusList;
	}

	@Override
	public String toString() {
		String toString = "\nNobility Path\n";
		Iterator<Bonus> bonusIterator = bonusList.iterator();
		Bonus bonus;
		int counter = 1;

		while (bonusIterator.hasNext()) {
			bonus = bonusIterator.next();

			toString += "\n" + counter + ")";
			toString += bonus.toString() + "\n";

			counter += 1;
		}
		
		return toString;
	}

}