/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

/**
 * @author Federico
 *
 */
public class NotAvailableQuickActionException extends Exception {

	private static final long serialVersionUID = -4268708614723931450L;

}
