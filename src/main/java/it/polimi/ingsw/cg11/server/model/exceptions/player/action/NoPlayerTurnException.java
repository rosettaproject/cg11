package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

/**
 * Exception thrown when it isn't the turn of that player.
 * @author paolasanfilippo
 *
 */
public class NoPlayerTurnException extends Exception {

	private static final long serialVersionUID = -2935672772988152249L;

	/**
	 * Exception thrown when it isn't the turn of that player.
	 */
	public NoPlayerTurnException() {
		super();
	}
	/**
	 * Exception thrown when it isn't the turn of that player.
	 * @param message
	 */
	public NoPlayerTurnException(String message) {
		super(message);
	}
}
