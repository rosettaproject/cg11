package it.polimi.ingsw.cg11.server.model.market;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for the {@link MarketOffer}
 * @author paolasanfilippo
 *
 */
public class MarketOfferFactory {

	private MarketOffer marketOffer;

	/**
	 * Creates a {@link AssistantsOffer} from the offering player, the amount of assistants and the cost
	 * @param offeringPlayer
	 * @param offeredObject
	 * @param offerCost
	 * @return the market offer
	 */
	public MarketOffer createMarketOffer(PlayerState offeringPlayer, int offeredObject, int offerCost) {

		marketOffer = new AssistantsOffer(offeringPlayer, offeredObject, offerCost);
		return marketOffer;
	}
	/**
	 * Creates a {@link PoliticCardOffer} from the offering player, the {@link PoliticCard} offered and the cost
	 * @param offeringPlayer
	 * @param offeredObject
	 * @param offerCost
	 * @return the market offer
	 */
	public MarketOffer createMarketOffer(PlayerState offeringPlayer, PoliticCard offeredObject,
			int offerCost) {

		marketOffer = new PoliticCardOffer(offeringPlayer, offeredObject, offerCost);
		return marketOffer;
	}
	/**
	 * Creates a {@link PermitCardOffer} from the offering player, the {@link PermitCard} offered and the cost
	 * @param offeringPlayer
	 * @param offeredObject
	 * @param offerCost
	 * @return the market offer created
	 */
	public MarketOffer createMarketOffer(PlayerState offeringPlayer, PermitCard offeredObject,
			int offerCost) {

		marketOffer = new PermitCardOffer(offeringPlayer, offeredObject, offerCost);
		return marketOffer;
	}

}
