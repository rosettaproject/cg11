package it.polimi.ingsw.cg11.server.model.cards;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * A specific type of {@link Card}, that has a {@link Bonus} and a list of
 * {@link City}
 * 
 * @author francesco
 *
 */
public class PermitCard implements Card {

	private Bonus permitBonus;
	private List<City> cities;

	/**
	 * A specific type of {@link Card}, that has a {@link Bonus} and a list of
	 * {@link City}
	 * 
	 * @param permitBonus:
	 *            {@link Bonus} that will be part of the permit card
	 * @param cities:
	 *            List of cities on which the player can build an
	 *            {@link Emporium}
	 */
	public PermitCard(Bonus permitBonus, List<City> cities) {
		this.permitBonus = permitBonus;
		this.cities = cities;
	}

	/**
	 * Gets the {@link Bonus} in the permit card
	 * @return {@link Bonus} 
	 */
	public Bonus getPermitBonus() {
		return this.permitBonus;

	}

	/**
	 * Returns the list of cities of the permit card, where the player can build an {@link Emporium}
	 * 
	 * @return cities: A list of cities 
	 */
	public List<City> getCities() {
		return cities;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		int i;
		String permitToString;

		permitToString = "PermitCard";
		if (permitBonus != null)
			permitToString = permitToString + permitBonus.toString() + "\ncities:";
		else
			permitToString = permitToString + "null" + "\ncities:";

		if (cities != null)
			for (i = 0; i < cities.size(); i++)
				permitToString = permitToString.concat(" " + cities.get(i).getName());

		return permitToString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cities == null) ? 0 : cities.hashCode());
		result = prime * result + ((permitBonus == null) ? 0 : permitBonus.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermitCard other = (PermitCard) obj;
		if (cities == null) {
			if (other.cities != null)
				return false;
		} else if (!other.cities.containsAll(cities) || !cities.containsAll(other.cities))
			return false;
		if (permitBonus == null) {
			if (other.permitBonus != null)
				return false;
		} else if (!permitBonus.equals(other.permitBonus))
			return false;
		return true;
	}

}