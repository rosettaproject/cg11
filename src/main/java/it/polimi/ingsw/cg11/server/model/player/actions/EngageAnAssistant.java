package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughMoneyException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the EngageAnAssistant action and tries to execute
 * it. EngageAnAssistant is a quick action that permits to engage an assistant
 * 
 * @author Federico
 *
 */
public class EngageAnAssistant extends Performable {

	private static final int NEEDED_MONEY_AMOUNT = 3;
	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);
	private PlayerState player;
	private TurnManager turnManager;
	private String gameTopic;

	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param gameTopic
	 */
	public EngageAnAssistant(PlayerState player, TurnManager turnManager, String gameTopic) {
		this.player = player;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			playerResourcesControls.enoughMoneyControl(player, NEEDED_MONEY_AMOUNT);

		} catch (NoPlayerTurnException | NoEnoughMoneyException | NotCorrectGamePhaseException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getQuickActionsHandler().hireOneAssistant();
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);

		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}