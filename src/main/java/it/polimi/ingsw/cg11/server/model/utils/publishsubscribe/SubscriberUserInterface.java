package it.polimi.ingsw.cg11.server.model.utils.publishsubscribe;

import java.rmi.Remote;

import it.polimi.ingsw.cg11.server.view.Token;

/**
 * Implement this interface to allow a class to subscribe to a topic and receive published messages related to that topic 
 * @author francesco
 *
 * @param <M>
 */
public interface SubscriberUserInterface<M> extends Remote {

	/**
	 * Allows the subscriber to receive a message
	 * @param message
	 */
	public void dispatchMessage(M message);
	/**
	 * Returns the Subcriber's unique identifier
	 * @return identifier
	 */
	public String getSubscriberIdentifier();
	/**
	 * Returns the token. It's a default method.
	 * @return token
	 */
	public default Token getToken(){
		return null;
	}
}
