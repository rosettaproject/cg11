package it.polimi.ingsw.cg11.server.model.player.actionspool;

import java.util.List;
import java.util.ListIterator;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermit;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;

/**
 * Implements method to realize the choices made by a player in terms of
 * selecting a {@link Bonus} or {@link PermitCard}
 * 
 * @author francesco
 *
 */
public class PlayerChoicesHandler {
	
	public static final String ACTION_KEY = "Action";
	public static final String ACTOR_ID_KEY = "ActorID";
	public static final String EXTRA_BONUS_TYPE = "ExtraBonus";
	public static final String EXTRA_PERMIT_TYPE = "ExtraPermit";
	public static final String EXTRA_PERMIT_BONUS_TYPE = "ExtraPermitBonus";
	public static final String ACTOR_KEY = "ActorID";
	public static final String ACQUIRED_PERMIT_KEY = "AcquiredPermit";
	public static final String REGION_NAME_KEY = "Region";

	private PlayerState player;
	private PlayerChoice playerChoice;
	private List<GameAction> availableActions;
	/**
	 * 
	 * @param player
	 */
	public PlayerChoicesHandler(PlayerState player) {
		this.player = player;
		this.availableActions = player.getPlayerActionExecutor().getAvailableActions();
	}

	/**
	 * Allows the player to obtain an extra bonus of his choice among the city
	 * bonuses of cities where he previously built an emporium
	 * 
	 * @param availableActions
	 * @param bonus
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges chooseAnExtraBonus(Bonus bonus) throws IllegalStateChangeException{
		ModelChanges modelChanges;
		playerChoice = new PlayerChoice(EXTRA_BONUS_TYPE);
		
		try {
			modelChanges = new ModelChanges();
			// Removes the choice from the list of available actions
			extractAvailableChoice(availableActions, playerChoice);
			
			modelChanges.addToChanges(
					bonus.obtain(player));
			
			modelChanges.addToChanges(ACTOR_ID_KEY, player.getPlayerData().getPlayerID());
			modelChanges.addToChanges(ACTION_KEY,"ChooseAnExtraBonus");
			
			return modelChanges;
			
		} catch (TransactionException e) {
			throw new IllegalStateChangeException(e);
		}

	}

	/**
	 * Lets the player obtain a permit bonus from a previously bought permit
	 * card
	 * 
	 * @param availableActions
	 * @param permitCard
	 * @return 
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges chooseAnExtraPermitBonus(PermitCard permitCard) throws IllegalStateChangeException{
		ModelChanges modelChanges;
		playerChoice = new PlayerChoice(EXTRA_PERMIT_BONUS_TYPE);

		try {
			modelChanges = new ModelChanges();
			// Remove the choice from the list of available actions
			extractAvailableChoice(availableActions, playerChoice);
			
			modelChanges.addToChanges(
					permitCard.getPermitBonus().obtain(player));
			
			modelChanges.addToChanges(ACTOR_ID_KEY, player.getPlayerData().getPlayerID());
			modelChanges.addToChanges(ACTION_KEY,"ChooseAnExtraPermitBonus");
			
			return modelChanges;
			
		} catch (TransactionException e) {
			throw new IllegalStateChangeException(e);
		}
	}
	/**
	 * Lets the player obtain an extra {@link PermitCard} from the {@link FaceUpPermits}
	 * @param permitCard
	 * @param region
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges chooseAnExtraPermitCard(PermitCard permitCard,Region region) throws IllegalStateChangeException{
		ModelChanges modelChanges;
		playerChoice = new PlayerChoice(EXTRA_PERMIT_TYPE);
		
		FaceUpPermits faceUpPermits = region.getFaceUpPermitCards();

		try {
			modelChanges = new ModelChanges();
			// Remove the choice from the list of available actions
			extractAvailableChoice(availableActions, playerChoice);
			
			modelChanges.addToChanges(
					permitCard.getPermitBonus().obtain(player));
			
			faceUpPermits.removeCard(permitCard);
			player.getPlayerData().addToPermitsHand(permitCard);
			
			addChooseAnExtraPermitCardChanges(modelChanges,permitCard,region);
			
			return modelChanges;
			
		} catch (TransactionException | NoSuchPermitCardException e) {
			throw new IllegalStateChangeException(e);
		}
	}

	/**
	 * Adds {@link ChooseExtraPermit} to the changes
	 * @param modelChanges
	 * @param permitCard with the key "AcquiredPermit"
	 * @param region with the key "Region"
	 */
	private void addChooseAnExtraPermitCardChanges(ModelChanges modelChanges, PermitCard permitCard, Region region) {
	
		modelChanges.addToChanges(ACTION_KEY,"ChooseAnExtraPermitCard");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());	
		modelChanges.addToChanges(ACQUIRED_PERMIT_KEY,permitCard);
		modelChanges.addToChanges(REGION_NAME_KEY, region.getName());		
	}

	/**
	 * Removes the player's choice, passed as parameter, from the player's available actions
	 * @param availableActions
	 * @param playerChoice
	 * @throws IllegalStateChangeException
	 */
	public void extractAvailableChoice(List<GameAction> availableActions, PlayerChoice playerChoice)
			throws IllegalStateChangeException {

		ListIterator<GameAction> actionsIterator = availableActions.listIterator();
		GameAction availableChoice;

		while (actionsIterator.hasNext()) {
			availableChoice = actionsIterator.next();
			if (availableChoice.getActionType().equals(playerChoice.getActionType())) {
				availableActions.remove(availableChoice);
				return;
			}
		}
		
		throw new IllegalStateChangeException(".CanNotPerform" + playerChoice.getActionType() + "Exception");
	}
	/**
	 * Returns the kind of the extraBonus
	 * @return name of the extra bonus
	 */
	public static String getExtraBonusType() {
		return EXTRA_BONUS_TYPE;
	}
	/**
	 * Returns the kind of the extraPermit
	 * @return name of the extra permit
	 */
	public static String getExtraPermitType() {
		return EXTRA_PERMIT_TYPE;
	}
	/**
	 * Returns the kind of the extraPermitBonus
	 * @return name of the extra permit bonus
	 */
	public static String getExtraPermitBonusType() {
		return EXTRA_PERMIT_BONUS_TYPE;
		}
}
