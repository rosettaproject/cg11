package it.polimi.ingsw.cg11.server.model.exceptions.utils;

/**
 * Exception thrown when a transaction doesn't end correctly.
 * @author paolasanfilippo
 *
 */
public class TransactionException extends Exception {

	private static final long serialVersionUID = -7764157804332923210L;

	/**
	 * Exception thrown when a transaction doesn't end correctly.
	 */
	public TransactionException() {
		super();
	}
	/**
	 * Exception thrown when a transaction doesn't end correctly.
	 * @param cause
	 */
	public TransactionException(Throwable cause) {
		super(cause);
	}

}
