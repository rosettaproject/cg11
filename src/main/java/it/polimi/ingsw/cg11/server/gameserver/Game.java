package it.polimi.ingsw.cg11.server.gameserver;

import java.util.logging.Level;
import java.util.logging.Logger;


import it.polimi.ingsw.cg11.server.control.Controller;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.PublisherInterface;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.SubscriberUserInterface;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;

/**
 * This class represents a game. It contains a model and a controller
 * 
 * @author Federico
 *
 */
public class Game implements SubscriberUserInterface<Visitable>, PublisherInterface<Visitable, String> {

	private static final Logger LOG = Logger.getLogger(Game.class.getName());
	
	private static int counter = 0;
	public static final String PREFIX = "game_";

	private String gameIdentifier;
	private Controller controller;
	private GameMap gameMap;


	/**
	 * Instantiates a map and a controller
	 * 
	 * @param gameMap
	 * @throws CannotCreateMapException
	 */
	public Game(GameMap gameMap)
			throws CannotCreateMapException {
		
		this.gameMap = gameMap;
		this.gameIdentifier = PREFIX+String.valueOf(counter);
		controller = new Controller(getGameMap());
		counter += 1;
	}

	/**
	 * Calls dispatchMessage method of {@link Controller} class
	 * 
	 * @param message
	 */
	@Override
	public void dispatchMessage(Visitable message) {
		String messageToLog = gameIdentifier + " received this message from broker:\n" + 
				message.toString() + "Topic of message: " + message.getTopic();
		
		LOG.log(Level.INFO, messageToLog);
		
		controller.dispatchMessage(message);
	}

	/**
	 * Calls publish method of {@link Controller} class
	 * 
	 * @param message
	 * @param topic
	 */
	@Override
	public void publish(Visitable message, String topic) {
		controller.publish(message, topic);
	}

	/**
	 * Returns the identifier of the game
	 * @return identifier of the game
	 */
	public String getGameIdentifier() {
		return gameIdentifier;
	}

	/**
	 * Returns the game map
	 * @return the gameMap
	 */
	public GameMap getGameMap() {
		return gameMap;
	}


	@Override
	public String getSubscriberIdentifier() {
		return gameIdentifier;
	}
	/**
	 * Returns the counter of the initialized games
	 * @return counter
	 */
	public static int getCounter(){
		return counter;
	}

}
