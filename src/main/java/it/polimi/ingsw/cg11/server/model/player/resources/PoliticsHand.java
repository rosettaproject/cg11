package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerActionExecutor;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Represents a hand of {@link PoliticCard}
 * 
 * @author francesco
 * @see Hand
 *
 */
public class PoliticsHand extends Hand<PoliticCard> {

	private static final int STARTING_CARDS = 6;
	private static final Logger LOG = Logger.getLogger(PoliticsHand.class.getName());
	private Deck<PoliticCard> politicsDeck;
	private Deck<PoliticCard> secondDeck;

	/**
	 * initializes the reference to the {@link PoliticCardsDeck} on the map
	 * 
	 * @param politicsDeck
	 * @param secondDeck 
	 * @param playerID
	 * @see {@link Deck}
	 */
	public PoliticsHand(Deck<PoliticCard> politicsDeck, Deck<PoliticCard> secondDeck, int playerID) {
		super();
		this.politicsDeck = politicsDeck;
		this.secondDeck = secondDeck;
		int cardNumber;
		int startingCards = STARTING_CARDS;
		// the first player gets an extra card
		if (playerID == 0)
			startingCards++;

		for (cardNumber = 0; cardNumber < startingCards; cardNumber++) {
			try {
				draw();
			} catch (EmptyPoliticDeckException e) {
				LOG.log(Level.INFO, "Configuration error", e);
			}
		}
	}

	/**
	 * Draws a card from politicDeck and adds it to player's (politic) hand
	 * @return model changes
	 * @throws EmptyPoliticDeckException
	 */
	public ModelChanges draw() throws EmptyPoliticDeckException {

		ModelChanges modelChanges = new ModelChanges();
		PoliticCard card;

		try {
			card = politicsDeck.draw();
			addToHand(card);
			modelChanges.addToChanges(PlayerActionExecutor.POLITIC_CARD, card);

			return modelChanges;

		} catch (EmptyDeckException | ColorNotFoundException e) {

			LOG.log(Level.FINE, "Empty politic deck", e);
			try {
				while (!secondDeck.getCards().isEmpty()) {
					politicsDeck.addToDeck(secondDeck.draw());
				}
				politicsDeck.shuffle();

				card = politicsDeck.draw();
				addToHand(card);
				modelChanges.addToChanges(PlayerActionExecutor.POLITIC_CARD, card);

				return modelChanges;

			} catch (EmptyDeckException | ColorNotFoundException e1) {
				modelChanges.addToChanges(PlayerActionExecutor.EMPTY_DECK_MESSAGE, "The deck is empty!");
				throw new EmptyPoliticDeckException(e1);
			}
		}
	}

	/**
	 * Discards from the hand a list of politic cards passed as parameter
	 * @param politicCards
	 * @throws NoSuchPoliticCardException
	 */
	public void discard(List<PoliticCard> politicCards) throws NoSuchPoliticCardException {
		List<PoliticCard> playerPoliticCards = getCards();
		Iterator<PoliticCard> iterator = politicCards.iterator();
		int politicCardToRemoveIndex;
		PoliticCard politicCardToDiscard;

		while (iterator.hasNext()) {
			politicCardToDiscard = iterator.next();
			politicCardToRemoveIndex = playerPoliticCards.indexOf(politicCardToDiscard);
			if (politicCardToRemoveIndex < 0) {
				throw new NoSuchPoliticCardException();
			}
			secondDeck.addToDeck(playerPoliticCards.remove(politicCardToRemoveIndex));
		}
	}
	/**
	 * Removes a politic card from the hand
	 * @param politicCard
	 * @return politic card removed
	 * @throws NoSuchPoliticCardException
	 */
	public PoliticCard removeCard(PoliticCard politicCard) throws NoSuchPoliticCardException {
		if (getCards().indexOf(politicCard) >= 0)
			return getCards().remove(getCards().indexOf(politicCard));
		else
			throw new NoSuchPoliticCardException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString;
		Iterator<PoliticCard> iterator = getCards().iterator();

		toString = "PoliticsHand \n";
		while (iterator.hasNext())
			try {
				toString = toString + "PoliticCard = " + ColorMap.getString(iterator.next().getColor()) + "\n";
			} catch (ColorNotFoundException e) {
				LOG.log(Level.INFO, "Color not found", e);
			}

		return toString;
	}

}