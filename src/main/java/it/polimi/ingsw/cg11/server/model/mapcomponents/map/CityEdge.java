package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import org.jgrapht.EdgeFactory;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;

/**
 * This class represent an edge of the graphed map. The edge is not oriented.
 * 
 * @author Federico
 *
 */
public class CityEdge implements EdgeFactory<City, CityEdge> {

	private City sourceCity;
	private City targetCity;

	/**
	 * This constructor is used only to create an object used like city edge
	 * factory
	 */
	public CityEdge() {

		/*
		 * Empty because of this class represents both an edge of graph and an
		 * edge factory. This constructor is used only to initialize the the
		 * edge factory of graphedMap. On the other hand, the parametric
		 * constructor is used to create a CityEdge object.
		 */

	}

	/**
	 * @param sourceCity : starting city
	 * @param destinationCity : destination city
	 */
	private CityEdge(City sourceCity, City destinationCity) {
		this.sourceCity = sourceCity;
		this.targetCity = destinationCity;
	}

	/**
	 * Returns the starting city
	 * @return the starting city
	 */
	public City getSource() {
		return sourceCity;
	}

	/**
	 * Returns the destination city
	 * @return the destination city
	 */
	public City getTarget() {
		return targetCity;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CityEdge [sourceCity=" + sourceCity.getName() + ", targetCity=" + targetCity.getName() + "]";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jgrapht.EdgeFactory.createEdge
	 */
	@Override
	public CityEdge createEdge(City sourceVertex, City targetVertex) {
		return new CityEdge(sourceVertex, targetVertex);
	}
}
