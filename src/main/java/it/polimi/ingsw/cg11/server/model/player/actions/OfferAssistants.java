package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughAssistantsException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
/**
 * Represents the action of offering an amount of {@link Assistants}, during the market game phase.
 * @author paolasanfilippo
 *
 */
public class OfferAssistants extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("MarketOfferingPhase", false);
	private PlayerState player;
	private TurnManager turnManager;
	private MarketOffers marketOffers;
	private int assistantsToOffer;
	private int offerCost;
	private String gameTopic;
	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param marketOffers
	 * @param assistantsToOffer
	 * @param offerCost
	 * @param gameTopic
	 */
	public OfferAssistants(PlayerState player, TurnManager turnManager, MarketOffers marketOffers,
			int assistantsToOffer, int offerCost, String gameTopic) {
		this.player = player;
		this.marketOffers = marketOffers;
		this.assistantsToOffer = assistantsToOffer;
		this.offerCost = offerCost;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		modelChanges = player.getPlayerActionExecutor().getMarketActionsHandler().offerAssistants(marketOffers,
				assistantsToOffer, offerCost);
		modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
		publish(modelChanges, modelChanges.getTopic());
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			playerResourcesControls.enoughAssistantControl(player, assistantsToOffer);
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoEnoughAssistantsException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}