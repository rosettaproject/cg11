package it.polimi.ingsw.cg11.server.model.player.resources;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;

/**
 * Used to store a player's nobility points. The amount is stored
 * as an integer and it can be accessed or increased, but cannot be decreased.
 * Doesn't guarantee that the operation performed is legal
 * 
 * @author francesco
 */
public class NobilityPoints {

	private int amount;
	private static final int MAXIMUM_AMOUNT = 20;

	/**
	 * Increases amount of nobility points by gainedAmount
	 * 
	 * @param gainedAmount
	 * @throws IllegalResourceException 
	 */
	public void gain(int gainedAmount) throws IllegalResourceException {
		if (amount + gainedAmount >= MAXIMUM_AMOUNT) {
			amount = MAXIMUM_AMOUNT;
		} else {
			try{
				if(gainedAmount<0)
					throw new IllegalArgumentException("gained amount can't be negative");
				this.amount += gainedAmount;	
			} catch(IllegalArgumentException e) {
				throw new IllegalResourceException(e);
			}
		}
	}

	/**
	 * Returns the maximum amount of nobility points
	 * @return the maximumAmount
	 */
	public static int getMaximumAmount() {
		return MAXIMUM_AMOUNT;
	}

	/**
	 * Return amount of nobility point
	 * 
	 * @return amount of nobility point
	 */
	public int getAmount() {
		return this.amount;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NobilityPoints [amount=" + amount + "]";
	}
	
}
