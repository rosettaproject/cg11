package it.polimi.ingsw.cg11.server.gameserver;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.view.rmi.RemoteFactory;
import it.polimi.ingsw.cg11.server.view.rmi.RemoteFactoryImpl;
import it.polimi.ingsw.cg11.server.view.socket.SocketServer;

/**
 * This class manages connections. It can start and stop RMI and Socket
 * connections.
 * 
 * @author Federico
 *
 */
public class ConnectionsHandler {

	private static final Logger LOG = Logger.getLogger(ConnectionsHandler.class.getName());

	private static final int SOCKET_PORT = 9998;
	private static final int RMI_PORT = 9999;
	private static final int EXPORT_PORT = 0;
	private static final String IMPL_FACTORY_NAME = "ImplFactory";

	private SocketServer socketServer;
	private RemoteFactory implFactory;
	private RemoteFactory implFactoryStub;
	private Registry registry;
	private static ConnectionsHandler connectionHandler;

	private final ExecutorService executor;

	/**
	 * Constructor. If doesn't already exist, it is invoked by
	 * getConnectionHandler() method. It's a singleton.
	 */
	private ConnectionsHandler() {
		executor = Executors.newCachedThreadPool();
	}

	/**
	 * Returns the {@link ConnectionsHandler}
	 * 
	 * @return
	 */
	public static ConnectionsHandler getConnectionHandler() {
		if (connectionHandler == null) {
			connectionHandler = new ConnectionsHandler();
		}
		return connectionHandler;

	}

	/**
	 * Invoked by the server, it starts both socket and rmi servers
	 */
	public void startConnections() {
		startRMIServer();
		startSocketServer();
	}

	/**
	 * Stops both socket and rmi servers
	 */
	public void stopConnections() {
		stopSocketServer();
		stopRMIServer();
	}

	/**
	 * Invoked by startConnections(), it starts a {@link SocketServer}. If it
	 * doesn't already exist, it creates a new SocketServer. If it is impossible
	 * to create a socketServer, it logs an {@link IOException}
	 */
	public void startSocketServer() {
		if (socketServer == null) {
			try {
				socketServer = new SocketServer(SOCKET_PORT);
			} catch (IOException e) {
				LOG.log(Level.SEVERE, "Socket server cannot start", e);
			}
		}
		if (socketServer != null) {
			executor.execute(socketServer);
		}
	}

	/**
	 * Stops the {@link SocketServer}
	 */
	public void stopSocketServer() {
		if (socketServer != null) {
			socketServer.stopServer();
		}
	}

	/**
	 * Creates a new {@link RemoteFactoryImpl} to allow remote methods
	 * invocation. If that fails, logs a {@link RemoteException}
	 */
	public void startRMIServer() {
		if (registry == null) {
			try {
				registry = LocateRegistry.createRegistry(RMI_PORT);
				implFactory = new RemoteFactoryImpl();
				implFactoryStub = (RemoteFactory) UnicastRemoteObject.exportObject(implFactory, EXPORT_PORT);
				registry.rebind(IMPL_FACTORY_NAME, implFactoryStub);
				LOG.log(Level.INFO, "RMI server initialized");
			} catch (RemoteException e) {
				LOG.log(Level.SEVERE, "RMI server cannot be initialized", e);
			}
		}
	}
	/**
	 * Stops the RMI server
	 */
	public void stopRMIServer() {
		if (registry != null) {
			try {
				registry.unbind(IMPL_FACTORY_NAME);
				UnicastRemoteObject.unexportObject(implFactoryStub, true);
			} catch (RemoteException | NotBoundException e) {
				LOG.log(Level.SEVERE, "Can not stop RMI server", e);
			}
		}
	}

}
