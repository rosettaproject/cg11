package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.cards.Deck;

/**
 * Exception thrown when the {@link Deck} is empty.
 * @author paolasanfilippo
 *
 */
public class EmptyDeckException extends Exception {

	private static final long serialVersionUID = 8851523057164637643L;

	/**
	 * Exception thrown when the {@link Deck} is empty.
	 */
	public EmptyDeckException() {
		super();
	}

}
