package it.polimi.ingsw.cg11.server.model.actionsfactory;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Factory for {@link ElectCouncillor}
 * @author paolasanfilippo
 *
 */
public class ElectCouncillorFactory extends ActionFactory {

	private static final Logger LOG = Logger.getLogger(ElectCouncillorFactory.class.getName());
	
	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {


		CouncillorPoolManager councillorPoolManager;
		TurnManager turnManager;
		
		PlayerState player;
		Color color;
		Balcony balcony;

		try {
			LOG.log(Level.INFO, "Getting ElectCouncillor action parameters...\n");
			
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			color = ColorMap.getColor(actionData.getString("color"));
			balcony = getBalconyFromActionData(actionData,gameMap,"balcony");
		
			councillorPoolManager = gameMap.getCouncillorsPool();
			turnManager = gameMap.getTurnManager();
			
			LOG.log(Level.INFO, "Creating ElectCouncillor action...\n");
			
			return new ElectCouncillor(councillorPoolManager,player,turnManager,color,balcony, gameMap.getGameTopic());
			
		} catch (NoSuchPlayerException | ColorNotFoundException | NoSuchRegionException | IllegalCreationCommandException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
