package it.polimi.ingsw.cg11.server.control;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.PublisherInterface;

/**
 * ActionCommand abstract class defines the pattern of the player's command.
 * Each command that player can does has two methods: execute() and isAllowed()
 * The former executes the command and the latter controls if command can be executed
 * @author Federico
 *
 */
public abstract class Performable implements PublisherInterface<ModelChanges, String>{

	public static final String CHANGE_SUFFIX = "_CHANGES";
	public static final String OBTAINED_CARDS = "ObtainedCards";
	
	/**
	 * Executes the command. This method invokes isAllowed() method
	 * @throws NotAllowedActionException
	 */
	public abstract void execute() throws NotAllowedActionException;

	/**
	 * Controls if the command can be executed. It checks if preconditions (defined by rules of game) are satisfied
	 * @throws NotAllowedActionException if the action is not allowed
	 */
	public abstract void isAllowed() throws NotAllowedActionException;
	
	/**
	 * Invokes the same method on {@link Broker}: it's a publish method that 
	 * publishes for all players except for one, passed as parameter
	 * @param excludedPlayer
	 * @param message
	 * @param topic
	 */
	public void publishExceptFor(PlayerState excludedPlayer, ModelChanges message, String topic){
		Broker.getBroker().publishExceptFor(excludedPlayer, message, topic);
	}

	/**
	 * Allows to publish a change without showing the colors of the obtained politic cards to opponents
	 * @param modelChanges
	 * @param gameTopic
	 * @param turnManager
	 * @param player
	 */
	public void publishSelectiveChange(ModelChanges modelChanges,String gameTopic,TurnManager turnManager,PlayerState player){
		
		if (modelChanges.containsChange(OBTAINED_CARDS)) {
			modelChanges
					.setTopic(gameTopic + '_' + turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID());
			publish(modelChanges, modelChanges.getTopic());
			
			modelChanges.removeFromChanges(OBTAINED_CARDS);
			
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publishExceptFor(player, modelChanges, modelChanges.getTopic());
		}else{
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());
		}
	}
}