package it.polimi.ingsw.cg11.server.model.market;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
/**
 * Implements an offer of {@link Assistants}
 * @author paolasanfilippo
 *
 */
public class AssistantsOffer implements MarketOffer {

	public static final String OFFERER_ID_KEY = "OffererID";
	public static final String OFFER_COST_KEY = "OfferCost";
	public static final String ASSISTANTS_KEY = "Assistants";
	public static final String ACTOR_KEY = "ActorID";
	public static final String ACTION_KEY = "Action";
	private PlayerState offeringPlayer;
	private int assistantsAmount;
	private int offerCost;
	/**
	 * 
	 * @param offeringPlayer
	 * @param assistantsAmount
	 * @param offerCost
	 */
	public AssistantsOffer(PlayerState offeringPlayer, int assistantsAmount, int offerCost) {
		this.offeringPlayer = offeringPlayer;
		this.assistantsAmount = assistantsAmount;
		this.offerCost = offerCost;
	}

	@Override
	public ModelChanges acceptOffer(PlayerState acceptingPlayer) throws TransactionException {
		try {
			acceptingPlayer.getPlayerData().getMoney().spend(offerCost);
			offeringPlayer.getPlayerData().getMoney().gain(offerCost);
			offeringPlayer.getPlayerData().getAssistants().use(assistantsAmount);
			offeringPlayer.getPlayerData().getOfferedResources().removeAssistants(assistantsAmount);
			acceptingPlayer.getPlayerData().getAssistants().gain(assistantsAmount);
			
			return addModelChanges(offerCost,assistantsAmount,offeringPlayer,acceptingPlayer);
		} catch (IllegalResourceException e) {
			throw new TransactionException(e);
		}
	}
	/**
	 * Adds the following parameters with the following keys to a
	 * {@link ModelChanges} object, which is then returned by this method
	 * 
	 * @param cost mapped with the "OfferCost" key
	 * @param amount mapped with the "Assistants" key
	 * @param offering mapped with the "OffererID" key
	 * @param accepting mapped with the "ActorID" key
	 * @return {@link ModelChanges}
	 */
	private ModelChanges addModelChanges(int cost, int amount, PlayerState offering,
			PlayerState accepting) {
	ModelChanges modelChanges = new ModelChanges();
	
	modelChanges.addToChanges(ACTION_KEY,"AcceptAssistantsOffer");
	modelChanges.addToChanges(ACTOR_KEY, accepting.getPlayerData().getPlayerID());
	modelChanges.addToChanges(OFFERER_ID_KEY, offering.getPlayerData().getPlayerID());
	modelChanges.addToChanges(OFFER_COST_KEY, cost);
	modelChanges.addToChanges(ASSISTANTS_KEY, amount);
	
	return modelChanges;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assistantsAmount;
		result = prime * result + offerCost;
		result = prime * result + ((offeringPlayer == null) ? 0 : offeringPlayer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssistantsOffer other = (AssistantsOffer) obj;
		if (assistantsAmount != other.assistantsAmount)
			return false;
		if (offerCost != other.offerCost)
			return false;
		if (offeringPlayer == null) {
			if (other.offeringPlayer != null)
				return false;
		} else if (offeringPlayer.getPlayerData().getPlayerID() != other.offeringPlayer.getPlayerData().getPlayerID())
			return false;
		return true;
	}

	@Override
	public int getOfferCost() {
		return offerCost;
	}
	
	@Override
	public PlayerState getOfferingPlayer(){
		return offeringPlayer;
	}
}
