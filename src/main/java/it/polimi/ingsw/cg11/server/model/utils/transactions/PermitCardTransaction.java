package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoicesHandler;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Implements a transaction in which the player gets {@link PermitCard}
 * @author paolasanfilippo
 *
 */
public class PermitCardTransaction implements Transaction {

	int amount;
	/**
	 * 
	 * @param amount
	 */
	public PermitCardTransaction(int amount) {
		this.amount = amount;
	}

	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {

		int index;

		for (index = 0; index < this.amount; index++)
			transactionSubject.getPlayerActionExecutor()
					.addToAvailableActions(new PlayerChoice(PlayerChoicesHandler.getExtraPermitType()));
	}

	@Override
	public int getAmount() {
		return this.amount;
	}

}
