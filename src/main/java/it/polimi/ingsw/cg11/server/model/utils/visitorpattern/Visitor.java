
package it.polimi.ingsw.cg11.server.model.utils.visitorpattern;

import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;


/**
 * This generic interface represents a generic visitor that is able to visit a {@link SerializableMapStartupConfig}
 */
@FunctionalInterface
public interface Visitor {

	/**
	 * This visit is used to visit the {@link SerializableMapStartupConfig} message
	 * @param serializableMapStartupConfig
	 */
	public void visit(SerializableMapStartupConfig serializableMapStartupConfig);
	
}
