package it.polimi.ingsw.cg11.server.model.player.resources;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NegativeValueException;

/**
 * This class is used to store a player's money. The amount is stored as an
 * integer and it can be accessed, increased or decreased. It doesn't guarantee that
 * the operation performed is legal
 * 
 * @author francesco
 */

public class Money {

	private int amount;

	/**
	 * Increases amount of money by gainedAmount. 
	 * 
	 * @param gainedAmount
	 * @throws IllegalResourceException
	 */
	public void gain(int gainedAmount) throws IllegalResourceException {
		try {
			if (gainedAmount < 0)
				throw new IllegalArgumentException("gained amount can't be negative");
			this.amount += gainedAmount;
		} catch (IllegalArgumentException e) {
			throw new IllegalResourceException(e);
		}
	}

	/**
	 * Decreases amount of money by usedAmount
	 * 
	 * @param usedAmount
	 * @throws NegativeValueException
	 *             if amount of money is less than usedAmount
	 * @throws IllegalArgumentException
	 *             if usedAmount is negative
	 */
	public void spend(int usedAmount) throws IllegalResourceException {
		try {
			if (usedAmount < 0)
				throw new IllegalArgumentException("usedAmount can't be negative");
			if (amount - usedAmount < 0)
				throw new NegativeValueException("Money amount can't be negative");
			this.amount -= usedAmount;
		} catch (IllegalArgumentException | NegativeValueException e) {
			throw new IllegalResourceException(e);
		}
	}

	/**
	 * Returns the amount of money
	 * 
	 * @return amount of money
	 */
	public int getAmount() {
		return this.amount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Money [amount=" + amount + "]";
	}

}