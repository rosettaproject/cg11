package it.polimi.ingsw.cg11.server.model.player.actionspool;

/**
 * Represents an action in the list of available actions that the player can
 * perform
 * 
 * @author francesco
 *
 */
public abstract class GameAction {

	private final String actionType;
	/**
	 * 
	 * @param actionType
	 */
	public GameAction(String actionType) {
		this.actionType = actionType;
	}
	/**
	 * Returns a string with the action type
	 * @return action type 
	 */
	public String getActionType() {
		return actionType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionType == null) ? 0 : actionType.hashCode());
		return result;
	}

	/**
	 * This implementation only controls that the class matches
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameAction other = (GameAction) obj;
		if (actionType == null && other.actionType != null)
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "GameAction: " + actionType;
	}
	
	

}
