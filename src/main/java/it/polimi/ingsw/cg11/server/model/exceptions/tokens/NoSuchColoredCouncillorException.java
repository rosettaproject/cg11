package it.polimi.ingsw.cg11.server.model.exceptions.tokens;

import java.awt.Color;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;

/**
 * Exception thrown when there isn't a {@link Councillor} of that {@link Color}.
 * @author paolasanfilippo
 *
 */
public class NoSuchColoredCouncillorException extends Exception {

	private static final long serialVersionUID = 8489981442418050933L;

	/**
	 * Exception thrown when there isn't a {@link Councillor} of that {@link Color}.
	 * @param cause
	 */
	public NoSuchColoredCouncillorException(Throwable cause) {
		super(cause);
	}


}
