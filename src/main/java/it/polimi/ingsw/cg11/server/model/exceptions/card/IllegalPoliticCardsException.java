package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;

/**
 * Exception thrown when the {@link PoliticCard} isn't acceptable.
 * @author paolasanfilippo
 *
 */
public class IllegalPoliticCardsException extends Exception {

	private static final long serialVersionUID = 1845194262192576308L;

	/**
	 * Exception thrown when the {@link PoliticCard} isn't acceptable.
	 */
	public IllegalPoliticCardsException() {
		super();
	}

	/**
	 * Exception thrown when the {@link PoliticCard} isn't acceptable.
	 * @param cause
	 */
	public IllegalPoliticCardsException(Throwable cause) {
		super(cause);
	}
}
