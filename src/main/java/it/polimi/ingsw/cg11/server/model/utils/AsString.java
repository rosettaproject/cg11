package it.polimi.ingsw.cg11.server.model.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Contains a list of methods that returns a string that represents the object
 * passed. Cannot take null object as parameter.
 * 
 * @author Federico
 *
 */
public class AsString {

	private static final int DEFAULT_INITIAL_INDEX = 0;
	private static final String CARD = "CARD ";
	private static final String POSITION = "POSITION ";
	private static final String OFFER = "OFFER ";
	private static final String EMPTY = "empty\n";

	private AsString() {
	}

	/**
	 * Return a string that represents a generic market, ready to be printed
	 * 
	 * @param listOfOffers
	 * @return generic market as string
	 */
	public static <E> String marketAsString(List<E> listOfOffers) {

		return asStringWithName(listOfOffers, OFFER);
	}

	/**
	 * Return a string that represents a generic path, ready to be printed
	 * 
	 * @param listOfPositions
	 * @return generic path as string
	 */
	public static <E> String pathAsString(List<E> listOfPositions) {

		return asStringWithName(listOfPositions, POSITION);
	}

	/**
	 * Return a string that represents a generic deck, ready to be printed
	 * 
	 * @param listOfCards
	 * @return generic deck as string
	 */
	public static <E> String cardsAsString(List<E> listOfCards) {

		return asStringWithName(listOfCards, CARD);
	}

	/**
	 * Return a string that represents a generic list of cards, ready to be
	 * printed.
	 * 
	 * @param listOfCards
	 * @param startIndex
	 *            represents the first number of card that will be displayed
	 * @return generic list of cards as string
	 */
	public static <E> String cardsAsStringWithStartIndex(List<E> listOfCards, int startIndex) {

		return asStringWithNameAndWithStartIndex(listOfCards, CARD, startIndex);
	}

	/**
	 * Return a string that represents a generic list of object, ready to be
	 * printed
	 * 
	 * @param listOfObjects
	 * @param indexName
	 *            represents the name of the single object
	 * @return a generic list of object as string
	 */
	public static <E> String asStringWithName(List<E> listOfObjects, String indexName) {

		return asStringWithNameAndWithStartIndex(listOfObjects, indexName, DEFAULT_INITIAL_INDEX);

	}

	/**
	 * Return a string that represents a generic list of object, ready to be
	 * printed
	 * 
	 * @param listOfObjects
	 * @param indexies
	 *            if it is true, will be added index next to the elements
	 * @return a generic list of objects as string
	 */
	public static <E> String asString(List<E> listOfObjects, boolean indexies) {

		return asStringWithIndexWithDash(listOfObjects, indexies, false);

	}

	/**
	 * Return a string that represents a generic list of object, ready to be
	 * printed
	 * 
	 * @param listOfObjects
	 * @return a generic list of objects as string when each element has a dash
	 *         before itself
	 */
	public static <E> String asStringWithDashes(List<E> listOfObjects) {

		return asStringWithIndexWithDash(listOfObjects, false, true);

	}

	/**
	 * Return a string that represents a generic list of object, ready to be
	 * printed
	 * 
	 * @param listOfObjects
	 * @param indexName
	 *            is the name of single object
	 * @param startIndex
	 *            is the first index that will be displayed
	 * @return a generic list of objects as string
	 */
	public static <E> String asStringWithNameAndWithStartIndex(List<E> listOfObjects, String indexName,
			int startIndex) {

		String toString = "";
		int indexCard;

		Iterator<E> objIterator = listOfObjects.iterator();
		E tmpPoliticCard;

		if (listOfObjects.isEmpty())
			return EMPTY;

		indexCard = startIndex;
		while (objIterator.hasNext()) {
			tmpPoliticCard = objIterator.next();

			toString += indexName + indexCard + "\n";
			toString += tmpPoliticCard.toString() + "\n";

			indexCard += 1;
		}
		return toString;
	}

	/**
	 * Return a string that represents a generic list of object, ready to be
	 * printed
	 * 
	 * @param listOfObjects
	 * @param indexies
	 *            if it is true will be added an index next to elements
	 * @param dash
	 *            if it is true will be added a dash next to elements
	 * @return a generic list of objects as string
	 */
	public static <E> String asStringWithIndexWithDash(List<E> listOfObjects, boolean indexies, boolean dash) {

		String toString = "";
		int index;

		Iterator<E> objIterator = listOfObjects.iterator();
		E tmpPoliticCard;

		if (listOfObjects.isEmpty())
			return EMPTY;

		index = DEFAULT_INITIAL_INDEX;
		while (objIterator.hasNext()) {
			tmpPoliticCard = objIterator.next();

			if (indexies)
				toString += index + ") ";
			if (dash)
				toString += " - ";
			toString += tmpPoliticCard.toString() + "\n";

			index += 1;
		}
		return toString;
	}

	/**
	 * Return a string that represents a generic map of key-object, ready to be
	 * printed
	 * 
	 * @param map
	 * @param indexies
	 *            if it is true will be added an index next to elements
	 * @return a generic map as string
	 */
	public static <K, E> String asString(Map<K, E> map, boolean indexies) {
		String toString = "";
		int index;

		Iterator<K> keyIterator = map.keySet().iterator();
		K tmpKey;
		E value;

		if (map.isEmpty())
			return EMPTY;

		index = DEFAULT_INITIAL_INDEX;
		while (keyIterator.hasNext()) {
			tmpKey = keyIterator.next();
			value = map.get(tmpKey);

			if (indexies)
				toString += index + ") ";
			toString += tmpKey.toString() + ": " + value.toString() + "\n";

			index += 1;
		}

		return toString;
	}

	/**
	 * Adds spaces before capital letter
	 * 
	 * @param stringParameter
	 * @return a string with a space before each capital letter
	 */
	public static String addSpaces(String stringParameter) {
		String tmpString = "";
		Character character;

		for (int position = 0; position < stringParameter.length(); position += 1) {
			character = stringParameter.charAt(position);
			if ('A' <= character && character <= 'Z' && position > 0) {
				tmpString += " ";
			}
			tmpString += character;
		}
		return tmpString;
	}

}
