package it.polimi.ingsw.cg11.server.model.market;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.exceptions.market.NoSuchOfferException;

/**
 * Contains all the offers
 * 
 * @author paolasanfilippo
 *
 */
public class MarketOffers {

	private List<MarketOffer> offers;

	/**
	 * Constructor
	 */
	public MarketOffers() {
		resetList();
	}

	/**
	 * Adds and offer, passed as paramter, to the {@link MarketOffers}
	 * @param offerToAdd
	 */
	public void addOffer(MarketOffer offerToAdd) {

		getOffers().add(offerToAdd);
	}
	/**
	 * Removes an offer from the {@link MarketOffers}. It could throw a {@link NoSuchOfferException}
	 * if the offer, passed as parameter, doesn't exist in {@link MarketOffers}
	 * @param offerToRemove
	 * @return the market offer removed
	 * @throws NoSuchOfferException
	 */
	public MarketOffer removeOffer(MarketOffer offerToRemove) throws NoSuchOfferException {

		int offerIndex = getOffers().indexOf(offerToRemove);

		if (offerIndex >= 0){
			getOffers().remove(offerIndex);
			return offerToRemove;
		}
		throw new NoSuchOfferException();
		
	
	}
	/**
	 * Resets the list of {@link MarketOffers}
	 */
	public void resetList() {
		this.offers = new ArrayList<>();
	}
	/**
	 * Returns all the available offers
	 * @return list of offers
	 */
	public List<MarketOffer> getOffers() {
		return offers;
	}
}