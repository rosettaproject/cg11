package it.polimi.ingsw.cg11.server.model.mapcomponents.city;

import java.awt.Color;
import java.util.*;

import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * Represent a city.
 * 
 * @author paolasanfilippo
 *
 */
public class City {

	private final String name;
	private final Color color;
	private final Region region;
	private List<City> adjacentCities;
	private final Bonus cityBonus;
	private final List<Emporium> emporiums;

	/**
	 * City's constructor
	 * 
	 * @param name
	 *            : name of the city
	 * @param color
	 *            : color of the city
	 * @param region
	 *            : region that contains the city
	 * @param cityBonus
	 *            : bonus on the city
	 */
	public City(String name, Color color, Region region, Bonus cityBonus) {
		this.name = name;
		this.color = color;
		this.region = region;
		this.cityBonus = cityBonus;
		this.emporiums = new ArrayList<>();
	}

	/**
	 * Returns the {@link Color} of the city.
	 * 
	 * @return color
	 */
	public Color getCOLOR() {
		return this.color;
	}

	/**
	 * Returns the name of the city.
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the cities adjacent to this city.
	 * 
	 * @return list of adjacents
	 */
	public List<City> getAdjacentCities() {
		return this.adjacentCities;
	}

	/**
	 * Returns the {@link Bonus} of the city.
	 * 
	 * @return bonus
	 */
	public Bonus getCityBonus() {
		return this.cityBonus;
	}

	/**
	 * Returns the {@link Region} of the city.
	 * 
	 * @return region
	 */
	public Region getRegion() {
		return this.region;
	}

	/**
	 * Returns an ArrayList of {@link City}s from a list containing their names
	 * 
	 * @param cityNames
	 *            : list of strings with the names of the cities
	 * @param currentMap
	 * @return requestedCities : list of cities
	 */
	private ArrayList<City> getCityList(List<String> cityNames, GameMap currentMap) {

		List<City> mapCities = currentMap.getCities();
		ArrayList<City> requestedCities;

		requestedCities = new ArrayList<>();
		ListIterator<City> mapCitiesIterator = mapCities.listIterator();
		City city;

		while (mapCitiesIterator.hasNext()) {
			city = mapCitiesIterator.next();
			if (cityNames.contains(city.getName()))
				requestedCities.add(city);
		}

		return requestedCities;
	}

	/**
	 * Used only once, when all cities have been created. Realizes the
	 * connection between a {@link City} and a list of cities. Takes a list of
	 * cities' names as parameter
	 * 
	 * @param cityNames
	 *            : list of strings with the names of the cities
	 * @param currentMap
	 */
	public void initializeAdjacentCities(List<String> cityNames, GameMap currentMap) {
		this.adjacentCities = getCityList(cityNames, currentMap);
	}

	/**
	 * Tells if there's an emporium of that player.
	 * 
	 * @param player
	 * @return <tt>true</tt> if there's an {@link Emporium} owned by the player,
	 *         <tt>false</tt> if there's not
	 */
	public boolean existsPlayerEmporium(PlayerState player) {
		Iterator<Emporium> emporiumIterator = emporiums.iterator();
		Emporium currentEmporium;
		int playerID = player.getPlayerData().getPlayerID();

		while (emporiumIterator.hasNext()) {
			currentEmporium = emporiumIterator.next();
			if (currentEmporium.getPlayer() != null && currentEmporium.getPlayer().getPlayerID() == playerID) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the number of {@link Emporium}s.
	 * 
	 * @return number of emporiums
	 */
	public int numberOfEmporiums() {
		return emporiums.size();
	}

	/**
	 * Adds an {@link Emporium} on the city. If there's already an emporium of
	 * that player, it throws an {@link ExistEmporiumException}
	 * 
	 * @param emporium
	 * @throws ExistEmporiumException
	 */
	public void addEmporium(Emporium emporium) throws ExistEmporiumException {
		if (emporium != null) {
			if (emporiums.contains(emporium)) {
				throw new ExistEmporiumException("Player ID: " + emporium.getPlayer().getPlayerID());
			}
			emporiums.add(emporium);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "\nCity = " + name;
		Iterator<City> adjacentIterator = adjacentCities.iterator();
		Iterator<Emporium> emporiumIterator = emporiums.iterator();
		Emporium emporium;
		City city;

		toString += "\nemporiums:";
		if (emporiums.isEmpty())
			toString += " empty";
		while (emporiumIterator.hasNext()) {
			emporium = emporiumIterator.next();
			toString += emporium.toString();
		}

		toString += cityBonus.toString();

		toString += "\n- adjacent cities -\n";
		while (adjacentIterator.hasNext()) {
			city = adjacentIterator.next();
			toString += city.getName() + "\n";
		}

		return toString;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityBonus == null) ? 0 : cityBonus.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (cityBonus == null) {
			if (other.cityBonus != null)
				return false;
		} else if (!cityBonus.equals(other.cityBonus))
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		return true;
	}

}