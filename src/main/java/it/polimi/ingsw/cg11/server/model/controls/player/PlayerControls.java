/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.controls.player;


import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Contains controls on {@link PlayerState}
 * 
 * @author Federico
 *
 */
public class PlayerControls {

	/**
	 * Controls if player can actually play, telling if it's the player's turn; if it's not, this method throws a {@link NoPlayerTurnException}.
	 * 
	 * @param player
	 * @throws NoPlayerTurnException
	 */
	public void playerTurnControl(PlayerState player) throws NoPlayerTurnException {
		if (!player.isAllowedToPlay()) {
			throw new NoPlayerTurnException();
		}
	}
}
