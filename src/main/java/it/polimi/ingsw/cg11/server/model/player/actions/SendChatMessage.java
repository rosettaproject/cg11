package it.polimi.ingsw.cg11.server.model.player.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * Represents the action of sending a message to another player.
 * 
 * @author Federico
 *
 */
public class SendChatMessage extends Performable {

	private static final Logger LOG = Logger.getLogger(SendChatMessage.class.getName());

	private static final String ALL_PLAYERS = "allPlayers";
	private static final String CHAT_TO_ALL = "_CHAT_TO_ALL";

	private SerializableChatMessage chatMessage;

	/**
	 * 
	 * @param senderUsernamePlayer
	 * @param recipientUsernamePlayer
	 * @param message
	 * @param gameTopic
	 */
	public SendChatMessage(String senderUsernamePlayer, String recipientUsernamePlayer, String message,
			String gameTopic) {
		String topic;

		topic = gameTopic;
		if (recipientUsernamePlayer.equalsIgnoreCase(ALL_PLAYERS)) {
			topic += CHAT_TO_ALL;
		} else {
			topic += "_" + recipientUsernamePlayer;
		}
		chatMessage = new SerializableChatMessage(senderUsernamePlayer, message);
		chatMessage.setTopic(topic);
	}

	@Override
	public void execute() throws NotAllowedActionException {

		isAllowed();
		LOG.log(Level.INFO, "Message: " + chatMessage.getMessage() + " TOPIC " + chatMessage.getTopic());
		publish(chatMessage, chatMessage.getTopic());
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		// There are no condition to satisfy
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		LOG.log(Level.WARNING, "\n\nThere are no model changes to send\n\n");
	}

	public void publish(SerializableChatMessage message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}
