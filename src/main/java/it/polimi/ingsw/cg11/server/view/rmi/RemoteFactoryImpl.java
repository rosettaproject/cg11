package it.polimi.ingsw.cg11.server.view.rmi;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.view.connections.ClientView;
import it.polimi.ingsw.cg11.server.gameserver.GameFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.NoSuchViewException;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.ServerBrokerInterface;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * Used by the client to reach all the needed remote objects, contains a list of
 * RMI views, used by the clients to communicate with the server
 * 
 * @author francesco
 *
 */
public class RemoteFactoryImpl implements RemoteFactory {

	private static final Logger LOG = Logger.getLogger(RemoteFactoryImpl.class.getName());

	private ServerBrokerInterface<Visitable, String> broker;
	private List<RMIView> rmiViewList;

	/**
	 * Creates a remote objects factory. Takes a broker as a parameter to
	 * implement the view's publish-subscribe methods
	 * 
	 * @param broker
	 */
	public RemoteFactoryImpl() {
		this.broker = Broker.getBroker();
		rmiViewList = new ArrayList<>();
	}

	@Override
	public ServerBrokerInterface<Visitable, String> getBroker() throws RemoteException {
		return broker;
	}

	@Override
	public synchronized void createRMIView(ClientView clientViewStub, Token clientIdentifier) throws RemoteException {
		RMIView rmiView = new RMIView(clientViewStub, clientIdentifier, this);
		GameFactory factoryGame = GameFactory.getGameFactory();

		LOG.log(Level.INFO, "RMI client " + rmiView.getClientIdentifier().getClientUsername() + " connected\n");

		rmiViewList.add(rmiView);
		factoryGame.addPlayerView(rmiView);

	}

	@Override
	public synchronized void deleteRMIView(Token playerTokenToRemove) {
		RMIView tmpView;
		int indexToRemove;
		boolean removed = false;

		indexToRemove = 0;
		while (indexToRemove < rmiViewList.size() && !removed) {
			tmpView = rmiViewList.get(indexToRemove);

			if (tmpView.getClientIdentifier().equals(playerTokenToRemove)) {
				rmiViewList.remove(indexToRemove);
				LOG.log(Level.INFO, "\nRMI view of " + playerTokenToRemove.getClientUsername() + " has been removed\n");

				Broker.getBroker().unsubscibeAll(tmpView);
				removed = true;
			}

			indexToRemove += 1;
		}
	}

	@Override
	public synchronized void readMessage(Visitable message, Token clientIdentifier) throws RemoteException {
		RMIView publishingView;

		try {
			publishingView = getViewFromToken(clientIdentifier);
			publishingView.publish(message, clientIdentifier.getCurrentGame());
		} catch (NoSuchViewException e) {
			LOG.log(Level.WARNING, "\nAutentification failed\n", e);
		}

	}

	/**
	 * Returns an RMIView such that it's token equals the one given as a
	 * parameter. If there is no such view, a {@link NoSuchViewException} is
	 * thrown
	 * 
	 * @param clientIdentifier
	 * @return the corresponding RMI view
	 * @throws NoSuchViewException
	 */
	private RMIView getViewFromToken(Token clientIdentifier) throws NoSuchViewException {

		for (RMIView view : rmiViewList) {
			if (view.getClientIdentifier().equals(clientIdentifier))
				return view;
		}
		throw new NoSuchViewException();
	}
}
