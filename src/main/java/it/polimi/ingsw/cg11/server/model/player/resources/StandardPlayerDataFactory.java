package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.EmporiumPoolManager;
import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;

/**
 * implements {@link PlayerDataFactory} gives a fixed amount of resources to
 * each {@link PlayerData} based on the playerID
 * 
 * @author francesco
 * 
 */

public class StandardPlayerDataFactory implements PlayerDataFactory {

	private static final int STARTING_EMPORIUMS = 10;
	private static final Logger LOG = Logger.getLogger(StandardPlayerDataFactory.class.getName());
	
	/**
	 * 
	 * Initializes the resources based on the player's ID and returns the newly
	 * created PlayerData
	 * 
	 * @param playerID
	 *            player unique identifier
	 * @param politicsDeck
	 *            {@link PoliticCardsDeck}
	 * @param secondDeck {@link PoliticCardsDeck}
	 * @return PlayerData {@link PlayerData}
	 * @throws IllegalResourceException 
	 * 
	 */



	@Override
	public PlayerData initializePlayerData(int playerID, Deck<PoliticCard> politicsDeck,Deck<PoliticCard> secondDeck) throws IllegalResourceException {

		// starting resources are given as per standard game rules, extended to
		// any number players

		int startingMoney = 10 + playerID;
		int startingAssistants = 1+playerID;

		Assistants assistants = new Assistants();
		Money money = new Money();


		try {
			money.gain(startingMoney);
			assistants.gain(startingAssistants);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE,"Negative assistants added, "+e);
			throw new IllegalResourceException();
		}

		return new PlayerData(playerID, politicsDeck,secondDeck, money, assistants);
	}
	/**
	 * Acquires the emporium from the {@link EmporiumPoolManager}
	 * @param playerData
	 * @param emporiumPoolManager
	 * @throws NoSuchEmporiumException
	 */
	public void acquireEmporiums(PlayerData playerData, EmporiumPoolManager emporiumPoolManager)
			throws NoSuchEmporiumException {

		List<Emporium> emporiums = new ArrayList<>();

		int index;

		for (index = 0; index < STARTING_EMPORIUMS; index++) {
			emporiums.add(emporiumPoolManager.obtainPieceFromPool(playerData.getPlayerID()));

		}
		playerData.addEmporiums(emporiums);
	}

}
