package it.polimi.ingsw.cg11.server.model.exceptions.player;

/**
 * Exception thrown when the player doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchPlayerException extends Exception {

	private static final long serialVersionUID = 3411188236163701368L;

	/**
	 * Exception thrown when the player doesn't exist.
	 */
	public NoSuchPlayerException() {
		super();
	}
	/**
	 * Exception thrown when the player doesn't exist.
	 * @param message
	 */
	public NoSuchPlayerException(String message) {
		super(message);
	}

}
