package it.polimi.ingsw.cg11.server.model.utils.publishsubscribe;

import java.rmi.Remote;


@FunctionalInterface
public interface PublisherInterface<M, T> extends Remote {

	/**
	 * Allows a publisher to send a message to all the subscribers to the given topic stored in a {@link Broker}
	 * @param message
	 * @param topic
	 */
	public void publish(M message, T topic);
	
}
