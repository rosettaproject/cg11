package it.polimi.ingsw.cg11.server.model.mapcomponents.pieces;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;

/**
 * Implements the King piece in the game
 * 
 * @author francesco
 *
 */
public class King extends Piece {

	private final int movingCost;

	/**
	 * King's constructor
	 * 
	 * @param city
	 *            : starting city
	 * @param movingCost
	 *            : cost to move
	 */
	public King(City startingCity, int movingCost) {
		super("King", startingCity);
		this.movingCost = movingCost;
	}

	/**
	 * Returns the city on which there's the king
	 */
	@Override
	public City getCurrentCity() {
		return super.getCurrentCity();
	}

	/**
	 * Returns the king's moving cost
	 * 
	 * @return how much money is needed to move the king
	 */
	public int getMovingCost() {
		return movingCost;
	}

	/**
	 * Returns the piece's name
	 */
	@Override
	public String getName() {
		return super.getName();
	}

	/**
	 * Moves the king from his current position to the destination city. Simply
	 * performs the action, knows nothing of the ruling behind it
	 * 
	 * @param destinationCity
	 */
	@Override
	public void move(City destinationCity) {
		super.setCurrentCity(destinationCity);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		String toString = getName() + "\n";
		toString += "Current city: " + getCurrentCity();
		return toString;
	}

}