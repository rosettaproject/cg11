package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.jgrapht.Graphs;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import it.polimi.ingsw.cg11.server.model.exceptions.map.NotLinkedCitiesException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * This class represents a graph. With this graph is possible, for example, to
 * establish the shortest path between two cities
 * 
 * @author Federico
 *
 */
public class GraphedMap extends SimpleGraph<City, CityEdge> {

	private static final long serialVersionUID = -8892736938084413005L;

	private GraphedMap() {
		super(new CityEdge());
	}

	/**
	 * Initializes the graph.
	 * @param graph : graph we want to initialize
	 * @param cities : list of cities
	 * @param cityFilter : cities to be filtered
	 */
	private static void initializeGraph(GraphedMap graph, List<City> cities, Predicate<City> cityFilter) {
		List<City> filteredCities = cities.stream().filter(cityFilter).collect(Collectors.toList());
		Iterator<City> cityIterator = filteredCities.iterator();
		City currentCity;
		City vertexCity;
		List<City> adjacentCities;

		// Adding city
		while (cityIterator.hasNext()) {
			currentCity = cityIterator.next();
			graph.addVertex(currentCity);
		}

		// Creating edges
		cityIterator = filteredCities.iterator();
		while (cityIterator.hasNext()) {
			currentCity = cityIterator.next();
			adjacentCities = currentCity.getAdjacentCities();
			adjacentCities = adjacentCities.stream().filter(city -> graph.vertexSet().contains(city))
					.collect(Collectors.toList());
			for (Iterator<City> iterator = adjacentCities.iterator(); iterator.hasNext();) {
				vertexCity = iterator.next();
				graph.addEdge(currentCity, vertexCity);
			}
		}
	}

	/**
	 * Returns a new GraphedMap object
	 * 
	 * @param gameMap {@link GameMap}
	 * @return a GraphedMap object
	 */
	public static GraphedMap createGraphedMap(GameMap gameMap) {
		GraphedMap graph = new GraphedMap();
		List<City> mapCities = gameMap.getCities();
		initializeGraph(graph, mapCities, city -> true);
		return graph;
	}

	/**
	 * Returns a new GraphedMap object in which there are cities with emporiums.
	 * 
	 * @param gameMap
	 * @param player
	 * @return a GrapedMap object
	 */
	public static GraphedMap createCityWithEmporiumGraphedMap(GameMap gameMap, PlayerState player) {
		GraphedMap graph = new GraphedMap();
		List<City> mapCities = gameMap.getCities();
		initializeGraph(graph, mapCities, city -> city.existsPlayerEmporium(player));
		return graph;
	}

	/**
	 * Returns, given 2 cities, the minimum number of edges you have to cross
	 * to go from the first city to the destination's one (and viceversa)
	 * 
	 * @param kingCity : city on which there's the {@link King}
	 * @param destinationCity
	 * @return the distance from the 2 cities
	 */
	public int getDistance(City kingCity, City destinationCity) {
		int numberOfCity;

		// Perform Dijkstra algorithm
		DijkstraShortestPath<City, CityEdge> dijkstra = new DijkstraShortestPath<>(this, kingCity, destinationCity);
		GraphPath<City, CityEdge> shortestPath = dijkstra.getPath();

		// Counts how many cities there are in the shortest path
		numberOfCity = Graphs.getPathVertexList(shortestPath).size();
		numberOfCity -= 1;
		return numberOfCity;
	}

	/**
	 * Controls that is possible reach the destination {@link City} from the starting'one.
	 * It throws a {@link NotLinkedCitiesException} if the cities aren't linked.
	 * 
	 * @param startingCity
	 * @param destinationCity
	 * @throws NotLinkedCitiesException
	 */
	public void linkedCitiesControls(City startingCity, City destinationCity) throws NotLinkedCitiesException {
		DijkstraShortestPath<City, CityEdge> dijkstra = new DijkstraShortestPath<>(this, startingCity, destinationCity);
		GraphPath<City, CityEdge> shortestPath = dijkstra.getPath();

		if (shortestPath == null)
			throw new NotLinkedCitiesException();
	}

	/**
	 * Returns list of adjacent cities in which the player has built, included
	 * city passed
	 * @param startingCity
	 * @param player
	 * @return a list of linked cities
	 */
	public List<City> getLinkedCity(City startingCity, PlayerState player) {
		GraphIterator<City, CityEdge> iterator = new BreadthFirstIterator<>(this);
		List<City> linkedCity = new ArrayList<>();
		City cityToAdd;

		while (iterator.hasNext()) {
			cityToAdd = iterator.next();
			try {
				linkedCitiesControls(startingCity, cityToAdd);
				if (cityToAdd.existsPlayerEmporium(player))
					linkedCity.add(cityToAdd);
			} catch (NotLinkedCitiesException e) { //NOSONAR
				// Catch do nothing
			}
		}

		return linkedCity;
	}

}
