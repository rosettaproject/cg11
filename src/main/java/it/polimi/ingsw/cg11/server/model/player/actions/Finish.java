package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.*;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to builds the Finish action and tries to execute it. Finish
 * action permits to finish player turn
 * 
 * @author Federico
 *
 */
public class Finish extends Performable {

	private PlayerState player;
	private TurnManager turnManager;
	private String gameTopic;
	private GameMap gameMap;

	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param gameTopic
	 * @param gameMap
	 * @throws IllegalCreationCommandException
	 */
	public Finish(PlayerState player, TurnManager turnManager, String gameTopic, GameMap gameMap)
			throws IllegalCreationCommandException {
		this.turnManager = turnManager;
		this.player = player;
		this.gameTopic = gameTopic;
		this.gameMap = gameMap;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;
		int actorID = player.getPlayerData().getPlayerID();

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().finish(turnManager, gameMap);

			if (modelChanges.containsChange(PlayerActionExecutor.DRAWING_PLAYER_ID_KEY)) {
				modelChanges
						.setTopic(gameTopic + '_' + turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID());
				publish(modelChanges, modelChanges.getTopic());
				
				modelChanges = new ModelChanges();
				modelChanges.addToChanges(PlayerActionExecutor.ACTOR_ID, actorID);
				modelChanges.addToChanges(PlayerActionExecutor.BLIND_DRAW_ID,
						turnManager.getCurrentlyPlaying().getPlayerData().getPlayerID());
				modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			}
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());

		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		try {
			playerControls.playerTurnControl(player);
		} catch (NoPlayerTurnException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}