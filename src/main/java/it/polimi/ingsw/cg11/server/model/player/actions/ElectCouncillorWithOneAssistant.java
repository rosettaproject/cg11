package it.polimi.ingsw.cg11.server.model.player.actions;

import java.awt.Color;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.TokenControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughAssistantsException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchColoredCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the ElectCouncilorWithOneAssistant action and tries
 * to execute it. ElectCouncilorWithOneAssistant is a quick action that permits
 * to elect one councillor using one assistant
 * 
 * @author Federico
 *
 */
public class ElectCouncillorWithOneAssistant extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);

	private Color color;
	private PlayerState player;
	private CouncillorPoolManager councillorPoolManager;
	private Balcony balcony;
	private TurnManager turnManager;
	private String gameTopic;

	private static final int NEEDED_ASSISTANTS_AMOUNT = 1;

	/**
	 * 
	 * @param councillorPoolManager
	 * @param player
	 * @param turnManager
	 * @param color
	 * @param balcony
	 * @param gameTopic
	 * @throws IllegalCreationCommandException
	 */
	public ElectCouncillorWithOneAssistant(CouncillorPoolManager councillorPoolManager, PlayerState player,
			TurnManager turnManager, Color color, Balcony balcony, String gameTopic)
			throws IllegalCreationCommandException {

		TokenControls tokenControls = new TokenControls();

		this.councillorPoolManager = councillorPoolManager;
		this.color = color;
		this.player = player;
		this.balcony = balcony;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;

		try {
			tokenControls.councillorColorConsistencyCheck(color);
		} catch (ColorNotFoundException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getQuickActionsHandler()
					.electCouncilorWithOneAssistant(color, councillorPoolManager, balcony);
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		TokenControls tokenControls = new TokenControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			tokenControls.notEmptyPoolControl(councillorPoolManager);
			tokenControls.councillorColorAvailabilityControl(councillorPoolManager, color);
			playerResourcesControls.enoughAssistantControl(player, NEEDED_ASSISTANTS_AMOUNT);

		} catch (NoPlayerTurnException | NoEnoughAssistantsException | NoSuchCouncillorException
				| NoSuchColoredCouncillorException | NotCorrectGamePhaseException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}