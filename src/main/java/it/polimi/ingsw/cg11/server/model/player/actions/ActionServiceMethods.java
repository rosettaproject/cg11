package it.polimi.ingsw.cg11.server.model.player.actions;

import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.CityControls;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.RegionControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.IllegalPoliticCardsException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.LastTurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoice;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerChoicesHandler;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.model.utils.ColoredBonus;
/**
 * This class contains various methods that are used by {@link Performable} to perform operations
 * needed to create proper actions.
 * @author paolasanfilippo
 *
 */
public class ActionServiceMethods {

	private static final String MULTICOLOR = "MULTICOLOR";
	private static final int LAST_EMPORIUM_VICTORY_POINTS_REWARD = 3;

	/**
	 * Return needed money to obtain permit card with
	 * {@link numberOfPoliticCard} owned
	 * 
	 * @param politicCards used to corrupt the councill
	 * @return needed money that is (when there aren't multicolor cards) 10, 7, 4, 0 
	 * 			<br> if numberOfPoliticCard is respectively 1, 2, 3, 4.
	 * 	<br> For each multicolor card the amount of money needed increases by 1
	 * @throws IllegalPoliticCardsException
	 */
	public int neededMoneyToCorruptCouncil(List<PoliticCard> politicCards) throws IllegalPoliticCardsException {
		int neededMoney;
		int numberOfPoliticCard = politicCards.size();
		int numberOfMulticolorCard;
		try {
			numberOfMulticolorCard = numberOfMulticolor(politicCards);
		} catch (ColorNotFoundException e) {
			throw new IllegalPoliticCardsException(e);
		}

		switch (numberOfPoliticCard) {
		case 1:
			neededMoney = 10;
			break;
		case 2:
			neededMoney = 7;
			break;
		case 3:
			neededMoney = 4;
			break;
		case 4:
			neededMoney = 0;
			break;
		default:
			throw new IllegalPoliticCardsException();
		}

		return neededMoney + numberOfMulticolorCard;
	}
	/**
	 * Counts the number of multicolor {@link PoliticCard} present in the politic card used to
	 * corrupt the council.
	 * @param politicCards
	 * @return number of multicolor cards
	 * @throws ColorNotFoundException
	 */
	private int numberOfMulticolor(List<PoliticCard> politicCards) throws ColorNotFoundException {

		int count;
		String color;
		Iterator<PoliticCard> iterator = politicCards.iterator();

		count = 0;

		while (iterator.hasNext()) {
			color = ColorMap.getString(iterator.next().getColor());
			if (color.equals(MULTICOLOR))
				count++;
		}
		return count;
	}

	/**
	 * Gives to player all bonus present in the cities
	 * 
	 * @param cities
	 * @param player
	 * @return modelChanges
	 * @throws TransactionException
	 */
	public ModelChanges obtainCityBonus(List<City> cities, PlayerState player) throws TransactionException {
		ModelChanges bonusChanges = new ModelChanges();
		Iterator<City> iterator = cities.iterator();
		Bonus bonus;

		while (iterator.hasNext()) {
			bonus = iterator.next().getCityBonus();
			bonusChanges.addToChanges(bonus.obtain(player));
		}
		return bonusChanges;
	}
	/**
	 * Takes the {@link City}'s {@link ColoredBonus}, if it is still available.
	 * It could throw a {@link TransactionException} if the color of the bonus isn't acceptable
	 * @param gameMap
	 * @param player
	 * @param city
	 * @return modelChanges
	 * @throws TransactionException
	 */
	public ModelChanges takeColorBonusIfAvailable(GameMap gameMap, PlayerState player, City city)
			throws TransactionException {
		CityControls cityControls = new CityControls();
		ColoredBonus coloredBonus;
		ModelChanges bonusChanges = new ModelChanges();

		try {
			if (cityControls.isColorBonusAvailable(gameMap, player, city)) {
				coloredBonus = gameMap.getCityColorBonus(city.getCOLOR());
				bonusChanges.addToChanges(coloredBonus.obtain(player));
				coloredBonus.setAvailable(false);
			}
			return bonusChanges;
		} catch (ColorNotFoundException e) {
			throw new TransactionException(e);
		}

	}
	/**
	 * Takes the {@link Region} bonus if it still available.
	 * It could throw {@link TransactionException} if it isn't possible to 
	 * obtain the player from the region bonus.
	 * @param player
	 * @param region
	 * @return modelChanges
	 * @throws TransactionException
	 */
	public ModelChanges takeRegionBonusIfAvailable(PlayerState player, Region region) throws TransactionException {
		RegionControls regionControls = new RegionControls();
		Bonus regionBonus;
		ModelChanges bonusChanges = new ModelChanges();

		if (regionControls.isRegionBonusAvailable(player, region)) {
			regionBonus = region.getRegionBonus();
			region.setBonusAvailable(false);
			bonusChanges.addToChanges(
					regionBonus.obtain(player));
		}
		return bonusChanges;
	}

	/**
	 * Controls on victory: checks if there isn't already a player who finished the
	 * emporiums, then makes the {@link LastTurnManager} count and assigns the victory
	 * points to the player
	 * @param gameMap
	 */
	public void victoryControl(GameMap gameMap) {

		List<PlayerState> players;

		players = gameMap.getPlayers();
		LastTurnManager lastTurnManager = gameMap.getTurnManager().getLastTurnManager();

		for (PlayerState player : players) {
			if (player.getPlayerData().getRemainingEmporiums() == 0 && !lastTurnManager.isCounting()) {
				lastTurnManager.startCounting();
				player.getPlayerData().getVictoryPoints().gain(LAST_EMPORIUM_VICTORY_POINTS_REWARD);

			}
		}
	}
	/**
	 * Controls if a player can choose an extra bonus or not.
	 * @param player
	 * @param gameMap
	 */
	public void canChooseExtraBonusControl(PlayerState player,GameMap gameMap){
		
		PlayerChoice chooseExtraBonus = new PlayerChoice(PlayerChoicesHandler.EXTRA_BONUS_TYPE);
		boolean canChooseExtraBonus = false;
		
		while (player.getPlayerActionExecutor().getAvailableActions().contains(chooseExtraBonus) && !canChooseExtraBonus){

			for(City city : gameMap.getCities()){
				if(city.existsPlayerEmporium(player) && !(city.getCityBonus().getNobilityPointsAmount() > 0))
					canChooseExtraBonus = true;
			}
			if(!canChooseExtraBonus)
				player.getPlayerActionExecutor().getAvailableActions().remove(chooseExtraBonus);
			
		}
	}
	/**
	 * Controls if a player can choose an extra permit card 
	 * @param player
	 */
	public void canChoosePermitCardBonusControl(PlayerState player){
		
		PlayerChoice chooseExtraPermitBonus = new PlayerChoice(PlayerChoicesHandler.EXTRA_PERMIT_BONUS_TYPE);

		
		while(player.getPlayerActionExecutor().getAvailableActions().contains(chooseExtraPermitBonus)
				 && player.getPlayerData().getPermitsHand().getNumberOfCards() <= 0){
			player.getPlayerActionExecutor().getAvailableActions().remove(chooseExtraPermitBonus);
		}
	}

}
