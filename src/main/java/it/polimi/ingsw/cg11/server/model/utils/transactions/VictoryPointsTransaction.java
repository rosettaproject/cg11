package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Implements a transaction in which the player gets {@link VictoryPoints}
 * 
 * @author francesco
 *
 */
public class VictoryPointsTransaction implements Transaction {

	private int amount;
	/**
	 * 
	 * @param amount
	 */
	public VictoryPointsTransaction(int amount) {
		this.amount = amount;
	}
	@Override
	public int getAmount(){
		return this.amount;
	}

	/**
	 * Lets the player gain the set amount of victory points
	 * 
	 * @param player
	 */
	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {
		transactionSubject.getPlayerData().getVictoryPoints().gain(amount);
	}

}