package it.polimi.ingsw.cg11.server.model.player.actionspool;

import java.awt.Color;
import java.util.List;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.AvailableMandatoryPlayerChoiceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAvailableMainActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GraphedMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ActionServiceMethods;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillor;
import it.polimi.ingsw.cg11.server.model.player.actions.TakePermitCard;
import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;

/**
 * Contains the methods to perform all the main actions
 * 
 * @author francesco
 *
 */
public class MainActionsHandler {

	private static final int ELECTION_REWARD = 4;

	public static final String ACTOR_KEY = "ActorID";
	public static final String ACQUIRED_PERMIT = "AcquiredPermit";
	public static final String NEW_PERMIT_KEY = "NewPermit";
	public static final String USED_PERMIT_KEY = "UsedPermit";
	public static final String DISCARDED_KEY = "DiscardedCards";
	public static final String CITY_NAME_KEY = "DestinationCity";
	public static final String NEEDED_MONEY_KEY = "NeededMoney";
	public static final String NEEDED_ASSISTANTS_KEY = "NeededAssistants";
	public static final String REGION_NAME_KEY = "Region";
	public static final String COUNCILLOR_KEY = "Councillor";
	public static final String BALCONY_KEY = "BalconyName";
	public static final String ACTION_KEY = "Action";

	private PlayerState player;
	private MainAction mainAction;
	private PlayerChoice playerChoice;
	private List<GameAction> availableActions;
	/**
	 * 
	 * @param player
	 */
	public MainActionsHandler(PlayerState player) {
		this.player = player;
		this.mainAction = new MainAction();
		this.playerChoice = new PlayerChoice("Generic");
		this.availableActions = player.getPlayerActionExecutor().getAvailableActions();
	}

	/**
	 * Main Action: Takes a {@link Councillor} of the selected color from
	 * {@link CouncillorPoolManager} and puts it in {@link Balcony}, the player
	 * gets 4 coins
	 * 
	 * @param color
	 *            : the color of the councillor
	 * @param councillorPoolManager
	 * @param balcony
	 *            : the balcony selected to elect the councillor
	 * @return model changes
	 *
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges electCouncillor(Color color, CouncillorPoolManager councillorPoolManager, Balcony balcony)
			throws IllegalStateChangeException {

		ModelChanges modelChanges;
		
		if (!availableActions.contains(mainAction))
			throw new IllegalStateChangeException(new NotAvailableMainActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		try {
			balcony.addCouncillor(color, councillorPoolManager);
			player.getPlayerData().getMoney().gain(ELECTION_REWARD);
			availableActions.remove(mainAction);

			modelChanges = new ModelChanges();

			addElectCouncillorChanges(modelChanges, color, balcony);

			return modelChanges;

		} catch (NoSuchCouncillorException | IllegalResourceException | ColorNotFoundException e) {
			throw new IllegalStateChangeException(e);
		}
	}

	/**
	 * Main Action: Gives to player a permit card. 
	 * 
	 * @param permitCard
	 * @param region
	 * @param playerPoliticCards
	 * @param neededMoney
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges takePermitCard(PermitCard permitCard, Region region, List<PoliticCard> playerPoliticCards,
			int neededMoney) throws IllegalStateChangeException {
		
		FaceUpPermits faceUpPermits = region.getFaceUpPermitCards();
		PlayerData playerData = player.getPlayerData();
		
		PermitCard newFaceUpPermit;
		ModelChanges modelChanges;

		if (!availableActions.contains(mainAction))
			throw new IllegalStateChangeException(new NotAvailableMainActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		try {
			modelChanges = new ModelChanges();

			playerData.getPoliticsHand().discard(playerPoliticCards);
			playerData.getMoney().spend(neededMoney);

			modelChanges.addToChanges(permitCard.getPermitBonus().obtain(player));

			newFaceUpPermit = faceUpPermits.peekAtFirstCard();
			faceUpPermits.removeCard(permitCard);
			playerData.addToPermitsHand(permitCard);
			availableActions.remove(mainAction);

			addTakePermitCardChanges(modelChanges, permitCard,newFaceUpPermit, region, playerPoliticCards, neededMoney);

			return modelChanges;

		} catch (TransactionException | NoSuchPermitCardException | NoSuchPoliticCardException
				| IllegalResourceException | ColorNotFoundException e) {
			throw new IllegalStateChangeException(e);
		}

	}

	/**
	 * Main Action: Builds an emporium on {@link city} whit a permit card. This is a main
	 * action
	 * 
	 * @param gameMap
	 * @param destinationCity
	 * @param permitCard
	 * @param neededAssistants
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges buildEmporiumWithPermitCard(GameMap gameMap, City destinationCity, PermitCard permitCard,
			int neededAssistants) throws IllegalStateChangeException {

		ActionServiceMethods serviceMethod = new ActionServiceMethods();
		GraphedMap graphedMap;
		List<City> linkedCities;
		PermitCard usedCard;
		ModelChanges modelChanges;

		if (!availableActions.contains(mainAction))
			throw new IllegalStateChangeException(new NotAvailableMainActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		try {
			modelChanges = new ModelChanges();

			player.getPlayerData().getAssistants().use(neededAssistants);
			destinationCity.addEmporium(player.getPlayerData().getOneEmporium());

			graphedMap = GraphedMap.createCityWithEmporiumGraphedMap(gameMap, player);
			linkedCities = graphedMap.getLinkedCity(destinationCity, player);

			modelChanges.addToChanges(serviceMethod.obtainCityBonus(linkedCities, player));
			modelChanges.addToChanges(serviceMethod.takeColorBonusIfAvailable(gameMap, player, destinationCity));
			modelChanges.addToChanges(serviceMethod.takeRegionBonusIfAvailable(player, destinationCity.getRegion()));

			usedCard = player.getPlayerData().getPermitsHand().removeCard(permitCard);

			player.getPlayerData().getUsedPermits().addToHand(usedCard);
			availableActions.remove(mainAction);

			addBuildEmporiumWithPermitCardChanges(modelChanges, usedCard, neededAssistants, destinationCity.getName());

			return modelChanges;

		} catch (ExistEmporiumException | TransactionException | IllegalResourceException
				| NoSuchPermitCardException e) {
			throw new IllegalStateChangeException(e);
		}
	}

	/**
	 * Main Action: Builds an emporium on {@link city} with king help This is a main action
	 * 
	 * @param gameMap
	 * @param destinationCity
	 * @param politicCards
	 * @param neededMoney
	 * @param neededAssistants
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges buildEmporiumWithKing(GameMap gameMap, City destinationCity, List<PoliticCard> politicCards,
			int neededMoney, int neededAssistants) throws IllegalStateChangeException {

		ActionServiceMethods serviceMethod = new ActionServiceMethods();
		List<City> linkedCities;
		King king = gameMap.getKing();
		GraphedMap graphedMap;
		ModelChanges modelChanges;

		if (!availableActions.contains(mainAction))
			throw new IllegalStateChangeException(new NotAvailableMainActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		try {
			modelChanges = new ModelChanges();

			player.getPlayerData().getMoney().spend(neededMoney);
			destinationCity.addEmporium(player.getPlayerData().getOneEmporium());
			king.move(destinationCity);

			graphedMap = GraphedMap.createCityWithEmporiumGraphedMap(gameMap, player);
			linkedCities = graphedMap.getLinkedCity(destinationCity, player);

			modelChanges.addToChanges(serviceMethod.obtainCityBonus(linkedCities, player));
			modelChanges.addToChanges(serviceMethod.takeRegionBonusIfAvailable(player, destinationCity.getRegion()));
			modelChanges.addToChanges(serviceMethod.takeColorBonusIfAvailable(gameMap, player, destinationCity));

			player.getPlayerData().getPoliticsHand().discard(politicCards);
			player.getPlayerData().getAssistants().use(neededAssistants);
			availableActions.remove(mainAction);

			addBuildEmporiumWithKingChanges(modelChanges, neededMoney, neededAssistants, destinationCity.getName(),
					politicCards);

			return modelChanges;

		} catch (ExistEmporiumException | TransactionException | NoSuchPoliticCardException | IllegalResourceException
				| ColorNotFoundException e) {
			throw new IllegalStateChangeException(e);
		}

	}
	/**
	 * Adds to changes the parameters of a {@link BuildEmporiumWithKing} main action
	 * @param modelChanges
	 * @param neededMoney with the key of "NeededMoney"
	 * @param neededAssistants with the key "NeededAssistants"
	 * @param cityName with the key "DestinationCity"
	 * @param politicCards with the key "DiscardedCards"
	 * @throws ColorNotFoundException
	 */
	private void addBuildEmporiumWithKingChanges(ModelChanges modelChanges, int neededMoney, int neededAssistants,
			String cityName, List<PoliticCard> politicCards) throws ColorNotFoundException {

		modelChanges.addToChanges(ACTION_KEY, "BuildEmporiumWithKing");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(NEEDED_MONEY_KEY, neededMoney);
		modelChanges.addToChanges(NEEDED_ASSISTANTS_KEY, neededAssistants);
		modelChanges.addToChanges(CITY_NAME_KEY, cityName);
		modelChanges.addToChanges(DISCARDED_KEY, politicCards);

	}
	/**
	 * Adds to changes the parameters of a {@link BuildEmporiumWithPermitCard} main action
	 * @param modelChanges
	 * @param usedCard with the key "UsedPermit"
	 * @param neededAssistants with the key "NeededAssistants"
	 * @param cityName with the key "DestinationCity"
	 */
	private void addBuildEmporiumWithPermitCardChanges(ModelChanges modelChanges, PermitCard usedCard,
			int neededAssistants, String cityName) {

		modelChanges.addToChanges(ACTION_KEY, "BuildEmporiumWithPermitCard");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(CITY_NAME_KEY, cityName);
		modelChanges.addToChanges(USED_PERMIT_KEY, usedCard);
		modelChanges.addToChanges(NEEDED_ASSISTANTS_KEY, neededAssistants);
	}
	/**
	 * Adds to changes the parameters of a {@link TakePermitCard} main action
	 * @param modelChanges
	 * @param permitCard with the key "AcquiredPermit"
	 * @param newPermit with the key "NewPermit"
	 * @param region with the key "Region"
	 * @param politicCards with the key "DiscardedCards"
	 * @param neededMoney with the key of "NeededMoney"
	 * @throws ColorNotFoundException
	 */
	private void addTakePermitCardChanges(ModelChanges modelChanges, PermitCard permitCard,PermitCard newPermit, Region region,
			List<PoliticCard> politicCards, int neededMoney) throws ColorNotFoundException {

		modelChanges.addToChanges(ACTION_KEY, "TakePermitCard");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(ACQUIRED_PERMIT, permitCard);
		if(newPermit != null)
			modelChanges.addToChanges(NEW_PERMIT_KEY,newPermit);
		else
			modelChanges.addToChanges("EmptyDeck", "Permit deck is empty");
		modelChanges.addToChanges(REGION_NAME_KEY, region.getName());
		modelChanges.addToChanges(DISCARDED_KEY, politicCards);
		modelChanges.addToChanges(NEEDED_MONEY_KEY, neededMoney);
	}
	/**
	 * Adds to changes the parameters of a {@link ElectCouncillor} main action
	 * @param modelChanges
	 * @param color with the key "Councillor"
	 * @param balcony with the key "BalconyName"
	 * @throws ColorNotFoundException
	 */
	private void addElectCouncillorChanges(ModelChanges modelChanges, Color color, Balcony balcony)
			throws ColorNotFoundException {

		modelChanges.addToChanges(ACTION_KEY, "ElectCouncillor");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(COUNCILLOR_KEY, color);
		modelChanges.addToChanges(BALCONY_KEY, balcony.getNAME());
	}

}
