package it.polimi.ingsw.cg11.server.view;

import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.PublisherInterface;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.SubscriberUserInterface;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;

/**
 * This interface shall be implemented by an object functioning as a view for a client in the client-server communication
 * @author francesco
 */
public interface View extends PublisherInterface<Visitable, String>, SubscriberUserInterface<Visitable> {
	
	/**
	 * Return the {@link Token} that identifies the player
	 * @return token of player
	 */
	public Token getClientIdentifier();
	
	/**
	 * Sets the ID of player. It is used by the server when has created the game
	 * @param playerID
	 */
	public void sendIDToPlayer(int playerID);
	
	/**
	 * Sets the username of player. This is used in case of namesakes 
	 * @param username
	 */
	public void setUpdatedUsername(String username);
	
}
