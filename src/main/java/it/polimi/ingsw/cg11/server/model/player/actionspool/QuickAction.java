package it.polimi.ingsw.cg11.server.model.player.actionspool;

/**
 * If this object is in the player's available actions list, he can perform a
 * quick action
 * 
 * @author francesco
 *
 */
public class QuickAction extends GameAction {
	/**
	 * Constructor
	 */
	public QuickAction() {
		super("QuickAction");

	}

}
