package it.polimi.ingsw.cg11.server.model.mapcomponents.tokens;

import it.polimi.ingsw.cg11.server.model.player.resources.*;

/**
 * Emporium is a token that represents an emporium. Each player has a prefixed
 * number of emporiums, that are used when the player wants to build on a city
 * {@link City}.
 *
 * @author Paola
 *
 */
public class Emporium {

	private final PlayerData playerData;

	/**
	 * Constructor
	 * @param playerData
	 *            {@link PlayerData}
	 */
	protected Emporium(PlayerData playerData) {
		this.playerData = playerData;
	}

	/**
	 * Returns the PlayerData of the player who owns the emporium
	 * @return playerdata
	 */
	public PlayerData getPlayer() {
		return this.playerData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((playerData == null) ? 0 : playerData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Emporium other = (Emporium) obj;
		if (playerData == null) {
			if (other.playerData != null)
				return false;
		} else if (!playerData.equals(other.playerData))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return " ID = " + playerData.getPlayerID();
	}
	
	

}