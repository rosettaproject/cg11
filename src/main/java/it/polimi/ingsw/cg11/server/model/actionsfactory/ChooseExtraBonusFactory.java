package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraBonus;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link ChooseExtraBonus}
 * @author paolasanfilippo
 *
 */
public class ChooseExtraBonusFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		
		PlayerState player;
		TurnManager turnManager;
		City city;
		
		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			turnManager = gameMap.getTurnManager();
			city = getCityFromActionData(actionData,gameMap,"city");
			
			return new ChooseExtraBonus(player,turnManager,city, gameMap.getGameTopic());
		} catch (NoSuchPlayerException | NoSuchCityException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
