package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import it.polimi.ingsw.cg11.server.model.market.MarketOffers;

/**
 * Manages the initialization and alternation of {@link GamePhase}s
 * 
 * @author francesco
 *
 */
public class GamePhaseManager {

	private static final String FIRST_GAME_PHASE = "StandardPlayPhase";
	private static final String SECOND_GAME_PHASE = "MarketOfferingPhase";
	private static final String THIRD_GAME_PHASE = "MarketAcceptingPhase";
	private static final String GAME_IS_OVER = "GameIsOver";

	List<GamePhase> gamePhases;
	GamePhase currentGamePhase;
	GamePhase startingGamePhase;
	GamePhase gameOverPhase;
	MarketOffers marketModel;
	/**
	 * Game phase manager's constructor
	 * @param startingPhaseName
	 * @param marketModel
	 */
	public GamePhaseManager(String startingPhaseName, MarketOffers marketModel) {

		this.marketModel = marketModel;
		this.gamePhases = new ArrayList<>();
		this.gameOverPhase = new GamePhase(GAME_IS_OVER, true);

		gamePhases.add(new GamePhase(FIRST_GAME_PHASE, false));
		gamePhases.add(new GamePhase(SECOND_GAME_PHASE, false));
		gamePhases.add(new GamePhase(THIRD_GAME_PHASE, true));

		ListIterator<GamePhase> phasesIterator = gamePhases.listIterator();
		GamePhase gamePhase;

		while (phasesIterator.hasNext()) {
			gamePhase = phasesIterator.next();
			if (gamePhase.getPhaseName().equals(startingPhaseName)) {
				this.startingGamePhase = gamePhase;
				this.currentGamePhase = gamePhase;
			}
		}
	}

	/**
	 * Changes the current {@link GamePhase}
	 */
	public void changeGamePhase() {

		if (!currentGamePhase.equals(gameOverPhase)) {

			int lastPhasePosition = gamePhases.size() - 1;

			if (currentGamePhase.equals(gamePhases.get(lastPhasePosition)))
				currentGamePhase = startingGamePhase;
			else
				currentGamePhase = gamePhases.get(gamePhases.indexOf(currentGamePhase) + 1);

			if (currentGamePhase.getPhaseName().equals(getFirstGamePhase().getPhaseName()))
				marketModel.resetList();
		} 
	}
	/**
	 * Ends the game
	 * 
	 */
	public void endTheGame() {
		currentGamePhase = gameOverPhase;
	}

	/**
	 * Returns the current {@link GamePhase}
	 * @return current game phase
	 */
	public GamePhase getCurrentGamePhase() {
		return currentGamePhase;
	}

	/**
	 * Gets the starting {@link GamePhase}
	 * @return starting game phase
	 */
	public GamePhase getStartingGamePhase() {
		return startingGamePhase;
	}
	/**
	 * Returns the first game phase
	 * @return first game phase
	 */
	public GamePhase getFirstGamePhase() {
		return new GamePhase(FIRST_GAME_PHASE,false);
	}
}
