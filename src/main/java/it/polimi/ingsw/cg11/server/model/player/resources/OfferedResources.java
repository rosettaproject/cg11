package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
/**
 * Contains all the elements that have been offered, that can be {@link PermitCard}s,
 * {@link PoliticCard}s and {@link Assistants}
 * @author paolasanfilippo
 *
 */
public class OfferedResources {

	private int offeredAssistants;
	private  List<PoliticCard> offeredPoliticCards;
	private  List<PermitCard> offeredPermitCards;
	private  List<PermitCard> offeredUsedPermitCards;
	/**
	 * Constructor
	 */
	public OfferedResources() {

		offeredAssistants = 0;
		this.offeredPoliticCards = new ArrayList<>();
		this.offeredPermitCards = new ArrayList<>();
		this.offeredUsedPermitCards = new ArrayList<>();
	}
	/**
	 * Returns the amount of offered assistants
	 * @return number of assistants offered
	 */
	public int getOfferedAssistants() {
		return offeredAssistants;
	}
	/**
	 * Returns the politic cards offered
	 * @return list of politic cards offered
	 */
	public List<PoliticCard> getOfferedPoliticCards() {
		return offeredPoliticCards;
	}
	/**
	 * Returns the politic cards offered
	 * @return list of politic cards offered
	 */
	public List<PermitCard> getOfferedPermitCards() {
		return offeredPermitCards;
	}
	/**
	 * Returns the permit cards offered
	 * @return list of permit cards offered
	 */
	public List<PermitCard> getOfferedUsedPermitCards() {
		return offeredUsedPermitCards;
	}
	/**
	 * Adds a politic card, passed as parameter, to the {@link OfferedResources}
	 * @param card
	 */
	public void addPoliticCard(PoliticCard card){
		getOfferedPoliticCards().add(card);
	}
	/**
	 * Adds an unused permit card, passed as parameter, to the {@link OfferedResources}
	 * @param card
	 */
	public void addPermitCard(PermitCard card){
		getOfferedPermitCards().add(card);
	}
	/**
	 * Adds an used permit card, passed as parameter, to the {@link OfferedResources}
	 * @param card
	 */
	public void addUsedPermitCard(PermitCard card){
		getOfferedUsedPermitCards().add(card);
	}
	/**
	 * Removes a politic card, passed as parameter, from the {@link OfferedResources}
	 * @param card
	 */
	public void removePoliticCard(PoliticCard card){
		getOfferedPoliticCards().remove(card);
	}
	/**
	 * Removes an unused permit card, passed as parameter, from the {@link OfferedResources}
	 * @param card
	 */
	public void removePermitCard(PermitCard card){
		getOfferedPermitCards().remove(card);
	}
	/**
	 * Removes an used permit card, passed as parameter, from the {@link OfferedResources}
	 * @param card
	 */
	public void removeUsedPermitCard(PermitCard card){
		getOfferedUsedPermitCards().remove(card);
	}
	/**
	 * Adds an amount of assistants, passed as parameter, to the {@link OfferedResources}
	 * @param assistants : amount of assistants
	 */
	public void addAssistants(int assistants){
		offeredAssistants += assistants;
	}
	/**
	 * Removes an amount of assistants, passed as parameter, from the {@link OfferedResources}
	 * @param assistants : amount of assistants
	 */
	public void removeAssistants(int assistants){
		offeredAssistants -= assistants;
	}
	/**
	 * Resets the offered resources at the end of a market phase
	 */
	public void reset(){
		offeredAssistants = 0;
		offeredPoliticCards = new ArrayList<>();
		offeredPermitCards = new ArrayList<>();
		offeredUsedPermitCards = new ArrayList<>();
	}
}
