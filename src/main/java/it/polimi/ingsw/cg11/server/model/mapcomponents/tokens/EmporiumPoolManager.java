package it.polimi.ingsw.cg11.server.model.mapcomponents.tokens;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * EmporiumPoolManager handles the pool of emporiums created for each player. It
 * makes possible to obtain one emporium from the Pool and add a new one
 * 
 * @author Paola
 * 
 */

public class EmporiumPoolManager {

	private static final int MAX_DUMMY_EMPORIUMS = 9;
	private List<Emporium> emporiums;
	/**
	 * Constructor
	 * @param currentMap
	 * @param emporiumsPerPlayer
	 * @throws NoSuchPlayerException
	 */
	public EmporiumPoolManager(GameMap currentMap, int emporiumsPerPlayer) throws NoSuchPlayerException {

		this.emporiums = new ArrayList<>();
		
		int emporiumNumber;
		List<PlayerState> players = currentMap.getPlayers();
		ListIterator<PlayerState> playersIterator = players.listIterator();

		for (emporiumNumber = 0; emporiumNumber < emporiumsPerPlayer; emporiumNumber++) {
			while(playersIterator.hasNext())
				emporiums.add(
						new Emporium(playersIterator.next().getPlayerData()));
			playersIterator = players.listIterator();
		}
		if(currentMap.getPlayers().size() == 2)
			for (emporiumNumber = 0; emporiumNumber < MAX_DUMMY_EMPORIUMS; emporiumNumber++)
				emporiums.add(new Emporium(null));
	}
	/**
	 * Returns the {@link PlayerState} from the player id
	 * @param playerId
	 * @param currentMap
	 * @return {@link PlayerState}
	 * @throws NoSuchPlayerException
	 */
	public PlayerState getPlayerFromId(int playerId, GameMap currentMap) throws NoSuchPlayerException {

		List<PlayerState> players = currentMap.getPlayers();
		PlayerState player;

		ListIterator<PlayerState> playerIterator = players.listIterator();

		while (playerIterator.hasNext()) {
			player = playerIterator.next();
			if (player.getPlayerData().getPlayerID() == playerId)
				return player;
		}
		throw new NoSuchPlayerException("Player " + playerId + " not found");
	}

	/**
	 * Return a player's emporium. It throws a NoSuchEmporiumException if player
	 * has no more available emporiums.
	 * 
	 * @param playerID
	 * @return emporium {@link Emporium}
	 * @throws NoSuchEmporiumException
	 */
	public Emporium obtainPieceFromPool(int playerID) throws NoSuchEmporiumException {
		
		Emporium emporium;

		int poolSize = emporiums.size();
		int emporiumIndex;

		for (emporiumIndex = 0; emporiumIndex < poolSize; emporiumIndex++) {
			emporium = emporiums.get(emporiumIndex);
			if(playerID < 0 && emporium.getPlayer() == null)
				return emporiums.remove(emporiumIndex);
			if (emporium.getPlayer().getPlayerID() == playerID)
				return emporiums.remove(emporiumIndex);

		}

		throw new NoSuchEmporiumException(String.valueOf(playerID));
	}

}