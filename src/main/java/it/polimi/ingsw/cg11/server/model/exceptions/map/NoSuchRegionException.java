package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;

/**
 * Exception thrown when the searched {@link Region} doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchRegionException extends Exception {

	private static final long serialVersionUID = 478887782582996056L;

	/**
	 * Exception thrown when the searched {@link Region} doesn't exist.
	 */
	public NoSuchRegionException() {
		super();
	}

}
