package it.polimi.ingsw.cg11.server.model.exceptions.tokens;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;

/**
 * This exception is thrown when there are no more player's {@link Emporium}.
 * @author Federico
 *
 */
public class NoSuchEmporiumException extends Exception {

	private static final long serialVersionUID = 8924849625683164387L;

	/**
	 * This exception is thrown when there are no more player's {@link Emporium}.
	 */
	public NoSuchEmporiumException() {
		super();
	}
	/**
	 * This exception is thrown when there are no more player's {@link Emporium}.
	 * @param message
	 */
	public NoSuchEmporiumException(String message) {
		super(message);
	}
}
