package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
/**
 * Represents the action of offering a {@link PoliticCard} from the ones in player's hand, during the market game phase.
 * @author paolasanfilippo
 *
 */
public class OfferPoliticCard extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("MarketOfferingPhase", false);
	private PlayerState player;
	private TurnManager turnManager;
	private MarketOffers marketOffers;
	private PoliticCard cardToOffer;
	private int offerCost;
	private String gameTopic;
	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param marketOffers
	 * @param cardToOffer
	 * @param offerCost
	 * @param gameTopic
	 */
	public OfferPoliticCard(PlayerState player, TurnManager turnManager, MarketOffers marketOffers,
			PoliticCard cardToOffer, int offerCost, String gameTopic) {
		this.player = player;
		this.marketOffers = marketOffers;
		this.cardToOffer = cardToOffer;
		this.offerCost = offerCost;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getMarketActionsHandler().offerPoliticCard(marketOffers,
					cardToOffer, offerCost);
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());
		} catch (ColorNotFoundException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		CardsControls cardsControls = new CardsControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			cardsControls.playerHasPoliticCard(player, cardToOffer);
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoSuchPoliticCardException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}
