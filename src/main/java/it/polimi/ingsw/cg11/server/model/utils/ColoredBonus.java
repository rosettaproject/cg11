package it.polimi.ingsw.cg11.server.model.utils;

import java.awt.Color;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.gameserver.ConnectionsHandler;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.transactions.Transaction;
/**
 * A specific kind of bonus: it represents the bonus linked to the color of the city.
 * It can be earned by the first player who manages to build an emporium on all the cities of that color.
 * @author paolasanfilippo
 *
 */
public class ColoredBonus extends Bonus {

	private static final Logger LOG = Logger.getLogger(ConnectionsHandler.class.getName());

	private Color color;
	private boolean isAvailable;
	/**
	 * 
	 * @param color
	 * @param chainOfCommands
	 */
	public ColoredBonus(Color color, Map<String, Transaction> chainOfCommands) {
		super(chainOfCommands);
		this.color = color;
		this.setAvailable(true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColoredBonus other = (ColoredBonus) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}
	/**
	 * Returns the color
	 * @return color
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * Tells if the colored bonus is available or not
	 * @return <tt>true</tt> if it is available, <tt>false</tt> otherwise
	 */
	public boolean isAvailable() {
		return isAvailable;
	}
	/**
	 * Sets if the colored bonus is available
	 * @param isAvailable
	 */
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	@Override
	public String toString() {
		String toString = "";
		try {
			toString += "ColoredBonus [color=" + ColorMap.getString(color) + ", isAvailable=" + isAvailable + "]\n";
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "Color not found", e);
		}

		toString += super.toString() + "\n";

		return toString;
	}

}
