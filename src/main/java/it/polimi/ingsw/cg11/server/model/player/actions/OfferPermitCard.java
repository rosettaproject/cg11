package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
/**
 * Represents the action of offering a {@link PermitCard} from the used and unused ones in player's hand, during the market game phase.
 * @author paolasanfilippo
 *
 */
public class OfferPermitCard extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("MarketOfferingPhase", false);
	private PlayerState player;
	private TurnManager turnManager;
	private MarketOffers marketOffers;
	private PermitCard cardToOffer;
	private int offerCost;
	private boolean isUsedPermit;
	private String gameTopic;
	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param marketOffers
	 * @param cardToOffer
	 * @param offerCost
	 * @param gameTopic
	 */
	public OfferPermitCard(PlayerState player, TurnManager turnManager, MarketOffers marketOffers,
			PermitCard cardToOffer, int offerCost, String gameTopic) {
		this.player = player;
		this.marketOffers = marketOffers;
		this.cardToOffer = cardToOffer;
		this.offerCost = offerCost;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;
		this.isUsedPermit = false;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		modelChanges = player.getPlayerActionExecutor().getMarketActionsHandler().offerPermitCard(marketOffers,
				cardToOffer, offerCost,isUsedPermit);
		modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
		publish(modelChanges, modelChanges.getTopic());
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		CardsControls cardsControls = new CardsControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			isUsedPermit = cardsControls.checkIfPlayerHasPermitCard(player,cardToOffer);
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoSuchPermitCardException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}