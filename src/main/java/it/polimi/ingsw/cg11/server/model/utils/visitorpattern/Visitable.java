package it.polimi.ingsw.cg11.server.model.utils.visitorpattern;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This interface defines visitable object. Only a visitor can visit these kind
 * of object
 * 
 * @author Federico
 *
 */
public interface Visitable extends Serializable {

	/**
	 * Returns the topic
	 * 
	 * @return topic
	 */
	public String getTopic();

	/**
	 * Allows the {@link ServerVisitor} to call a visit on this object
	 * 
	 * @param visitor
	 */
	public default void accept(ServerVisitor visitor) {
		Logger log = Logger.getLogger(Visitable.class.getName());
		log.log(Level.SEVERE, "Attempted visit from unsupported visitor: " + visitor.getClass().getSimpleName());
	}

	/**
	 * Allows the {@link ClientVisitor} to call a visit on this object.
	 * 
	 * @param visitor
	 */
	public default void accept(ClientVisitor visitor) {
		Logger log = Logger.getLogger(Visitable.class.getName());
		log.log(Level.SEVERE, "Attempted visit from unsupported visitor: " + visitor.getClass().getSimpleName());
	}

	/**
	 * Sets topic
	 * 
	 * @param topic
	 */
	public void setTopic(String topic);

	/**
	 * Returns player's token
	 * 
	 * @return player's token
	 */
	public Token getPlayerToken();

	/**
	 * Sets player's token
	 * 
	 * @param playerToken
	 */
	public void setPlayerToken(Token playerToken);

}
