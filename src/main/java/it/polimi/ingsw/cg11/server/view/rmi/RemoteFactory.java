package it.polimi.ingsw.cg11.server.view.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg11.client.view.connections.ClientView;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.ServerBrokerInterface;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This class defines a remote factory, that represents the stub of the server.
 * Through this class a client can establishes a communication with server and
 * send messages (usually representing actions) to the server
 * 
 * @author Federico
 *
 */
public interface RemoteFactory extends Remote {

	/**
	 * Returns the {@link Broker} that the class uses to send messages
	 * 
	 * @return the {@link Broker}
	 * @throws RemoteException
	 */
	public ServerBrokerInterface<Visitable, String> getBroker() throws RemoteException;

	/**
	 * Creates a server-side {@link RMIView}. With this object the client can
	 * communicates this server through RMI technology
	 * 
	 * @param clientViewStub
	 *            represents the client's skeleton
	 * @param clientIdentifier
	 * @throws RemoteException
	 */
	public void createRMIView(ClientView clientViewStub, Token clientIdentifier) throws RemoteException;

	/**
	 * Removes the {@link RMIView} from the set of {@link RMIView}s. This method
	 * is used when it is necessary to interrupt the communication between
	 * server and correspondent client
	 * 
	 * @param playerTokenToRemove
	 * @throws RemoteException
	 */
	public void deleteRMIView(Token playerTokenToRemove) throws RemoteException;

	/**
	 * Allows a client to publish a {@link Visitable} message that will be
	 * delivered to the current game's controller
	 * 
	 * @param message
	 * @param clientIdentifier
	 *            of the (client) sender
	 * @throws RemoteException
	 */
	public void readMessage(Visitable message, Token clientIdentifier) throws RemoteException;

}
