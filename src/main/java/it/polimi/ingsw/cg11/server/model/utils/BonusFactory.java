package it.polimi.ingsw.cg11.server.model.utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.utils.transactions.AssistantsTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.ExtraBonusTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.MainActionTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.MoneyTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.NobilityTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.PermitBonusTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.PermitCardTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.PoliticCardTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.Transaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.VictoryPointsTransaction;

/**
 * Factory for the {@link Bonus} class. Uses {@link org.json}
 * 
 * @author francesco
 *
 */
public class BonusFactory {

	
	private static final String COLOR = "color";
	
	private static final String[] fieldKeysArray = { "Assistants", "Money", "Nobility", "VictoryPoints", "PoliticCards",
			"ExtraBonus", "MainActions", "ExtraPermitBonus", "ExtraPermit" };
	private HashMap<String, Transaction> chainOfCommands;
	private List<String> fieldKeys;

	private String fieldKey;
	
	/**
	 * A call to this method returns a bonus with the effects specified in
	 * bonusConfig
	 * 
	 * @param bonusConfig
	 * @param currentMap
	 * @return bonus
	 */
	public Bonus createBonus(JSONObject bonusConfig, GameMap currentMap) {

		// for each type of transaction, extract the value
		// and call the constructor, than add the transaction to the chain

		chainOfCommands = new HashMap<>();
		fieldKeys = Arrays.asList(fieldKeysArray);

		ListIterator<String> fieldKeysIterator = fieldKeys.listIterator();

		while (fieldKeysIterator.hasNext()) {
			fieldKey = fieldKeysIterator.next();
			if (bonusConfig.has(fieldKey))
				constructTransaction(bonusConfig, currentMap);
		}

		return new Bonus(chainOfCommands);
	}
	/**
	 * Creates a color city's bonus
	 * @param bonusConfig
	 * @param currentMap
	 * @return Colored bonus
	 * @throws ColorNotFoundException
	 */
	public ColoredBonus createColoredBonus(JSONObject bonusConfig, GameMap currentMap) throws ColorNotFoundException {
	
		Bonus bonus;
		String colorString;
		Color color;
		Map<String, Transaction> bonusChainOfCommands;
		bonus = createBonus(bonusConfig, currentMap);
		bonusChainOfCommands = bonus.getChainOfCommands();
		colorString = bonusConfig.getString(COLOR);
		color = ColorMap.getColor(colorString);

		return new ColoredBonus(color, bonusChainOfCommands);
	}

	/**
	 * Returns a list of {@link Bonus}
	 * 
	 * @param bonusListConfig
	 * @param currentMap
	 * @return
	 */
	public List<Bonus> createListOfBonus(JSONArray bonusListConfig, GameMap currentMap) {

		int index;
		ArrayList<Bonus> bonusList = new ArrayList<>();

		for (index = 0; index < bonusListConfig.length(); index++)
			bonusList.add(createBonus(bonusListConfig.getJSONObject(index), currentMap));

		return bonusList;
	}
	/**
	 * Creates a list of color city's bonuses
	 * @param bonusListConfig
	 * @param currentMap
	 * @return
	 * @throws ColorNotFoundException
	 */
	public List<ColoredBonus> createListOfColoredBonus(JSONArray bonusListConfig, GameMap currentMap) throws ColorNotFoundException{
		
		int index;
		ArrayList<ColoredBonus> bonusList = new ArrayList<>();

		for (index = 0; index < bonusListConfig.length(); index++)
			bonusList.add(createColoredBonus(bonusListConfig.getJSONObject(index), currentMap));

		return bonusList;
	}
	/**
	 * This method creates a transaction of the kind indicated by the "fieldKey"
	 * @param bonusConfig
	 * @param currentMap
	 */
	private void constructTransaction(JSONObject bonusConfig, GameMap currentMap) {

		switch (fieldKey) {

		case "Assistants":
			chainOfCommands.put(fieldKey, new AssistantsTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "Money":
			chainOfCommands.put(fieldKey, new MoneyTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "Nobility":
			chainOfCommands.put(fieldKey, new NobilityTransaction(bonusConfig.getInt(fieldKey), currentMap));
			break;
		case "VictoryPoints":
			chainOfCommands.put(fieldKey, new VictoryPointsTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "PoliticCards":
			chainOfCommands.put(fieldKey,
					new PoliticCardTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "ExtraBonus":
			chainOfCommands.put(fieldKey, new ExtraBonusTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "MainActions":
			chainOfCommands.put(fieldKey, new MainActionTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "ExtraPermitBonus":
			chainOfCommands.put(fieldKey, new PermitBonusTransaction(bonusConfig.getInt(fieldKey)));
			break;
		case "ExtraPermit":
			chainOfCommands.put(fieldKey, new PermitCardTransaction(bonusConfig.getInt(fieldKey)));
			break;
		default:
			break;
		}
	}
}
