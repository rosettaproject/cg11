package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.*;
/**
 * Represents a {@link Hand} of {@link PermitCard}s owned by a player
 * @author paolasanfilippo
 *
 */
public class PermitsHand extends Hand<PermitCard> {


	/**
	 * Initializes the reference to the {@link FaceUpPermits} on the map
	 * 
	 * @param faceUpPermitsList
	 */

	public PermitsHand() {
		super();
	}
	/**
	 * Removes a card from the {@link Hand}
	 * @param permitCard
	 * @return permitCard removed
	 * @throws NoSuchPermitCardException
	 */
	public PermitCard removeCard(PermitCard permitCard) throws NoSuchPermitCardException {
		int index = getCards().indexOf(permitCard);
		if (index >= 0)
			return getCards().remove(index);
		else throw new NoSuchPermitCardException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString;
		Iterator<PermitCard> iterator = getCards().iterator();

		toString = "";
		while (iterator.hasNext())
			toString = toString + iterator.next().toString() + "\n\n";

		return toString;

	}

}