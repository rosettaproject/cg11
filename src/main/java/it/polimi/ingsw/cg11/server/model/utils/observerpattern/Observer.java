package it.polimi.ingsw.cg11.server.model.utils.observerpattern;

/**
 * Observer interface
 * @author Federico
 *
 * @param <M>
 */
@FunctionalInterface
public interface Observer<M> {
	/**
	 * This method is called when the observed object has some update to communicate to
	 * the observers
	 * @param message
	 */
	public void update(M message);

}
