package it.polimi.ingsw.cg11.server.model.player.actionspool;
/**
 * Represent the kind of choices a player could take during the nobility path.
 * See also {@link PlayerChoicesHandler}
 * @author paolasanfilippo
 *
 */
public class PlayerChoice extends GameAction {
	/**
	 * 
	 * @param choiceType
	 */
	public PlayerChoice(String choiceType) {
		super(choiceType);
	}

}
