package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPoliticCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link OfferPoliticCard}
 * @author paolasanfilippo
 *
 */
public class OfferPoliticCardFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
	
		PlayerState player;
		TurnManager turnManager;
		MarketOffers marketOffers;
		PoliticCard politicCard;
		int politicCardIndex;
		int offerCost;

		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData, gameMap, "PlayerID");
			offerCost = actionData.getInt("cost");
			turnManager = gameMap.getTurnManager();
			marketOffers = gameMap.getMarketModel();
			politicCard = createPoliticCardFromActionData(actionData,"politiccard");
			politicCardIndex = player.getPlayerData().getPoliticsHand().indexOf(politicCard);
			
			if(politicCardIndex<0)
				throw new NoSuchPoliticCardException();
			else
				politicCard = player.getPlayerData().getPoliticsHand().getCard(politicCardIndex);

			return new OfferPoliticCard(player, turnManager,marketOffers,politicCard,offerCost, gameMap.getGameTopic());
		} catch (NoSuchPlayerException | ColorNotFoundException | NoSuchPoliticCardException e) {
			throw new IllegalCreationCommandException(e);
		}
	
	}

}
