package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MainAction;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Implements a transaction used for main action
 * @author paolasanfilippo
 *
 */
public class MainActionTransaction implements Transaction {

	private int amount;
	/**
	 * 
	 * @param amount
	 */
	public MainActionTransaction(int amount) {
		this.amount = amount;
	}

	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {

		int index;

		for (index = 0; index < this.amount; index++)
			transactionSubject.getPlayerActionExecutor().addToAvailableActions(new MainAction());
	}

	@Override
	public int getAmount() {
		return this.amount;
	}

}
