package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.awt.Color;
import java.util.*;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.*;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * This class contains the state of the board and players, as well as the
 * current game phase and turn
 * 
 * @author francesco
 *
 */
public class GameMap {

	private String gameTopic; 
	
	private TurnManager turnManager;
	private GamePhaseManager gamePhaseManager;
	private List<PlayerState> players;
	
	private MarketOffers marketModel;
	private GraphedMap graphedMap;
	private List<Region> regions;
	private Balcony kingBalcony;
	private Deck<PoliticCard> firstPoliticsDeck;
	private Deck<PoliticCard> secondPoliticsDeck;
	private NobilityPath nobilityPath;
	private List<Bonus> kingBonuses;
	private CouncillorPoolManager councillorsPool;
	private EmporiumPoolManager emporiumPool;
	private List<City> cities;
	private List<ColoredBonus> cityColorBonusList;
	private King king;

	/**
	 * 
	 * @param startingGamePhase
	 */
	protected GameMap(String startingGamePhase) {
		this.marketModel = new MarketOffers();
		this.gamePhaseManager = new GamePhaseManager(startingGamePhase, getMarketModel());
		this.turnManager = new TurnManager(gamePhaseManager);

		this.secondPoliticsDeck = new Deck<>(new LinkedList<PoliticCard>());
	}

	/**
	 * Gets the {@link TurnManager}
	 * 
	 * @return turn manager
	 */
	public TurnManager getTurnManager() {
		return turnManager;
	}

	/**
	 * Gets the {@link GamePhaseManager}
	 * 
	 * @return game phase manager
	 */
	public GamePhaseManager getGamePhaseManager() {
		return gamePhaseManager;
	}

	/**
	 * Gets the {@link Region}s
	 * 
	 * @return list of regions
	 */
	public List<Region> getRegions() {
		return regions;
	}

	/**
	 * Gets the king's {@link Balcony}
	 * 
	 * @return king's balcony
	 */
	public Balcony getKingBalcony() {
		return this.kingBalcony;
	}

	/**
	 * Gets the Politic {@link Deck}
	 * 
	 * @return a deck of politic cards
	 */
	public Deck<PoliticCard> getPoliticsDeck() {
		return firstPoliticsDeck;
	}

	/**
	 * Gets the {@link NobilityPath}
	 * 
	 * @return nobility path
	 */
	public NobilityPath getNobilityPath() {
		return this.nobilityPath;
	}

	/**
	 * Gets the king's {@link Bonus}es
	 * 
	 * @return king's bonuses
	 */
	public List<Bonus> getKingBonuses() {
		return this.kingBonuses;
	}

	/**
	 * Gets the {@link CouncillorPoolManager}
	 * 
	 * @return councillor pool manager
	 */
	public CouncillorPoolManager getCouncillorsPool() {
		return this.councillorsPool;
	}

	/**
	 * Gets the {@link EmporiumPoolManager}
	 * 
	 * @return emporium pool manager
	 */
	public EmporiumPoolManager getEmporiumPool() {
		return this.emporiumPool;
	}

	/**
	 * Gets the {@link City}s
	 * 
	 * @return list of cities
	 */
	public List<City> getCities() {
		return this.cities;
	}

	/**
	 * Gets the players
	 * 
	 * @return list of {@link PlayerState}
	 */
	public List<PlayerState> getPlayers() {
		return this.players;
	}

	/**
	 * Gets the {@link King}
	 * 
	 * @return king
	 */
	public King getKing() {
		return this.king;
	}

	/**
	 * Gets second politic {@link Deck}
	 * 
	 * @return a deck of politic cards
	 */
	public Deck<PoliticCard> getSecondPoliticsDeck() {
		return secondPoliticsDeck;
	}
	/**
	 * Returns the {@link MarketOffers}
	 * @return the market model
	 */
	public MarketOffers getMarketModel() {
		return marketModel;
	}

	/**
	 * Sets the {@link TurnManager}
	 */
	protected void setTurnManager() {
		this.turnManager.setPlayers(players);
	}

	/**
	 * Sets the {@link Region}s
	 * 
	 * @param regions
	 */
	protected void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	/**
	 *
	 * Sets the {@link King}'s {@link Balcony}
	 * 
	 * @param mapBalcony
	 *            : balcony we want to be the new king's balcony
	 */
	protected void setKingBalcony(Balcony mapBalcony) {
		this.kingBalcony = mapBalcony;
	}

	/**
	 * Sets the politic {@link Deck}
	 * 
	 * @param politicsDeck
	 */
	protected void setPoliticsDeck(Deck<PoliticCard> politicsDeck) {
		this.firstPoliticsDeck = politicsDeck;
	}

	/**
	 * Sets the {@link NobilityPath}
	 * 
	 * @param nobilityPath
	 */
	protected void setNobilityPath(NobilityPath nobilityPath) {
		this.nobilityPath = nobilityPath;
	}

	/**
	 * Sets the {@link King}'s {@link Bonus}es
	 * 
	 * @param kingBonuses
	 */
	protected void setKingBonuses(List<Bonus> kingBonuses) {
		this.kingBonuses = kingBonuses;
	}

	/**
	 * Sets the {@link CouncillorPoolManager}
	 * 
	 * @param councillorsPool
	 */
	protected void setCouncillorsPool(CouncillorPoolManager councillorsPool) {
		this.councillorsPool = councillorsPool;
	}

	/**
	 * Sets the {@link EmporiumPoolManager}
	 * 
	 * @param emporiumPool
	 */
	protected void setEmporiumPool(EmporiumPoolManager emporiumPool) {
		this.emporiumPool = emporiumPool;
	}

	/**
	 * Sets the {@link City}s
	 * 
	 * @param cities
	 */
	protected void setCities(List<City> cities) {
		this.cities = cities;
	}

	/**
	 * Sets the players
	 * 
	 * @param players
	 */
	protected void setPlayers(List<PlayerState> players) {
		this.players = players;
	}

	/**
	 * Sets the {@link King}
	 * @param king
	 */
	protected void setKing(King king) {
		this.king = king;
	}
	/**
	 * Gets the {@link GraphedMap}
	 * @return graphed map
	 */
	public GraphedMap getGraphedMap() {
		return graphedMap;
	}
	/**
	 * Sets the {@link GraphedMap}
	 * @param graphedMap
	 */
	public void setGraphedMap(GraphedMap graphedMap) {
		this.graphedMap = graphedMap;
	}
	/**
	 * Gets the city's color bonus.If the color is not found, it throws {@link ColorNotFoundException}
	 * @param color
	 * @return {@link ColoredBonus}
	 * @throws ColorNotFoundException
	 */
	public ColoredBonus getCityColorBonus(Color color) throws ColorNotFoundException {
		ColoredBonus coloredBonus;
		Iterator<ColoredBonus> iterator = cityColorBonusList.iterator();
		while (iterator.hasNext()) {
			coloredBonus = iterator.next();
			if (coloredBonus.getColor().equals(color)) {
				return coloredBonus;
			}
		}
		throw new ColorNotFoundException();
	}
	/**
	 * Sets a list of city's color bonuses
	 * @param cityColorBonusList
	 */
	public void setCityColorBonusList(List<ColoredBonus> cityColorBonusList) {
		this.cityColorBonusList = cityColorBonusList;
	}
	/**
	 * Returns the game topic
	 * @return game topic
	 */
	public String getGameTopic() {
		return gameTopic;
	}
	/**
	 * Sets the game topic
	 * @param gameTopic
	 */
	protected void setGameTopic(String gameTopic) {
		this.gameTopic = gameTopic;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "\nGameMap (topic: " + gameTopic + " )\n\n";
		
		Iterator<PlayerState> playerIterator = players.iterator();
		PlayerState tmpPlayer;
		
		Iterator<Region> regionIterator = regions.iterator();
		Region tmpRegion;
		
		Iterator<ColoredBonus> coloredBonusIterator = cityColorBonusList.iterator();
		ColoredBonus tmpColoredBonus;
		
		
		toString += "Players\n\n";
		while(playerIterator.hasNext()){
			tmpPlayer = playerIterator.next();
			
			toString += tmpPlayer.getPlayerData().toString();
		}
		
		toString += "\n\nRegions\n\n";
		while(regionIterator.hasNext()){
			tmpRegion = regionIterator.next();
			
			toString += tmpRegion.toString();
		}
		
		toString += kingBalcony.toString();
		
		toString += "\nKing City\n";
		toString += king.getCurrentCity().getName() + "\n\n";
		
		toString += "\nColored bonus\n";
		while(coloredBonusIterator.hasNext()){
			tmpColoredBonus = coloredBonusIterator.next();
			
			toString += tmpColoredBonus.toString();
		}
		
		toString += nobilityPath.toString();
		
		return toString;
	}
	
	
}
