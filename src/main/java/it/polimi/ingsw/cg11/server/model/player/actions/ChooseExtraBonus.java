package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.bonus.BonusControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.CityControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.IllegalBonusTypeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.NobilityPath;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
/**
 * Represents one of the bonuses of the {@link NobilityPath}. It gives to the player 
 * the opportunity to choose the bonus of one of the cities, on which he/she has built
 * (except for the nobility bonus)
 * @author paolasanfilippo
 *
 */
public class ChooseExtraBonus extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);
	private PlayerState player;
	private TurnManager turnManager;
	private City city;
	private String gameTopic;
	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param city
	 * @param gameTopic
	 */
	public ChooseExtraBonus(PlayerState player, TurnManager turnManager, City city, String gameTopic) {
		this.player = player;
		this.turnManager = turnManager;
		this.city = city;
		this.gameTopic = gameTopic;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getPlayerChoicesHandler()
					.chooseAnExtraBonus(city.getCityBonus());

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}
	
	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		CityControls cityControls = new CityControls();
		BonusControls bonusControls = new BonusControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			cityControls.emporiumIsPresentControl(city, player);
			bonusControls.isNotNobilityBonus(city.getCityBonus());
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoSuchEmporiumException
				| IllegalBonusTypeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}
