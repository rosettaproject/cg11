package it.polimi.ingsw.cg11.server.model.exceptions.map;
/**
 * Exception thrown when it is impossible to create the map.
 * @author paolasanfilippo
 *
 */
public class CannotCreateMapException extends Exception {

	private static final long serialVersionUID = 3938649688251633693L;

	/**
	 * Exception thrown when it is impossible to create the map.
	 */
	public CannotCreateMapException() {
		super();
	}

	/**
	 * Exception thrown when it is impossible to create the map.
	 * @param cause
	 */
	public CannotCreateMapException(Throwable cause) {
		super(cause);
	}
}
