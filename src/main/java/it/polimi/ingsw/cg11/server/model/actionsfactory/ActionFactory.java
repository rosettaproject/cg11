package it.polimi.ingsw.cg11.server.model.actionsfactory;

import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticsDeckFactory;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.market.NoSuchOfferException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.market.MarketOffer;
import it.polimi.ingsw.cg11.server.model.market.MarketOfferFactory;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Abstract class to create an action, an object that implements
 * {@link Performable}. Extend this class to create a concrete factory to create
 * a specific action. Also contains general methods to acquire the needed
 * references from a game map to construct an action.
 * 
 * @author francesco
 *
 */
public abstract class ActionFactory {

	private static final String KING_BALCONY_NAME = "King";

	/**
	 * Creates a new game action from a JSONObject containing the action
	 * parameters. The game map passed as a parameter allows this method to
	 * acquire the necessary references to objects contained in such map to
	 * construct the action. This method does not in any way modify the current
	 * state of the game map.Throws a {@link IllegalCreationCommandException} if the command doesn't correspond to a legal action
	 * 
	 * @param action : the serializable action we want to create
	 * @param gameMap 
	 * @return a {@link Performable} action
	 * @throws IllegalCreationCommandException
	 */
	public abstract Performable createAction(SerializableAction action, GameMap gameMap)
			throws IllegalCreationCommandException;

	/**
	 * Returns the first {@link PlayerState} among the players list with a
	 * PlayerID equal to the one passed as a parameter
	 * 
	 * @param players : list of players in which we want to find a player
	 * @param playerID : player we're looking for
	 * @return player
	 * @throws NoSuchPlayerException
	 *             if there is not a player with playerID passed
	 */
	public PlayerState getPlayerFromID(List<PlayerState> players, int playerID) throws NoSuchPlayerException {
		PlayerState currentPlayer;
		int currentPlayerID;

		Iterator<PlayerState> playerIterator = players.iterator();
		while (playerIterator.hasNext()) {
			currentPlayer = playerIterator.next();
			currentPlayerID = currentPlayer.getPlayerData().getPlayerID();
			if (currentPlayerID == playerID)
				return currentPlayer;
		}
		throw new NoSuchPlayerException();
	}

	/**
	 * Returns the first {@link Region} among the regions list with a region
	 * name equal to the one passed as a parameter
	 * 
	 * @param regions
	 *            list of regions of the map
	 * @param regionNameRequested
	 * @return region
	 * @throws NoSuchRegionException
	 *             if if there is not a region with region name passed
	 */
	public Region getRegionFromName(List<Region> regions, String regionNameRequested) throws NoSuchRegionException {
		Region currentRegion;
		String currentRegionName;

		Iterator<Region> regionIterator = regions.iterator();
		while (regionIterator.hasNext()) {
			currentRegion = regionIterator.next();
			currentRegionName = currentRegion.getName();

			if (currentRegionName.equalsIgnoreCase(regionNameRequested))
				return currentRegion;
		}
		throw new NoSuchRegionException();
	}

	/**
	 * Returns the first {@link City} among the city list with a city name equal
	 * to the one passed as a parameter
	 * 
	 * @param cities
	 *            list of cities of the map
	 * @param cityNameRequested
	 * @return city
	 * @throws NoSuchCityException
	 *             if there is not a city with city name passed
	 */
	public City getCityFromName(List<City> cities, String cityNameRequested) throws NoSuchCityException {
		City currentCity;
		String currentCityName;

		Iterator<City> cityIterator = cities.iterator();
		while (cityIterator.hasNext()) {
			currentCity = cityIterator.next();
			currentCityName = currentCity.getName();

			if (currentCityName.equalsIgnoreCase(cityNameRequested))
				return currentCity;
		}
		throw new NoSuchCityException();
	}

	

	/**
	 * Returns the balcony associated with the region whose name is given as a
	 * parameter. If there is no such region, returns null. A region always has
	 * a balcony, so if the return value is null, it means that the region
	 * itself is not present in this map
	 * 
	 * @param regionName
	 * @param gameMap
	 * @return balcony 
	 * @throws NoSuchRegionException
	 */
	public Balcony getBalconyFromRegionName(String regionName, GameMap gameMap) throws NoSuchRegionException {

		List<Region> regionList = gameMap.getRegions();

		for (Region region : regionList) {
			if (region.getName().equalsIgnoreCase(regionName))
				return region.getBalcony();
		}
		throw new NoSuchRegionException();
	}

	/**
	 * Gets the reference of a {@link PlayerState} contained in the
	 * {@link GameMap} passed as a parameter from a JSONObject containing it's
	 * ID under the specified key
	 * 
	 * @param actionData
	 * @param gameMap
	 * @return the requested player
	 * @throws NoSuchPlayerException
	 *             if no player with the specified ID exists on the map
	 */
	protected PlayerState getPlayerFromActionData(JSONObject actionData, GameMap gameMap, String key)
			throws NoSuchPlayerException {

		PlayerState player;
		int playerID;

		playerID = actionData.getInt(key);
		player = getPlayerFromID(gameMap.getPlayers(), playerID);

		return player;
	}

	/**
	 * Gets the reference of a {@link City} contained in the {@link GameMap}
	 * passed as a parameter from a JSONObject containing it's name under the
	 * specified key
	 * 
	 * @param actionData
	 * @param gameMap
	 * @param key
	 * @return the requested city
	 * @throws NoSuchCityException
	 *             if no city with the specified name exists on the map
	 */
	protected City getCityFromActionData(JSONObject actionData, GameMap gameMap, String key)
			throws NoSuchCityException {

		City city;
		String cityName;

		cityName = actionData.getString(key);
		city = getCityFromName(gameMap.getCities(), cityName);

		return city;
	}

	/**
	 * Returns a new list of {@link PoliticCard}s as listed in the JSONObject
	 * passed as a parameter. Does not reference in any way any object contained
	 * in the game map, since it creates all the cards using a new
	 * {@link PoliticsDeckFactory}
	 * 
	 * @param actionData
	 * @param gameMap
	 * @param key
	 * @return a list containing the politic cards
	 * @throws ColorNotFoundException
	 *             if the color is either null or not appropriate for a
	 *             {@link PoliticCard}
	 */
	protected List<PoliticCard> createPoliticCardsFromActionData(JSONObject actionData, String key)
			throws ColorNotFoundException {

		PoliticsDeckFactory deckFactory;
		CardsControls cardsControls;
		List<PoliticCard> politicCards;

		deckFactory = new PoliticsDeckFactory();
		cardsControls = new CardsControls();

		politicCards = deckFactory.createCardList(actionData.getJSONArray(key));
		cardsControls.cardColorsConsistencyCheck(politicCards);

		return politicCards;
	}

	/**
	 * Gets the reference of a {@link Balcony} contained in the {@link GameMap}
	 * passed as a parameter whose name matches the one contained in the
	 * JSONObject under the specified key
	 * 
	 * @param actionData
	 * @param gameMap
	 * @param key
	 * @return balcony
	 * @throws NoSuchRegionException
	 *             if there is no region containing the specified balcony and it
	 *             is not the king's
	 */
	protected Balcony getBalconyFromActionData(JSONObject actionData, GameMap gameMap, String key)
			throws NoSuchRegionException {

		String balconyName;
		Balcony balcony;

		balconyName = actionData.getString(key);

		if (balconyName.equalsIgnoreCase(KING_BALCONY_NAME))
			balcony = gameMap.getKingBalcony();
		else
			balcony = getBalconyFromRegionName(balconyName, gameMap);

		return balcony;
	}

	/**
	 * Gets the reference of a {@link Region} contained in the {@link GameMap}
	 * passed as a parameter from a JSONObject containing it's name under the
	 * specified key
	 * 
	 * @param actionData
	 * @param gameMap
	 * @param key
	 * @return region
	 * @throws NoSuchRegionException
	 */
	protected Region getRegionFromActionData(JSONObject actionData, GameMap gameMap, String key)
			throws NoSuchRegionException {

		String regionName;
		Region region;

		regionName = actionData.getString(key);
		region = getRegionFromName(gameMap.getRegions(), regionName);

		return region;
	}

	/**
	 * Returns a new {@link PermitCard} configured as listed in the JSONObject
	 * passed as a parameter. Does not reference in any way any object contained
	 * in the game map, since it creates the card using a
	 * {@link PermitsDeckFactory}
	 * 
	 * @param actionData
	 * @param gameMap
	 * @param regionKey
	 * @param permitKey
	 * @return permitCard
	 * @throws NoSuchRegionException
	 */
	protected PermitCard createPermitCardFromActionData(JSONObject actionData, GameMap gameMap, String regionKey,
			String permitKey) throws NoSuchRegionException {

		PermitsDeckFactory permitsDeckFactory;
		JSONArray permitConfig;
		Region region;
		PermitCard permitCard;

		permitsDeckFactory = new PermitsDeckFactory();
		permitConfig = actionData.getJSONArray(permitKey);
		region = getRegionFromActionData(actionData, gameMap, regionKey);
		permitCard = permitsDeckFactory.createPermitCardFromConfig(permitConfig, region, gameMap);

		return permitCard;
	}
	/**
	 * 
	 * @param actionData
	 * @param key
	 * @return politicCard
	 * @throws ColorNotFoundException
	 */
	protected PoliticCard createPoliticCardFromActionData(JSONObject actionData,String key) throws ColorNotFoundException{
		
		PoliticsDeckFactory politicsDeckFactory;
		PoliticCard politicCard;
		
		politicsDeckFactory = new PoliticsDeckFactory();
		politicCard = politicsDeckFactory.createPoliticCardFromConfig(actionData.getJSONObject(key));
		
		return politicCard;
	}
	/**
	 * Returns the reference to the first {@link MarketOffer} that equals a market offer created with the parameters
	 * contained in the {@link JSONObject} actionData
	 * @param actionData
	 * @param gameMap
	 * @return the requested market offer
	 * @throws NoSuchOfferException
	 */
	protected MarketOffer getMarketOfferFromActionData(JSONObject actionData, GameMap gameMap)
			throws NoSuchOfferException {

		MarketOffer marketOffer;
		int offerIndex;
		
		try {
			marketOffer = createMarketOfferFromActionData(actionData,gameMap);
		} catch (IllegalCreationCommandException e) {
			throw new NoSuchOfferException(e);
		}
		
		if (gameMap.getMarketModel().getOffers().contains(marketOffer)) {
			offerIndex = gameMap.getMarketModel().getOffers().indexOf(marketOffer);
			marketOffer = gameMap.getMarketModel().getOffers().get(offerIndex);
		} else
			throw new NoSuchOfferException();
		return marketOffer;

	}

	/**
	 * Uses the {@link JSONObject} actionData to create a new {@link MarketOffer} with the specified parameters:
	 * "OfferType" can be one of the strings "AssistantsOffer","PoliticCardOffer" or "PermitCardOffer".
	 * "OfferingPlayer" must contain the PlayerID of a player present in the current {@link GameMap}.
	 * "OfferCost" contains an integer representing the cost of the offer
	 * @param actionData
	 * @param gameMap
	 * @return a new market offer of the specified type
	 * @throws IllegalCreationCommandException
	 */
	protected MarketOffer createMarketOfferFromActionData(JSONObject actionData, GameMap gameMap) throws IllegalCreationCommandException {

		String offerType;
		MarketOffer marketOffer;
		PlayerState offeringPlayer;
		int offerCost;

		try {
			offerType = actionData.getString("OfferType");
			offeringPlayer = getPlayerFromActionData(actionData, gameMap, "OfferingPlayer");
			offerCost = actionData.getInt("OfferCost");

			switch (offerType) {
			case "AssistantsOffer":
				marketOffer = createAssistantsOffer(actionData, offeringPlayer, offerCost);
				break;
			case "PoliticCardOffer":
				marketOffer = createPoliticCardOffer(actionData, offeringPlayer, offerCost);
				break;
			case "PermitCardOffer":
				marketOffer = createPermitCardOffer(actionData, gameMap, offeringPlayer, offerCost);
				break;
			default:
				throw new IllegalCreationCommandException();
			}
			
			return marketOffer;
		} catch (ColorNotFoundException | NoSuchRegionException | NoSuchPlayerException e) {
			throw new IllegalCreationCommandException(e);
		}
	}
	/**
	 * 
	 * @param offerConfig
	 * @param offeringPlayer
	 * @param offerCost
	 * @return market offer
	 */
	private MarketOffer createAssistantsOffer(JSONObject offerConfig, PlayerState offeringPlayer, int offerCost) {

		MarketOfferFactory marketOfferFactory;
		int assistants;

		marketOfferFactory = new MarketOfferFactory();
		assistants = offerConfig.getInt("assistants");

		return marketOfferFactory.createMarketOffer(offeringPlayer, assistants, offerCost);

	}
	/**
	 * 
	 * @param offerConfig
	 * @param offeringPlayer
	 * @param offerCost
	 * @return market offer
	 * @throws ColorNotFoundException
	 */
	private MarketOffer createPoliticCardOffer(JSONObject offerConfig, PlayerState offeringPlayer, int offerCost)
			throws ColorNotFoundException {

		MarketOfferFactory marketOfferFactory;
		PoliticCard politicCard;

		marketOfferFactory = new MarketOfferFactory();
		politicCard = createPoliticCardFromActionData(offerConfig,"politiccard");

		return marketOfferFactory.createMarketOffer(offeringPlayer, politicCard, offerCost);

	}
	/**
	 * 
	 * @param offerConfig
	 * @param gameMap
	 * @param offeringPlayer
	 * @param offerCost
	 * @return market offer
	 * @throws NoSuchRegionException
	 */
	private MarketOffer createPermitCardOffer(JSONObject offerConfig, GameMap gameMap, PlayerState offeringPlayer,
			int offerCost) throws NoSuchRegionException {

		MarketOfferFactory marketOfferFactory;
		PermitCard permitCard;

		marketOfferFactory = new MarketOfferFactory();
		permitCard = createPermitCardFromActionData(offerConfig, gameMap, "region", "permitcard");

		return marketOfferFactory.createMarketOffer(offeringPlayer, permitCard, offerCost);

	}
}
