package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actions.ChangePermitCards;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link ChangePermitCards}
 * @author paolasanfilippo
 *
 */
public class ChangePermitCardsFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {

		PlayerState player;
		TurnManager turnManager;
		Region region;

		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData, gameMap, "PlayerID");
			turnManager = gameMap.getTurnManager();
			region = getRegionFromActionData(actionData, gameMap, "region");

			return new ChangePermitCards(player, turnManager, region, gameMap.getGameTopic());
		} catch (NoSuchPlayerException | NoSuchRegionException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
