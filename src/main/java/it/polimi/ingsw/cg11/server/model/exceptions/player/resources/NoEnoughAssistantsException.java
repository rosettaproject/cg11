package it.polimi.ingsw.cg11.server.model.exceptions.player.resources;

import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;

/**
 * Exception thrown when the amount of {@link Assistants} isn't enough.
 * @author paolasanfilippo
 *
 */
public class NoEnoughAssistantsException extends Exception {

	private static final long serialVersionUID = 2935860184207015937L;

	/**
	 * Exception thrown when the amount of {@link Assistants} isn't enough.
	 */
	public NoEnoughAssistantsException() {
		super();
	}
}
