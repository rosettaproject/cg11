package it.polimi.ingsw.cg11.server.model.player.actionspool;

import java.awt.Color;
import java.util.List;


import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.AvailableMandatoryPlayerChoiceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAvailableQuickActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actions.ChangePermitCards;
import it.polimi.ingsw.cg11.server.model.player.actions.ElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.server.model.player.actions.EngageAnAssistant;
import it.polimi.ingsw.cg11.server.model.player.actions.OneMoreMainAction;

/**
 * Contains the methods to perform all the quick actions
 * 
 * @author francesco
 *
 */
public class QuickActionsHandler {

	private static final int MAIN_ACTION_COST = 3;
	private static final int QUICK_ELECTION_COST = 1;
	private static final int SHUFFLE_BACK_COST = 1;
	private static final int ASSISTANT_COST = 3;

	public static final String ACTOR_KEY = "ActorID";
	public static final String ASSISTANT_COST_KEY = "AssistantsCost";
	public static final String SECOND_PERMIT_KEY = "SecondPermit";
	public static final String REGION_NAME_KEY = "RegionName";
	public static final String FIRST_PERMIT_KEY = "FirstPermit";
	public static final String ASSISTANT_KEY = "Assistant";
	public static final String MONEY_COST_KEY = "MoneyCost";
	public static final String ACTION_KEY = "Action";
	public static final String COUNCILLOR_KEY = "Councillor";
	public static final String BALCONY_KEY = "BalconyName";

	private PlayerState player;
	private QuickAction quickAction;
	private PlayerChoice playerChoice;
	private List<GameAction> availableActions;
	/**
	 * 
	 * @param player
	 */
	public QuickActionsHandler(PlayerState player) {
		this.player = player;
		this.quickAction = new QuickAction();
		this.playerChoice = new PlayerChoice("Generic");
		this.availableActions = player.getPlayerActionExecutor().getAvailableActions();
	}

	/**
	 * Engages an assistant from the pool for 3 coins.
	 * 
	 * @return model changes
	 * 
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges hireOneAssistant() throws IllegalStateChangeException {
		ModelChanges modelChanges;

		if (!availableActions.contains(quickAction))
			throw new IllegalStateChangeException(new NotAvailableQuickActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());

		try {
			player.getPlayerData().getMoney().spend(ASSISTANT_COST);
			player.getPlayerData().getAssistants().gain(1);
			availableActions.remove(quickAction);

			modelChanges = new ModelChanges();

			addHireOneAssistant(modelChanges, ASSISTANT_COST);

			return modelChanges;
		} catch (IllegalResourceException e) {
			throw new IllegalStateChangeException(e);
		}

	}

	/**
	 * Returns the two face up permit cards to the bottom of the
	 *             corresponding deck and draws two new ones from the top of the
	 *             deck. Costs 1 assistant
	 * @param region : region in which the player wants to change the face up permits
	 * @return model changes
	 * @throws IllegalStateChangeException 
	 */
	public ModelChanges changePermitCard(Region region) throws IllegalStateChangeException {
		ModelChanges modelChanges;

		if (!availableActions.contains(quickAction))
			throw new IllegalStateChangeException(new NotAvailableQuickActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());
		try {
			player.getPlayerData().getAssistants().use(SHUFFLE_BACK_COST);
			region.getFaceUpPermitCards().shuffleBack();
			availableActions.remove(quickAction);

			modelChanges = new ModelChanges();

			PermitCard newFirstPermit = region.getFaceUpPermitCards().getPermit(0);
			PermitCard newSeondPermit = region.getFaceUpPermitCards().getPermit(1);

			addChangePermitCard(modelChanges, region.getName(), SHUFFLE_BACK_COST, newFirstPermit, newSeondPermit);

			return modelChanges;

		} catch (NoSuchFaceUpPermitsException | IllegalResourceException e) {
			throw new IllegalStateChangeException(e);
		}
	}

	/**
	 * Takes a councillor and inserts it in a balcony. Player doesn't earn any
	 * coins by performing this action and he/she spend one assistant
	 * 
	 * @param color
	 * @param councillorPoolManager
	 * @param balcony
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges electCouncilorWithOneAssistant(Color color, CouncillorPoolManager councillorPoolManager,
			Balcony balcony) throws IllegalStateChangeException {

		ModelChanges modelChanges;
		if (!availableActions.contains(quickAction))
			throw new IllegalStateChangeException(new NotAvailableQuickActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());
		try {
			player.getPlayerData().getAssistants().use(QUICK_ELECTION_COST);
			balcony.addCouncillor(color, councillorPoolManager);
			availableActions.remove(quickAction);

			modelChanges = new ModelChanges();

			addElectCouncilorWithOneAssistant(modelChanges, color, balcony);

			return modelChanges;

		} catch (IllegalResourceException | NoSuchCouncillorException | ColorNotFoundException e) {
			throw new IllegalStateChangeException(e);
		}

	}

	/**
	 * Makes player able to perform another one main action. Costs 3
	 * assistants.
	 * 
	 * @return model changes
	 * 
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges performAnAdditionalMainAction() throws IllegalStateChangeException {
		ModelChanges modelChanges;

		if (!availableActions.contains(quickAction))
			throw new IllegalStateChangeException(new NotAvailableQuickActionException());
		if (availableActions.contains(playerChoice))
			throw new IllegalStateChangeException(new AvailableMandatoryPlayerChoiceException());
		try {
			player.getPlayerData().getAssistants().use(MAIN_ACTION_COST);
			availableActions.add(new MainAction());
			availableActions.remove(quickAction);

			modelChanges = new ModelChanges();

			addPerformAnAdditionalMainActionChanges(modelChanges, MAIN_ACTION_COST);

			return modelChanges;
		} catch (IllegalResourceException e) {
			throw new IllegalStateChangeException(e);
		}

	}
	/**
	 * Adds to changes the {@link OneMoreMainAction}
	 * @param modelChanges
	 * @param mainActionCost with the key  "AssistantsCost"
	 */
	private void addPerformAnAdditionalMainActionChanges(ModelChanges modelChanges, int mainActionCost) {

		modelChanges.addToChanges(ACTION_KEY, "PerformAnAdditionalMainAction");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(ASSISTANT_COST_KEY, mainActionCost);
	}
	/**
	 * Adds to changes the {@link ElectCouncillorWithOneAssistant}
	 * @param modelChanges
	 * @param color with the key "Councillor"
	 * @param balcony with the key "BalconyName"
	 * @throws ColorNotFoundException
	 */
	private void addElectCouncilorWithOneAssistant(ModelChanges modelChanges, Color color, Balcony balcony)
			throws ColorNotFoundException {

		modelChanges.addToChanges(ACTION_KEY, "ElectCouncillorWithOneAssistant");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(COUNCILLOR_KEY, color);
		modelChanges.addToChanges(BALCONY_KEY, balcony.getNAME());
	}
	/**
	 * Adds to changes the {@link ChangePermitCards}
	 * @param modelChanges
	 * @param regionName with the key "RegionName"
	 * @param shuffleBackCost with the key "AssistantsCost"
	 * @param newFirstPermit with the key "FirstPermit"
	 * @param newSeondPermit with the key "SecondPermit"
	 */
	private void addChangePermitCard(ModelChanges modelChanges, String regionName, int shuffleBackCost,
			PermitCard newFirstPermit, PermitCard newSeondPermit) {

		modelChanges.addToChanges(ACTION_KEY, "ChangePermitCard");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(ASSISTANT_COST_KEY, shuffleBackCost);
		modelChanges.addToChanges(REGION_NAME_KEY, regionName);
		modelChanges.addToChanges(FIRST_PERMIT_KEY, newFirstPermit);
		modelChanges.addToChanges(SECOND_PERMIT_KEY, newSeondPermit);
	}
	/**
	 * Adds to changes the {@link EngageAnAssistant}
	 * @param modelChanges
	 * @param assistantCost with the key "MoneyCost"
	 */
	private void addHireOneAssistant(ModelChanges modelChanges, int assistantCost) {

		modelChanges.addToChanges(ACTION_KEY, "HireOneAssistant");
		modelChanges.addToChanges(ACTOR_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(MONEY_COST_KEY, assistantCost);
		modelChanges.addToChanges(ASSISTANT_KEY, 1);
	}
}
