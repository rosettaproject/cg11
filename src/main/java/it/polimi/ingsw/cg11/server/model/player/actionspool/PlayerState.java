package it.polimi.ingsw.cg11.server.model.player.actionspool;

import it.polimi.ingsw.cg11.server.model.player.resources.PlayerData;
/**
 * Contains references to reach all the model elements related to the state of a player
 * @author francesco
 *
 */
public class PlayerState {

	private final PlayerData playerData;
	private PlayerActionExecutor playerActionExecutor;
	private boolean isAllowedToPlay;
	/**
	 * 
	 * @param playerData
	 */
	public PlayerState(PlayerData playerData) {
		this.playerData = playerData;
		this.isAllowedToPlay = false;
		this.playerActionExecutor = new PlayerActionExecutor(this);
		this.playerActionExecutor.setActionHandlers();
	}
	/**
	 * Tells if the player is allowed to play or not
	 * @return <tt>true</tt> if the player can actually play now, <tt>false</tt> otherwise
	 */
	public boolean isAllowedToPlay() {
		return isAllowedToPlay;
	}
	/**
	 * Sets if a player is allowed to play or not
	 * @param isAllowedToPlay
	 */
	public void setAllowedToPlay(boolean isAllowedToPlay) {
		this.isAllowedToPlay = isAllowedToPlay;
	}
	/**
	 * Returns the {@link PlayerData}
	 * @return player's data
	 */
	public PlayerData getPlayerData() {
		return playerData;
	}
	/**
	 * Returns the {@link PlayerActionExecutor}
	 * @return playerActionExecutor
	 */
	public PlayerActionExecutor getPlayerActionExecutor() {
		return playerActionExecutor;
	}
	
	@Override
	public String toString() {
		return playerData.toString();
	}
}
