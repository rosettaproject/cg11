package it.polimi.ingsw.cg11.server.model.player.actionspool;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.market.NoSuchOfferException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.market.MarketOffer;
import it.polimi.ingsw.cg11.server.model.market.MarketOfferFactory;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.AcceptMarketOffer;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferAssistants;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPoliticCard;
/**
 * Handles the offers and the accept action
 * @author paolasanfilippo
 *
 */
public class MarketActionsHandler {

	public static final String ACTOR_ID_KEY = "ActorID";
	public static final String ACTION_KEY = "Action";
	public static final String OFFERED_ITEM_KEY = "OfferedItem";
	public static final String OFFER_COST_KEY = "OfferCost";
	private PlayerState player;
	private MarketOfferFactory marketOfferFactory;
	/**
	 * 
	 * @param player
	 */
	public MarketActionsHandler(PlayerState player) {
		this.player = player;
		this.marketOfferFactory = new MarketOfferFactory();
	}
	/**
	 * Adds to the {@link MarketOffers} a {@link OfferPoliticCard} 
	 * @param marketOffers
	 * @param offeredCard
	 * @param offerCost : cost set for the offer
	 * @return modelchanges
	 * @throws ColorNotFoundException
	 */
	public ModelChanges offerPoliticCard(MarketOffers marketOffers, PoliticCard offeredCard, int offerCost) throws ColorNotFoundException{
		
		player.getPlayerData().getOfferedResources().addPoliticCard(offeredCard);
		marketOffers.addOffer(marketOfferFactory.createMarketOffer(player, offeredCard, offerCost));
		
		ModelChanges modelChanges = new ModelChanges();
		
		addOfferPoliticCardChanges(modelChanges,offeredCard,offerCost);
		
		return modelChanges;
	}

	/**
	 * Adds to the {@link MarketOffers} a {@link OfferPermitCard} 
	 * @param marketOffers
	 * @param offeredCard
	 * @param offerCost : cost set for the offer
	 * @param isUsed : boolean that tells if the offering player has already used that permitcard
	 * @return model changes
	 */
	public ModelChanges offerPermitCard(MarketOffers marketOffers, PermitCard offeredCard, int offerCost,boolean isUsed){
		
		if(!isUsed)
			player.getPlayerData().getOfferedResources().addPermitCard(offeredCard);
		else
			player.getPlayerData().getOfferedResources().addUsedPermitCard(offeredCard);
		
		marketOffers.addOffer(marketOfferFactory.createMarketOffer(player, offeredCard, offerCost));
	
		ModelChanges modelChanges = new ModelChanges();
		
		addOfferPermitCardChanges(modelChanges,offeredCard,offerCost);
		
		return modelChanges;
	}

	/**
	 * Adds to the {@link MarketOffers} a {@link OfferAssistants} 
	 * @param marketOffers
	 * @param offeredAssistants : amount of assistant
	 * @param offerCost : cost set for the offer
	 * @return model changes
	 */
	public ModelChanges offerAssistants(MarketOffers marketOffers, int offeredAssistants, int offerCost){
		
		player.getPlayerData().getOfferedResources().addAssistants(offeredAssistants);
		marketOffers.addOffer(marketOfferFactory.createMarketOffer(player, offeredAssistants, offerCost));
		
		ModelChanges modelChanges = new ModelChanges();
		
		addOfferAssistantsChanges(modelChanges,offeredAssistants,offerCost);
		
		return modelChanges;
	}

	/**
	 * Handles the {@link AcceptMarketOffer} action
	 * @param marketOffers : all the market offers
	 * @param marketOffer : specific offer the player wants to accept
	 * @return model changes
	 * @throws IllegalStateChangeException
	 */
	public ModelChanges acceptOffer(MarketOffers marketOffers, MarketOffer marketOffer) throws IllegalStateChangeException {
		
		MarketOffer offerToAccept;
		ModelChanges modelChanges = new ModelChanges();
		try {
			offerToAccept = marketOffers.removeOffer(marketOffer);
			
			modelChanges.addToChanges(
					offerToAccept.acceptOffer(player));
			
			return modelChanges;
		} catch (TransactionException | NoSuchOfferException e) {
			throw new IllegalStateChangeException(e);
		}
	}
	
	/**
	 * Adds to changes the parameters of a {@link OfferPoliticCard}
	 * @param modelChanges
	 * @param offeredCard with the key "OfferedItem"
	 * @param offerCost with the key "OfferCost"
	 * @throws ColorNotFoundException
	 */
	private void addOfferPoliticCardChanges(ModelChanges modelChanges, PoliticCard offeredCard, int offerCost) throws ColorNotFoundException {
		
		modelChanges.addToChanges(ACTION_KEY,"AddPoliticCardOffer");
		modelChanges.addToChanges(ACTOR_ID_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFERED_ITEM_KEY, offeredCard);
		modelChanges.addToChanges(OFFER_COST_KEY, offerCost);
	}
	/**
	 * Adds to changes the parameters of a {@link OfferPermitCard}
	 * @param modelChanges
	 * @param offeredCard with the key "OfferedItem"
	 * @param offerCost with the key "OfferCost"
	 */
	private void addOfferPermitCardChanges(ModelChanges modelChanges, PermitCard offeredCard, int offerCost) {

		modelChanges.addToChanges(ACTION_KEY,"AddPermitCardOffer");
		modelChanges.addToChanges(ACTOR_ID_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFERED_ITEM_KEY, offeredCard);
		modelChanges.addToChanges(OFFER_COST_KEY, offerCost);
	}
	/**
	 * Adds to changes the parameters of a {@link OfferAssistants}
	 * @param modelChanges
	 * @param offeredAssistants with the key "OfferedItem"
	 * @param offerCost with the key "OfferCost"
	 */
	private void addOfferAssistantsChanges(ModelChanges modelChanges, int offeredAssistants, int offerCost) {

		modelChanges.addToChanges(ACTION_KEY,"AddAssistantsOffer");
		modelChanges.addToChanges(ACTOR_ID_KEY, player.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFERED_ITEM_KEY, offeredAssistants);
		modelChanges.addToChanges(OFFER_COST_KEY, offerCost);
	}
}
