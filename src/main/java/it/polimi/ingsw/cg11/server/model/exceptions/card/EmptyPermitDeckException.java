package it.polimi.ingsw.cg11.server.model.exceptions.card;
/**
 * Exception thrown when the Permit {@link Deck} is empty.
 * @author paolasanfilippo
 *
 */
public class EmptyPermitDeckException extends Exception {

	private static final long serialVersionUID = -4387355895891257971L;

	/**
	 * Exception thrown when the Permit {@link Deck} is empty.
	 */
	public EmptyPermitDeckException() {
		super();
	}

	/**
	 * Exception thrown when the Permit {@link Deck} is empty.
	 * @param cause
	 */
	public EmptyPermitDeckException(Throwable cause) {
		super(cause);
	}

}
