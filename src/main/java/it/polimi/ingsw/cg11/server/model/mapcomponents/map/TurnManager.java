package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Calendar.Builder;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.Messages;
import it.polimi.ingsw.cg11.server.gameserver.Server;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * Records the turn order and the current active player. Through this class it
 * is possible to change the current active player, as well as the game phase
 * (normal/market)
 * 
 * @author francesco
 *
 */
public class TurnManager {

	private static final Logger LOG = Logger.getLogger(TurnManager.class.getName());
	private static final int TIMEOUT_SECONDS = Server.getTurnsTimer();
	private static final int FIRST = 0;

	private Timer timer;

	private List<PlayerState> players;
	private List<PlayerState> playersInNaturalOrder;
	private List<PlayerState> outOfGamePlayers;
	private PlayerState currentPlayer;
	private GamePhaseManager gamePhaseManager;
	private LastTurnManager lastTurnManager;
	private int currentPlayerIndex;

	/**
	 * Constructor
	 * 
	 * @param gamePhaseManager
	 */
	protected TurnManager(GamePhaseManager gamePhaseManager) {
		this.gamePhaseManager = gamePhaseManager;

		outOfGamePlayers = new ArrayList<>();
		timer = new Timer();
	}

	/**
	 * Sets the playes
	 * 
	 * @param players
	 *            : list of playerstates
	 */
	protected void setPlayers(List<PlayerState> players) {
		this.players = clonePlayersList(players);
		this.playersInNaturalOrder = clonePlayersList(players);
		this.currentPlayerIndex = FIRST;

		currentPlayer = players.get(currentPlayerIndex);
		currentPlayer.setAllowedToPlay(true);
		lastTurnManager = new LastTurnManager(players.size()-1);
	}

	/**
	 * Returns the starting {@link GamePhase}
	 * 
	 * @return startingGamePhase
	 */
	public String getStartingGamePhase() {
		return this.gamePhaseManager.getStartingGamePhase().getPhaseName();
	}

	/**
	 * This method is called by {@link PlayerState} after a turn-ending action
	 */
	public void changeTurn() {

		resetTimer();
		currentPlayer.setAllowedToPlay(false);

		if (players.size() == 1) {
			lastTurnManager.setGameOver(true);
			sendWinMessageToRemainedPlayer(players.get(0));
		}

		if (!lastTurnManager.gameIsOver()) {

			if (currentPlayerIndex < players.size() - 1) {
				currentPlayerIndex += 1;
				currentPlayer = players.get(currentPlayerIndex);
			} else {
				gamePhaseManager.changeGamePhase();
				if (gamePhaseManager.currentGamePhase.equals(gamePhaseManager.getFirstGamePhase())) {
					resetOfferedResources();
					resetTurnActions();
				}
				if (gamePhaseManager.getCurrentGamePhase().isInRandomOrder()) {
					shufflePlayingOrder();
				} else {
					restorePlayingOrder();
				}
				currentPlayerIndex = FIRST;
				currentPlayer = players.get(currentPlayerIndex);
			}
			currentPlayer.setAllowedToPlay(true);
			startTimer();
		} else {
			gamePhaseManager.changeGamePhase();
		}

		if (lastTurnManager.isCounting()
				&& gamePhaseManager.currentGamePhase.equals(gamePhaseManager.getFirstGamePhase()))
			lastTurnManager.decreaseRemainingTurnsCounter();
		if (lastTurnManager.gameIsOver()) {
			gamePhaseManager.endTheGame();
		}
	}

	private void sendWinMessageToRemainedPlayer(PlayerState lastPlayer) {
		String message = "";
		String currentGame;
		int playerID;

		message += "\n\n\n\nCONGRATULATIONS! YOU HAVE WON! (others players have left the game)\n\n\n\n";
		currentGame = lastPlayer.getPlayerData().getCurrentGame();
		playerID = lastPlayer.getPlayerData().getPlayerID();

		Messages.sendToPlayer(message, currentGame, playerID);

	}

	/**
	 * Resets the turn action
	 */
	private void resetTurnActions() {
		for (PlayerState player : players) {
			player.getPlayerActionExecutor().resetTurnActions();
		}
	}

	/**
	 * Resets the {@link PoliticCard}s, {@link PermitCard}s and
	 * {@link Assistants} that were offered during the market phase, but haven't
	 * been accepted by anyone
	 */
	private void resetOfferedResources() {

		for (PlayerState player : players) {
			player.getPlayerData().getOfferedResources().reset();
		}

	}

	/**
	 * Tells if a player is playing or not
	 * 
	 * @param playerID
	 * @return true if is this player's turn to play
	 */
	public boolean isPlaying(PlayerState player) {
		if (this.currentPlayer.equals(player))
			return true;
		return false;
	}

	/**
	 * Returns the player who is enabled to play
	 * 
	 * @return the player enable to perform actions
	 */
	public PlayerState getCurrentlyPlaying() {
		return currentPlayer;
	}

	/**
	 * Returns the current {@link GamePhase}
	 * 
	 * @return the current game phase
	 */
	public GamePhase getCurrentGamePhase() {
		return gamePhaseManager.getCurrentGamePhase();
	}

	/**
	 * Returns the first {@link GamePhase}
	 * 
	 * @return first game phase
	 */
	public GamePhase getFirstGamePhase() {
		return gamePhaseManager.getFirstGamePhase();
	}

	/**
	 * Shuffle the players' order (for accepting market phase)
	 */
	public void shufflePlayingOrder() {

		Collections.shuffle(players);
	}

	/**
	 * The players order is restored to default, meaning that the order is
	 * decided by the player's ID (after the accepting market phase)
	 */
	public void restorePlayingOrder() {
		players = clonePlayersList(playersInNaturalOrder);
	}

	/**
	 * Clones the players list so then the initial order can be easily restored
	 * after being shuffled
	 * 
	 * @param listToClone
	 *            : list of playerState to clone
	 * @return the cloned list
	 */
	public List<PlayerState> clonePlayersList(List<PlayerState> listToClone) {

		ListIterator<PlayerState> pListIterator = listToClone.listIterator();
		List<PlayerState> cloneList = new ArrayList<>();

		while (pListIterator.hasNext()) {
			cloneList.add(pListIterator.next());
		}
		return cloneList;
	}

	/**
	 * Returns the last {@link TurnManager}
	 * 
	 * @return last turn manager
	 */
	public LastTurnManager getLastTurnManager() {
		return lastTurnManager;
	}

	/**
	 * Removes the players that have left before the game ends
	 * 
	 * @param disconnectedPlayer
	 */
	public void removeDisconnectedPlayer(PlayerState disconnectedPlayer) {
		players.remove(disconnectedPlayer);
		playersInNaturalOrder.remove(disconnectedPlayer);
		outOfGamePlayers.add(disconnectedPlayer);
		currentPlayerIndex -= 1;

		sendDisconnectedMessageToPlayers(disconnectedPlayer);
		LOG.info("\n\nSent notice message to players\n\n");

		changeTurn();
	}

	/**
	 * Resets the timer
	 */
	private void resetTimer() {
		timer.cancel();
	}

	/**
	 * Makes the turn's timer start
	 */
	public void startTimer() {
		int milliseconds = TIMEOUT_SECONDS * 1000;
		timer = new Timer();

		sendTurnChangeMessage(currentPlayer);

		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				PlayerState disconnectedPlayer = currentPlayer;

				removeDisconnectedPlayer(getCurrentlyPlaying());
				LOG.info("\n\n!!Player " + disconnectedPlayer.getPlayerData().getUsername()
						+ " has been removed from game due to timeout!!\n\n");

			}
		}, milliseconds);
	}

	/**
	 * Creates a message to be sent to all players with the information of the
	 * new {@link GamePhase} and the name of the player who can play
	 * 
	 * @param currentPlayer
	 */
	private void sendTurnChangeMessage(PlayerState currentPlayer) {
		int minutesToPlay;
		Integer timeoutHours;
		Integer timeoutMinutes;

		String timeoutMinutesAsString;

		Builder builder;
		Calendar calendar;
		String message = "";
		String currentPhase;
		String currentGame = currentPlayer.getPlayerData().getCurrentGame();
		String playerUsername = currentPlayer.getPlayerData().getUsername();

		currentPhase = gamePhaseManager.getCurrentGamePhase().getPhaseName();
		currentPhase = AsString.addSpaces(currentPhase);

		minutesToPlay = TIMEOUT_SECONDS / 60;

		builder = new Calendar.Builder().setInstant(System.currentTimeMillis());
		calendar = builder.build();
		calendar.add(Calendar.MINUTE, minutesToPlay);
		timeoutHours = calendar.get(Calendar.HOUR_OF_DAY);
		timeoutMinutes = calendar.get(Calendar.MINUTE);

		timeoutMinutesAsString = (timeoutMinutes < 10) ? "0" + timeoutMinutes : timeoutMinutes.toString();

		message += "\n\n\nGame phase: " + currentPhase + "\n";
		message += "<<Player " + playerUsername + " is playing>>\n";
		message += playerUsername + " has " + minutesToPlay + " minutes to play.";
		message += " Time out at: " + timeoutHours + ":" + timeoutMinutesAsString + "\n";

		Messages.sendToAllPlayers(message, currentGame);

	}

	/**
	 * Creates a message to be sent to all players with the name of the player
	 * who has disconnected
	 * 
	 * @param currentPlayer
	 */
	private void sendDisconnectedMessageToPlayers(PlayerState currentPlayer) {
		String message;

		String currentGame = currentPlayer.getPlayerData().getCurrentGame();
		String disconnectedPlayerUsername = currentPlayer.getPlayerData().getUsername();

		message = "<<Player " + disconnectedPlayerUsername + " has disconnected>>\n";

		Messages.sendToAllPlayers(message, currentGame);
	}

}
