package it.polimi.ingsw.cg11.server.view;

import java.io.Serializable;
import java.util.Random;

/**
 * This class represent a unique identifier of a player ( = client)
 * 
 * @author Federico
 *
 */
public class Token implements Serializable {

	private static final long serialVersionUID = -4399961032218773353L;

	private int playerID;
	private final int uniqueIdentifier;
	private String clientUsername;
	private String currentGame;

	/**
	 * Token's constructor
	 * 
	 * @param clientUsername
	 */
	public Token(String clientUsername) {

		Random randomNumberGenerator = new Random();
		this.uniqueIdentifier = randomNumberGenerator.nextInt(Integer.MAX_VALUE);
		this.setClientUsername(clientUsername);
	}

	/**
	 * Returns a name representing the game to which the player is playing
	 * 
	 * @return current game name
	 */
	public String getCurrentGame() {
		return currentGame;
	}

	/**
	 * Sets the current game name of the player
	 * 
	 * @param currentGame
	 */
	public void setCurrentGame(String currentGame) {
		this.currentGame = currentGame;
	}

	/**
	 * @return the clientUsername
	 */
	public String getClientUsername() {
		return clientUsername;
	}

	/**
	 * @return the player ID
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID
	 *            the playerID to set
	 */
	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	/**
	 * @param clientUsername
	 *            the clientUsername to set
	 */
	public void setClientUsername(String clientUsername) {
		this.clientUsername = clientUsername;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientUsername == null) ? 0 : clientUsername.hashCode());
		result = prime * result + ((currentGame == null) ? 0 : currentGame.hashCode());
		result = prime * result + uniqueIdentifier;
		result = prime * result + playerID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (clientUsername == null) {
			if (other.clientUsername != null)
				return false;
		} else if (!clientUsername.equals(other.clientUsername))
			return false;
		if (currentGame == null) {
			if (other.currentGame != null)
				return false;
		} else if (!currentGame.equals(other.currentGame))
			return false;
		if (playerID != other.playerID)
			return false;
		if (uniqueIdentifier != other.uniqueIdentifier)
			return false;
		return true;
	}

}
