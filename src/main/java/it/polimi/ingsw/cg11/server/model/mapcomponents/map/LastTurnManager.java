package it.polimi.ingsw.cg11.server.model.mapcomponents.map;
/**
 * This class manages the last turn of the game: id est when a player builds his last emporium, the 
 * last turn starts. All the remaining players have a last turn to make their move, before the game ends
 * @author paolasanfilippo
 *
 */
public class LastTurnManager {

	private boolean gameOver;
	private boolean isCounting;
	private int lastTurnCounter;
	
	/**
	 * Constructor
	 * @param lastTurnounter
	 */
	public LastTurnManager(int lastTurnounter) {
		this.gameOver = false;
		this.isCounting = false;
		this.lastTurnCounter = lastTurnounter;
	}
	/**
	 * Decrement the counter of the remaining turns
	 */
	public void decreaseRemainingTurnsCounter() {
		if (lastTurnCounter > 0)
			lastTurnCounter--;
		else
			setGameOver(true); 
	}
	/**
	 * Returns the status of the game: if it's over or not
	 * @return <tt>true</tt> if the game is over, <tt>false</tt> if is not
	 */
	public boolean gameIsOver(){
		return gameOver;
	}
	/**
	 * Starts to count the remaining turns
	 */
	public void startCounting(){
		isCounting = true;
	}
	/**
	 * Returns if the {@link LastTurnManager} is counting or not
	 * @return <tt>true</tt> if it is, <tt>false</tt> if is not
	 */
	public boolean isCounting(){
		return isCounting;
	}
	/**
	 * Set's the game over status
	 * @param gameOver
	 */
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
}
