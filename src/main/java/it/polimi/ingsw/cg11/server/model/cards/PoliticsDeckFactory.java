package it.polimi.ingsw.cg11.server.model.cards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Implements the {@link Deck} factory for the politic {@link Card}.
 * Uses {@link org.json.simple}
 * @author francesco
 */

public class PoliticsDeckFactory {

	private static final String COLOR = "color";
	
	/**
	 * Creates a {@link Deck} of {@link PoliticCard}s. If the color doesn't exist, it throws a {@link ColorNotFoundException}
	 * @param deckConfig : a JSONArray containing the cards. Cannot be null
	 * @return {@link Deck} of politic cards
	 * @throws ColorNotFoundException 
	 */

	public Deck<PoliticCard> createDeck(JSONArray deckConfig) throws ColorNotFoundException {
		
		LinkedList<PoliticCard> cards = new LinkedList<>();
		
		addCardsFromJsonArray(deckConfig,cards,COLOR);
		
		return new Deck<>(cards);
		
	}
	/**
	 * Creates a lit of {@link PoliticCard}s from a {@link JSONArray}. Throws a {@link ColorNotFoundException}
	 * if there isn't the color.
	 * @param cardList : jsonarray used to create the card list
	 * @return list of politic cards
	 * @throws ColorNotFoundException
	 */
	public List<PoliticCard> createCardList(JSONArray cardList) throws ColorNotFoundException{
		
		List<PoliticCard> cards = new ArrayList<>();
		
		addCardsFromJsonArray(cardList,cards,COLOR);
		
		return cards;
	}
	/**
	 * Add cards from {@link JSONArray} to a collection of {@link PoliticCard}s.
	 * Throws a {@link ColorNotFoundException} if there isn't such color.
	 * @param cardList : jsonarray from which the cards will be added
	 * @param cards : a collection of cards
	 * @param color : color tag
	 * @throws ColorNotFoundException
	 */
	private void addCardsFromJsonArray(JSONArray cardList,Collection<PoliticCard> cards,String color) throws ColorNotFoundException{
		
		Iterator<Object> cardListIterator = cardList.iterator();
		JSONObject card;
		
		while(cardListIterator.hasNext()){
			card = (JSONObject)cardListIterator.next();
			cards.add(
					new PoliticCard(ColorMap.getColor(card.getString(color))));
		}
	}
	/**
	 * Creates a single {@link PoliticCard} from a {@link JSONObject}
	 * @param cardConfig : jsonobject from which the politic card is created
	 * @return a politic card
	 * @throws ColorNotFoundException
	 */
	public PoliticCard createPoliticCardFromConfig(JSONObject cardConfig) throws ColorNotFoundException{
		
		String cardColor;
		PoliticCard card;
		
		cardColor = cardConfig.getString(COLOR);
		card = new PoliticCard(ColorMap.getColor(cardColor));
		
		return card;
	}

}
