package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Implements a transaction in which the player gets {@link Assistants}
 * 
 * @author francesco
 *
 */
public class AssistantsTransaction implements Transaction {

	private int amount;
	/**
	 * 
	 * @param amount
	 */
	public AssistantsTransaction(int amount) {
		this.amount = amount;
	}
	
	@Override
	public int getAmount(){
		return this.amount;
	}

	/**
	 * Lets the player gain the set amount of assistants
	 * 
	 * @param player
	 * @throws TransactionException 
	 */
	@Override

	public void execute(PlayerState transactionSubject) throws TransactionException {
		
		if (amount < 0) {
			try {
				transactionSubject.getPlayerData().getAssistants().use(Math.abs(amount));
			} catch (IllegalResourceException e) {
				throw new TransactionException(e);
			}
		}else
			try {
				transactionSubject.getPlayerData().getAssistants().gain(amount);
			} catch (IllegalResourceException e) {
				throw new TransactionException(e);
			}
	}

}