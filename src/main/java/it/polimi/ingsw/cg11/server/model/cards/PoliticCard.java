package it.polimi.ingsw.cg11.server.model.cards;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * A specific type of {@link Card}, which has a {@link Color}
 * @author paolasanfilippo
 *
 */

public class PoliticCard implements Card {

	public static final Logger LOG = Logger.getLogger(PoliticCard.class.getName());
	private Color color;

	/**
	 * A specific type of {@link Card}, which has a {@link Color}
	 * @param color : The color of the politic card
	 */
	public PoliticCard(Color color) {
		this.color = color;

	}
	/**
	 * Gets the {@link Color} of the politic {@link Card}
	 * @return Color
	 */
	public Color getColor() {
		return this.color;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoliticCard other = (PoliticCard) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return "PoliticCard [color= " + ColorMap.getString(color) + " ]";
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "Color not found",e);
			return "";
		}
	}
	/**
	 * Creates and returns a copy of the politic {@link Card}
	 * @return : a copy of the politic {@link Card}
	 * 
	 */
	public PoliticCard getCopy(){
			return new PoliticCard(getColor());

	}


}