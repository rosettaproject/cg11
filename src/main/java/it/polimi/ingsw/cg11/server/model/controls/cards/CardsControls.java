
package it.polimi.ingsw.cg11.server.model.controls.cards;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.model.utils.transactions.Transaction;

/**
 * Contains all the card-related controls and exception handling
 * 
 * @author Federico
 *
 */
public class CardsControls {

	public static final Logger LOG = Logger.getLogger(CardsControls.class.getName());
	private static final Color[] ALLOWED_CARD_COLORS = { Color.BLACK, Color.ORANGE, Color.PINK, new Color(138, 43, 226),
			Color.CYAN, Color.WHITE, new Color(127, 126, 125) };

	/**
	 * Controls that the permit card deck is not empty. If it is empty it throws
	 * an {@link EmptyPermitDeckException}
	 * 
	 * @param deck
	 *            : deck of permit card. Cannot be null
	 * @throws EmptyPermitDeckException
	 */
	public void emptyPermitDeckControl(Deck<PermitCard> deck) throws EmptyPermitDeckException {
		if (deck.isEmpty())
			throw new EmptyPermitDeckException();
	}

	/**
	 * Controls that the politic card deck is not empty. If it is empty, it
	 * throws an {@link EmptyPoliticDeckException}
	 * 
	 * @param deck
	 *            of politic cards. Cannot be null
	 * @throws EmptyPoliticDeckException
	 */
	public void emptyPoliticDeckControl(Deck<PoliticCard> deck) throws EmptyPoliticDeckException {
		if (deck.isEmpty())
			throw new EmptyPoliticDeckException();
	}

	/**
	 * Controls that there are at least {@link MAX_PERMIT_CARDS} (=2 in standard
	 * rules) permit cards.
	 * 
	 * @param region
	 *            of the {@link FaceUpPermits}.
	 * @param numberOfActualPermits
	 *            : how many {@link FaceUpPermits} there are
	 * @throws NoSuchFaceUpPermitsException
	 */
	public void faceUpPermitsNumberControl(Region region, int numberOfActualPermits)
			throws NoSuchFaceUpPermitsException {
		FaceUpPermits faceUpPermitCards = region.getFaceUpPermitCards();
		if (faceUpPermitCards.getListSize() < numberOfActualPermits)
			throw new NoSuchFaceUpPermitsException(region.getName());
	}

	/**
	 * Returns the permit cards in which there is the {@link City} passed as
	 * parameter. If the city isn't in anyone of the cards, it throws a
	 * {@link NoSuchPermitCardException}.
	 * 
	 * @param permitCards
	 *            : list of permit cards in which the existence of the city is
	 *            searched.
	 * @param cityToControl
	 *            : city that has to be found
	 * @return list of permit cards in which there is the wanted city
	 * @throws NoSuchPermitCardException
	 */
	public List<PermitCard> cityFilter(List<PermitCard> permitCards, City cityToControl)
			throws NoSuchPermitCardException {
		PermitCard permitCardToAdd;
		List<PermitCard> allowedPermitCards = new ArrayList<>();
		ListIterator<PermitCard> permitCardsIterator = permitCards.listIterator();

		while (permitCardsIterator.hasNext()) {
			permitCardToAdd = permitCardsIterator.next();

			try {
				cityContainedInPermiCardControl(permitCardToAdd, cityToControl);
				allowedPermitCards.add(permitCardToAdd);
			} catch (NoSuchCityException e) {
				LOG.log(Level.SEVERE, "City not found", e);
			}

		}
		if (allowedPermitCards.isEmpty())
			throw new NoSuchPermitCardException();
		return allowedPermitCards;
	}

	/**
	 * Checks if in a permit card there is a specific {@link City}. Returns
	 * <tt>true</tt> if there is, <tt>false</tt> if not.
	 * 
	 * @param permitCard
	 *            : permit card you want to control, cannot be null
	 * @param city
	 *            : city you want to find, cannot be null
	 * @return <tt>true</tt> if there is, <tt>false</tt> if not
	 * @throws NoSuchCityException 
	 *             if permitCard or city are null
	 */
	public void cityContainedInPermiCardControl(PermitCard permitCard, City city) throws NoSuchCityException {
		if (!permitCard.getCities().contains(city))
			throw new NoSuchCityException();
	}

	/**
	 * Returns the permit cards in which there is the {@link Bonus} passed
	 * as parameter. If the bonus isn't in anyone of the cards, it throws a
	 * {@link NoSuchPermitCardException}.
	 *
	 * @param permitCards : list of permit cards in which the existence of the bonus is searched
	 * @param chainOfCommands : bonus searched
	 * @return list of permit cards in which there is the wanted bonus
	 * @throws NoSuchPermitCardException
	 */
	public List<PermitCard> bonusFilter(List<PermitCard> permitCards, Map<String, Transaction> chainOfCommands)
			throws NoSuchPermitCardException {

		List<PermitCard> allowedPermitCards = new ArrayList<>();
		ListIterator<PermitCard> permitCardsIterator = permitCards.listIterator();
		PermitCard permitCardToAdd;

		while (permitCardsIterator.hasNext()) {
			permitCardToAdd = permitCardsIterator.next();
			if (permitCardToAdd.getPermitBonus().isEqual(chainOfCommands))
				allowedPermitCards.add(permitCardToAdd);
		}
		if (allowedPermitCards.isEmpty())
			throw new NoSuchPermitCardException();
		return allowedPermitCards;
	}

	/**
	 * Returns the permit cards in which there are the {@link City}s passed as
	 * parameter. If the cities aren't in anyone of the cards, it throws a
	 * {@link NoSuchPermitCardException}.
	 * 
	 * @param permitCards
	 *            : list of permit cards in which the existence of the cities is
	 *            searched.
	 * @param citiesToControl
	 *            : cities that have to be found
	 * @return list of permit cards in which there are the wanted cities
	 * @throws NoSuchPermitCardException
	 */
	public List<PermitCard> citiesFilter(List<PermitCard> permitCards, List<City> citiesToControl)
			throws NoSuchPermitCardException {

		List<PermitCard> allowedPermitsCard = new ArrayList<>();
		Iterator<City> citiesIterator = citiesToControl.listIterator();
		City cityToControl;

		while (citiesIterator.hasNext()) {
			cityToControl = citiesIterator.next();
			allowedPermitsCard = cityFilter(permitCards, cityToControl);
		}
		if (allowedPermitsCard.isEmpty())
			throw new NoSuchPermitCardException();

		return allowedPermitsCard;
	}

	/**
	 * It takes a list of politic cards and it checks if the colors are allowed.
	 * If a color isn't allowed for a politicCard, it throws a
	 * {@link ColorNotFoundException}
	 * 
	 * @param cardsToCheck
	 *            : cards we want to be controlled
	 * @throws ColorNotFoundException
	 */
	public void cardColorsConsistencyCheck(List<PoliticCard> cardsToCheck) throws ColorNotFoundException {

		ListIterator<PoliticCard> cardsIterator = cardsToCheck.listIterator();
		List<Color> allowedColors = Arrays.asList(ALLOWED_CARD_COLORS);

		while (cardsIterator.hasNext()) {
			if (!allowedColors.contains(cardsIterator.next().getColor()))
				throw new ColorNotFoundException();
		}

	}

	/**
	 * It checks if a player has a specific permit card. If he/she doesn't, it
	 * throws a {@link NoSuchPermitCardException}
	 * 
	 * @param player
	 *            : player that could have the card
	 * @param permitCard
	 *            : card that will be searched.
	 * @throws NoSuchPermitCardException
	 */
	public void playerHasPermitCard(PlayerState player, PermitCard permitCard) throws NoSuchPermitCardException {
		List<PermitCard> playerPermitCards;
		List<PermitCard> offeredPermitCards;
		
		playerPermitCards = new ArrayList<>();
		playerPermitCards.addAll(player.getPlayerData().getPermitsHand().getCards());
		
		offeredPermitCards = player.getPlayerData().getOfferedResources().getOfferedPermitCards();
		
		Iterator<PermitCard> offeredCardsIterator = offeredPermitCards.listIterator();
		
		while(offeredCardsIterator.hasNext()){
			PermitCard offeredCard = offeredCardsIterator.next();
			playerPermitCards.remove(offeredCard);
		}

		if (!playerPermitCards.contains(permitCard)) {
			throw new NoSuchPermitCardException();
		}
	}

	/**
	 * Controls if a player has used a specific {@link PermitCard}.If the player
	 * hasn't used the permit card it throws a {@link NoSuchPermitCardException}
	 * 
	 * @param player
	 *            : player that could have used the permit card. Cannot be null
	 * @param permitCard
	 *            : permit card that we want to know if it has been used or not
	 *            by the player. Cannot be null
	 * @throws NoSuchPermitCardException
	 */
	public void playerHasUsedPermitCard(PlayerState player, PermitCard permitCard) throws NoSuchPermitCardException {
		
		List<PermitCard> playerUsedPermitCards;
		List<PermitCard> offeredUsedPermitCards;
		
		playerUsedPermitCards = new ArrayList<>();
		playerUsedPermitCards.addAll(player.getPlayerData().getUsedPermits().getCards());
		
		offeredUsedPermitCards = player.getPlayerData().getOfferedResources().getOfferedUsedPermitCards();
		
		Iterator<PermitCard> offeredCardsIterator = offeredUsedPermitCards.listIterator();
		
		while(offeredCardsIterator.hasNext()){
			PermitCard offeredCard = offeredCardsIterator.next();
			playerUsedPermitCards.remove(offeredCard);
		}

		if (!playerUsedPermitCards.contains(permitCard)) {
			throw new NoSuchPermitCardException();
		}
	}

	/**
	 * It checks if a player has a specific list of politic cards. If he/she
	 * doesn't, it throws a {@link NoSuchPoliticCardException}
	 * 
	 * @param player
	 *            : player that should have the cards
	 * @param politicCards
	 *            : cards that will be searched.
	 * @throws NoSuchPoliticCardException
	 */
	public void playerHasPoliticCards(PlayerState player, List<PoliticCard> politicCards)
			throws NoSuchPoliticCardException {
		List<PoliticCard> playerPoliticCards = player.getPlayerData().getPoliticsHand().getCards();
		Iterator<PoliticCard> iterator = politicCards.iterator();
		PoliticCard politicCardToCheck;
		int politicCardToCheckIndex;

		while (iterator.hasNext()) {
			politicCardToCheck = iterator.next();
			politicCardToCheckIndex = playerPoliticCards.indexOf(politicCardToCheck);
			if (politicCardToCheckIndex < 0)
				throw new NoSuchPoliticCardException();
		}

	}

	/**
	 * It checks if a player has a specific politic card. If he/she doesn't, it
	 * throws a {@link NoSuchPoliticCardException}
	 * 
	 * @param player
	 *            : player that could have the card
	 * @param politicCard
	 *            : card that will be searched.
	 * @throws NoSuchPoliticCardException
	 */
	public void playerHasPoliticCard(PlayerState player, PoliticCard politicCard) throws NoSuchPoliticCardException {
		List<PoliticCard> playerPoliticCards; 
		List<PoliticCard> offeredPoliticCards;
		
		playerPoliticCards = new ArrayList<>();
		playerPoliticCards.addAll(player.getPlayerData().getPoliticsHand().getCards());
		
		offeredPoliticCards = player.getPlayerData().getOfferedResources().getOfferedPoliticCards();
		
		Iterator<PoliticCard> offeredCardsIterator = offeredPoliticCards.listIterator();
		
		while(offeredCardsIterator.hasNext()){
			PoliticCard offeredCard = offeredCardsIterator.next();
			playerPoliticCards.remove(offeredCard);
		}

		if (!playerPoliticCards.contains(politicCard))
			throw new NoSuchPoliticCardException();
	}

	/**
	 * Checks if there is a correspondence between the {@link PoliticCard}s of
	 * the player and the {@link Balcony}. If there isn't, that is if a politic
	 * card hasn't the same color of one of the councillors, it throws a
	 * {@link NoSuchPoliticCardException}.
	 * 
	 * @param balcony
	 *            : balcony with the councillors that we want to check
	 * @param playerPoliticCards
	 *            : player's politic cards that should match the councillors'
	 *            colors
	 * @throws NoSuchPoliticCardException
	 */
	public void councilAndCardsMatchingControl(Balcony balcony, List<PoliticCard> playerPoliticCards)
			throws NoSuchPoliticCardException {
		Queue<Councillor> council = balcony.getCouncil();
		Iterator<Councillor> councilIterator = council.iterator();
		Iterator<PoliticCard> cardIterator = playerPoliticCards.iterator();

		List<Color> councilColors = new ArrayList<>();
		Color politicCardColor;
		Color multicolor = null;
		
		try {
			multicolor = ColorMap.getColor("MULTICOLOR");
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "Multicolor not mapped", e);
		}
		
		int councillorMatchedIndex;

		while (councilIterator.hasNext())
			councilColors.add(councilIterator.next().getColor());

		while (cardIterator.hasNext()) {
			politicCardColor = cardIterator.next().getColor();
			if (!politicCardColor.equals(multicolor)) {
				councillorMatchedIndex = councilColors.indexOf(politicCardColor);
				if (councillorMatchedIndex < 0)
					throw new NoSuchPoliticCardException();
				councilColors.remove(councillorMatchedIndex);
			}
		}
	}

	/**
	 * Checks if a player has a specific {@link PermitCard}
	 * 
	 * @param player
	 * @param permitCard
	 * @return true if the permit card is not used
	 * @return false if the permit card is used
	 * @throws NoSuchPermitCardException player has not the permit card
	 */
	public boolean checkIfPlayerHasPermitCard(PlayerState player, PermitCard permitCard)
			throws NoSuchPermitCardException {
		boolean isUsed;
		try {
			playerHasPermitCard(player, permitCard);
			isUsed = false;
		} catch (NoSuchPermitCardException e) {
			try {
				playerHasUsedPermitCard(player, permitCard);
				isUsed = true;
			} catch (NoSuchPermitCardException e1) {
				e1.addSuppressed(e);
				throw new NoSuchPermitCardException(e1);
			}
		}
		return isUsed;
	}
}
