package it.polimi.ingsw.cg11.server.model.cards;

import org.json.JSONArray;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;

/**
 * Extends the {@link PoliticsDeckFactory} and it is used to create a shuffled {@link Deck} of {@link PoliticCard}s
 * @author Federico
 *
 */
public class RandomPoliticDeckFactory extends PoliticsDeckFactory {

	@Override
	public Deck<PoliticCard> createDeck(JSONArray deckConfig) throws ColorNotFoundException {
		Deck<PoliticCard> deck = super.createDeck(deckConfig);
		deck.shuffle();
		return deck;
	}
}
