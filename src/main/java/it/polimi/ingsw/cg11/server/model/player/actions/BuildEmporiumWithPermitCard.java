package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.CityControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughAssistantsException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the BuildEmporiumWithPermitCard action and tries to
 * execute it. BuildEmporiumWithPermitCard is a main action that permits to
 * build on a {@link City} with a {@link PermitCard}
 * 
 * @author Federico
 *
 */
public class BuildEmporiumWithPermitCard extends Performable {

	private static final int NEEDED_ASSISTANTS_FOR_EMPORIUM = 1;
	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);
	private City destinationCity;
	private PermitCard choosenPermitCard;
	private PlayerState player;
	private TurnManager turnManager;
	private GameMap gameMap;
	private int numberOfEmporiumsBuilt;
	private int neededAssistants;
	private ActionServiceMethods actionServiceMethods;
	private String gameTopic;

	/**
	 * 
	 * @param gameMap
	 * @param player
	 * @param destinationCity
	 * @param permitCard
	 * @param gameTopic
	 * @throws IllegalCreationCommandException
	 */
	public BuildEmporiumWithPermitCard(GameMap gameMap, PlayerState player, City destinationCity, PermitCard permitCard,
			String gameTopic) throws IllegalCreationCommandException {

		CardsControls cardsControls = new CardsControls();

		this.player = player;
		this.destinationCity = destinationCity;
		this.choosenPermitCard = permitCard;
		this.gameTopic = gameTopic;
		this.turnManager = gameMap.getTurnManager();
		this.gameMap = gameMap;
		this.numberOfEmporiumsBuilt = destinationCity.numberOfEmporiums();
		this.neededAssistants = NEEDED_ASSISTANTS_FOR_EMPORIUM * numberOfEmporiumsBuilt;
		this.gameMap = gameMap;

		try {
			cardsControls.cityContainedInPermiCardControl(choosenPermitCard, destinationCity);
		} catch (NoSuchCityException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;
		actionServiceMethods = new ActionServiceMethods();
		
		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getMainActionsHandler().buildEmporiumWithPermitCard(gameMap,
					destinationCity, choosenPermitCard, neededAssistants);
			
			actionServiceMethods.victoryControl(gameMap);
			actionServiceMethods.canChooseExtraBonusControl(player, gameMap);
			actionServiceMethods.canChoosePermitCardBonusControl(player);

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);

		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	/*
	 * Tests: - only one emporium's player for each city - player must has
	 * enough assistants -> 1 assistants for each emporium just built - player
	 * must has enough money
	 */
	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		CityControls cityControls = new CityControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		CardsControls cardsControls = new CardsControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			cityControls.emporiumIsNotPresentControl(destinationCity, player);
			playerResourcesControls.enoughAssistantControl(player, neededAssistants);
			cardsControls.playerHasPermitCard(player, choosenPermitCard);

		} catch (NoPlayerTurnException | ExistEmporiumException | NoEnoughAssistantsException
				| NotCorrectGamePhaseException | NoSuchPermitCardException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}