package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughMoneyException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffer;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
/**
 * Represent the action that allows the player to accept a {@link MarketOffer}
 * @author paolasanfilippo
 *
 */
public class AcceptMarketOffer extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("MarketAcceptingPhase", true);
	
	private GameMap gameMap;
	private PlayerState player;
	private TurnManager turnManager;
	private MarketOffer offerToAccept;
	private MarketOffers marketOffers;
	private String gameTopic;
	/**
	 * 
	 * @param gameMap
	 * @param player
	 * @param turnManager
	 * @param offerToAccept
	 * @param marketOffers
	 * @param gameTopic
	 */
	public AcceptMarketOffer(GameMap gameMap, PlayerState player, TurnManager turnManager, MarketOffer offerToAccept,
			MarketOffers marketOffers, String gameTopic) {
		this.gameMap = gameMap;
		this.player = player;
		this.turnManager = turnManager;
		this.offerToAccept = offerToAccept;
		this.marketOffers = marketOffers;
		this.gameTopic = gameTopic;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;
		ActionServiceMethods actionServiceMethods = new ActionServiceMethods();
		
		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getMarketActionsHandler().acceptOffer(marketOffers,
					offerToAccept);

			actionServiceMethods.canChooseExtraBonusControl(player, gameMap);
			actionServiceMethods.canChoosePermitCardBonusControl(player);

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);
			
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			playerResourcesControls.enoughMoneyControl(player, offerToAccept.getOfferCost());
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoEnoughMoneyException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}
