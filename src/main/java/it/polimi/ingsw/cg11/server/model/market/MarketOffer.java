package it.polimi.ingsw.cg11.server.model.market;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * This object represents a market offer which is created and accepted by the players
 * 
 * @author francesco
 *
 */
public interface MarketOffer {

	/**
	 * This method allows a player to accept the offer
	 * 
	 * @param acceptingPlayer
	 * @return model change
	 * @throws TransactionException
	 */
	public ModelChanges acceptOffer(PlayerState acceptingPlayer) throws TransactionException;
	
	/**
	 * Returns the offer's cost
	 * @return cost
	 */
	public int getOfferCost();
	
	/**
	 * Returns the player who made the offer
	 * @return player
	 */
	public PlayerState getOfferingPlayer();
	
}
