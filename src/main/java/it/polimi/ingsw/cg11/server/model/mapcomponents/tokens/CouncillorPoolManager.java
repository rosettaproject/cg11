package it.polimi.ingsw.cg11.server.model.mapcomponents.tokens;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.AsString;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * CouncillorPoolManager creates the Councillors, converting the informations
 * obtained by file. After the creation of the whole pool of councillors and the
 * start of the game, it contains the councillors that aren't in use. It also
 * allows to get a councillor and to put it back in the pool when it's not used
 * anymore in a balcony {@link Balcony}.
 * 
 * @author Paola
 *
 */
public class CouncillorPoolManager {

	private static final String COLOR = "color";

	private List<Councillor> councillors;

	/**
	 * Constructor
	 * @param councillorPool
	 * @throws ColorNotFoundException
	 */
	public CouncillorPoolManager(JSONArray councillorPool) throws ColorNotFoundException {

		Iterator<Object> poolIterator = councillorPool.iterator();
		JSONObject councillor = new JSONObject();

		String color = new String(COLOR);

		this.councillors = new ArrayList<>();

		while (poolIterator.hasNext()) {
			councillor = (JSONObject) poolIterator.next();
			councillors.add(new Councillor(ColorMap.getColor(councillor.getString(color))));
		}

	}

	/**
	 * AddPieceToPool adds a Councillor to the CouncillorPoolManager
	 * 
	 * @param councillor
	 *            {@link Councillor}
	 */
	public void addPieceToPool(Councillor councillor) {
		councillors.add(councillor);
	}

	/**
	 * ObtainPieceFromPool draws from the CouncillorPoolManager a {@link Councillor},
	 * chosen by its color. It returns the Councillor and removes it from the
	 * CouncillorPoolManager. If there aren't councillors of the color selected,
	 * it throws a NoSuchCouncillorException
	 * 
	 * @param color
	 *            {@link Color}
	 * @return councillor {@link Councillor}
	 * @exception NoSuchCouncillorException
	 */
	public Councillor obtainPieceFromPool(Color color) throws NoSuchCouncillorException {

		int index;
		int poolSize = councillors.size();

		for (index = 0; index < poolSize; index++)
			if (councillors.get(index).getColor().equals(color))
				return councillors.remove(index);

		throw new NoSuchCouncillorException();
	}
	
	/**
	 * Tells if a CouncillorPoolManager has a {@link Councillor}
	 * @param color
	 * @return <tt>true</tt> if it is contained, <tt>false</tt> if is not
	 */
	public boolean hasCouncillor(Color color){
		return councillors.contains(new Councillor(color));
	}
	/**
	 * Returns and remove a councillor from the pool
	 * @return councillor
	 * @throws NoSuchCouncillorException
	 */
	public Councillor obtainPieceFromPool() throws NoSuchCouncillorException {
		if (!councillors.isEmpty())
			return councillors.remove(0);
		throw new NoSuchCouncillorException();
	}
	/**
	 * Shuffles the councillors' collection
	 */
	public void shuffleCouncillors() {
		Collections.shuffle(councillors);
	}

	/**
	 * Returns a list of councillors
	 * @return the councillors
	 */
	public List<Councillor> getCouncillors() {
		return councillors;
	}

	@Override
	public String toString() {
		String toString = "";
		
		toString += "\n\n<<Available councillors>>\n";
		toString += AsString.asString(councillors, true);
		
		return toString;
	}

}
