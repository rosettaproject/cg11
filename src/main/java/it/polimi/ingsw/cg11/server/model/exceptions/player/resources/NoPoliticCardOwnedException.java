package it.polimi.ingsw.cg11.server.model.exceptions.player.resources;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;

/**
 * Exception thrown when the player hasn't {@link PoliticCard}s.
 * @author paolasanfilippo
 *
 */
public class NoPoliticCardOwnedException extends Exception {

	private static final long serialVersionUID = -7378548467965935286L;

	/**
	 * Exception thrown when the player hasn't {@link PoliticCard}s.
	 */
	public NoPoliticCardOwnedException() {
		super();
	}
}
