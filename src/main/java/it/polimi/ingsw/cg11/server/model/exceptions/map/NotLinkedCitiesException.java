package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;

/**
 * Exception thrown when someone tries to perform an action over two {@link City}s that aren't linked.
 * @author paolasanfilippo
 *
 */
public class NotLinkedCitiesException extends Exception {
	
	private static final long serialVersionUID = 7290856948461688791L;

	/**
	 * Exception thrown when someone tries to perform an action over two {@link City}s that aren't linked.
	 */
	public NotLinkedCitiesException() {
		super();
	}

}
