package it.polimi.ingsw.cg11.server.model.utils.publishsubscribe;

import java.rmi.Remote;

/**
 * This interface shall be implemented by an object functioning as a broker in a pub-sub structure
 * @author paolasanfilippo
 *
 * @param <M>
 * @param <T>
 */
public interface ServerBrokerInterface<M, T> extends Remote {

	/**
	 * subscribes the subscriber passed as a parameter to the given topic in this broker
	 * @param subscriber
	 * @param topic
	 */
	public void clientSubscribe(SubscriberUserInterface<M> subscriber, T topic);

	/**
	 * unsubscribes the subscriber passed as a parameter from the given topic in this broker
	 * @param subscriber
	 * @param topic
	 */
	public void unsubscibe(SubscriberUserInterface<M> subscriber, T topic);

	/**
	 * Publishes the given message to all the subscribers to the given topic contained in this broker
	 * @param message
	 * @param topic
	 */
	public void publish(M message, T topic);

}
