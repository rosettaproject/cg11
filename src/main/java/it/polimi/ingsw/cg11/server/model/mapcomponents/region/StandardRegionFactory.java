package it.polimi.ingsw.cg11.server.model.mapcomponents.region;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.Balcony;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * Standard factory for {@link Region}. Leaves {@link PermitCardsDeck},{@link City}s and
 * {@link FaceUpPermit} references at null, so they should be set later
 * 
 * @author francesco
 *
 */
public class StandardRegionFactory {

	/**
	 * Creates region
	 * @param regionConfig
	 * @param currentMap
	 * @return region created
	 * @throws NoSuchCouncillorException
	 */
	public Region createRegion(JSONObject regionConfig, GameMap currentMap) throws NoSuchCouncillorException {

		String regionName;
		Bonus regionBonus;
		Balcony regionBalcony;

		BonusFactory bonusFactory;

		regionName = regionConfig.getString("Name");
		bonusFactory = new BonusFactory();
		regionBonus = bonusFactory.createBonus(regionConfig.getJSONObject("Bonus"), currentMap);

		regionBalcony = new Balcony(regionName, createCouncillorQueue(currentMap));

		return new Region(regionName, regionBonus, regionBalcony);
	}
	/**
	 * Creates a filled {@link Councillor} queue
	 * @param currentMap
	 * @return councillorQueue
	 * @throws NoSuchCouncillorException
	 */

	private Queue<Councillor> createCouncillorQueue(GameMap currentMap) throws NoSuchCouncillorException {

		ArrayBlockingQueue<Councillor> councillorQueue = new ArrayBlockingQueue<>(Balcony.getMaxSize());

		while (councillorQueue.remainingCapacity() != 0)
			councillorQueue.add(currentMap.getCouncillorsPool().obtainPieceFromPool());

		return councillorQueue;
	}
}