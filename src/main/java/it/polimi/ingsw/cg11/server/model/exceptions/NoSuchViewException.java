package it.polimi.ingsw.cg11.server.model.exceptions;

/**
 * Exception thrown when there's no such view
 * 
 * @author paolasanfilippo
 *
 */
public class NoSuchViewException extends Exception {

	private static final long serialVersionUID = -9031599022000528736L;

}
