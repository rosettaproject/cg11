package it.polimi.ingsw.cg11.server.model.player.actions;

import java.util.List;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.CityControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.IllegalPoliticCardsException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NotLinkedCitiesException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughMoneyException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoPoliticCardOwnedException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GraphedMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the BuildEmporiumWithKing action and tries to
 * execute it. BuildEmporiumWithKing is a main action that permits to build on a
 * {@link City} with the {@link King}'s help
 * 
 * @author Federico
 *
 */
public class BuildEmporiumWithKing extends Performable {

	private static final int COST_PER_CITY = 2;
	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);
	private static final int NEEDED_ASSISTANTS_FOR_EMPORIUM = 1;

	private City destinationCity;
	private City kingCity;
	private GameMap gameMap;
	private PlayerState player;
	private List<PoliticCard> playerPoliticCards;
	private TurnManager turnManager;
	private int neededMoney;
	private int numberOfEmporiumsBuilt;
	private int neededAssistants;
	private ActionServiceMethods actionServiceMethods;
	private String gameTopic;

	/**
	 * 
	 * @param gameMap
	 * @param player
	 * @param destinationCity
	 * @param politicCards
	 * @param gameTopic
	 * @throws IllegalCreationCommandException
	 * @throws ColorNotFoundException
	 * @throws NoSuchPlayerException
	 */
	public BuildEmporiumWithKing(GameMap gameMap, PlayerState player, City destinationCity,
			List<PoliticCard> politicCards, String gameTopic) throws IllegalCreationCommandException {

		int hopsToMake;
		GraphedMap graphedMap = gameMap.getGraphedMap();
		CardsControls cardsControls = new CardsControls();
		this.actionServiceMethods = new ActionServiceMethods();
		this.player = player;
		this.destinationCity = destinationCity;
		this.gameMap = gameMap;
		this.numberOfEmporiumsBuilt = destinationCity.numberOfEmporiums();
		this.neededAssistants = NEEDED_ASSISTANTS_FOR_EMPORIUM * numberOfEmporiumsBuilt;
		this.gameTopic = gameTopic;
		this.playerPoliticCards = politicCards;
		this.turnManager = gameMap.getTurnManager();

		try {
			kingCity = gameMap.getKing().getCurrentCity();
			cardsControls.councilAndCardsMatchingControl(gameMap.getKingBalcony(), playerPoliticCards);
			graphedMap.linkedCitiesControls(kingCity, destinationCity);
			hopsToMake = graphedMap.getDistance(kingCity, destinationCity);
			neededMoney = hopsToMake * COST_PER_CITY;
			neededMoney += actionServiceMethods.neededMoneyToCorruptCouncil(politicCards);

		} catch (NotLinkedCitiesException | IllegalPoliticCardsException | NoSuchPoliticCardException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;
		
		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getMainActionsHandler().buildEmporiumWithKing(gameMap,
					destinationCity, playerPoliticCards, neededMoney, neededAssistants);

			actionServiceMethods.victoryControl(gameMap);
			actionServiceMethods.canChooseExtraBonusControl(player, gameMap);
			actionServiceMethods.canChoosePermitCardBonusControl(player);

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	/*
	 * Tests if the player: has has enough money to satisfy the king's council
	 */
	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		CityControls cityControls = new CityControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			playerResourcesControls.ownsPoliticCardsControl(player, playerPoliticCards);
			playerResourcesControls.enoughMoneyControl(player, neededMoney);
			cityControls.emporiumIsNotPresentControl(destinationCity, player);

		} catch (NoPlayerTurnException | NoPoliticCardOwnedException | NoEnoughMoneyException
				| NotCorrectGamePhaseException | ExistEmporiumException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}