package it.polimi.ingsw.cg11.server.model.utils;

import java.nio.file.Paths;
/**
 * Service class used to write a resource's path
 * @author paolasanfilippo
 *
 */
public class ResourcesPath {

	private static final String POM_DIRECTORY = "cg11";

	private ResourcesPath() {
	}
	/**
	 * Returns the pom path
	 * @return pom path
	 */
	public static String getPomPath() {
		String pathAsString;
		String os = System.getProperty("os.name");

		pathAsString = ResourcesPath.class.getProtectionDomain().getCodeSource().getLocation().getPath();

		if (os.contains("Windows") && (pathAsString.startsWith("/") || pathAsString.startsWith("\\"))) {
			pathAsString = pathAsString.substring(1);
		}
		
		while (!pathAsString.endsWith(POM_DIRECTORY)) {
			pathAsString = Paths.get(pathAsString).getParent().toString();
		}
		
		return pathAsString;
	}

}
