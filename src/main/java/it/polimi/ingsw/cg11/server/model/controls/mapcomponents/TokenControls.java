/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.controls.mapcomponents;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchColoredCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;

/**
 * Controls on {@link Tokens} (councillors and emporiums)
 * 
 * @author Federico
 *
 */
public class TokenControls {

	private static final Color[] ALLOWED_COUNCILLOR_COLORS = { Color.BLACK, Color.ORANGE, Color.PINK,
			new Color(138, 43, 226), Color.CYAN, Color.WHITE };

	/**
	 * Tests if the pool of councillors is empty. It throws a
	 * {@link NoSuchCouncillorException} if it is.
	 * 
	 * @param councillorPoolManager
	 *            : pool of councillors we want to control that isn't empty
	 * @throws NoSuchCouncillorException
	 *             if the pool of councillors is empty
	 */
	public void notEmptyPoolControl(CouncillorPoolManager councillorPoolManager) throws NoSuchCouncillorException {
		Councillor councillorTestToAdd;

		councillorTestToAdd = councillorPoolManager.obtainPieceFromPool();
		councillorPoolManager.addPieceToPool(councillorTestToAdd);

	}

	/**
	 * Controls that there is a councillor with a specific {@link color}
	 * available, if there isn't it throws
	 * {@link NoSuchColoredCouncillorException}.
	 * 
	 * @param councillorPoolManager
	 *            : pool of councillors we want to control that isn't empty
	 * @param color
	 *            : Color
	 * @throws NoSuchColoredCouncillorException
	 */
	public void councillorColorAvailabilityControl(CouncillorPoolManager councillorPoolManager, Color color)
			throws NoSuchColoredCouncillorException {
		Councillor councillorTestToAdd;

		try {
			councillorTestToAdd = councillorPoolManager.obtainPieceFromPool(color);
		} catch (NoSuchCouncillorException e) {
			throw new NoSuchColoredCouncillorException(e);
		}
		councillorPoolManager.addPieceToPool(councillorTestToAdd);
	}

	/**
	 * Checks if the councillor color is allowed. If a color isn't allowed for a
	 * politicCard, it throws a {@link ColorNotFoundException}
	 * 
	 * @param councillorColor : color of the councillor we want to check
	 * @throws ColorNotFoundException
	 */
	public void councillorColorConsistencyCheck(Color councillorColor) throws ColorNotFoundException {
		List<Color> allowedColors = Arrays.asList(ALLOWED_COUNCILLOR_COLORS);

		if (!allowedColors.contains(councillorColor))
			throw new ColorNotFoundException();

	}

}
