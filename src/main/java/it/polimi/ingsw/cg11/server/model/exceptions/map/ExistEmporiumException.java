package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;

/**
 * Exception thrown when there's already an {@link Emporium}.
 * @author paolasanfilippo
 *
 */
public class ExistEmporiumException extends Exception {

	private static final long serialVersionUID = -5919205764342605033L;

	/**
	 * Exception thrown when there's already an {@link Emporium}.
	 */
	public ExistEmporiumException() {
		super();
	}

	/**
	 * Exception thrown when there's already an {@link Emporium}.
	 * @param message
	 */
	public ExistEmporiumException(String message) {
		super(message);
	}

	/**
	 * Exception thrown when there's already an {@link Emporium}.
	 * @param cause
	 */
	public ExistEmporiumException(Throwable cause) {
		super(cause);
	}
}
