package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPoliticDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Implements a transaction in which the player gets {@link PoliticCard}
 * @author Casa
 *
 */
public class PoliticCardTransaction implements Transaction {

	private final int cardsToDraw;

	/**
	 * 
	 * @param cardsToDraw
	 */
	public PoliticCardTransaction(int cardsToDraw) {
		this.cardsToDraw = cardsToDraw;
	}

	/**
	 * Lets the player draw the set amount of {@link PoliticCard}s
	 * 
	 * @param transactionSubject
	 * @throws EmptyPermitDeckException
	 */
	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {
		
		int loopIndex;

		for (loopIndex = 0; loopIndex < cardsToDraw; loopIndex++) {
			try {
				transactionSubject.getPlayerData().getPoliticsHand().draw();
			} catch (EmptyPoliticDeckException e) {
				throw new TransactionException(new EmptyPermitDeckException(e));
			}
		}
	}

	@Override
	public int getAmount() {
		return this.cardsToDraw;
	}

}