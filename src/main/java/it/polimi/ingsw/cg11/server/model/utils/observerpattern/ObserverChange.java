package it.polimi.ingsw.cg11.server.model.utils.observerpattern;

import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;

/**
 * Observer for the change messages
 * @author Federico
 *
 */
@FunctionalInterface
public interface ObserverChange extends Observer<ChangesMessage> {

	/**
	 * This method is called when the observed object has some update to communicate to
	 * the observers
	 */
	@Override
	public void update(ChangesMessage modelChange);

}
