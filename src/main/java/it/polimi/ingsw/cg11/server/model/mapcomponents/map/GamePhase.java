package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

/**
 * Contains a string representing the game phase's name
 * @author francesco
 *
 */
public class GamePhase {

	private final String phaseName;
	private final boolean inRandomOrder;
	/**
	 * Gamephase's constructor
	 * @param name
	 * @param inRandomOrder
	 */
	public GamePhase(String name,boolean inRandomOrder) {
		this.phaseName = name;
		this.inRandomOrder = inRandomOrder; 
	}
	/**
	 * Gets the phase name
	 * @return string with the game's phase
	 */
	public String getPhaseName() {
		return phaseName;
	}
	/**
	 * Checks if the {@link GamePhase}'s order is random
	 * @return <tt>true</tt> if the order is random, <tt>false</tt> if is not.
	 */
	public boolean isInRandomOrder(){
		return this.inRandomOrder;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (inRandomOrder ? 1231 : 1237);
		result = prime * result + ((phaseName == null) ? 0 : phaseName.hashCode());
		return result;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GamePhase other = (GamePhase) obj;
		if (inRandomOrder != other.inRandomOrder)
			return false;
		if (phaseName == null) {
			if (other.phaseName != null)
				return false;
		} else if (!phaseName.equals(other.phaseName))
			return false;
		return true;
	}

}
