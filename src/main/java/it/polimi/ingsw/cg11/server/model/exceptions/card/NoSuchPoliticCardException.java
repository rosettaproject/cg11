package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;

/**
 * Exception thrown when the {@link PoliticCard} doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchPoliticCardException extends Exception {

	private static final long serialVersionUID = 2567209557771565563L;
	
	/**
	 * Exception thrown when the {@link PoliticCard} doesn't exist.
	 */
	public NoSuchPoliticCardException() {
		super();
	}
}
