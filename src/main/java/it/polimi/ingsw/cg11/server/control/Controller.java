package it.polimi.ingsw.cg11.server.control;

import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.*;
import it.polimi.ingsw.cg11.server.model.actionsfactory.*;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.AsString;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.PublisherInterface;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.SubscriberUserInterface;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ServerVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;

/**
 * This class receives notifications from different views attached to it and
 * performs the requested changes to its assigned game map if allowed, otherwise
 * notifies the observers of the exception occurred while trying to execute the
 * requested operation
 * 
 * @author francesco
 * 
 */
public class Controller
		implements SubscriberUserInterface<Visitable>, PublisherInterface<Visitable, String>, ServerVisitor {

	private static final Logger LOG = Logger.getLogger(Controller.class.getName());

	private int playerID;
	private GameMap gameMap;
	private ActionFactory actionFactory;
	private Performable action;

	/**
	 * The game map to which this controller is assigned is passed as a
	 * parameter in this constructor
	 * 
	 * @param gameMap
	 */
	public Controller(GameMap gameMap) {
		this.gameMap = gameMap;
	}

	/**
	 * Visits the object
	 * 
	 * @param message
	 */
	@Override
	public void dispatchMessage(Visitable message) {
		message.accept(this);
	}

	@Override
	public void visit(SerializableElectCouncillor serializedAction) {

		LOG.log(Level.INFO, "Visiting ElectCouncillor action\n");
		actionFactory = new ElectCouncillorFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableAcceptMarketOffer serializedAction) {

		actionFactory = new AcceptMarketOfferFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableBuildEmporiumWithKing serializedAction) {

		actionFactory = new BuildEmporiumWithKingFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableBuildEmporiumWithPermitCard serializedAction) {

		actionFactory = new BuildEmporiumWithPermitCardFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableChangePermitCards serializedAction) {

		actionFactory = new ChangePermitCardsFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableChooseExtraBonus serializedAction) {

		actionFactory = new ChooseExtraBonusFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableChooseExtraPermit serializedAction) {

		actionFactory = new ChooseExtraPermitFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableChooseExtraPermitBonus serializedAction) {

		actionFactory = new ChooseExtraPermitBonusFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableElectCouncillorWithOneAssistant serializedAction) {

		actionFactory = new ElectCouncillorWithOneAssistantFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableFinish serializedAction) {

		actionFactory = new FinishFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableOfferAssistants serializedAction) {

		actionFactory = new OfferAssistantsFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableOfferPermitCard serializedAction) {

		actionFactory = new OfferPermitCardFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableOfferPoliticCard serializedAction) {

		actionFactory = new OfferPoliticCardFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableOneMoreMainAction serializedAction) {

		actionFactory = new OneMoreMainActionFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableTakePermitCard serializedAction) {

		actionFactory = new TakePermitCardFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(SerializableEngageAssistant serializedAction) {
		actionFactory = new EngageAnAssistantFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}

	@Override
	public void visit(ErrorMessage errorMessage) {
		LOG.log(Level.SEVERE, errorMessage.getMessage());
	}

	@Override
	public void visit(SerializableMapStartupConfig serializableMapStartupConfig) {
		publish(serializableMapStartupConfig, serializableMapStartupConfig.getTopic());
	}
	

	@Override
	public void visit(DisconnectedPlayerMessage disconnectedPlayer) {
		List<PlayerState> players;
		PlayerState player;
		
		actionFactory = new FinishFactory();
		playerID = disconnectedPlayer.getPlayerToken().getPlayerID();
		players = gameMap.getPlayers();
		
		try {
			player = actionFactory.getPlayerFromID(players, playerID);
			gameMap.getTurnManager().removeDisconnectedPlayer(player);
		} catch (NoSuchPlayerException e) {
			LOG.log(Level.SEVERE, "\n\nPlayer was not removed (can not find player ID: " + playerID + ")\n\n", e);
		}
	}

	@Override
	public void visit(SerializableChatMessage serializedAction) {
		actionFactory = new SendChatMessageFactory();
		createAndExecute(actionFactory, serializedAction, serializedAction.getTopic());
	}
	/**
	 * Creates and execute an action  ( {@link Performable} type), invoking the {@link ActionFactory}'s method createAction() with 
	 * a specific serializable as parameter. Then the action created will be executed.
	 * @param actionFactory
	 * @param serializableAction
	 * @param topic
	 */
	private void createAndExecute(ActionFactory actionFactory, SerializableAction serializableAction, String topic) {
		ErrorMessage errorMessage;
		String message = "";
		
		try {
			if (gameMap.getTurnManager().getLastTurnManager().gameIsOver())
				throw new NotAllowedActionException();

			action = actionFactory.createAction(serializableAction, gameMap);
			action.execute();

			LOG.log(Level.INFO, "Excecuted action\n");

		} catch (IllegalCreationCommandException | NotAllowedActionException e) {
			LOG.log(Level.WARNING, "Can not execute action\n", e);
			
			playerID = serializableAction.getPlayerToken().getPlayerID();
			
			message += "\nUnable to perform action: ";
			message += getReason(e);
			
			errorMessage = new ErrorMessage(message, topic);
			errorMessage.getPlayerToken().setPlayerID(playerID);
			
			publish(errorMessage, topic + "_" + playerID);
		}
	}

	/**
	 * Adds a simple reason to error message
	 * @param e
	 * @return reason
	 */
	private String getReason(Exception e) {
		String reason = "";
		String token = "";
		StringTokenizer tokenizer;
		
		int startOfString;
		int endOfString;
		
		reason += e.toString();
		tokenizer = new StringTokenizer(reason, ".");
		
		while(tokenizer.hasMoreTokens()){
			token = tokenizer.nextToken();
		}

		startOfString = 0;
		endOfString = token.length() - "Exception".length();
		
		reason = token.substring(startOfString, endOfString);
		reason = AsString.addSpaces(reason);
		
		return reason;
	}

	@Override
	public void publish(Visitable message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

	@Override
	public String getSubscriberIdentifier() {
		return gameMap.getGameTopic();
	}

}
