package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

/**
 * Exception thrown when the player tries to perform an action that is not allowed to perform.
 * @author paolasanfilippo
 *
 */
public class NotAllowedActionException extends Exception {

	private static final long serialVersionUID = -6293718257501918823L;
	
	/**
	 * Exception thrown when the player tries to perform an action that is not allowed to perform.
	*/
	public NotAllowedActionException() {
		super();
	}
	/**
	 * Exception thrown when the player tries to perform an action that is not allowed to perform.
	 * @param cause
	 */
	public NotAllowedActionException(Throwable cause) {
		super(cause);
	}
}
