package it.polimi.ingsw.cg11.server.model.utils.publishsubscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;

/*
 * TOPICS ----------------- Subscribers
 * 
 * game_x  		  ->  		game_x, view1, view2,...  [actions from views to game, updates from game to views]
 * playerToken	  ->		corresponding view
 * 
 */

/**
 * A broker that receives {@link Visitable} as messages and manages them with
 * strings as topics. It is used to dispatch messages from the users to the game
 * {@link Controller} and from the model to the users
 * 
 * @author francesco
 *
 */
public class Broker implements ServerBrokerInterface<Visitable, String> {

	private static final Logger LOG = Logger.getLogger(Broker.class.getName());

	private HashMap<String, List<SubscriberUserInterface<Visitable>>> subscriptions;
	private static Broker broker;

	private Broker() {
		subscriptions = new HashMap<>();
	}

	public static Broker getBroker() {
		if (broker == null)
			broker = new Broker();
		return broker;
	}

	@Override
	public synchronized void clientSubscribe(SubscriberUserInterface<Visitable> subscriber, String topic) {

		if (!subscriptions.containsKey(topic))
			subscriptions.put(topic, new ArrayList<SubscriberUserInterface<Visitable>>());

		subscriptions.get(topic).add(subscriber);
	}

	@Override
	public synchronized void unsubscibe(SubscriberUserInterface<Visitable> subscriber, String topic) {

		if (subscriptions.containsKey(topic)) {
			subscriptions.get(topic).remove(subscriber);
			LOG.log(Level.INFO, "Unsubscibed from broker\n");
		}
	}

	@Override
	public void publish(Visitable message, String topic) {
		publishExceptFor(null, message, topic);
	}

	/**
	 * Publishes the Visitable passed as a parameter to all subscribers except the one associated with the 
	 * {@link PlayerState} passed as a parameter
	 * passed as a parameter
	 * @param excludedPlayer
	 * @param message
	 * @param topic
	 */
	public synchronized void publishExceptFor(PlayerState excludedPlayer, Visitable message, String topic) {
		List<SubscriberUserInterface<Visitable>> setOfSubscribedToTopic = subscriptions.get(topic);
		SubscriberUserInterface<Visitable> tmpSub;
		int subscriberIndex;
		int excudedPlayerID;

		if (setOfSubscribedToTopic != null) {
			
			subscriberIndex = 0;

			while (subscriberIndex < setOfSubscribedToTopic.size()) {
				tmpSub = setOfSubscribedToTopic.get(subscriberIndex);
				if (excludedPlayer == null) {
					tmpSub.dispatchMessage(message);
				} else {
					excudedPlayerID = excludedPlayer.getPlayerData().getPlayerID();
					if (tmpSub.getToken() != null && tmpSub.getToken().getPlayerID() != excudedPlayerID) { // NOSONAR
						tmpSub.dispatchMessage(message);
					}
				}
				subscriberIndex += 1;
			}
		}

	}

	@Override
	public synchronized String toString() {
		String toString;
		Set<String> keySet = subscriptions.keySet();
		Iterator<String> keyIterator = keySet.iterator();

		String tmpKey;
		List<SubscriberUserInterface<Visitable>> tmpSubscriber;
		SubscriberUserInterface<Visitable> tmpSub;

		String topic;
		String userSubscriber;

		toString = "Broker\n";

		while (keyIterator.hasNext()) {
			tmpKey = keyIterator.next();
			tmpSubscriber = subscriptions.get(tmpKey);

			Iterator<SubscriberUserInterface<Visitable>> subIterator = tmpSubscriber.iterator();

			topic = "\nTopic: " + tmpKey + "\n";
			toString += topic;

			while (subIterator.hasNext()) {
				tmpSub = subIterator.next();

				userSubscriber = tmpSub.getSubscriberIdentifier();
				toString += userSubscriber + "\n";
			}
		}

		return toString;
	}

	/**
	 * Unsubscribes all subscribers to this broker
	 * @param subscriber
	 */
	public synchronized void unsubscibeAll(SubscriberUserInterface<Visitable> subscriber) {
		Set<String> listOfTopic = subscriptions.keySet();
		Iterator<String> topicIterator = listOfTopic.iterator();
		String singleTopic;

		while (topicIterator.hasNext()) {
			singleTopic = topicIterator.next();
			unsubscibe(subscriber, singleTopic);
		}
	}

}
