package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;

/**
 * Exception thrown when there's not the {@link Councillor} searched.
 * @author paolasanfilippo
 *
 */
public class NoCouncillorContainedException extends Exception {

	private static final long serialVersionUID = -305105468779734586L;
	/**
	 * Exception thrown when there's not the {@link Councillor} searched.
	 */
	public NoCouncillorContainedException() {
		super();
	}
}
