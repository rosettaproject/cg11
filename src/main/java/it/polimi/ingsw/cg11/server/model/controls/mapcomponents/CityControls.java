package it.polimi.ingsw.cg11.server.model.controls.mapcomponents;

import java.awt.Color;
import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Contains controls on {@link City}
 * 
 * @author Federico
 *
 */
public class CityControls {

	/**
	 * Controls that the player hasn't already built an {@link Emporium} on the
	 * destination city. If there's already an emporium of the same player, it
	 * throws a {@link ExistingEmporiumException}
	 * 
	 * @param destinationCity
	 *            : city on which we look there isn't the player's emporium.
	 *            Cannot be null
	 * @param player
	 *            : owner of the emporium. Cannot be null
	 * @throws ExistingEmporiumException
	 */
	public void emporiumIsNotPresentControl(City destinationCity, PlayerState player) throws ExistEmporiumException {
		if (destinationCity.existsPlayerEmporium(player))
			throw new ExistEmporiumException();
	}

	/**
	 * Controls that there's already an {@link Emporium} from the player passed
	 * as parameter. If there's not an emporium of this player, it throws a
	 * {@link NoSuchEmporiumException}.
	 * 
	 * @param city
	 *            : city on which we look there is the player's emporium. Cannot
	 *            be null
	 * @param player
	 *            : owner of the emporium. Cannot be null
	 * @throws NoSuchEmporiumException
	 */
	public void emporiumIsPresentControl(City city, PlayerState player) throws NoSuchEmporiumException {
		if (!city.existsPlayerEmporium(player))

			throw new NoSuchEmporiumException();
	}

	/**
	 * Tells if a player could earn a city's color bonus.
	 * 
	 * @param gameMap
	 *            :actual map status
	 * @param player
	 *            : player who could earn the bonus
	 * @param city
	 *            : from this city, this method takes the color and uses it to
	 *            look for cities of the same color
	 * @return <tt>true</tt> if the city's color bonus is available for the
	 *         player, <tt>false</tt> if is not.
	 * @throws ColorNotFoundException
	 */
	public boolean isColorBonusAvailable(GameMap gameMap, PlayerState player, City city) throws ColorNotFoundException {

		boolean isColorBonusAvailable = true;
		Color cityColor = city.getCOLOR();
		List<City> mapCities = gameMap.getCities();
		Iterator<City> cityIterator = mapCities.iterator();
		City cityToControl;
		Color purple = ColorMap.getColor("purple");
		
		if(city.getCOLOR().equals(purple)){
			return false;
		}
		
		if (!gameMap.getCityColorBonus(cityColor).isAvailable()) {
			return false;
		}

		while (cityIterator.hasNext() && isColorBonusAvailable) {
			cityToControl = cityIterator.next();
			if (cityToControl.getCOLOR().equals(cityColor) && !cityToControl.existsPlayerEmporium(player)) {

				isColorBonusAvailable = false;

			}
		}

		return isColorBonusAvailable;

	}

}
