/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

/**
 * @author Federico
 *
 */
public class AvailableMandatoryPlayerChoiceException extends Exception {

	private static final long serialVersionUID = 5253714370914748150L;

}
