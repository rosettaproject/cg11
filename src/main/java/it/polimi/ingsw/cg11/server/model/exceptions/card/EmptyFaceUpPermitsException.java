package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;

/**
 * Exception thrown when there aren't {@link FaceUpPermits}
 * @author paolasanfilippo
 *
 */
public class EmptyFaceUpPermitsException extends Exception {

	
	private static final long serialVersionUID = -446346823317001962L;

	/**
	 * Exception thrown when there aren't {@link FaceUpPermits}
	 */
	public EmptyFaceUpPermitsException() {
		super();
	}

	/**
	 * Exception thrown when there aren't {@link FaceUpPermits}
	 * @param cause
	 */
	public EmptyFaceUpPermitsException(Throwable cause) {
		super(cause);
	}

}
