package it.polimi.ingsw.cg11.server.model.utils.visitorpattern;

import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.messages.ErrorMessage;
import it.polimi.ingsw.cg11.messages.GenericMessage;
import it.polimi.ingsw.cg11.messages.ModelChanges;

/**
 * This interface is used to define a visitor that can visit messages that the client receives
 * @author Federico
 *
 */
public interface ClientVisitor extends Visitor{

	/**
	 * This method can visit a {@link ModelChanges} message
	 * @param changes
	 */
	public void visit(ModelChanges changes);

	/**
	 * This method can visit a {@link errorMessage} message
	 * @param errorMessage
	 */
	public void visit(ErrorMessage errorMessage);
	
	/**
	 * This method can visit a {@link genericMessage} message
	 * @param genericMessage
	 */
	public void visit(GenericMessage genericMessage);
	
	/**
	 * This method can visit a {@link chatMessage} message
	 * @param chatMessage
	 */
	public void visit(SerializableChatMessage chatMessage);
}
