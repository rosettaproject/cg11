package it.polimi.ingsw.cg11.server.model.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;

/**
 * Generic class to represent a deck of cards as a queue
 * @author francesco
 *
 * @param <E>
 */


public class Deck<E extends Card> {
		
	private Queue<E> cards;
	
	/**
	 * This constructor gets the queue of cards as a parameter
	 * @param cards: Cannot be null
	 */
	public Deck(Queue<E> cards) {
		
		this.cards = cards;
	}
	/**
	 * Add a {@link Card} to the bottom of the Deck
	 * @param card: Cannot be null
	 */
	public void addToDeck(E card){
		cards.add(card);
	}
	
	/**
	 * Add a queue of {@link Card}s to the bottom of the Deck
	 * @param cards
	 */
	public void addAllToDeck(Queue<E> cardsToAdd){
		cards.addAll(cardsToAdd);
	}
	
	/**
	 * Shuffles the deck using the shuffle method of {@link Collections}
	 * 
	 */
	public void shuffle() {
		List<E> listOfCards = new ArrayList<>(cards);
		Collections.shuffle(listOfCards);
		cards = new LinkedList<>(listOfCards);
	}
	
	/**
	 * If the deck has some elements in it, returns a {@link Card}. Otherwise, the deck is empty and this method throws an {@link EmptyDeckException}.
	 * @return card: the drawn card
	 * @throws EmptyDeckException 
	 */
	public E draw() throws EmptyDeckException{
		if(cards.isEmpty())
			throw new EmptyDeckException();
		else{
			return cards.remove();
		} 
	}
	/**
	 * Checks if the deck is empty or not
	 * @return <tt>true</tt> if the deck is empty, <tt>false</tt> if is not
	 */
	public boolean isEmpty(){
		return cards.isEmpty();
	}
	/**
	 * Returns how many cards there are in the deck
	 * @return number of cards in the deck
	 */
	public int numberOfCards(){
		return cards.size();
	}
	/**
	 * Returns the cards in the deck
	 * @return cards
	 */
	public Queue<E> getCards(){
		return cards;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "DECK: " + cards.size() + " cards\n\n";
		Iterator<E> iterator = cards.iterator();
		while(iterator.hasNext())
			toString = toString + iterator.next().toString() + "\n\n";
		toString = toString + "--------------";
		return toString;
	}
		
	
}