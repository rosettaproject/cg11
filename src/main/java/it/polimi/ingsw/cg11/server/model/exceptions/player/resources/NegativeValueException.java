package it.polimi.ingsw.cg11.server.model.exceptions.player.resources;

/**
 * Exception thrown when the value passed is negative and it shouldn't be.
 * @author paolasanfilippo
 *
 */
public class NegativeValueException extends Exception {

	private static final long serialVersionUID = 3732126838320750370L;

	/**
	 * Exception thrown when the value passed is negative and it shouldn't be.
	 */
	public NegativeValueException() {
		super();
	}
	/**
	 * Exception thrown when the value passed is negative and it shouldn't be.
	 * @param message
	 */
	public NegativeValueException(String message) {
		super(message);
	}

	
}
