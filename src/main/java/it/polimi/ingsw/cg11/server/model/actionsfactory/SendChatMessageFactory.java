package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.player.actions.SendChatMessage;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * Factory for {@link SendChatMessage}
 * @author paolasanfilippo
 *
 */
public class SendChatMessageFactory extends ActionFactory {

	private JSONObject actionData;

	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		Token senderPlayerToken;

		String senderUsername;
		String recipient;
		String message;

		actionData = new JSONObject(action.getJsonConfigAsString());
		senderPlayerToken = action.getPlayerToken();
		senderUsername = senderPlayerToken.getClientUsername();
		recipient = actionData.getString("player");
		message = actionData.getString("message");

		return new SendChatMessage(senderUsername, recipient, message, gameMap.getGameTopic());

	}

}
