package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;

/**
 * Exception thrown when there aren't the {@link FaceUpPermits} searched.
 * @author paolasanfilippo
 *
 */
public class NoSuchFaceUpPermitsException extends Exception {

	private static final long serialVersionUID = -3630250976956360936L;

	/**
	 * Exception thrown when there aren't the {@link FaceUpPermits} searched.
	 */
	public NoSuchFaceUpPermitsException() {
		super();
	}
	/**
	 * Exception thrown when there aren't the {@link FaceUpPermits} searched.
	 * @param message
	 */
	public NoSuchFaceUpPermitsException(String message) {
		super(message);
	}


}
