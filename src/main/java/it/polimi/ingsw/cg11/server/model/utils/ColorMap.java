package it.polimi.ingsw.cg11.server.model.utils;

import java.awt.Color;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;

/**
 * Static map to associate a string with a {@link Color}
 * 
 * @author francesco
 *
 */
public class ColorMap {

	private static final DualHashBidiMap<String, Color> COLOR_MAP;
	
	static {
		COLOR_MAP = new DualHashBidiMap<>();
		COLOR_MAP.put("YELLOW", Color.YELLOW);
		COLOR_MAP.put("RED", Color.RED);
		COLOR_MAP.put("GRAY", Color.GRAY);
		COLOR_MAP.put("BLUE", Color.BLUE);
		COLOR_MAP.put("GREEN", Color.GREEN);
		COLOR_MAP.put("ORANGE", Color.ORANGE);
		COLOR_MAP.put("PINK", Color.PINK);
		COLOR_MAP.put("BLACK", Color.BLACK);
		COLOR_MAP.put("WHITE", Color.WHITE);
		COLOR_MAP.put("CYAN", Color.CYAN);
		COLOR_MAP.put("PURPLE", new Color(138, 43, 226));
		COLOR_MAP.put("MULTICOLOR", new Color(127, 126, 125));
	}
	
	/**
	 * Private constructor to hide the default public one
	 */
	private ColorMap() {
		
	}
	/**
	 * Returns the color from the string
	 * @param color
	 * @return Color
	 * @throws ColorNotFoundException
	 */
	public static Color getColor(String color) throws ColorNotFoundException {
		Color colorToReturn;
		colorToReturn = COLOR_MAP.get(color.toUpperCase());
		if (colorToReturn == null)
			throw new ColorNotFoundException();
		return colorToReturn;
	}
	/**
	 * Returns the string from the color
	 * @param color
	 * @return the name of the color
	 * @throws ColorNotFoundException
	 */
	public static String getString(Color color) throws ColorNotFoundException {
		String colorToReturn;
		colorToReturn = COLOR_MAP.getKey(color);
		if (colorToReturn == null)
			throw new ColorNotFoundException();
		return colorToReturn;
	}

}
