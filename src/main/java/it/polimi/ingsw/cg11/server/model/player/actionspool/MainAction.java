package it.polimi.ingsw.cg11.server.model.player.actionspool;

/**
 * If this object is in the player's available actions list, he/she can perform an
 *  additional main action
 * 
 * @author francesco
 *
 */
public class MainAction extends GameAction {
	
	/**
	 * Constructor
	 */
	public MainAction() {
		super("MainAction");
	}
}
