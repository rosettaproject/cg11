package it.polimi.ingsw.cg11.server.model.mapcomponents.tokens;


import org.json.JSONArray;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;

/**
 * CouncillorPoolManager creates the Councillors, converting the informations
 * obtained by file, as {@link CouncillorPoolManager} does. In addition to this, it shuffles the councillor.
 * Throws a {@link ColorNotFoundException} if the color chosen for a councillor is not one of the allowed ones.
 * @author Paola
 *
 */
public class RandomCouncillorPoolManager extends CouncillorPoolManager {

	/**
	 * Works as a {@link CouncillorPoolManager} but shuffling the {@link Councillor}s
	 * @param councillorPool
	 * @throws ColorNotFoundException
	 */
	public RandomCouncillorPoolManager(JSONArray councillorPool) throws ColorNotFoundException {
		super(councillorPool);
		shuffleCouncillors();
	}

}
