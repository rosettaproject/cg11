package it.polimi.ingsw.cg11.server.model.exceptions.player.resources;

/**
 * Exception thrown when the player's resources aren't acceptable.
 * @author paolasanfilippo
 *
 */
public class IllegalResourceException extends Exception {

	private static final long serialVersionUID = -410543493379861849L;

	/**
	 * Exception thrown when the player's resources aren't acceptable.
	 */
	public IllegalResourceException() {
		super();
	}
	/**
	 * Exception thrown when the player's resources aren't acceptable.
	 * @param cause
	 */
	public IllegalResourceException(Throwable cause) {
		super(cause);
	}


}
