package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Interface to be implemented by any class used to transfer a resource to a player.
 * The transaction details are to be specified in the constructor 
 * @author francesco
 *
 */

public interface Transaction {

	/**
	 * Executes the transaction
	 * @param transactionSubject
	 * @throws TransactionException
	 */
	abstract void execute(PlayerState transactionSubject) throws TransactionException;
	
	/**
	 * Returns the amount of object traded
	 * @return amount
	 */
	abstract int getAmount();
}