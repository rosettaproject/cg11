package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.*;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.CannotCreateMapException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.ExistEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.*;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.pieces.King;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.*;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.StandardPlayerDataFactory;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.BonusFactory;

/**
 * Factory of a standard map
 * 
 * @author paolasanfilippo
 *
 */
public class StandardMapFactory {

	private static final String CITIES_KEY = "Cities";
	private static final int NUMBER_OF_CITIES = 15;
	private static final int NUMBER_OF_REGIONS = 3;
	private static final int KING_MOVING_COST = 2;
	private static final String STARTING_GAME_PHASE = "StandardPlayPhase";
	private static final int EMPORIUMS_PER_PLAYER = 10;
	private JSONObject mapStartupConfig;
	private List<ModelChanges> startingHands;
	private String kingCityName;
	private GraphedMap graphedMap;
	private EmporiumPoolManager emporiumPoolManager;
	private static final Logger LOG = Logger.getLogger(StandardMapFactory.class.getName());
	private static final int FAKE_PLAYER_INDEX = -1;

	/**
	 * Given a JSON object, creates a standard map
	 * 
	 * @param mapConfig
	 * @param numberOfPlayers
	 * @param gameTopic
	 * @throws NoSuchPlayerException
	 * @throws NoSuchEmporiumException
	 * @throws NoSuchCouncillorException
	 * @throws NoSuchRegionException
	 * @throws JSONException
	 * @throws EmptyPermitDeckException
	 * @throws ColorNotFoundException
	 * @return a game map
	 */
	public GameMap createMap(JSONObject mapConfig, int numberOfPlayers, String gameTopic)
			throws CannotCreateMapException {

		/*
		 * push kingCityName into the map Randomize bonus
		 */
		GameMap standardMap = new GameMap(getStartingGamePhase());

		// this JSONObject will be sent to the clients to allow the
		// initialization of a game
		mapStartupConfig = new JSONObject();

		StandardPlayerDataFactory standardPlayerDataFactory = new StandardPlayerDataFactory();
		StandardRegionFactory standardRegionFactory = new StandardRegionFactory();

		ArrayList<City> cities = new ArrayList<>();
		CityFactory cityFactory = new CityFactory();

		try {
			assignKingCity(mapConfig);
			assignPoliticsCardDeck(standardMap, mapConfig);
			assignPlayers(standardMap, standardMap.getPoliticsDeck(), standardMap.getSecondPoliticsDeck(),
					numberOfPlayers, standardPlayerDataFactory);
			assignPoolManagers(mapConfig, standardMap, standardPlayerDataFactory);
			assignNobilityPath(mapConfig, standardMap);
			assignRegions(mapConfig, standardMap, standardRegionFactory);
			assignCities(mapConfig, standardMap, cityFactory, cities);	
			assignPermitsDeckToRegion(mapConfig, standardMap);
			if(numberOfPlayers == 2)
				addExtraEmporiums(standardMap);
			assignFaceUpPermits(standardMap);
			assignKingBalcony(standardMap);
			assignKingBonuses(mapConfig, standardMap);
			assignKing(standardMap, cities);
			assignCityColorBonusList(mapConfig, standardMap);
		} catch (JSONException | ColorNotFoundException | NoSuchPlayerException | NoSuchEmporiumException
				| NoSuchRegionException | NoSuchCouncillorException | EmptyPermitDeckException e) {
			throw new CannotCreateMapException(e);
		}

		setGraphedMap(GraphedMap.createGraphedMap(standardMap));
		standardMap.setGraphedMap(getGraphedMap());
		standardMap.setGameTopic(gameTopic);
		return standardMap;
	}

	/**
	 * Method used only when there's to configure a game with only two players
	 * @param standardMap
	 */
	private void addExtraEmporiums(GameMap standardMap) {
	
		List<Region> regions = standardMap.getRegions();
		Iterator<Region> regionsIterator = regions.listIterator();
		List<City> cities = new ArrayList<>();
		
		while(regionsIterator.hasNext()){
			
			PermitCard[] deckAsArray ={};
			
			deckAsArray = regionsIterator.next().getPermitCardsDeck().getCards().toArray(deckAsArray);
			
			int ranomIndex = new Random().nextInt(deckAsArray.length-1);
			
			PermitCard extractedCard = deckAsArray[ranomIndex];
			
			cities.addAll(extractedCard.getCities());
		}
		
		try {
			JSONArray dummyEmporiums = new JSONArray();
			
			for(City city : cities){
				dummyEmporiums.put(city.getName());
				city.addEmporium(getEmporiumPoolManager().obtainPieceFromPool(FAKE_PLAYER_INDEX));
			}
			mapStartupConfig.put("DummyEmporiums", dummyEmporiums);
		} catch (ExistEmporiumException | NoSuchEmporiumException e) {
			LOG.log(Level.SEVERE, "Configuration error", e);
		}
		
	}

	/**
	 * Creates a queue with councillor, taken from the map
	 * 
	 * @param standardMap
	 * @return councillorQueue
	 * @throws NoSuchCouncillorException
	 */
	public Queue<Councillor> createCouncillorQueue(GameMap standardMap) throws NoSuchCouncillorException {

		ArrayBlockingQueue<Councillor> councillorQueue = new ArrayBlockingQueue<>(Balcony.getMaxSize());

		while (councillorQueue.remainingCapacity() != 0)
			councillorQueue.add(standardMap.getCouncillorsPool().obtainPieceFromPool());

		return councillorQueue;
	}

	/**
	 * Sets the {@link King}'s {@link City}, taking it from the JSONObject
	 * passed as parameter
	 * 
	 * @param mapConfig
	 */
	public void assignKingCity(JSONObject mapConfig) {
		this.kingCityName = mapConfig.getString("KingCity");
		this.mapStartupConfig.put("KingCity", kingCityName);
	}

	/**
	 * Sets and assigns to the map a {@link PoliticCard} {@link Deck}
	 * 
	 * @param standardMap
	 * @param mapConfig
	 * @throws ColorNotFoundException
	 * @throws JSONException
	 */
	public void assignPoliticsCardDeck(GameMap standardMap, JSONObject mapConfig) throws ColorNotFoundException {

		PoliticsDeckFactory politicsDeckFactory = new PoliticsDeckFactory();
		Deck<PoliticCard> politicCardsDeck;

		politicCardsDeck = politicsDeckFactory.createDeck(mapConfig.getJSONArray("PoliticsDeck"));
		standardMap.setPoliticsDeck(politicCardsDeck);
	}

	/**
	 * Sets and assigns to the map the players
	 * 
	 * @param standardMap
	 * @param politicCardsDeck
	 *            : deck of politic cards from which the player will draw
	 * @param secondDeck
	 *            : deck of discarded politic cards
	 * @param numberOfPlayers
	 * @param standardPlayerDataFactory
	 * @throws ColorNotFoundException
	 */
	public void assignPlayers(GameMap standardMap, Deck<PoliticCard> politicCardsDeck, Deck<PoliticCard> secondDeck,
			int numberOfPlayers, StandardPlayerDataFactory standardPlayerDataFactory) throws ColorNotFoundException {

		List<PlayerState> players = new ArrayList<>();
		List<ModelChanges> playersHands = new ArrayList<>();

		int playerID;

		PlayerState player;

		for (playerID = 0; playerID < numberOfPlayers; playerID++) {

			try {
				player = new PlayerState(
						standardPlayerDataFactory.initializePlayerData(playerID, politicCardsDeck, secondDeck));
			} catch (IllegalResourceException e) {
				LOG.log(Level.SEVERE, "PlayerData initialization error\n", e );
				return;
			}
			player.getPlayerActionExecutor().setActionHandlers();
			playersHands.add(getStartingHandConfig(player));
			players.add(player);
		}

		standardMap.setPlayers(players);
		standardMap.setTurnManager();
		standardMap.getTurnManager().setPlayers(players);
		startingHands = playersHands;
		mapStartupConfig.put("numberOfPlayers", players.size());
	}

	/**
	 * Gets the starting hand configuration of a player passed as parameter
	 * 
	 * @param player
	 * @return starting hand configuration as model change
	 * @throws ColorNotFoundException
	 */
	private ModelChanges getStartingHandConfig(PlayerState player) throws ColorNotFoundException {

		List<PoliticCard> politicCards;
		ModelChanges startingHand = new ModelChanges();

		politicCards = player.getPlayerData().getPoliticsHand().getCards();

		int index = 0;
		for (PoliticCard card : politicCards) {

			startingHand.addToChanges(String.valueOf(index), card);
			index++;
		}

		return startingHand;
	}

	/**
	 * Sets and assigns to the map the pool managers {@link EmporiumPoolManager}
	 * and {@link CouncillorPoolManager}
	 * 
	 * @param mapConfig
	 * @param standardMap
	 * @param standardPlayerDataFactory
	 * @throws NoSuchPlayerException
	 * @throws NoSuchEmporiumException
	 * @throws ColorNotFoundException
	 */
	public void assignPoolManagers(JSONObject mapConfig, GameMap standardMap,
			StandardPlayerDataFactory standardPlayerDataFactory)
			throws NoSuchPlayerException, NoSuchEmporiumException, ColorNotFoundException {

		setEmporiumPoolManager(new EmporiumPoolManager(standardMap, getEmporiumsPerPlayer()));

		CouncillorPoolManager councillorPoolManager = new CouncillorPoolManager(
				mapConfig.getJSONArray("CouncillorPool"));

		standardMap.setCouncillorsPool(councillorPoolManager);
		standardMap.setEmporiumPool(getEmporiumPoolManager());

		ListIterator<PlayerState> playerIterator = standardMap.getPlayers().listIterator();

		while (playerIterator.hasNext()) {
			standardPlayerDataFactory.acquireEmporiums(playerIterator.next().getPlayerData(), getEmporiumPoolManager());
		}
	}

	/**
	 * Sets and assigns to the map the {@link Region}s
	 * 
	 * @param mapConfig
	 * @param standardMap
	 * @param standardRegionFactory
	 * @throws NoSuchCouncillorException
	 */
	public void assignRegions(JSONObject mapConfig, GameMap standardMap, StandardRegionFactory standardRegionFactory)
			throws NoSuchCouncillorException {
		int regionIndex;
		ArrayList<Region> regions = new ArrayList<>();
		JSONArray array = mapConfig.getJSONArray("Regions");

		for (regionIndex = 0; regionIndex < getNumberOfRegions(); regionIndex++) {

			regions.add(standardRegionFactory.createRegion(array.getJSONObject(regionIndex), standardMap));
		}
		standardMap.setRegions(regions);
		mapStartupConfig.put("Regions", array);
	}

	/**
	 * Sets and assigns to the map the {@link City}s
	 * 
	 * @param mapConfig
	 * @param standardMap
	 * @param cityFactory
	 * @param cities
	 * @throws NoSuchRegionException
	 * @throws ColorNotFoundException
	 */
	public void assignCities(JSONObject mapConfig, GameMap standardMap, CityFactory cityFactory, List<City> cities)
			throws NoSuchRegionException, ColorNotFoundException {

		int cityIndex;
		int bonusIndex;
		JSONArray cityBonuses = mapConfig.getJSONArray("CityBonuses");

		for (cityIndex = 0, bonusIndex = cityIndex; cityIndex < getNumberOfCities(); cityIndex++, bonusIndex++) {

			cities.add(cityFactory.createCity(mapConfig.getJSONArray(CITIES_KEY).getJSONObject(cityIndex),
					cityBonuses, getKingCityName(), bonusIndex, standardMap));

			// If is the king's city, the bonus at the bonusIndex position needs
			// to be re used
			if (cities.get(cityIndex).getName().equals(getKingCityName()))
				bonusIndex--;
		}

		standardMap.setCities(cities);
		mapStartupConfig.put("CityBonuses", cityBonuses);

		ListIterator<Region> regionIterator = standardMap.getRegions().listIterator();
		ListIterator<City> cityIterator = cities.listIterator();

		JSONObject adjacentCities =  mapConfig.getJSONObject("Adjacents");
		JSONArray citiesJsonArray = mapConfig.getJSONArray(CITIES_KEY);

		City city;

		while (cityIterator.hasNext()) {
			city = cityIterator.next();
			city.initializeAdjacentCities(cityFactory.getAdjacentCities(city.getName(), adjacentCities), standardMap);
		}

		mapStartupConfig.put("Adjacents", adjacentCities);

		mapStartupConfig.put(CITIES_KEY,citiesJsonArray);


		while (regionIterator.hasNext())
			regionIterator.next().setCities(standardMap);

	}

	/**
	 * Sets and assigns to the map a {@link PermitCard} deck to each
	 * {@link Region}
	 * 
	 * @param mapConfig
	 * @param standardMap
	 */
	public void assignPermitsDeckToRegion(JSONObject mapConfig, GameMap standardMap) {

		ListIterator<Region> regionIterator;

		PermitsDeckFactory permitsDeckFactory = new PermitsDeckFactory();
		Deck<PermitCard> permitCardsDeck;
		Region region;

		int iteratorIndex;
		regionIterator = standardMap.getRegions().listIterator();

		while (regionIterator.hasNext()) {
			iteratorIndex = regionIterator.nextIndex();
			region = regionIterator.next();
			permitCardsDeck = permitsDeckFactory
					.createDeck(mapConfig.getJSONArray("PermitDecks").getJSONArray(iteratorIndex), region, standardMap);
			region.setPermitCardsDeck(permitCardsDeck);
		}
	}

	/**
	 * Sets and assigns to the map the {@link FaceUpPermits} to each
	 * {@link Region}
	 * 
	 * @param standardMap
	 * @throws EmptyPermitDeckException
	 */
	public void assignFaceUpPermits(GameMap standardMap) throws EmptyPermitDeckException {

		ListIterator<Region> regionIterator = standardMap.getRegions().listIterator();
		Region region;

		while (regionIterator.hasNext()) {
			region = regionIterator.next();
			region.setFaceUpPermitCards(new FaceUpPermits(region.getPermitCardsDeck(), region));
		}
	}

	/**
	 * Sets and assigns to the map a {@link King}'s {@link Balcony}
	 * 
	 * @param standardMap
	 * @throws NoSuchCouncillorException
	 */
	public void assignKingBalcony(GameMap standardMap) throws NoSuchCouncillorException {

		standardMap.setKingBalcony(new Balcony("King", createCouncillorQueue(standardMap)));
	}

	/**
	 * Sets and assigns to the map a {@link NobilityPath}
	 * 
	 * @param mapConfig
	 * @param standardMap
	 */
	public void assignNobilityPath(JSONObject mapConfig, GameMap standardMap) {
		BonusFactory bonusFactory = new BonusFactory();
		JSONArray nobilityPath = mapConfig.getJSONArray("NobilityPath");

		standardMap.setNobilityPath(new NobilityPath(bonusFactory.createListOfBonus(nobilityPath, standardMap)));
		mapStartupConfig.put("NobilityPath", nobilityPath);
	}

	/**
	 * Sets and assigns to the map the {@link City}'s color bonuses
	 * 
	 * @param mapConfig
	 * @param standardMap
	 * @throws ColorNotFoundException
	 */
	public void assignCityColorBonusList(JSONObject mapConfig, GameMap standardMap) throws ColorNotFoundException {
		BonusFactory bonusFactory = new BonusFactory();
		JSONArray cityColorBonusList = mapConfig.getJSONArray("CityColorBonusList");

		standardMap.setCityColorBonusList(bonusFactory.createListOfColoredBonus(cityColorBonusList, standardMap));
		mapStartupConfig.put("CityColorBonusList", cityColorBonusList);
	}

	/**
	 * Sets and assigns to the map a {@link King}'s {@link Bonus}es.
	 * 
	 * @param mapConfig
	 * @param standardMap
	 */
	public void assignKingBonuses(JSONObject mapConfig, GameMap standardMap) {

		BonusFactory bonusFactory = new BonusFactory();
		JSONArray kingBonuses = mapConfig.getJSONArray("KingBonuses");
		standardMap.setKingBonuses(bonusFactory.createListOfBonus(kingBonuses, standardMap));
		mapStartupConfig.put("KingBonuses", kingBonuses);
	}

	/**
	 * Sets and assigns to the map a {@link King}, starting from the king's city
	 * 
	 * @param standardMap
	 * @param cities
	 */
	public void assignKing(GameMap standardMap, List<City> cities) {

		ListIterator<City> cityIterator = cities.listIterator();
		City city;

		while (cityIterator.hasNext()) {
			city = cityIterator.next();
			if (city.getName().equals(getKingCityName()))
				standardMap.setKing(new King(city, KING_MOVING_COST));
		}

	}

	/**
	 * Shuffles a {@link JSONArray} containing {@link JSONObject}s by creating a
	 * list of such objects, shuffling it and than assigning it back to the
	 * JSONArray. Only works if the array contains JSONObjects
	 * 
	 * @param arrayToShuffle
	 * @return shuffled JSONArray
	 */
	protected JSONArray shuffleJsonArray(JSONArray arrayToShuffle) {

		JSONArray shuffledArray = new JSONArray();
		List<JSONObject> objects = new ArrayList<>();
		Iterator<Object> jsonArrayIterator = arrayToShuffle.iterator();

		while (jsonArrayIterator.hasNext())

			objects.add((JSONObject) jsonArrayIterator.next());

		Collections.shuffle(objects);

		for (JSONObject object : objects)
			shuffledArray.put(object);
		return shuffledArray;
	}

	/**
	 * Shuffles a JSONArray of arrays
	 * 
	 * @param arrayToShuffle
	 * @return shuffled jsonarray
	 */
	protected JSONArray shuffleJsonArrayOfArrays(JSONArray arrayToShuffle) {

		JSONArray shuffledArray = new JSONArray();
		List<JSONArray> arrays = new ArrayList<>();
		Iterator<Object> jsonArrayIterator = arrayToShuffle.iterator();

		while (jsonArrayIterator.hasNext())

			arrays.add((JSONArray) jsonArrayIterator.next());

		Collections.shuffle(arrays);

		for (JSONArray array : arrays)
			shuffledArray.put(array);
		return shuffledArray;
	}

	/**
	 * Returns the starting {@link GamePhase}
	 * 
	 * @return the startingGamePhase
	 */
	public static String getStartingGamePhase() {
		return STARTING_GAME_PHASE;
	}

	/**
	 * Returns the starting {@link King}'s {@link City} name
	 * 
	 * @return the kingCityName
	 */
	public String getKingCityName() {
		return kingCityName;
	}


	/**
	 * Returns the {@link GraphedMap}
	 * 
	 * @return the graphedMap
	 */
	public GraphedMap getGraphedMap() {
		return graphedMap;
	}

	/**
	 * Sets the {@link GraphedMap}
	 * 
	 * @param graphedMap
	 *            the graphedMap to set
	 */
	public void setGraphedMap(GraphedMap graphedMap) {
		this.graphedMap = graphedMap;
	}
	/**
	 * Returns the {@link EmporiumPoolManager}
	 * @return emporium pool manager
	 */
	public EmporiumPoolManager getEmporiumPoolManager() {
		return emporiumPoolManager;
	}
	/**
	 * Sets the {@link EmporiumPoolManager}
	 * @param emporiumPoolManager
	 */
	public void setEmporiumPoolManager(EmporiumPoolManager emporiumPoolManager) {
		this.emporiumPoolManager = emporiumPoolManager;
	}

	/**
	 * Returns the amount of {@link Emporium}s per player
	 * 
	 * @return the emporiumsPerPlayer
	 */
	public static int getEmporiumsPerPlayer() {
		return EMPORIUMS_PER_PLAYER;
	}

	/**
	 * Returns the number of {@link City}
	 * 
	 * @return number of cities
	 */
	public static int getNumberOfCities() {
		return NUMBER_OF_CITIES;
	}

	/**
	 * Returns the number of {@link Region}s in the map
	 * 
	 * @return number of regions
	 */
	public static int getNumberOfRegions() {
		return NUMBER_OF_REGIONS;
	}

	/**
	 * Returns a {@link JSONObject} containing the necessary information for a
	 * client to initialize the game. Call this method only after a map has been
	 * created using this factory, or else this method will return null
	 * 
	 * @return the startup map configuration for the clients
	 */
	public JSONObject getMapStartupConfig() {
		return mapStartupConfig;
	}

	/**
	 * Gets the starting hand configuration
	 * 
	 * @return a list with all the starting hands
	 */
	public List<ModelChanges> getStartingHands() {
		return startingHands;
	}

}