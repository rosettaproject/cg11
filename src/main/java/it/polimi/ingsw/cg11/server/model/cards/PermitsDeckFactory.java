package it.polimi.ingsw.cg11.server.model.cards;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.BonusFactory;

/**
 * Implements the {@link Deck} factory for the permit {@link Card}s. Assumes
 * {@link City}s have already been created in {@link Region}. Uses
 * {@link org.json.}
 * 
 * @author francesco
 */

public class PermitsDeckFactory {

	/**
	 * Realizes the factory
	 * 
	 * @param deckConfig
	 *            : a JSONArray containing the cards. Cannot be null
	 * @param region
	 *            : the permit deck's {@link Region}}. Cannot be null
	 * @param gameMap
	 *            : used in bonus factory to locate the {@link Deck} of permit
	 *            cards. Cannot be null
	 * @return {@link Deck} of permit cards created
	 */

	public Deck<PermitCard> createDeck(JSONArray deckConfig, Region region, GameMap gameMap) {


		LinkedList<PermitCard> cards = new LinkedList<>();
		PermitCard permitCard;

		JSONArray permitConfig;

		Iterator<Object> deckIterator = deckConfig.iterator();

		while (deckIterator.hasNext()) {	
			
			permitConfig = (JSONArray) deckIterator.next();

			permitCard = createPermitCardFromConfig(permitConfig,region,gameMap);

			cards.add(permitCard);
		}
		return new Deck<>(cards);
	}

	/**
	 * Returns a list of {@link City}s contained in {@link Region} which were
	 * also named in the permit card initialization file
	 * 
	 * @param permitCityNames
	 *            : A list of strings containing the name of cities, that are in
	 *            the permit card
	 * @param region
	 *            : The region of the permit deck
	 * @return list of cities in the permit card
	 */
	private ArrayList<City> getCityList(ArrayList<String> permitCityNames, Region region) {

		List<City> regionCities = region.getCities();
		ArrayList<City> permitCities = new ArrayList<>();
		City regionCity;

		ListIterator<City> cityIterator = regionCities.listIterator();

		while (cityIterator.hasNext()) {
			regionCity = cityIterator.next();
			if (permitCityNames.contains(regionCity.getName()))
				permitCities.add(regionCity);
		}
		return permitCities;
	}

	/**
	 * Returns a {@link PermitCard} with the bonus and city list specified in the permitConfig JSONArray
	 * @param permitConfig
	 * @param region
	 * @param gameMap
	 * @return a {@link PermitCard}
	 */
	public PermitCard createPermitCardFromConfig(JSONArray permitConfig, Region region, GameMap gameMap) {

		JSONArray cityNames;
		JSONObject permitBonus;
		ArrayList<City> cities;
		ArrayList<String> cityNamesToString;
		Bonus bonus;
		BonusFactory bonusFactory;
		int cityIndex;

		bonusFactory = new BonusFactory();
		cityNamesToString = new ArrayList<>();

		cityNames = permitConfig.getJSONArray(0);
		permitBonus = permitConfig.getJSONObject(1);

		//creates a list of the city names as strings
		for (cityIndex = 0; cityIndex < cityNames.length(); cityIndex++) {
			cityNamesToString.add(cityNames.getString(cityIndex));
		}

		cities = getCityList(cityNamesToString, region);
		bonus = bonusFactory.createBonus(permitBonus, gameMap);

		return new PermitCard(bonus, cities);
	}
}