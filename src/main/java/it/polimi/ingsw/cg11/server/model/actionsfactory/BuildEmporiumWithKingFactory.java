package it.polimi.ingsw.cg11.server.model.actionsfactory;

import java.util.List;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithKing;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Factory for {@link BuildEmporiumWithKing}
 * @author paolasanfilippo
 *
 */
public class BuildEmporiumWithKingFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
				
		PlayerState player;
		City destinationCity;
		List<PoliticCard> politicCards;
		
		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			destinationCity = getCityFromActionData(actionData,gameMap,"city");
			politicCards = createPoliticCardsFromActionData(actionData,"politiccards");
			
			return new BuildEmporiumWithKing(gameMap,player,destinationCity,politicCards, gameMap.getGameTopic());
			
		} catch (NoSuchPlayerException | NoSuchCityException |  ColorNotFoundException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
