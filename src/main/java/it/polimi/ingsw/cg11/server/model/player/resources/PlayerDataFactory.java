package it.polimi.ingsw.cg11.server.model.player.resources;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;

/**
 * Implement this interface to create a factory used to initialize a player's {@link PlayerData}
 * 
 * @author francesco
 * 
 */
public interface PlayerDataFactory {

	/**
	 * 
	 * Initializes the resources based on the player's ID and returns the newly
	 * created PlayerData
	 * 
	 * @param playerID
	 *            player unique identifier
	 * @param politicsDeck :
	 *            {@link PoliticCardsDeck} to draw
	 * @param secondDeck : {@link PoliticCardsDeck} discarded
	 * @return PlayerData {@link PlayerData}
	 * @throws IllegalResourceException 
	 * 
	 */
	public abstract PlayerData initializePlayerData(int playerID, Deck<PoliticCard> politicsDeck,Deck<PoliticCard> secondDeck) throws IllegalResourceException;

}
