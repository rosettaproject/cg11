package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;

/**
 * Exception thrown when the {@link GamePhase} performed is not the expected one.
 * @author paolasanfilippo
 *
 */
public class NotCorrectGamePhaseException extends Exception {

	private static final long serialVersionUID = -4905011346900790060L;
	
	/**
	 * Exception thrown when the {@link GamePhase} performed is not the expected one.
	 */
	public NotCorrectGamePhaseException() {
		super();
	}
	
	public NotCorrectGamePhaseException(String message) {
		super(message);
	}
}
