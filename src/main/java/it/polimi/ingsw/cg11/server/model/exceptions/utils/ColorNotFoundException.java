package it.polimi.ingsw.cg11.server.model.exceptions.utils;

import java.awt.Color;

/**
 * Exception thrown when the wanted {@link Color} does not exist.
 * @author paolasanfilippo
 *
 */
public class ColorNotFoundException extends Exception {

	private static final long serialVersionUID = -1959747463669299667L;
	/**
	 * Exception thrown when the wanted {@link Color} does not exist.
	 */
	public ColorNotFoundException() {
		super();
	}
}
