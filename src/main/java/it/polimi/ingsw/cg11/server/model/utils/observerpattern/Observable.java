package it.polimi.ingsw.cg11.server.model.utils.observerpattern;

import java.rmi.RemoteException;

/**
 * This interface defines a generic Observable
 * @author Federico
 *
 * @param <M>
 */
public interface Observable<M> {

	/**
	 * Used when the observable wants to notify some updates to the observers
	 * @param message represents the update
	 * @throws RemoteException
	 */
	public void notifyToObservers(M message) throws RemoteException;
	
	/**
	 * Adds a new observer
	 * 
	 * @param observer
	 * @return <tt>true</tt> if the observer was not previously added
	 * @throws RemoteException
	 */
	public boolean addObserver(Observer<M> observer) throws RemoteException;
	
	/**
	 * Removes the observer
	 * 
	 * @param observer
	 * @return <tt>true</tt> if the observer is removed successfully 
	 * @throws RemoteException
	 */
	public void removeObserver(Observer<M> observer) throws RemoteException;
	
}
