package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchCityException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.player.actions.BuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link BuildEmporiumWithPermitCard}
 * @author paolasanfilippo
 *
 */
public class BuildEmporiumWithPermitCardFactory extends ActionFactory{

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {

		PlayerState player;
		City destinationCity;
		PermitCard permitCard;
		int permitIndex;
		
		try {		
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			destinationCity = getCityFromActionData(actionData,gameMap,"city");
			permitCard = createPermitCardFromActionData(actionData, gameMap, "region","permitcard");
			
			permitIndex = player.getPlayerData().getPermitsHand().indexOf(permitCard);
			
			if(permitIndex<0)
				throw new NoSuchPermitCardException();
			
			permitCard = player.getPlayerData().getPermitsHand().getCard(permitIndex);
			
			return new BuildEmporiumWithPermitCard(gameMap,player,destinationCity,permitCard, gameMap.getGameTopic());
		} catch (NoSuchRegionException | NoSuchPlayerException | NoSuchCityException | NoSuchPermitCardException e) {
			throw new IllegalCreationCommandException(e);
		}
	}
	
}
