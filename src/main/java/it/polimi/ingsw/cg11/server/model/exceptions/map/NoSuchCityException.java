package it.polimi.ingsw.cg11.server.model.exceptions.map;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;

/**
 * Exception thrown when the searched {@link City} doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchCityException extends Exception {

	private static final long serialVersionUID = -9133646163617107099L;

	/**
	 * Exception thrown when the searched {@link City} doesn't exist.
	 */
	public NoSuchCityException() {
		super();
	}
}
