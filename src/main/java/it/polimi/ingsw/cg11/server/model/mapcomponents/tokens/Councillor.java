package it.polimi.ingsw.cg11.server.model.mapcomponents.tokens;

import java.awt.Color;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Councillor is a token that represents a councillor. It can be distinguished
 * by a color.
 * 
 * @author Paola
 *
 */
public class Councillor {

	private final Color color;

	/**
	 * @param color
	 */
	protected Councillor(Color color) {
		this.color = color;
	}
	/**
	 * Returns the councillor's color
	 * @return
	 */
	public Color getColor() {
		return this.color;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString;
		try {
			toString = ColorMap.getString(color);
		} catch (ColorNotFoundException e) {
			toString = "\n!!toString Councillor: color not found!!\n"+e;
		}
		return toString;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Councillor other = (Councillor) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}
	
}