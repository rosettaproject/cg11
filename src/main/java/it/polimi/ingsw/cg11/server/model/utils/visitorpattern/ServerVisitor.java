package it.polimi.ingsw.cg11.server.model.utils.visitorpattern;

import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.messages.DisconnectedPlayerMessage;
import it.polimi.ingsw.cg11.messages.ErrorMessage;
import it.polimi.ingsw.cg11.messages.SerializableAcceptMarketOffer;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithKing;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.messages.SerializableChangePermitCards;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraBonus;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermit;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillor;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.messages.SerializableEngageAssistant;
import it.polimi.ingsw.cg11.messages.SerializableFinish;
import it.polimi.ingsw.cg11.messages.SerializableOfferAssistants;
import it.polimi.ingsw.cg11.messages.SerializableOfferPermitCard;
import it.polimi.ingsw.cg11.messages.SerializableOfferPoliticCard;
import it.polimi.ingsw.cg11.messages.SerializableOneMoreMainAction;
import it.polimi.ingsw.cg11.messages.SerializableTakePermitCard;

/**
 * This interface is used to define a visitor that can visit messages that the
 * client receives
 * 
 * @author paolsanfilippo
 *
 */
public interface ServerVisitor extends Visitor {
	/**
	 * This method can visit a {@link SerializableChatMessage}
	 * 
	 * @param chatMessage
	 */
	public void visit(SerializableChatMessage chatMessage);

	/**
	 * This method can visit a {@link ErrorMessage}
	 * 
	 * @param errorMessage
	 */
	public void visit(ErrorMessage errorMessage);

	/**
	 * This method can visit a {@link SerializableElectCouncillor}
	 * 
	 * @param visitable
	 */
	public void visit(SerializableElectCouncillor visitable);

	/**
	 * This method can visit a {@link SerializableAcceptMarketOffer}
	 * 
	 * @param visitable
	 */
	public void visit(SerializableAcceptMarketOffer visitable);

	/**
	 * This method can visit a {@link SerializableBuildEmporiumWithKing}
	 * 
	 * @param visitable
	 */
	public void visit(SerializableBuildEmporiumWithKing visitable);

	/**
	 * This method can visit a {@link SerializableBuildEmporiumWithPermitCard}
	 * 
	 * @param visitable
	 */
	public void visit(SerializableBuildEmporiumWithPermitCard visitable);
	/**
	 * This method can visit a {@link SerializableChangePermitCards}
	 * @param visitable
	 */
	public void visit(SerializableChangePermitCards visitable);
	/**
	 * This method can visit a {@link SerializableChooseExtraBonus}
	 * @param visitable
	 */
	public void visit(SerializableChooseExtraBonus visitable);
	/**
	 * This method can visit a {@link SerializableChooseExtraPermit}
	 * @param visitable
	 */
	public void visit(SerializableChooseExtraPermit visitable);
	/**
	 * This method can visit a {@link SerializableChooseExtraPermitBonus}
	 * @param visitable
	 */
	public void visit(SerializableChooseExtraPermitBonus visitable);
	/**
	 * This method can visit a {@link SerializableElectCouncillorWithOneAssistant}
	 * @param visitable
	 */
	public void visit(SerializableElectCouncillorWithOneAssistant visitable);
	/**
	 * This method can visit a {@link SerializableFinish}
	 * @param visitable
	 */
	public void visit(SerializableFinish visitable);
	/**
	 * This method can visit a {@link SerializableOfferAssistants}
	 * @param visitable
	 */
	public void visit(SerializableOfferAssistants visitable);
	/**
	 * This method can visit a {@link SerializableOfferPermitCard}
	 * @param visitable
	 */
	public void visit(SerializableOfferPermitCard visitable);
	/**
	 * This method can visit a {@link SerializableOfferPoliticCard}
	 * @param visitable
	 */
	public void visit(SerializableOfferPoliticCard visitable);
	/**
	 * This method can visit a {@link SerializableOneMoreMainAction}
	 * @param visitable
	 */
	public void visit(SerializableOneMoreMainAction visitable);
	/**
	 * This method can visit a {@link SerializableTakePermitCard}
	 * @param visitable
	 */
	public void visit(SerializableTakePermitCard visitable);
	/**
	 * This method can visit a {@link SerializableEngageAssistant}
	 * @param serializableEngageAssistant
	 */
	public void visit(SerializableEngageAssistant serializableEngageAssistant);
	/**
	 * This method can visit a {@link DisconnectedPlayerMessage}
	 * @param disconnectedPlayer
	 */
	public void visit(DisconnectedPlayerMessage disconnectedPlayer);

}
