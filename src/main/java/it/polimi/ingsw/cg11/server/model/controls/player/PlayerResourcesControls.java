/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.controls.player;

import java.util.List;

import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughAssistantsException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughMoneyException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoPoliticCardOwnedException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Controls if player's resources are enough to perform an action
 * 
 * @author Federico
 *
 */
public class PlayerResourcesControls {

	/**
	 * Controls that the player has enough money, if he/she hasn't it throws a {@link NoEnoughMoneyException}
	 * 
	 * @param player
	 * @param neededAmount : amount of money needed to perform the action
	 * @throws NoEnoughMoneyException
	 */
	public void enoughMoneyControl(PlayerState player, int neededAmount) throws NoEnoughMoneyException {
		int amountPlayerMoney;
		amountPlayerMoney = player.getPlayerData().getMoney().getAmount();
		if (amountPlayerMoney < neededAmount)
			throw new NoEnoughMoneyException();
	}

	/**
	 * Controls that the player has enough assistants, if he/she hasn't it throws a {@link NoEnoughAssistantsException}
	 * 
	 * @param player
	 * @param neededAmount
	 * @throws NoEnoughAssistantsException
	 */
	public void enoughAssistantControl(PlayerState player, int neededAmount) throws NoEnoughAssistantsException {
		int playerAssistantsAmount;
		int offeredAssistants = player.getPlayerData().getOfferedResources().getOfferedAssistants();
		
		playerAssistantsAmount = player.getPlayerData().getAssistants().getAmount();
		if (playerAssistantsAmount < neededAmount + offeredAssistants)
			throw new NoEnoughAssistantsException();
	}

	/**
	 * Controls that player has politic cards. If he/she doesn't it throws a {@link NoPoliticCardOwnedException}.
	 * @param player 
	 * @param politicCards : list of politic cards that we want to control 
	 * @throws NoPoliticCardOwnedException
	 */
	public void ownsPoliticCardsControl(PlayerState player, List<PoliticCard> politicCards)
			throws NoPoliticCardOwnedException {
		
		List<PoliticCard> playerPoliticCard = player.getPlayerData().getPoliticsHand().getCards();

		if (!playerPoliticCard.containsAll(politicCards))
			throw new NoPoliticCardOwnedException();
	}

}
