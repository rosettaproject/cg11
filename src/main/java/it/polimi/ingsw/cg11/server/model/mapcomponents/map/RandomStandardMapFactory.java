package it.polimi.ingsw.cg11.server.model.mapcomponents.map;

import java.util.List;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PermitsDeckFactory;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.cards.RandomPoliticDeckFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchEmporiumException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.CityFactory;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.EmporiumPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.player.resources.StandardPlayerDataFactory;

/**
 * Random factory of the standard map
 * @author Federico
 *
 */
public class RandomStandardMapFactory extends StandardMapFactory {

	/**
	 * Assigns a {@link PoliticCard} {@link Deck} to the map
	 * @param standardMap
	 * @param mapConfig : JSONObject configuration of the map
	 * @throws ColorNotFoundException
	 */
	@Override
	public void assignPoliticsCardDeck(GameMap standardMap, JSONObject mapConfig)
			throws ColorNotFoundException {

		RandomPoliticDeckFactory politicsDeckFactory = new RandomPoliticDeckFactory();
		Deck<PoliticCard> politicCardsDeck;

		politicCardsDeck = politicsDeckFactory.createDeck(mapConfig.getJSONArray("PoliticsDeck"));
		standardMap.setPoliticsDeck(politicCardsDeck);
	}
	
	/**
	 * 
	 * @param mapConfig
	 * @param standardMap
	 * @param standardPlayerDataFactory
	 * @throws NoSuchPlayerException
	 * @throws NoSuchEmporiumException
	 * @throws ColorNotFoundException
	 */
	@Override
	public void assignPoolManagers(JSONObject mapConfig, GameMap standardMap,
			StandardPlayerDataFactory standardPlayerDataFactory)
			throws NoSuchPlayerException, NoSuchEmporiumException, ColorNotFoundException {

		JSONArray shuffledCouncillorPool = shuffleJsonArray(mapConfig.getJSONArray("CouncillorPool"));
		setEmporiumPoolManager(new EmporiumPoolManager(standardMap, getEmporiumsPerPlayer()));

		CouncillorPoolManager councillorPoolManager = new CouncillorPoolManager(shuffledCouncillorPool);

		standardMap.setCouncillorsPool(councillorPoolManager);
		standardMap.setEmporiumPool(getEmporiumPoolManager());
		
		//adds the shuffled councillor pool to the map startup config
		super.getMapStartupConfig().put("councillorPool", shuffledCouncillorPool);

		ListIterator<PlayerState> playerIterator = standardMap.getPlayers().listIterator();

		while (playerIterator.hasNext()) {
			standardPlayerDataFactory.acquireEmporiums(playerIterator.next().getPlayerData(), getEmporiumPoolManager());
		}
	}
	
	
	/**
	 * 
	 * @param mapConfig
	 * @param standardMap
	 */
	@Override
	public void assignPermitsDeckToRegion(JSONObject mapConfig, GameMap standardMap) {

		ListIterator<Region> regionIterator;

		PermitsDeckFactory permitsDeckFactory = new PermitsDeckFactory();
		JSONArray shuffledDeck;
		JSONArray startupDecks = new JSONArray();
		Deck<PermitCard> permitCardsDeck;
		Region region;

		int iteratorIndex;
		regionIterator = standardMap.getRegions().listIterator();

		while (regionIterator.hasNext()) {
			iteratorIndex = regionIterator.nextIndex();
			region = regionIterator.next();
			shuffledDeck = shuffleJsonArrayOfArrays((JSONArray)mapConfig.getJSONArray("PermitDecks").get(iteratorIndex));
			startupDecks.put(shuffledDeck);
			permitCardsDeck = permitsDeckFactory.createDeck(shuffledDeck, region, standardMap);
			region.setPermitCardsDeck(permitCardsDeck);
		}
		super.getMapStartupConfig().put("regionDecks", startupDecks);
	}
	
	@Override 
	public void assignCities(JSONObject mapConfig, GameMap standardMap, CityFactory cityFactory, List<City> cities)
			throws NoSuchRegionException, ColorNotFoundException {

		int cityIndex;
		int bonusIndex;
		JSONArray shuffledBonuses;

		shuffledBonuses = shuffleJsonArray(mapConfig.getJSONArray("CityBonuses"));

		for (cityIndex = 0, bonusIndex = cityIndex; cityIndex < StandardMapFactory.getNumberOfCities(); cityIndex++, bonusIndex++) {
			cities.add(cityFactory.createCity(mapConfig.getJSONArray("Cities").getJSONObject(cityIndex),
					shuffledBonuses, getKingCityName(), bonusIndex, standardMap));
			// If is the king's city, the bonus at the bonusIndex position needs
			// to be re used
			if (cities.get(cityIndex).getName().equals(getKingCityName()))
				bonusIndex--;
		}

		standardMap.setCities(cities);
		super.getMapStartupConfig().put("CityBonuses",shuffledBonuses);
		
		ListIterator<Region> regionIterator = standardMap.getRegions().listIterator();
		ListIterator<City> cityIterator = cities.listIterator();
		JSONObject adjacentCities =  mapConfig.getJSONObject("Adjacents");
		JSONArray citiesJsonArray = mapConfig.getJSONArray("Cities");
		City city;

		while (cityIterator.hasNext()) {
			city = cityIterator.next();
			city.initializeAdjacentCities(
					cityFactory.getAdjacentCities(city.getName(), adjacentCities), standardMap);
		}
		super.getMapStartupConfig().put("Cities",citiesJsonArray);
		super.getMapStartupConfig().put("Adjacents", adjacentCities);

		while (regionIterator.hasNext())
			regionIterator.next().setCities(standardMap);
	}
	
	
}
