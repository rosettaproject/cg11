package it.polimi.ingsw.cg11.server.model.utils;

import java.util.*;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.transactions.NobilityTransaction;
import it.polimi.ingsw.cg11.server.model.utils.transactions.Transaction;

/**
 * Bonus is an object which stores a series of transaction that can be performed
 * by calling the obtain method
 * 
 * @author francesco
 *
 */
public class Bonus {

	private static final String NOBILITY = "Nobility";
	private static final String POLITIC_CARD = "PoliticCards";
	private Map<String, Transaction> chainOfCommands;

	/**
	 * @param chainOfCommands
	 */
	protected Bonus(Map<String, Transaction> chainOfCommands) {
		this.chainOfCommands = chainOfCommands;

	}

	/**
	 * Calling this method makes a player obtain all the benefits of the bonus,
	 * specified in the chainOfCommands
	 * 
	 * @param playerStateWrapper
	 * @return ModelChanges
	 * @throws TransactionException
	 */
	public ModelChanges obtain(PlayerState playerStateWrapper) throws TransactionException {

		Iterator<String> transactionIterator = getChainOfCommands().keySet().iterator();

		while (transactionIterator.hasNext()) {
			getChainOfCommands().get(transactionIterator.next()).execute(playerStateWrapper);
		}
		return addObtainedResources(playerStateWrapper);
	}
	/**
	 * Adds to changes the resources obtained from the bonus
	 * @param playerStateWrapper
	 * @return model changes
	 * @throws TransactionException
	 */
	private ModelChanges addObtainedResources(PlayerState playerStateWrapper) throws TransactionException {

		Iterator<String> transactionIterator = getChainOfCommands().keySet().iterator();
		ModelChanges modelChanges = new ModelChanges();

		while (transactionIterator.hasNext()) {
			String key = transactionIterator.next();

			if (getChainOfCommands().containsKey(key)) {
				if (key.equalsIgnoreCase(NOBILITY)) {
					NobilityTransaction transaction = (NobilityTransaction) getChainOfCommands().get(key);

					Bonus nobilityPathBonus = transaction
							.getNobilityBonus(playerStateWrapper.getPlayerData().getNobilityPoints().getAmount());
					modelChanges.addToChanges(nobilityPathBonus.obtain(playerStateWrapper));
				}
				if (key.equalsIgnoreCase(POLITIC_CARD)) {
					addPoliticCardsToChanges(modelChanges,playerStateWrapper,key);
				}

				modelChanges.addAsResource(key, getChainOfCommands().get(key).getAmount());
			}
		}
		return modelChanges;
	}
	/**
	 * Adds to changes the politiccards
	 * @param modelChanges
	 * @param playerState
	 * @param key "politiccards"
	 * @throws TransactionException
	 */
	private void addPoliticCardsToChanges(ModelChanges modelChanges,PlayerState playerState,String key) throws TransactionException{
		
		int numberOfCards;
		List<PoliticCard> obtainedCards = new ArrayList<>();
		int handSize = playerState.getPlayerData().getPoliticsHand().getNumberOfCards();
		
		for (numberOfCards = getChainOfCommands().get(key)
				.getAmount(); numberOfCards > 0; numberOfCards--) {
			PoliticCard card = playerState.getPlayerData().getPoliticsHand().getCard(handSize - numberOfCards);
			obtainedCards.add(card);
		}
		
		try {
			modelChanges.addToChanges("ObtainedCards", obtainedCards);
		} catch (ColorNotFoundException e) {
			throw new TransactionException(e);
		}
	}

	/**
	 * Tells if a chain of commands is equal to the bonus.
	 * 
	 * @param chainOfCommands
	 * @return <tt>true</tt> if it's equal, <tt>false</tt> if is not
	 */
	public boolean isEqual(Map<String, Transaction> chainOfCommands) {

		Iterator<String> transactionIterator = chainOfCommands.keySet().iterator();
		String currentKey;

		while (transactionIterator.hasNext()) {
			currentKey = transactionIterator.next();

			if ((this.getChainOfCommands().get(currentKey) == null && chainOfCommands.get(currentKey) != null)
					|| (this.getChainOfCommands().get(currentKey) != null && chainOfCommands.get(currentKey) == null))
				return false;

			else if (this.getChainOfCommands().get(currentKey).getAmount() != chainOfCommands.get(currentKey)
					.getAmount())
				return false;
		}
		return true;

	}
	/**
	 * Sets the chain of commands of the bonus
	 * @param chainOfCommands
	 */
	protected void setChainOfCommands(Map<String, Transaction> chainOfCommands) {
		this.chainOfCommands = chainOfCommands;
	}
	/**
	 * Returns the amount of assistants
	 * @return amount of assistants
	 */
	public int getAssistantsAmount() {
		if (getChainOfCommands().containsKey("Assistants"))
			return getChainOfCommands().get("Assistants").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of money
	 * @return amount of money
	 */
	public int getMoneyAmount() {
		if (getChainOfCommands().containsKey("Money"))
			return getChainOfCommands().get("Money").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of nobility points
	 * @return amount of nobility points
	 */
	public int getNobilityPointsAmount() {
		if (getChainOfCommands().containsKey(NOBILITY))
			return getChainOfCommands().get(NOBILITY).getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of victory points
	 * @return amount of victory points
	 */
	public int getVictoryPointsAmount() {
		if (getChainOfCommands().containsKey("VictoryPoints"))
			return getChainOfCommands().get("VictoryPoints").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of politic cards
	 * @return amount of politic cards
	 */
	public int getPoliticCardsAmount() {
		if (getChainOfCommands().containsKey(POLITIC_CARD))
			return getChainOfCommands().get(POLITIC_CARD).getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of extra bonus
	 * @return amount of extra bonus
	 */
	public int getExtraBonusAmount() {
		if (getChainOfCommands().containsKey("ExtraBonus"))
			return getChainOfCommands().get("ExtraBonus").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of main actions
	 * @return amount of main actions
	 */
	public int getMainActionsAmount() {
		if (getChainOfCommands().containsKey("MainActions"))
			return getChainOfCommands().get("MainActions").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of extra permit bonus
	 * @return amount of extra permit bonus
	 */
	public int getExtraPermitBonusAmount() {
		if (getChainOfCommands().containsKey("ExtraPermitBonus"))
			return getChainOfCommands().get("ExtraPermitBonus").getAmount();
		else
			return 0;
	}
	/**
	 * Returns the amount of extra permit 
	 * @return amount of extra permit
	 */
	public int getExtraPermitAmount() {
		if (getChainOfCommands().containsKey("ExtraPermit"))
			return getChainOfCommands().get("ExtraPermit").getAmount();
		else
			return 0;
	}

	@Override
	public String toString() {
		String toString = "";
		if (getAssistantsAmount() > 0)
			toString += "\nAssistantsAmount = " + getAssistantsAmount();
		if (getMoneyAmount() > 0)
			toString += "\nMoneyAmount = " + getMoneyAmount();
		if (getNobilityPointsAmount() > 0)
			toString += "\nNobilityPointsAmount = " + getNobilityPointsAmount();
		if (getVictoryPointsAmount() > 0)
			toString += "\nVictoryPointsAmount = " + getVictoryPointsAmount();
		if (getPoliticCardsAmount() > 0)
			toString += "\nPoliticCardsAmount = " + getPoliticCardsAmount();
		if (getExtraBonusAmount() > 0)
			toString += "\nExtraBonusAmount = " + getExtraBonusAmount();
		if (getExtraPermitBonusAmount() > 0)
			toString += "\ngetExtraPermitBonusAmount = " + getExtraPermitBonusAmount();
		if (getExtraPermitAmount() > 0)
			toString += "\ngetExtraPermitAmount = " + getExtraPermitAmount();
		if ("".equals(toString))
			toString = "\nempty";
		return toString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getChainOfCommands() == null) ? 0 : getChainOfCommands().hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bonus other = (Bonus) obj;
		if (getChainOfCommands() == null) {
			if (other.getChainOfCommands() != null)
				return false;
		} else if (!this.isEqual(other.getChainOfCommands())) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the chain of commands
	 * @return the chainOfCommands
	 */
	public Map<String, Transaction> getChainOfCommands() {
		return chainOfCommands;
	}

}