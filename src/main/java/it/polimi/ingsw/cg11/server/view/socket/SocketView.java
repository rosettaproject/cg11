/**
 * 
 */
package it.polimi.ingsw.cg11.server.view.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.DisconnectedPlayerMessage;
import it.polimi.ingsw.cg11.server.gameserver.GameFactory;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.server.view.View;

/**
 * This class represents the server side view with which the client can
 * communicates with server.
 * 
 * @author Federico
 *
 */
public class SocketView implements View, Runnable {

	private static final Logger LOG = Logger.getLogger(SocketView.class.getName());

	private static final int MAX_NUMBER_OF_ERROR = 3;

	private GameFactory factoryGame;
	private Token token;
	private Socket playerSocket;
	private Visitable clientMessage;
	private ObjectInputStream inputStream;
	private ObjectOutputStream outputStream;
	private String playerName;

	/**
	 * Public constructor that instantiates a new SocketView
	 * 
	 * @param clientSocket
	 *            represent the endpoint of the client
	 * @throws IOException
	 */
	public SocketView(Socket clientSocket) throws IOException {
		playerSocket = clientSocket;
		inputStream = new ObjectInputStream(playerSocket.getInputStream());
		outputStream = new ObjectOutputStream(playerSocket.getOutputStream());
		factoryGame = GameFactory.getGameFactory();
	}

	@Override
	public void dispatchMessage(Visitable message) {
		try {
			outputStream.writeObject(message);
			outputStream.flush();
			outputStream.reset();
			LOG.log(Level.INFO, "Sent message from server to " + playerName + "\n" + message.toString() + "\n");

		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Can not send Visitable message from server to client socket\n", e);
		}
	}

	@Override
	public void run() {
		String startingMessage = "ready";

		try {
			outputStream.writeObject(startingMessage);
			outputStream.flush();
			outputStream.reset();

			token = (Token) inputStream.readObject();
			playerName = token.getClientUsername();

			LOG.log(Level.INFO, "Token of " + playerName + " received from server\n");

			factoryGame.addPlayerView(this);

			listenClient();

		} catch (ClassNotFoundException | IOException e) {
			LOG.log(Level.SEVERE, "Can not send startingMessage from server to client socket", e);
		}
	}

	/**
	 * When this method is called, this {@link SocketView} will listen for
	 * incoming messages from client
	 */
	private void listenClient() {
		Object obj;
		int numberOfError = 0;
		String message;
		boolean playerIsConnected = true;

		while (numberOfError < MAX_NUMBER_OF_ERROR && playerIsConnected) {

			try {
				obj = inputStream.readObject();
			} catch (IOException | ClassNotFoundException e) {
				LOG.log(Level.SEVERE, "Error during receiving from socket view", e);
				numberOfError += 1;
				obj = null;
			}

			if (obj == null) {
				LOG.log(Level.WARNING, "SocketView: null object received from " + playerName);
			} else if (obj instanceof Visitable) {
				LOG.log(Level.INFO, "Message received from " + playerName + "\n");

				clientMessage = (Visitable) obj;

				LOG.log(Level.INFO, "Visitable message received from " + playerName + "\n");
				LOG.log(Level.INFO, "Topic: " + clientMessage.getTopic() + "\n");
				LOG.log(Level.INFO, clientMessage.toString() + "\n");

				clientMessage.setPlayerToken(token);
				publish(clientMessage, clientMessage.getTopic());
			} else {
				try {
					message = (String) obj;
					if ("disconect_player".equalsIgnoreCase(message)) { // NOSONAR
						playerIsConnected = false;
					}
				} catch (Exception e) {
					LOG.log(Level.SEVERE, "Error during receiving from socket view", e);
					numberOfError += 1;
				}
			}
		}
		disconnectPlayer();
		Broker.getBroker().unsubscibeAll(this);
		sendCloseConnectionMessage();
		LOG.log(Level.INFO, "Player " + playerName + " has disconnected \n");
	}

	/**
	 * This method is used when it is going to interrupt the communication with
	 * the server
	 */
	private void disconnectPlayer() {
		DisconnectedPlayerMessage disconnectedPlayerMessage;
		String topic;

		topic = token.getCurrentGame();

		disconnectedPlayerMessage = new DisconnectedPlayerMessage();
		disconnectedPlayerMessage.setPlayerToken(token);
		disconnectedPlayerMessage.setTopic(topic);

		publish(disconnectedPlayerMessage, topic);

	}

	@Override
	public Token getClientIdentifier() {
		return token;
	}

	@Override
	public void publish(Visitable message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

	@Override
	public String getSubscriberIdentifier() {
		return token.getClientUsername();
	}

	@Override
	public void sendIDToPlayer(int playerID) {
		try {
			outputStream.writeObject(playerID);
			outputStream.flush();
			outputStream.reset();
			LOG.log(Level.INFO, "Sent player ID to socket client view\n");
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Can not send playerID from server to client socket view\n", e);
		}
	}

	/**
	 * Sends a service message to the client warning him that he is not playing
	 * anymore
	 */
	public void sendCloseConnectionMessage() {
		String closeConnectionMessage = "player_disconnected";
		try {
			outputStream.writeObject(closeConnectionMessage);
			outputStream.flush();
			outputStream.reset();
			LOG.log(Level.INFO, "Sent disconnection message to socket client view\n");
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Can not send disconnection message from server to client socket view\n", e);
		}
	}

	@Override
	public Token getToken() {
		return token;
	}

	@Override
	public void setUpdatedUsername(String username) {
		try {
			outputStream.writeObject(username);
			outputStream.flush();
			outputStream.reset();
			LOG.log(Level.INFO, "Sent update username to socket client view\n");
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Can not send updated username from server to client socket view\n", e);
		}
	}
}
