package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferPermitCard;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link OfferPermitCard}
 * @author paolasanfilippo
 *
 */
public class OfferPermitCardFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		
		PlayerState player;
		TurnManager turnManager;
		MarketOffers marketOffers;
		PermitCard permitCard;
		int permitCardIndex;
		int offerCost;

		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData, gameMap, "PlayerID");
			offerCost = actionData.getInt("cost");
			turnManager = gameMap.getTurnManager();
			marketOffers = gameMap.getMarketModel();
			permitCard = createPermitCardFromActionData(actionData,gameMap,"region","permitcard");
			
			permitCardIndex = player.getPlayerData().getPermitsHand().indexOf(permitCard);
			if(permitCardIndex >= 0){
				permitCard = player.getPlayerData().getPermitsHand().getCard(permitCardIndex);
				return new OfferPermitCard(player, turnManager,marketOffers,permitCard,offerCost, gameMap.getGameTopic());
			} 
			
			permitCardIndex = player.getPlayerData().getUsedPermits().indexOf(permitCard);
			if(permitCardIndex >= 0){
				permitCard = player.getPlayerData().getUsedPermits().getCard(permitCardIndex);
				return new OfferPermitCard(player, turnManager,marketOffers,permitCard,offerCost, gameMap.getGameTopic());
			} 			
			throw new NoSuchPermitCardException();
			
		} catch (NoSuchPlayerException | NoSuchPermitCardException | NoSuchRegionException e) {
			throw new IllegalCreationCommandException(e);
		}
	
	
	}

}
