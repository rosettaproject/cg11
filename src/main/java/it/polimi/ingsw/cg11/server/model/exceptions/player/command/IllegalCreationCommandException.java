package it.polimi.ingsw.cg11.server.model.exceptions.player.command;

/**
 * Exception thrown when the command created is not acceptable.
 * @author paolasanfilippo
 *
 */
public class IllegalCreationCommandException extends Exception {

	private static final long serialVersionUID = -4272672627489691362L;
	/**
	 * Exception thrown when the command created is not acceptable.
	 */
	public IllegalCreationCommandException() {
		super();
	}
	/**
	 * Exception thrown when the command created is not acceptable.
	 * @param cause
	 */
	public IllegalCreationCommandException(Throwable cause) {
		super(cause);
	}
}
