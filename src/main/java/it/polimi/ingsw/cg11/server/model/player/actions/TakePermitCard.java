package it.polimi.ingsw.cg11.server.model.player.actions;

import java.util.List;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.mapcomponents.RegionControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.IllegalPoliticCardsException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughMoneyException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.FaceUpPermits;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the TakePermitCard action and tries to execute it.
 * TakePermitCard is a main action that permits to take a {@link FaceUpPermits}
 * from a {@link Region}
 * 
 * @author Federico
 *
 */
public class TakePermitCard extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);
	private GameMap gameMap;
	private int neededMoney;
	private PermitCard permitCardToTake;
	private PlayerState player;
	private List<PoliticCard> playerPoliticCards;
	private TurnManager turnManager;
	private Region region;
	private String gameTopic;
	private ActionServiceMethods actionServiceMethods;

	/**
	 * 
	 * @param gameMap
	 * @param player
	 * @param turnManager
	 * @param region
	 * @param permitCard
	 * @param playerPoliticCards
	 * @param gameTopic
	 * @throws IllegalCreationCommandException
	 */
	public TakePermitCard(GameMap gameMap, PlayerState player, TurnManager turnManager, Region region,
			PermitCard permitCard, List<PoliticCard> playerPoliticCards, String gameTopic)
			throws IllegalCreationCommandException {

		actionServiceMethods = new ActionServiceMethods();
		CardsControls cardsControls = new CardsControls();

		this.gameMap = gameMap;
		this.player = player;
		this.permitCardToTake = permitCard;
		this.playerPoliticCards = playerPoliticCards;
		this.turnManager = turnManager;
		this.region = region;
		this.gameTopic = gameTopic;

		try {
			neededMoney = actionServiceMethods.neededMoneyToCorruptCouncil(playerPoliticCards);
			cardsControls.councilAndCardsMatchingControl(region.getBalcony(), playerPoliticCards);
		} catch (IllegalPoliticCardsException | NoSuchPoliticCardException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

	@Override
	public void execute() throws NotAllowedActionException {
		actionServiceMethods = new ActionServiceMethods();
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getMainActionsHandler().takePermitCard(permitCardToTake,
					region, playerPoliticCards, neededMoney);

			actionServiceMethods.canChooseExtraBonusControl(player, gameMap);
			actionServiceMethods.canChoosePermitCardBonusControl(player);

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerControls playerControls = new PlayerControls();
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		CardsControls cardsControls = new CardsControls();
		RegionControls regionControls = new RegionControls();

		try {
			playerControls.playerTurnControl(player);
			playerResourcesControls.enoughMoneyControl(player, neededMoney);
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			cardsControls.playerHasPoliticCards(player, playerPoliticCards);
			regionControls.hasPermitCardFaceUp(region, permitCardToTake);

		} catch (NoPlayerTurnException | NoEnoughMoneyException | NotCorrectGamePhaseException
				| NoSuchPoliticCardException | NoSuchPermitCardException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}