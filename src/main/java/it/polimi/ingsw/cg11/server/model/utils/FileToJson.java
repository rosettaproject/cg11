package it.polimi.ingsw.cg11.server.model.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import org.json.*;

/**
 * Contains methods to get a JSONArray or JSONObject from a text file
 * 
 * @author francesco
 *
 */
public class FileToJson {
	/**
	 * Transforms a file into a jsonarray
	 * @param filePath
	 * @return jsonarray
	 * @throws FileNotFoundException
	 */
	public JSONArray jsonArrayFromFile(String filePath) throws FileNotFoundException {

		FileReader file = new FileReader(filePath);

		JSONTokener tokener = new JSONTokener(file);

		return new JSONArray(tokener);

	}
	/**
	 * Transforms a file into a jsonobject
	 * @param filePath
	 * @return jsonobject
	 * @throws FileNotFoundException
	 */
	public JSONObject jsonObjectFromFile(String filePath) throws FileNotFoundException {

		FileReader file;
		
		file = new FileReader(filePath);

		JSONTokener tokener = new JSONTokener(file);

		return new JSONObject(tokener);
	}
}
