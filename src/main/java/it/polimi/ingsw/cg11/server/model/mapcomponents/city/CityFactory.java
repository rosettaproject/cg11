package it.polimi.ingsw.cg11.server.model.mapcomponents.city;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.json.*;

import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.utils.Bonus;
import it.polimi.ingsw.cg11.server.model.utils.BonusFactory;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
/**
 * Factory method for the {@link City}
 * @author paolasanfilippo
 *
 */
public class CityFactory {

	private static final String COLOR = "color";
	private static final String CITY = "City";
	private static final String REGION = "Region";

	
	/**
	 * Factory method for {@link City}. It throws {@link ColorNotFoundException} and {@link NoSuchRegionException}
	 * @param cityConfig : city's configuration
	 * @param cityBonuses :city's bonuses
	 * @param kingCityName : name of the king's city
	 * @param bonusIndex : index for the bonus
	 * @param currentMap : map
	 * @return {@link City} created
	 * @throws NoSuchRegionException
	 * @throws ColorNotFoundException
	 */
	public City createCity(JSONObject cityConfig,JSONArray cityBonuses,
			String kingCityName,int bonusIndex,GameMap currentMap) throws NoSuchRegionException, ColorNotFoundException{

		String cityName;
		String colorString;
		Color cityColor;
		Region cityRegion;
		Bonus cityBonus;
		JSONObject bonusConfig;
		
		BonusFactory bonusFactory;
		
		cityName = cityConfig.getString(CITY);
		colorString = cityConfig.getString(COLOR);
		cityColor = ColorMap.getColor(colorString);
		cityRegion = getRegionFromString(cityConfig.getString(REGION),currentMap);
		
		bonusFactory = new BonusFactory();
		

		bonusConfig = cityBonuses.getJSONObject(bonusIndex);
		
		if(kingCityName.equals(cityName)){
			bonusConfig = new JSONObject();
			
		}
			
		cityBonus = bonusFactory.createBonus(bonusConfig, currentMap);
		
		return new City(cityName,cityColor,cityRegion,cityBonus);
	}
	/**
	 * Returns the {@link Region} whose name was given as a string parameter. If there isn't a region with that name, 
	 * this method throws a {@link NoSuchRegionException}.
	 * @param regionName : name of the region 
	 * @param currentMap : map
	 * @return region get from string
	 * @throws NoSuchRegionException
	 */
	private Region getRegionFromString(String regionName,GameMap currentMap)throws NoSuchRegionException{
		
		ListIterator<Region> regionIterator = currentMap.getRegions().listIterator();
		Region region;
		
			while(regionIterator.hasNext()){
				region = regionIterator.next();
				if(region.getName().equals(regionName))
					return region;
			}
		throw new NoSuchRegionException();	
	}
	
	/**
	 * Used to separate the parsing method from the creation of connections between cities
	 * @param cityName : city's name
	 * @param adjacentsConfig :configuration of the adjacent cities
	 * @return cityNames as a list of strings
	 */
	
	public List<String> getAdjacentCities(String cityName,JSONObject adjacentsConfig){
		
		ArrayList<String> cityNamesToString;
		JSONArray adjacentCities;
		
		cityNamesToString = new ArrayList<>();
		adjacentCities = adjacentsConfig.getJSONArray(cityName);
		
		int cityIndex;
		
		for(cityIndex = 0; cityIndex < adjacentCities.length(); cityIndex++ ){
			cityNamesToString.add(adjacentCities.getString(cityIndex));
		}
		
		return cityNamesToString;
	}
}

