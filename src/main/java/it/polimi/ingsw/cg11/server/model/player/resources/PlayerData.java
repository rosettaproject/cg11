package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Emporium;

/**
 * In this class all the player-related resources are stored contains a
 * {@link PoliticsHand}, {@link PermitsHand}, {@link Token},{@link Money},
 * {@link Assistants}, {@link VictoryPoints}
 * 
 * @author francesco
 * 
 */
public class PlayerData {

	private String username;
	private String currentGame;

	private final OfferedResources offeredResources;
	private final int playerId;
	private final List<Emporium> emporiums;
	private final PoliticsHand politicsHand;
	private final PermitsHand permitsHand;
	private final PermitsHand usedPermits;
	private final Money money;
	private final Assistants assistants;
	private final NobilityPoints nobilityPoints;
	private final VictoryPoints victoryPoints;

	
	/**
	 * 
	 * @param playerId : player unique identifier
	 * @param politicsDeck : the deck from which politic cards are drawn
	 * @param secondDeck : discarded politic cards
	 * @param money
	 * @param assistants
	 */
	protected PlayerData(int playerId, Deck<PoliticCard> politicsDeck, Deck<PoliticCard> secondDeck, Money money,
			Assistants assistants) {

		this.playerId = playerId;
		this.politicsHand = new PoliticsHand(politicsDeck, secondDeck, playerId);
		this.permitsHand = new PermitsHand();
		this.usedPermits = new PermitsHand();
		this.emporiums = new ArrayList<>();
		this.money = money;
		this.assistants = assistants;
		this.nobilityPoints = new NobilityPoints();
		this.victoryPoints = new VictoryPoints();
		offeredResources = new OfferedResources();

		username = null;
		currentGame = null;
	}
	/**
	 * Returns the player id
	 * @return playerId
	 */
	public int getPlayerID() {
		return playerId;
	}
	/**
	 * Returns {@link NobilityPoints}
	 * @return nobility points
	 */
	public NobilityPoints getNobilityPoints() {
		return nobilityPoints;
	}
	/**
	 * Returns the {@link PoliticsHand}
	 * @return politic hand
	 */
	public PoliticsHand getPoliticsHand() {
		return this.politicsHand;
	}
	/**
	 * Returns the {@link PermitsHand} of unused permits
	 * @return unused permit in hand
	 */
	public PermitsHand getPermitsHand() {
		return this.permitsHand;
	}
	/**
	 * Returns the {@link PermitsHand} of used permits
	 * @return used permit in hand
	 */
	public PermitsHand getUsedPermits() {
		return this.usedPermits;
	}
	/**
	 * Returns the amount of emporium that haven't been used yet
	 * @return amoun of unused emporiums
	 */
	public int getRemainingEmporiums() {
		return this.emporiums.size();
	}
	/**
	 * Returns one emporium, removing it from the emporiums
	 * @return emporium
	 */
	public Emporium getOneEmporium() {
		int lastEmporium = emporiums.size() - 1;
		if (lastEmporium >= 0)
			return emporiums.remove(lastEmporium);
		return null;

	}
	/**
	 * Returns {@link Money}
	 * @return money
	 */
	public Money getMoney() {
		return this.money;
	}
	/**
	 * Returns {@link VictoryPoints}
	 * @return victory points
	 */
	public VictoryPoints getVictoryPoints() {
		return this.victoryPoints;
	}
	/**
	 * Returns {@link Assistants}
	 * @return assistants
	 */
	public Assistants getAssistants() {
		return this.assistants;
	}
	/**
	 * Returns {@link OfferedResources}
	 * @return player's offered resources
	 */
	public OfferedResources getOfferedResources() {
		return offeredResources;
	}

	/**
	 * Adds a permit card to the player's used permits
	 * 
	 * @param permitCard
	 * 
	 */
	public void addToUsedPermits(PermitCard permitCard) {
		this.usedPermits.addToHand(permitCard);
	}

	/**
	 * Adds the card, passed as argument, to the player's {@link PoliticsHand}
	 * 
	 * @param politicCard
	 * 
	 */
	public void addToPoliticsHand(PoliticCard politicCard) {
		this.politicsHand.addToHand(politicCard);
	}

	/**
	 * Adds the card, passed as argument, to the player's {@link PermitsHand}
	 * 
	 * @param permitCard
	 * 
	 */
	public void addToPermitsHand(PermitCard permitCard) {
		this.permitsHand.addToHand(permitCard);
	}

	/**
	 * Adds an {@link Emporium} to the player's emporium list. Should be
	 * used only when initializing the resources
	 * 
	 * @param emporiumsToAdd
	 * 
	 */
	public void addEmporiums(List<Emporium> emporiumsToAdd) {
		int index;
		int size = emporiumsToAdd.size();

		for (index = 0; index < size; index++)
			this.emporiums.add(emporiumsToAdd.remove(0));
	}

	@Override
	public String toString() {
		String toString;
		ListIterator<Emporium> iterator = emporiums.listIterator();

		toString = "\n\nPlayerId= " + playerId + "\n\n" + "Emporiums:\n";

		while (iterator.hasNext())
			toString = toString + "[" + iterator.nextIndex() + " " + iterator.next().toString() + "]\n";

		toString = toString + "\n" + politicsHand.toString() + "\n" + "HandPermitCards\n" + permitsHand.toString()
				+ "\n" + "UsedPermitCards\n" + usedPermits.toString() + "\n" + money.toString() + ",\n"
				+ assistants.toString() + "\n" + nobilityPoints.toString() + "\n" + victoryPoints.toString();
		return toString;
	}

	/**
	 * Returns the player's username
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the player's username
	 * @param username
	 *            the username to set
	 * @return <tt>true</tt> if username wasn't set yet, <tt>false</tt> otherwise
	 */
	public boolean setUsername(String username) {
		if (this.username == null) {
			this.username = username;
			return true;
		}
		return false;
	}

	/**
	 * Returns the current game
	 * @return the currentGame
	 */
	public String getCurrentGame() {
		return currentGame;
	}

	/**
	 * Sets the current game
	 * @param currentGame
	 *            the currentGame to set
	 * @return <tt>true</tt> if the currentGame wasn't set, <tt>false</tt> otherwise
	 */
	public boolean setCurrentGame(String currentGame) {
		if (this.currentGame == null) {
			this.currentGame = currentGame;
			return true;
		}
		return false;
	}

}