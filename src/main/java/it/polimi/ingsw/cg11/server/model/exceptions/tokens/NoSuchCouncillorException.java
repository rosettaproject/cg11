package it.polimi.ingsw.cg11.server.model.exceptions.tokens;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;

/**
 * Exception thrown when there isn't the {@link Councillor} requested. 
 * @author paolasanfilippo
 *
 */
public class NoSuchCouncillorException extends Exception {

	private static final long serialVersionUID = 5123815237321253898L;

	/**
	 * Exception thrown when there isn't the {@link Councillor} requested. 
	 */
	public NoSuchCouncillorException() {
		super();
	}
	/**
	 * Exception thrown when there isn't the {@link Councillor} requested. 
	 * @param message
	 */
	public NoSuchCouncillorException(String message) {
		super(message);
	}
}
