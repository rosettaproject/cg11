package it.polimi.ingsw.cg11.server.model.utils.publishsubscribe;

import java.rmi.Remote;

public interface ClientBrokerInterface<M, T> extends Remote {

	/**
	 * Adds the {@link SubscriberUserInterface} passed as a parameter to the list of subscribers to the given topic
	 * @param subscriber
	 * @param topic
	 */
	public void subscribe(SubscriberUserInterface<M> subscriber, T topic);

	/**
	 * Removes the {@link SubscriberUserInterface} passed as a parameter from the list of subscribers to the given topic
	 * @param subscriber
	 * @param topic
	 */
	public void unsubscibe(SubscriberUserInterface<M> subscriber, T topic);

	/**
	 * Publishes the message passed as a parameter so that all the subscribers to the given topic will receive it
	 * @param message
	 * @param topic
	 */
	public void publish(M message, T topic);

}
