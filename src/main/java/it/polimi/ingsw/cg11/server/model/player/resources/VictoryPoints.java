package it.polimi.ingsw.cg11.server.model.player.resources;

/**
 * This class is used to store a player's victory points. The amount is stored as
 * an integer and it can be accessed or increased, but not decreased.
 * It doesn't guarantee that the operation performed is legal
 * 
 * @author francesco
 */

public class VictoryPoints {

	private int amount;
	private static final int MAX_POINT = 99;

	/**
	 * Lets the player gain victory points
	 * @param gainedAmount
	 * @throws IllegalArgumentException
	 *             if gainedAmount is negative
	 */
	public void gain(int gainedAmount) {
		if (gainedAmount < 0) {
			throw new IllegalArgumentException("gainedAmount can't be negative");
		}
		if (amount + gainedAmount > MAX_POINT) {
			amount = MAX_POINT;
		}
		else {
			this.amount += gainedAmount;
		}
	}
	/**
	 * Returns the amount of victory points
	 * @return number of victory points
	 */
	public int getAmount() {
		return this.amount;
	}

	/**
	 * Returns the maximum number of victory points
	 * @return the maxPoint
	 */
	public static int getMaxPoint() {
		return MAX_POINT;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VictoryPoints [amount=" + amount + "]";
	}
	

}