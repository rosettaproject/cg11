package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.market.NoSuchOfferException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffer;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.AcceptMarketOffer;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for the {@link AcceptMarketOffer}
 * @author paolasanfilippo
 *
 */
public class AcceptMarketOfferFactory extends ActionFactory{

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		
		PlayerState acceptingPlayer;
		TurnManager turnManager;
		MarketOffers marketOffers;
		MarketOffer marketOffer;
		
		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			acceptingPlayer = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			turnManager = gameMap.getTurnManager();
			marketOffers = gameMap.getMarketModel();
			marketOffer = getMarketOfferFromActionData(actionData,gameMap);
			
			return new AcceptMarketOffer(gameMap,acceptingPlayer,turnManager,marketOffer,marketOffers, gameMap.getGameTopic());
		} catch (NoSuchPlayerException | NoSuchOfferException e) {
			throw new IllegalCreationCommandException(e);
		}
	}
}
