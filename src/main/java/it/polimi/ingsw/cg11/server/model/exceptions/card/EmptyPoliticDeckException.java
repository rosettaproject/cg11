package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.cards.Deck;

/**
 * Exception thrown when the Politic {@link Deck} is empty.
 * @author paolasanfilippo
 *
 */public class EmptyPoliticDeckException extends Exception {

	 private static final long serialVersionUID = -4826194489528807315L;

	/**
	 * Exception thrown when the Politic {@link Deck} is empty.
	 */
	public EmptyPoliticDeckException() {
		super();
	}

	/**
	 * Exception thrown when the Politic {@link Deck} is empty.
	 * @param cause
	 */
	public EmptyPoliticDeckException(Throwable cause) {
		super(cause);
	}
}
