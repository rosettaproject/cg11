package it.polimi.ingsw.cg11.server.model.exceptions.player.state;

import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Exception thrown when the {@link PlayerState} isn't acceptable.
 * @author paolasanfilippo
 *
 */
public class IllegalStateChangeException extends Exception {

	private static final long serialVersionUID = -4740512776703991086L;

	/**
	 * Exception thrown when the {@link PlayerState} isn't acceptable.
	 */
	public IllegalStateChangeException() {
		super();
	}
	/**
	 * Exception thrown when the {@link PlayerState} isn't acceptable.
	 * @param cause
	 */
	public IllegalStateChangeException(Throwable cause) {
		super(cause);
	}
	/**
	 * Exception thrown when the {@link PlayerState} isn't acceptable.
	 * @param message
	 */
	public IllegalStateChangeException(String message) {
		super(message);
	}
}
