package it.polimi.ingsw.cg11.server.model.market;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Implements an offer of {@link PermitCard}
 * 
 * @author paolasanfilippo
 *
 */
public class PermitCardOffer implements MarketOffer {

	public static final String OFFER_COST_KEY = "OfferCost";
	public static final String OFFERER_ID_KEY = "OffererID";
	public static final String PERMIT_CARD_KEY = "PermitCard";
	public static final String ACTOR_KEY = "ActorID";
	public static final String ACTION_KEY = "Action";
	private static final Logger LOG = Logger.getLogger(PermitCardOffer.class.getName());
	private PlayerState offeringPlayer;
	private PermitCard offeredCard;
	private int offerCost;

	private ModelChanges modelChanges;

	/**
	 * 
	 * @param offeringPlayer
	 * @param offeredCard
	 * @param offerCost
	 */
	public PermitCardOffer(PlayerState offeringPlayer, PermitCard offeredCard, int offerCost) {
		this.offerCost = offerCost;
		this.offeredCard = offeredCard;
		this.offeringPlayer = offeringPlayer;
	}

	@Override
	public ModelChanges acceptOffer(PlayerState acceptingPlayer) throws TransactionException {

		try {
			acceptingPlayer.getPlayerData().getMoney().spend(offerCost);
			offeringPlayer.getPlayerData().getMoney().gain(offerCost);
			modelChanges = offeredCard.getPermitBonus().obtain(acceptingPlayer);
			offeringPlayer.getPlayerData().getOfferedResources().removePermitCard(offeredCard);
			removeOfferedPermit(offeringPlayer,acceptingPlayer);
			return addModelChanges(offerCost, offeredCard, offeringPlayer, acceptingPlayer);

		} catch (NoSuchPermitCardException | IllegalResourceException e) {
			throw new TransactionException(e);
		}
	}
	/**
	 * Tries to retrieve the offered permit card and add it to the accepting player's hand,
	 * first from the offering player's permits hand, and if it fails it
	 * tries to get it from the used permits, if it fails again, an exception is thrown
	 * @param offeringPlayer
	 * @param acceptingPlayer
	 * @throws NoSuchPermitCardException 
	 */
	private void removeOfferedPermit(PlayerState offeringPlayer,PlayerState acceptingPlayer) throws NoSuchPermitCardException{
		try {
			acceptingPlayer.getPlayerData().getPermitsHand()
					.addToHand(offeringPlayer.getPlayerData().getPermitsHand().removeCard(offeredCard));
		} catch (NoSuchPermitCardException e) {
			LOG.log(Level.FINE,"Trying to retrive card from used permits",e);
			acceptingPlayer.getPlayerData().getPermitsHand()
					.addToHand(offeringPlayer.getPlayerData().getUsedPermits().removeCard(offeredCard));
		}
	}

	/**
	 * Adds the following parameters with the following keys to a
	 * {@link ModelChanges} object, which is then returned by this method
	 * 
	 * @param cost
	 *            mapped with the "OfferCost" key
	 * @param offered
	 *            mapped with the "PermitCard" key
	 * @param offering
	 *            mapped with the "OffererID" key
	 * @param accepting
	 *            mapped with the "ActorID" key
	 * @return modelChanges
	 */
	private ModelChanges addModelChanges(int cost, PermitCard offered, PlayerState offering, PlayerState accepting) {

		modelChanges.addToChanges(ACTION_KEY, "AcceptPermitCardOffer");
		modelChanges.addToChanges(ACTOR_KEY, accepting.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFERER_ID_KEY, offering.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFER_COST_KEY, cost);

		modelChanges.addToChanges(PERMIT_CARD_KEY, offered);

		return modelChanges;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + offerCost;
		result = prime * result + ((offeredCard == null) ? 0 : offeredCard.hashCode());
		result = prime * result + ((offeringPlayer == null) ? 0 : offeringPlayer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermitCardOffer other = (PermitCardOffer) obj;
		if (offerCost != other.offerCost)
			return false;
		if (offeredCard == null) {
			if (other.offeredCard != null)
				return false;
		} else if (!offeredCard.equals(other.offeredCard))
			return false;
		if (offeringPlayer == null) {
			if (other.offeringPlayer != null)
				return false;
		} else if (offeringPlayer.getPlayerData().getPlayerID() != other.offeringPlayer.getPlayerData().getPlayerID())
			return false;
		return true;
	}

	@Override
	public int getOfferCost() {
		return offerCost;
	}

	@Override
	public PlayerState getOfferingPlayer() {
		return offeringPlayer;
	}

}
