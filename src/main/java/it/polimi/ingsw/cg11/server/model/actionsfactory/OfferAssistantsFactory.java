package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.market.MarketOffers;
import it.polimi.ingsw.cg11.server.model.player.actions.OfferAssistants;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link OfferAssistants}
 * @author paolasanfilippo
 *
 */
public class OfferAssistantsFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {

		PlayerState player;
		TurnManager turnManager;
		MarketOffers marketOffers;
		int assistantsToOffer;
		int offerCost;

		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData, gameMap, "PlayerID");
			assistantsToOffer = actionData.getInt("assistants");
			offerCost = actionData.getInt("cost");
			turnManager = gameMap.getTurnManager();
			marketOffers = gameMap.getMarketModel();

			return new OfferAssistants(player, turnManager,marketOffers,assistantsToOffer,offerCost, gameMap.getGameTopic());
		} catch (NoSuchPlayerException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

}
