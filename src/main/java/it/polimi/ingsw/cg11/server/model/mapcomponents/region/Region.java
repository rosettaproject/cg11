package it.polimi.ingsw.cg11.server.model.mapcomponents.region;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.*;
import it.polimi.ingsw.cg11.server.model.utils.*;

/**
 * Represents a region of the map in the game, has it's own {@link Deck} of
 * {@link PermitCards} and a {@link FaceUpPermits}, contains a list of
 * {@link City} and a [{@link Balcony}]
 * 
 * @author francesco
 *
 */
public class Region {

	private final String name;
	private Bonus regionBonus;
	private Deck<PermitCard> permitCardsDeck;
	private FaceUpPermits faceUpPermitCards;
	private List<City> cities;
	private Balcony balcony;
	private boolean isBonusAvailable;

	/**
	 * This constructor leaves the {@link PermitCardsDeck},{@link City}s and
	 * {@link FaceUpPermit} references at null
	 * 
	 * @param name
	 * @param bonus
	 * @param balcony
	 */
	protected Region(String name, Bonus bonus, Balcony balcony) {

		this.name = name;
		this.regionBonus = bonus;
		this.permitCardsDeck = null;
		this.faceUpPermitCards = null;
		this.cities = null;
		this.balcony = balcony;
		this.setBonusAvailable(true);
	}
	/**
	 * Returns the name
	 * @return region's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns region's bonus
	 * @return bonus of the region
	 */
	public Bonus getRegionBonus() {
		return this.regionBonus;
	}

	/**
	 * Returns the permit cards deck
	 * @return permit cards Deck
	 */
	public Deck<PermitCard> getPermitCardsDeck() {
		return this.permitCardsDeck;
	}

	/**
	 * Returns the cities in the region 
	 * @return cities of the region
	 */
	public List<City> getCities() {
		return cities;
	}

	/**
	 * Returns the balcony
	 * @return balcony of the region
	 */
	public Balcony getBalcony() {
		return this.balcony;
	}

	/**Returns face up permits
	 * @return the faceUpPermitCards
	 */
	public FaceUpPermits getFaceUpPermitCards() {
		return faceUpPermitCards;
	}

	/**
	 * Should be only used during initialization
	 * 
	 * @param permitCardsDeck
	 */
	public void setPermitCardsDeck(Deck<PermitCard> permitCardsDeck) {
		this.permitCardsDeck = permitCardsDeck;
	}

	/**
	 * Should be only used during initialization
	 * 
	 * @param faceUpPermitCards
	 */
	public void setFaceUpPermitCards(FaceUpPermits faceUpPermitCards) {
		this.faceUpPermitCards = faceUpPermitCards;
	}

	/**
	 * Once all cities have been created, this method is used to give a
	 * reference to the list of cities in the region
	 * 
	 * @param currentMap
	 */
	public void setCities(GameMap currentMap) {

		ListIterator<City> cityIterator = currentMap.getCities().listIterator();
		ArrayList<City> regionCityList = new ArrayList<>();
		City city;

		while (cityIterator.hasNext()) {
			city = cityIterator.next();
			if (city.getRegion().getName().equals(this.name))
				regionCityList.add(city);
		}
		this.cities = regionCityList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "\nRegion: " + name + "\n\nRegionBonus" + regionBonus.toString()
				 + faceUpPermitCards.toString() + balcony.toString() + "\n"
				 + citiesToString();
	}
	/**
	 * Region's cities to string
	 * @return
	 */
	public String citiesToString(){
		String toString = "[Cities of " + name +  " region]";
		Iterator<City> citiesIterator = cities.iterator();
		City city;
		
		while(citiesIterator.hasNext()){
			city = citiesIterator.next();
			toString = toString + city.toString();
		}
		
		return toString;
	}

	/**
	 * Tells if the bonus is available or not
	 * @return <tt>true</tt> if is available, <tt>false</tt> if is not
	 * */
	public boolean isBonusAvailable() {
		return isBonusAvailable;
	}

	/**
	 * Sets the boolean variable isBonusAvailable
	 * @param isBonusAvailable
	 */
	public void setBonusAvailable(boolean isBonusAvailable) {
		this.isBonusAvailable = isBonusAvailable;
	}
	

}