package it.polimi.ingsw.cg11.server.model.exceptions.utils;

import it.polimi.ingsw.cg11.server.model.utils.Bonus;

/**
 * Exception thrown when the {@link Bonus}'s type wanted isn't acceptable.
 * @author paolasanfilippo
 *
 */
public class IllegalBonusTypeException extends Exception {

	private static final long serialVersionUID = -5373597089435637163L;
	/**
	 * Exception thrown when the {@link Bonus}'s type wanted isn't acceptable.
	 */
	public IllegalBonusTypeException() {
		super();
	}
}
