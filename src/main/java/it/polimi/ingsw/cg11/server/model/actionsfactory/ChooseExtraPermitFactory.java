package it.polimi.ingsw.cg11.server.model.actionsfactory;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchRegionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.NoSuchPlayerException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GameMap;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actions.ChooseExtraPermit;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Factory for {@link ChooseExtraPermit}
 * @author paolasanfilippo
 *
 */
public class ChooseExtraPermitFactory extends ActionFactory {

	private JSONObject actionData;
	
	@Override
	public Performable createAction(SerializableAction action, GameMap gameMap) throws IllegalCreationCommandException {
		
		PlayerState player;
		TurnManager turnManager;
		Region region;
		PermitCard permitCardToTake;
		int permitIndex;
		
		try {
			actionData = new JSONObject(action.getJsonConfigAsString());
			player = getPlayerFromActionData(actionData,gameMap,"PlayerID");
			turnManager= gameMap.getTurnManager();
			region = getRegionFromActionData(actionData,gameMap,"region");
			permitCardToTake = createPermitCardFromActionData(actionData,gameMap,"region","permitcard");
			permitIndex = region.getFaceUpPermitCards().getPermitIndex(permitCardToTake);
			
			if(permitIndex<0)
				throw new NoSuchPermitCardException();
			else
				permitCardToTake = region.getFaceUpPermitCards().getPermit(permitIndex);
			
			return new ChooseExtraPermit(player,turnManager,region,permitCardToTake, gameMap.getGameTopic());
		} catch (NoSuchPlayerException | NoSuchRegionException | NoSuchPermitCardException e) {
			throw new IllegalCreationCommandException(e);
		}
		
		
	}

}
