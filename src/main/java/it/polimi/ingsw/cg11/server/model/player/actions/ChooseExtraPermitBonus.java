package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.other.NobilityPath;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * Represents one of the bonuses of the {@link NobilityPath}. It gives to the
 * player the opportunity to choose the bonus of one of the {@link PermitCard}
 * he/she has. (except for the nobility bonus)
 * 
 * @author paolasanfilippo
 *
 */
public class ChooseExtraPermitBonus extends Performable {

	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);

	private PlayerState player;
	private TurnManager turnManager;
	private PermitCard permitToTakeBonusFrom;
	private String gameTopic;

	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param permitToTakeBonusFrom
	 * @param gameTopic
	 */
	public ChooseExtraPermitBonus(PlayerState player, TurnManager turnManager, PermitCard permitToTakeBonusFrom,
			String gameTopic) {
		this.player = player;
		this.turnManager = turnManager;
		this.permitToTakeBonusFrom = permitToTakeBonusFrom;
		this.gameTopic = gameTopic;
	}

	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getPlayerChoicesHandler()
					.chooseAnExtraPermitBonus(permitToTakeBonusFrom);

			publishSelectiveChange(modelChanges, gameTopic, turnManager, player);
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void isAllowed() throws NotAllowedActionException {
		GamePhaseControl gamePhaseControl = new GamePhaseControl();
		PlayerControls playerControls = new PlayerControls();
		CardsControls cardsControls = new CardsControls();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			cardsControls.checkIfPlayerHasPermitCard(player, permitToTakeBonusFrom);
		} catch (NotCorrectGamePhaseException | NoPlayerTurnException | NoSuchPermitCardException e2) {
			throw new NotAllowedActionException(e2);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}
