package it.polimi.ingsw.cg11.server.model.market;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.cards.PoliticCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPoliticCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;
/**
 * Implements an offer of {@link PoliticCard}
 * @author paolasanfilippo
 *
 */
public class PoliticCardOffer implements MarketOffer {

	public static final String POLITIC_CARD_KEY = "PoliticCard";
	public static final String OFFER_COST_KEY = "OfferCost";
	public static final String OFFERER_ID_KEY = "OffererID";
	public static final String ACTOR_KEY = "ActorID";
	public static final String ACTION_KEY = "Action";
	private PlayerState offeringPlayer;
	private PoliticCard offeredCard;
	private int offerCost;
	/**
	 * 
	 * @param offeringPlayer
	 * @param offeredCard
	 * @param offerCost
	 */
	public PoliticCardOffer(PlayerState offeringPlayer, PoliticCard offeredCard, int offerCost) {
		this.offerCost = offerCost;
		this.offeredCard = offeredCard;
		this.offeringPlayer = offeringPlayer;
	}

	@Override
	public ModelChanges acceptOffer(PlayerState acceptingPlayer) throws TransactionException {
		try {
			acceptingPlayer.getPlayerData().getMoney().spend(offerCost);
			offeringPlayer.getPlayerData().getMoney().gain(offerCost);
			offeringPlayer.getPlayerData().getOfferedResources().removePoliticCard(offeredCard);
			acceptingPlayer.getPlayerData().getPoliticsHand()
					.addToHand(offeringPlayer.getPlayerData().getPoliticsHand().removeCard(offeredCard));

			return addModelChanges(offerCost, offeredCard, offeringPlayer, acceptingPlayer);
		} catch (NoSuchPoliticCardException | IllegalResourceException | ColorNotFoundException e) {
			throw new TransactionException(e);
		}
	}
	/**
	 * Adds the following parameters with the following keys to a {@link ModelChanges} object, which is 
	 * then returned by this method
	 * @param cost mapped with the "OfferCost" key
	 * @param offered mapped with the "PoliticCard" key
	 * @param offering mapped with the "OffererID" key
	 * @param accepting mapped with the "ActorID" key
	 * @return modelChange
	 * @throws ColorNotFoundException if the color of the politic card isn't acceptable
	 */
	private ModelChanges addModelChanges(int cost, PoliticCard offered, PlayerState offering, PlayerState accepting)
			throws ColorNotFoundException {

		ModelChanges modelChanges = new ModelChanges();

		modelChanges.addToChanges(ACTION_KEY, "AcceptPoliticCardOffer");
		modelChanges.addToChanges(ACTOR_KEY, accepting.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFERER_ID_KEY, offering.getPlayerData().getPlayerID());
		modelChanges.addToChanges(OFFER_COST_KEY, cost);

		modelChanges.addToChanges(POLITIC_CARD_KEY, offered);

		return modelChanges;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + offerCost;
		result = prime * result + ((offeredCard == null) ? 0 : offeredCard.hashCode());
		result = prime * result + ((offeringPlayer == null) ? 0 : offeringPlayer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PoliticCardOffer other = (PoliticCardOffer) obj;
		if (offerCost != other.offerCost)
			return false;
		if (offeredCard == null) {
			if (other.offeredCard != null)
				return false;
		} else if (!offeredCard.equals(other.offeredCard))
			return false;
		if (offeringPlayer == null) {
			if (other.offeringPlayer != null)
				return false;
		} else if (offeringPlayer.getPlayerData().getPlayerID() != other.offeringPlayer.getPlayerData().getPlayerID())
			return false;
		return true;
	}

	@Override
	public int getOfferCost() {
		return offerCost;
	}

	@Override
	public PlayerState getOfferingPlayer() {
		return offeringPlayer;
	}

}
