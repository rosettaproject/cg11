package it.polimi.ingsw.cg11.server.model.player.resources;

import java.util.*;

import it.polimi.ingsw.cg11.server.model.cards.*;

/**
 * this class is used to store and manage cards that belong to a player
 * 
 * @author francesco
 * @param <E> extends the interface {@link Card}
 */

public abstract class Hand<E extends Card> {

	private List<E> cards;

	protected Hand() {
		this.cards = new ArrayList<>();
	}
	/**
	 * Returns a list of the {@link Card}s in {@link Hand}
	 * @return
	 */
	public List<E> getCards() {
		return this.cards;
	}

	/**
	 * Adds the card passed as a parameter to the {@link Hand}
	 * 
	 * @param card
	 */
	public void addToHand(E card) {
		this.cards.add(card);
	}
	
	/**
	 * Returns the index of the first occurrence of the {@link Card} in {@link Hand}, 
	 * or -1 if the hand doesn't contain such card
	 * @param card
	 * @return index
	 */
	public int indexOf(E card){
		return cards.indexOf(card);
	}
	
	/**
	 * Returns the {@link Card} in {@link Hand} at the specified index
	 * @param index
	 * @return Card
	 */
	public E getCard(int index){
		return cards.get(index);
	}
	/**
	 * Returns the number of {@link Card}s that are in {@link Hand}
	 * @return
	 */
	public int getNumberOfCards(){
		return cards.size();
	}
	
}