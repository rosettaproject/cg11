/**
 * 
 */
package it.polimi.ingsw.cg11.server.view.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class manages the socket type incoming connections. When a client is
 * going to connect with server through socket connection, this class allocates
 * a {@link SocketView} for him.
 * 
 * @author Federico
 *
 */
public class SocketServer implements Runnable {

	private static final Logger LOG = Logger.getLogger(SocketServer.class.getName());

	private final int socketPort;
	private boolean isRunning;

	private final ExecutorService executor;

	private ServerSocket serverSocket;

	/**
	 * Instantiates a socket server. Henceforth server will listen the incoming
	 * socket type connections.
	 * 
	 * @param socketPort indicates the port on with the server will listen 
	 * @throws IOException
	 */
	public SocketServer(int socketPort) throws IOException {
		this.socketPort = socketPort;
		executor = Executors.newCachedThreadPool();
		serverSocket = new ServerSocket(socketPort);
		isRunning = false;
	}

	/**
	 * Starts the server if it is not already started
	 */
	public void startServer() {
		if (!isRunning()) {
			Socket socket;
			SocketView socketView;
			isRunning = true;

			LOG.log(Level.INFO, "Server socket started\n");

			while (isRunning()) {
				try {
					socket = serverSocket.accept();
					socketView = new SocketView(socket);
					executor.execute(socketView);
					LOG.log(Level.INFO, "Client socket connected\n");
				} catch (IOException e) {
					LOG.log(Level.SEVERE, "Can't start socket server\n", e);
				}
			}
		}
	}

	/**
	 * Stops the server  if it is not already stopped
	 */
	public void stopServer() {
		if (isRunning()) {
			isRunning = false;
			executor.shutdownNow();
		}
	}

	/**
	 * @return the TCP port of socket server process
	 */
	public int getPort() {
		return socketPort;
	}

	/**
	 * @return true if server is running, false otherwise
	 */
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public void run() {
		startServer();
	}

}
