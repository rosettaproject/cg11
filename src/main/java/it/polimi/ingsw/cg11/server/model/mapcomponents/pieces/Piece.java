package it.polimi.ingsw.cg11.server.model.mapcomponents.pieces;

import it.polimi.ingsw.cg11.server.model.mapcomponents.city.*;

/**
 * This class represents a generic piece on the map, that has a name and can be
 * moved
 * 
 * @author francesco
 *
 */
public abstract class Piece {

	private final String name;
	private City currentCity;

	protected Piece(String name, City startingCity) {
		this.name = name;
		this.currentCity = startingCity;
	}

	/**
	 * Returns the city on which there's the piece
	 */
	protected City getCurrentCity() {
		return this.currentCity;
	}

	/**
	 * Sets the city on which there's the piece
	 * @param currentCity
	 */
	protected void setCurrentCity(City currentCity) {
		this.currentCity = currentCity;
	}

	/**
	 * Returns the piece's name
	 */
	protected String getName() {
		return this.name;
	}

	/**
	 * Moves the piece from its current position to the destination city. Simply
	 * performs the action, knows nothing of the ruling behind it
	 * 
	 * @param destinationCity
	 */
	protected abstract void move(City destinationCity);

}