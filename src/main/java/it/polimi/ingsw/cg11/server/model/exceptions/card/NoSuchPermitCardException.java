package it.polimi.ingsw.cg11.server.model.exceptions.card;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;

/**
 * Exception thrown when the {@link PermitCard} doesn't exist.
 * @author paolasanfilippo
 *
 */
public class NoSuchPermitCardException extends Exception {

	private static final long serialVersionUID = 1986219000177317536L;

	/**
	 * Exception thrown when the {@link PermitCard} doesn't exist.
	 */
	public NoSuchPermitCardException() {
		super();
	}

	/**
	 * Exception thrown when the {@link PermitCard} doesn't exist.
	 * @param cause
	 */
	public NoSuchPermitCardException(Throwable cause) {
		super(cause);
	}
}
