/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.exceptions.player.action;

/**
 * @author Federico
 *
 */
public class NotAvailableMainActionException extends Exception {

	private static final long serialVersionUID = 2803226620198348782L;
}
