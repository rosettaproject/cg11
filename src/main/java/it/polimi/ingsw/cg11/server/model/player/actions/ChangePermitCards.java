package it.polimi.ingsw.cg11.server.model.player.actions;

import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.control.Performable;
import it.polimi.ingsw.cg11.server.model.cards.Deck;
import it.polimi.ingsw.cg11.server.model.controls.cards.CardsControls;
import it.polimi.ingsw.cg11.server.model.controls.gamephase.GamePhaseControl;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerControls;
import it.polimi.ingsw.cg11.server.model.controls.player.PlayerResourcesControls;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NoPlayerTurnException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotAllowedActionException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.action.NotCorrectGamePhaseException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.command.IllegalCreationCommandException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NoEnoughAssistantsException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.state.IllegalStateChangeException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.GamePhase;
import it.polimi.ingsw.cg11.server.model.mapcomponents.map.TurnManager;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.*;
import it.polimi.ingsw.cg11.server.model.player.actionspool.*;
import it.polimi.ingsw.cg11.server.model.utils.publishsubscribe.Broker;

/**
 * This class tries to build the ChangePermitCards action and tries to execute
 * it. ChangePermitCards is a quick action that permits to change
 * {@link FaceUpPermits} putting them under the {@link Deck}. Then, draws two
 * cards from top of permit deck and puts them faced up on the map.
 * 
 * @author Federico
 *
 */
public class ChangePermitCards extends Performable {

	private Region region;
	private PlayerState player;
	private TurnManager turnManager;
	private String gameTopic;

	private static final int NUMBER_OF_ASSISTANT_NEEDED = 1;
	private static final GamePhase EXPECTED_GAME_PHASE = new GamePhase("StandardPlayPhase", false);

	/**
	 * 
	 * @param player
	 * @param turnManager
	 * @param region
	 * @param gameTopic
	 * @throws IllegalCreationCommandException
	 */
	public ChangePermitCards(PlayerState player, TurnManager turnManager, Region region, String gameTopic)
			throws IllegalCreationCommandException {

		CardsControls cardsControls = new CardsControls();

		this.player = player;
		this.region = region;
		this.turnManager = turnManager;
		this.gameTopic = gameTopic;

		try {
			cardsControls.emptyPermitDeckControl(region.getPermitCardsDeck());
		} catch (EmptyPermitDeckException e) {
			throw new IllegalCreationCommandException(e);
		}
	}

	/**
	 * Executes the ChangePermitCards command.
	 * 
	 * @throws NotAllowedActionException
	 *             when the command can't be executed
	 */
	@Override
	public void execute() throws NotAllowedActionException {
		ModelChanges modelChanges;

		isAllowed();
		try {
			modelChanges = player.getPlayerActionExecutor().getQuickActionsHandler().changePermitCard(region);
			modelChanges.setTopic(gameTopic + CHANGE_SUFFIX);
			publish(modelChanges, modelChanges.getTopic());
		} catch (IllegalStateChangeException e) {
			throw new NotAllowedActionException(e);
		}
	}

	/*
	 * Tests: - permit deck can't be empty - face up permit cards must be 2 (or
	 * another constant) - player must have 1 assistant
	 */
	@Override
	public void isAllowed() throws NotAllowedActionException {
		PlayerResourcesControls playerResourcesControls = new PlayerResourcesControls();
		PlayerControls playerControls = new PlayerControls();
		GamePhaseControl gamePhaseControl = new GamePhaseControl();

		try {
			gamePhaseControl.controlGamePhase(turnManager, EXPECTED_GAME_PHASE);
			playerControls.playerTurnControl(player);
			playerResourcesControls.enoughAssistantControl(player, NUMBER_OF_ASSISTANT_NEEDED);

		} catch (NoEnoughAssistantsException | NoPlayerTurnException | NotCorrectGamePhaseException e) {
			throw new NotAllowedActionException(e);
		}
	}

	@Override
	public void publish(ModelChanges message, String topic) {
		Broker.getBroker().publish(message, topic);
	}

}