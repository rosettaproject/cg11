package it.polimi.ingsw.cg11.server.model.player.resources;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.NegativeValueException;

/**
 * This class is used to store a player's assistants. The amount is stored as an
 * integer and it can be accessed, increased or decreased. It doesn't guarantee that
 * the operation performed is legal
 * 
 * @author francesco
 * 
 */

public class Assistants {

	private int amount;

	/**
	 * Adds gainedAmount to assistant amount. It could throw {@link IllegalResourceException}
	 * if gainedAmount<0
	 * 
	 * @param gainedAmount
	 * @throws IllegalResourceException gainedAmount<0
	 */
	public void gain(int gainedAmount) throws IllegalResourceException {
		try{
			if(gainedAmount<0)
				throw new IllegalArgumentException("gained amount can't be negative");
			this.amount += gainedAmount;	
		} catch(IllegalArgumentException e) {
			throw new IllegalResourceException(e);
		}
	
	}

	/**
	 * Decreases amount of assistants by usedAmount. It could throw {@link IllegalResourceException}
	 * if gainedAmount<0 or if there aren't enough assistant to perform this action without 
	 * leaving a negative number of assistants
	 * 
	 * @param usedAmount
	 * @throws NegativeValueException
	 *             if usedAmount is negeative
	 */
	public void use(int usedAmount) throws IllegalResourceException {
		try{
			if(usedAmount<0)
				throw new IllegalArgumentException("usedAmount can't be negative");
			if (amount - usedAmount < 0)
				throw new NegativeValueException("The number of assistants can't be negative");
			this.amount -= usedAmount;
		} catch(IllegalArgumentException | NegativeValueException e) {
			throw new IllegalResourceException(e);
		}
	}
	
	/**
	 * Returns the amount of assistants
	 * @return amount of assistants
	 */
	public int getAmount() {
		return this.amount;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Assistants [amount=" + amount + "]";
	}
	

}