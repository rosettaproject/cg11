package it.polimi.ingsw.cg11.server.model.utils.observerpattern;


import java.rmi.RemoteException;

import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
/**
 * Represents an observable object of kind {@link Visitable}
 * @author paolasanfilippo
 *
 */
public interface VisitableObservable extends Observable<Visitable>{

	@Override
	public void notifyToObservers(Visitable message) throws RemoteException;

	@Override
	public boolean addObserver(Observer<Visitable> observer) throws RemoteException;

	@Override
	public void removeObserver(Observer<Visitable> observer) throws RemoteException;

}
