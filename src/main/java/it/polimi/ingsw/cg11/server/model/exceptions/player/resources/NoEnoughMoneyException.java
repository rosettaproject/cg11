package it.polimi.ingsw.cg11.server.model.exceptions.player.resources;

import it.polimi.ingsw.cg11.server.model.player.resources.Money;

/**
 * Exception thrown when the amount of {@link Money} isn't enough.
 * @author paolasanfilippo
 *
 */
public class NoEnoughMoneyException extends Exception {

	private static final long serialVersionUID = -2909412788898051683L;

	/**
	 * Exception thrown when the amount of {@link Money} isn't enough.
	 */
	public NoEnoughMoneyException() {
		super();
	}

}
