package it.polimi.ingsw.cg11.server.gameserver;

import java.util.Scanner;

import it.polimi.ingsw.cg11.client.view.cli.ScreenFormat;
import it.polimi.ingsw.cg11.server.gameserver.ConnectionsHandler;

/**
 * This class is the start point of the game (server-side). It creates a broker
 * and starts socket and RMI connections.
 * 
 * @author Federico
 *
 */
public class Server {

	private static int turnsTimer = 600;
	private static ConnectionsHandler connectionsHandler;

	private Server() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		connectionsHandler = ConnectionsHandler.getConnectionHandler();

		setTurnsTimer();
		connectionsHandler.startConnections();

	}
	/**
	 * Asks the user to set the turns' timer
	 */
	private static void setTurnsTimer() {

		Scanner inputScanner;
		String userInput;

		inputScanner = new Scanner(System.in);

		do {
			ScreenFormat.displayOnScreen("Set the turns' timer (in seconds)\n");
			userInput = inputScanner.nextLine();
			
		}while (!isAValidTimer(userInput));
		
		turnsTimer = Integer.parseInt(userInput);
		
		inputScanner.close();
	}
	/**
	 * Checks if the input for the turns' timer is acceptable or not
	 * @param userInput
	 * @return <tt>true</tt> if userInput is valid, <tt>false</tt> otherwise
	 */
	private static boolean isAValidTimer(String userInput) {

		try {
			Integer.parseInt(userInput);
			return true;
		} catch (NumberFormatException e) {
			ScreenFormat.displayOnScreen("Invalid timer\n");
			return false;
		}
	}
	/**
	 * Returns the set timer 
	 * @return timer
	 */
	public static int getTurnsTimer() {
		return turnsTimer;
	}

}
