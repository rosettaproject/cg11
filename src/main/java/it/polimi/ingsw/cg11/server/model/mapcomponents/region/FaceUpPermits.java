package it.polimi.ingsw.cg11.server.model.mapcomponents.region;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.cards.*;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyFaceUpPermitsException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.EmptyPermitDeckException;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.exceptions.map.NoSuchFaceUpPermitsException;

/**
 * This class represents the two face up permits cards. This cards can be seen
 * and one of the two can be drawn.
 * 
 * @author Federico
 */
public class FaceUpPermits {

	private static final Logger LOG = Logger.getLogger(FaceUpPermits.class.getName());
	private final int maxPermitCard;
	private final Region region;
	private List<PermitCard> faceUpPermitCards;
	private Deck<PermitCard> referenceDeck;

	/**
	 * Constructor
	 * @param referenceDeck
	 * @param region
	 * @throws EmptyPermitDeckException
	 */
	public FaceUpPermits(Deck<PermitCard> referenceDeck, Region region) throws EmptyPermitDeckException {
		this.maxPermitCard = 2;
		this.referenceDeck = referenceDeck;
		this.region = region;

		this.faceUpPermitCards = new ArrayList<>();

		for (int indexCard = 0; indexCard < maxPermitCard; indexCard++) {
			try {
				faceUpPermitCards.add(referenceDeck.draw());
			} catch (EmptyDeckException e) {
				throw new EmptyPermitDeckException(e);
			}
		}
	}

	/**
	 * Returns the maximum amount of {@link PermitCard}
	 * @return MAX_PERMIT_CARDS
	 */
	public int getMaxPermitCards() {
		return maxPermitCard;
	}

	/**
	 * Gets the {@link PermitCard} chosen by the index
	 * @param index
	 * @return permit card chosen
	 */
	public PermitCard getPermit(int index) {
		
		return faceUpPermitCards.get(index);
	}
	/**
	 * Return the index from a {@link PermitCard}
	 * @param permit Card
	 * @return index
	 */
	public int getPermitIndex(PermitCard permit) {
		return faceUpPermitCards.lastIndexOf(permit);
	}
	/**
	 * Returns the {@link FaceUpPermits} cards' size
	 * @return size of the list
	 */
	public int getListSize() {
		return faceUpPermitCards.size();
	}
	/**
	 * Tells if the {@link PermitCard} passed as parameter is contained in the {@link FaceUpPermits}
	 * @param permit card
	 * @return <tt>true</tt> if it is contained, <tt>false</tt> if is not
	 */
	public boolean contains(PermitCard permit) {
		return faceUpPermitCards.contains(permit);
	}

	/**
	 * This method returns a {@link FaceUpPermits}. After that, draws a card from Deck
	 * and puts it into the FaceUpPermits' list
	 * 
	 * @param numberOfPermit:
	 *            indicates the permit that has to be returned
	 * @return the chosen permit
	 * @throws EmptyFaceUpPermitsException
	 * @requires numberOfPermit < getListSize() and numberOfPermit>=0
	 * @ensures that a permit is present at the index 'permitIndex' in the
	 *          FaceUpPermits' list
	 */
	public PermitCard obtainPermit(int numberOfPermit) throws EmptyFaceUpPermitsException {
		PermitCard card;
		try {
			card = faceUpPermitCards.remove(numberOfPermit);		
		} catch ( IndexOutOfBoundsException e) {
			throw new EmptyFaceUpPermitsException(e);
		}
		try {
			faceUpPermitCards.add(numberOfPermit, referenceDeck.draw());
		} catch (EmptyDeckException e) {
			LOG.log(Level.FINE, "Empty permit deck", e);
		}
		return card;
	}

	/**
	 * This method puts the two {@link FaceUpPermits} cards under the reference deck
	 * and turn the two card on the top. Throws {@link NoSuchFaceUpPermitsException} 
	 * and {@link EmptyPermitDeckException}
	 * 
	 * @throws NoSuchFaceUpPermitsException
	 * @throws EmptyPermitDeckException
	 */
	public void shuffleBack() throws NoSuchFaceUpPermitsException {
		int firstCard = 0;
		if (faceUpPermitCards.size() != maxPermitCard)
			throw new NoSuchFaceUpPermitsException();

		for (int cardToRemoveIndex = 0; cardToRemoveIndex < maxPermitCard; cardToRemoveIndex++) {
			referenceDeck.addToDeck(faceUpPermitCards.remove(firstCard));
		}
		for (int index = 0; index < maxPermitCard; index++) {
			//Should always enter in this if. We decided to keep the if condition, 
			//just to be sure that the condition will be respected also in case of changes in the code
			if (!referenceDeck.isEmpty()) {
				try {
					faceUpPermitCards.add(referenceDeck.draw());
				} catch (EmptyDeckException e) {
					LOG.log(Level.FINE, "Empty permit deck", e);
				}
			}
		}
	}
	
	
	/**
	 * Removes a {@link PermitCard} and returns it. Throws {@link NoSuchPermitCardException}
	 * @param permitCard
	 * @return permitCard
	 * @throws NoSuchPermitCardException
	 */
	public PermitCard removeCard(PermitCard permitCard) throws NoSuchPermitCardException {
		PermitCard permitCardToRemove;
		int index = faceUpPermitCards.indexOf(permitCard);

		if (index >= 0) {
			try {
				permitCardToRemove = obtainPermit(index);
			} catch (EmptyFaceUpPermitsException e) {
				throw new NoSuchPermitCardException(e);
			}
			return permitCardToRemove;
		}
		throw new NoSuchPermitCardException();
	}
	/**
	 * Returns the {@link FaceUpPermits}' {@link Region}
	 * @return region
	 */
	public Region getREGION() {
		return region;
	}
	
	/**
	 * Retrieves the first card of the reference deck, without removing it
	 * @return Permit card peeked
	 */
	public PermitCard peekAtFirstCard(){
		return referenceDeck.getCards().peek();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "\n\n[FaceUpPermitCards (" + region.getName() + ")]\n";
		if (faceUpPermitCards.isEmpty())
			toString += "empty\n";
		toString += "\n";
		Iterator<PermitCard> iterator = faceUpPermitCards.iterator();
		while (iterator.hasNext())
			toString = toString + iterator.next().toString() + "\n\n";

		return toString;
	}

}