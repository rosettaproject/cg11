/**
 * 
 */
package it.polimi.ingsw.cg11.server.model.controls.mapcomponents;

import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.cards.PermitCard;
import it.polimi.ingsw.cg11.server.model.exceptions.card.NoSuchPermitCardException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.city.City;
import it.polimi.ingsw.cg11.server.model.mapcomponents.region.Region;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * This class contains controls on {@link Region}
 * 
 * @author Federico
 *
 */
public class RegionControls {

	/**
	 * Controls if in a {@link Region} there's a specific
	 * {@link FaceUpPermitsTest}. It throws a {@link NoSuchPermitCardException}
	 * if the permit card passed as a parameter isn' contained in the face up
	 * permit cards.
	 * 
	 * @param region
	 *            : cannot be null
	 * @param permitCard
	 *            : permit card that we want to know either is a face up permit
	 *            or not
	 * @throws NoSuchPermitCardException
	 */
	public void hasPermitCardFaceUp(Region region, PermitCard permitCard) throws NoSuchPermitCardException {
		if (!region.getFaceUpPermitCards().contains(permitCard))
			throw new NoSuchPermitCardException();
	}

	/**
	 * Checks if the {@link Region} bonus is still available. Returns
	 * <tt>true</tt> if it is, <tt>false</tt> if is not.
	 * 
	 * @param player
	 *            : player state
	 * @param region
	 * @return Returns <tt>true</tt> if the bonus is available, <tt>false</tt>
	 *         if is not.
	 */
	public boolean isRegionBonusAvailable(PlayerState player, Region region) {
		List<City> regionCities = region.getCities();
		Iterator<City> cityIterator = regionCities.iterator();
		City cityToControl;
		boolean isRegionBonusAvailable = true;

		if (!region.isBonusAvailable())
			return false;

		while (cityIterator.hasNext() && isRegionBonusAvailable) {
			cityToControl = cityIterator.next();
			if (!cityToControl.existsPlayerEmporium(player)) {
				isRegionBonusAvailable = false;
			}
		}

		return isRegionBonusAvailable;
	}

}
