package it.polimi.ingsw.cg11.server.model.utils.transactions;

import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.TransactionException;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerState;

/**
 * Implements a transaction in which the player gets {@link Money}
 * 
 * @author francesco
 *
 */
public class MoneyTransaction implements Transaction {

	private int amount;
	/**
	 * 
	 * @param amount
	 */
	public MoneyTransaction(int amount) {
		this.amount = amount;
	}

	/**
	 * Lets the player gain the set amount of money
	 * 
	 * @param player
	 */
	@Override
	public void execute(PlayerState transactionSubject) throws TransactionException {

		if (amount < 0) {
			try {
				transactionSubject.getPlayerData().getMoney().spend(Math.abs(amount));
			} catch (IllegalResourceException e) {
				throw new TransactionException(e);
			}
		}else
			try {
				transactionSubject.getPlayerData().getMoney().gain(amount);
			} catch (IllegalResourceException e) {
				throw new TransactionException(e);
			}
	
	}

	@Override
	public int getAmount() {
		return this.amount;
	}

}