package it.polimi.ingsw.cg11.server.model.mapcomponents.other;

import java.awt.Color;
import java.util.Iterator;
import java.util.Queue;

import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;

/**
 * Represents a balcony, with a queue of {@link Councillor}
 * @author paolasanfilippo
 *
 */
public class Balcony {

	private static final int MAX_SIZE = 4;

	private Queue<Councillor> council;
	private final String name;

	/**
	 * Balcony's constructor
	 * @param name : balcony's name
	 * @param councillors
	 */
	public Balcony(String name, Queue<Councillor> councillors) {
		this.name = name;
		this.council = councillors;
	}

	/**
	 * Adds councillor to the balcony. If the councillor passed as parameter isn't valid,
	 * it throws a {@link NoSuchCouncillorException}
	 * @param color
	 * @param councillorPoolManager
	 * @throws NoSuchCouncillorException
	 */
	public void addCouncillor(Color color, CouncillorPoolManager councillorPoolManager)
			throws NoSuchCouncillorException {

		Councillor councillorToAdd;
		Councillor councillorToRemove;

		councillorToRemove = council.remove();
		councillorPoolManager.addPieceToPool(councillorToRemove);

		councillorToAdd = councillorPoolManager.obtainPieceFromPool(color);
		council.add(councillorToAdd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString;
		
		Iterator<Councillor> iterator = council.iterator();
		
		toString = "Balcony (" + name + ") \n\n";

		while(iterator.hasNext())
			toString = toString + iterator.next().toString() + "\n";
		
		return toString;
	}

	/**
	 * Returns the maximum number of councillors
	 * @return the balcony's maximum number of councillors
	 */

	public static int getMaxSize() {
		return Balcony.MAX_SIZE;
	}
	/**
	 * Returns the balcony's name
	 * @return name
	 */
	public String getNAME() {
		return name;
	}

	/**
	 * Returns the council
	 * @return a queue of councillors
	 */
	public Queue<Councillor> getCouncil() {
		return council;
	}

}