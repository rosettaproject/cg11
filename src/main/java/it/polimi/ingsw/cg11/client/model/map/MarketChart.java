package it.polimi.ingsw.cg11.client.model.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * This object contains a list of {@link OfferChart}s representing all the available offers made by the players of
 * a game at an given moment
 * @author francesco
 *
 */
public class MarketChart {

	private final List<OfferChart> offers;

	/**
	 * Initializes the list of offers, which is initially empty
	 */
	public MarketChart() {
		this.offers = new ArrayList<>();
	}

	/**
	 * Adds the {@link OfferChart} passed as a parameter to this onject's list of offers
	 * @param offer
	 */
	public void addOffer(OfferChart offer) {
		offers.add(offer);
	}

	/**
	 * @param index
	 * @return the element at the 'index' position in this object's list of {@link OfferChart}s
	 */
	public OfferChart getOffer(int index) {
		return offers.get(index);
	}

	/**
	 * 
	 * @param offer
	 * @return the index of the element passed as a parameter in this object's list of {@link OfferChart}s.
	 * If the element is not present, returns -1
	 */
	public int getIndexOf(OfferChart offer) {
		return offers.indexOf(offer);
	}

	/**
	 * Remove the {@link OfferChart} at the given position in this object's list
	 * @param index
	 * @return
	 */
	public OfferChart removeOffer(int index) {
		return offers.remove(index);
	}

	/**
	 * @return the size of this object's list of {@link OfferChart}
	 */
	public int numberOfOffers() {
		return offers.size();
	}

	/**
	 * Removes all the {@link OfferChart}s from this object's list
	 */
	public void resetChart() {

		Iterator<OfferChart> offerIterator = offers.listIterator();

		while (offerIterator.hasNext()) {
			offers.remove(offerIterator.next());
		}
	}

	@Override
	public String toString() {
		String toString = "";

		toString += AsString.marketAsString(offers);

		return toString;
	}
}
