package it.polimi.ingsw.cg11.client.view.connections;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.messages.GenericMessage;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.utils.observerpattern.Observer;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This class is used to communicate with server throw a Sockey connection
 * 
 * @author Federico
 *
 */
public class ClientSocketView implements ClientView {

	private static final Logger LOG = Logger.getLogger(ClientSocketView.class.getName());

	private List<Observer<Visitable>> observers;

	private Socket socket;
	private Token token;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	private InetAddress address;
	private int port;

	private String gameIdentifier;

	private ExecutorService executor;
	private Visitable serverMessage;
	private SerializableMapStartupConfig initialMap;
	private GenericMessage startingMessage;

	private boolean playerIsConnecting = true;

	/**
	 * Default constructor
	 * 
	 * @param address
	 *            of server
	 * @param port
	 *            of server
	 * @param token
	 *            of the player
	 * @param controller
	 *            of the current game
	 * @throws IOException
	 */
	protected ClientSocketView(InetAddress address, int port, Token token, ClientController controller)
			throws IOException {
		super();

		this.port = port;
		this.address = address;
		this.token = token;

		observers = new ArrayList<>();
		addObserver(controller);
	}

	@Override
	public void connect() {
		executor = Executors.newCachedThreadPool();

		executor.execute(new Runnable() {

			@Override
			public void run() {
				String serverStartingMessage = "ready";
				String messageReceived;
				String updatedUsername;
				int playerID;
				Object objectReceived;

				try {
					socket = new Socket(address, port); //NOSONAR
					outputStream = new ObjectOutputStream(socket.getOutputStream());
					inputStream = new ObjectInputStream(socket.getInputStream());
				} catch (IOException e) {
					LOG.log(Level.SEVERE, "Can not create socket connection", e);
				}

				try {

					// Wait server starting message
					objectReceived = inputStream.readObject();
					messageReceived = (String) objectReceived;
					if (!messageReceived.equalsIgnoreCase(serverStartingMessage)) {
						throw new ClassNotFoundException();
					}

					// Send token to server
					outputStream.writeObject(token);
					outputStream.flush();
					outputStream.reset();

					// Waiting for player ID
					playerID = (int) inputStream.readObject();
					setPlayerID(playerID);
					
					// Waiting for updated username
					updatedUsername = (String) inputStream.readObject();
					setUpdatedUsername(updatedUsername);

					// Waiting initial message
					startingMessage = (GenericMessage) inputStream.readObject();
					notifyToObservers(startingMessage);

					// Waiting initial map
					initialMap = (SerializableMapStartupConfig) inputStream.readObject();
					gameIdentifier = initialMap.getTopic();
					notifyToObservers(initialMap);

				} catch (ClassNotFoundException | IOException e) {
					LOG.log(Level.SEVERE, "Error during connection setup.", e);
				}
				listenServer();
			}
		});
	}

	@Override
	public void sendToServer(Visitable message) {
		try {
			message.setTopic(gameIdentifier);

			outputStream.writeObject(message);
			outputStream.flush();
			outputStream.reset();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Can not send message to server", e);
		}
	}

	@Override
	public void readMessageFromServer(Visitable message) {
		notifyToObservers(message);
	}

	/**
	 * @return the token
	 */
	@Override
	public Token getToken() {
		return token;
	}

	@Override
	public void notifyToObservers(Visitable message) {
		Iterator<Observer<Visitable>> iterator = observers.iterator();
		Observer<Visitable> observer;
		while (iterator.hasNext()) {
			observer = iterator.next();
			observer.update(message);
		}
	}

	@Override
	public boolean addObserver(Observer<Visitable> observer) {
		return observers.add(observer);
	}

	@Override
	public void removeObserver(Observer<Visitable> observer) {
		int observerIndex;

		observerIndex = observers.indexOf(observer);
		if (observerIndex >= 0)
			observers.remove(observerIndex);

	}

	@Override
	public void disconnect() throws RemoteException {
		String request;

		try {
			request = "disconect_player";
			outputStream.writeObject(request);
			outputStream.flush();
			outputStream.reset();

		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Error during closing of socket connection", e);
		}

	}

	/**
	 * This method starts a thread that listen the server and dispatch incoming
	 * messages to the (client) controller
	 */
	private void listenServer() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				Object obj;
				String response;
				try {
					while (playerIsConnecting) {
						obj = inputStream.readObject();
						if (obj instanceof Visitable) {
							serverMessage = (Visitable) obj;
							readMessageFromServer(serverMessage);
						} else if (obj instanceof String) {
							response = (String) obj;
							if ("player_disconnected".equalsIgnoreCase(response)) { // NOSONAR
								playerIsConnecting = false;
								closeConnection();
							}
						}
					}
				} catch (IOException | ClassNotFoundException e) {
					LOG.log(Level.SEVERE, "Can not read server message", e);
				}
			}
		});
	}

	@Override
	public void setPlayerID(int playerID) throws RemoteException {
		token.setPlayerID(playerID);
	}

	/**
	 * Closes input (from server) and output (to server) stream and close socket
	 * connection
	 * 
	 * @throws IOException
	 */
	private void closeConnection() throws IOException {
		inputStream.close();
		outputStream.close();
		socket.close();
	}

	@Override
	public void setUpdatedUsername(String updatedUsername) throws RemoteException {
		token.setClientUsername(updatedUsername);
	}

}
