package it.polimi.ingsw.cg11.client.model.player;

import java.awt.Color;

import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
import it.polimi.ingsw.cg11.server.model.player.resources.Money;
import it.polimi.ingsw.cg11.server.model.player.resources.NobilityPoints;
import it.polimi.ingsw.cg11.server.model.player.resources.VictoryPoints;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 *Contains all the data necessary to display a player's resources
 * @author francesco
 *
 */
public class PlayerResourcesChart {


	private final String playerUsername;
	private final int playerID;
	private final Color playerColor;
	private final Assistants playerAssistants;
	private final Money playerMoney;
	private final NobilityPoints playerNobilityPoints;
	private final VictoryPoints playerVictoryPoints;
	private final PlayerEmporiums emporiums;
	

	public PlayerResourcesChart(Token playerToken,Color playerColor, Assistants playerAssistants, Money playerMoney,
			NobilityPoints playerNobilityPoints, VictoryPoints playerVictoryPoints, PlayerEmporiums emporiumList) {

		this.playerUsername = playerToken.getClientUsername();
		this.playerID = playerToken.getPlayerID();
		this.playerColor = playerColor;
		this.playerAssistants = playerAssistants;
		this.playerMoney = playerMoney;
		this.playerNobilityPoints = playerNobilityPoints;
		this.playerVictoryPoints = playerVictoryPoints;
		this.emporiums = emporiumList;
	}

	public int getRemainingEmporiums(){
		return emporiums.getEmporiumNumber();
	}
	
	public ClientEmporium removeEmporium(){
		return emporiums.removeEmporium();
	}
	public String getPlayerUsername() {
		return playerUsername;
	}

	public int getPlayerID(){
		return playerID;
	}
	public Assistants getPlayerAssistants() {
		return playerAssistants;
	}
	public Money getPlayerMoney() {
		return playerMoney;
	}
	public NobilityPoints getPlayerNobilityPoints() {
		return playerNobilityPoints;
	}
	public VictoryPoints getPlayerVictoryPoints() {
		return playerVictoryPoints;
	}

	public Color getPlayerColor() {
		return playerColor;
	}
	
	
	@Override
	public String toString() {
		String toString = "";
		
		toString += "\nUsername: " + playerUsername;
		toString += "\n" + playerAssistants;
		toString += "\n" + playerMoney;
		toString += "\n" + playerVictoryPoints;
		toString += "\n" + playerNobilityPoints;
		toString += "\n" + emporiums;
		
		return toString;
	}


}
