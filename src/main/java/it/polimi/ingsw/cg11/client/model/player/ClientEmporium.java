package it.polimi.ingsw.cg11.client.model.player;

import java.awt.Color;

/**
 * This immutable objects represents an emporium in the client. It is identified by a color, usually
 * randomly generated and associated with a player on the map in the current game
 * @author francesco
 *
 */
public class ClientEmporium {

	private final Color color;
	private final String playerUsername;
	
	/**
	 * 
	 * @param color the color of this emporium
	 * @param playerUsername the owner of this emporium's username
	 */
	public ClientEmporium(Color color, String playerUsername){
		this.color = color;
		this.playerUsername = playerUsername;
	}

	/**
	 * 
	 * @return this object's color
	 */
	public Color getColor() {
		return color;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((playerUsername == null) ? 0 : playerUsername.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientEmporium other = (ClientEmporium) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (playerUsername == null) {
			if (other.playerUsername != null)
				return false;
		} else if (!playerUsername.equals(other.playerUsername))
			return false;
		return true;
	}

	/**
	 * @return the playerUsername
	 */
	public String getPlayerUsername() {
		return playerUsername;
	}
	
	@Override
	public String toString() {
		String toString = "";
		
		toString += playerUsername;
		
		return toString;
	}

}
