/**
 * 
 */
package it.polimi.ingsw.cg11.client.model.exceptions;

/**
 * Exception signaling that the input received from the user does not correspond to any known command 
 * @author Federico
 *
 */
public class NotWellFormedInputCommandException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 758931869262400198L;

	/**
	 * 
	 */
	public NotWellFormedInputCommandException() {
		super();
	}

	/**
	 * @param message
	 */
	public NotWellFormedInputCommandException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NotWellFormedInputCommandException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotWellFormedInputCommandException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NotWellFormedInputCommandException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
