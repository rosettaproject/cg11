package it.polimi.ingsw.cg11.client.model.changes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.OfferChart;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.market.AssistantsOffer;
import it.polimi.ingsw.cg11.server.model.market.PermitCardOffer;
import it.polimi.ingsw.cg11.server.model.market.PoliticCardOffer;
import it.polimi.ingsw.cg11.server.model.player.actionspool.MarketActionsHandler;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This object is an extension of the {@link ActionsTranslator} abstract class,
 * specialized in translating {@link ModelChanges} generated by market phase related actions.
 * 
 * @author francesco
 *
 */
public class MarketActionsTranslator extends ActionsTranslator {

	private static final String S_OFFER = "'s offer ";
	private static final String SPENT = " spent ";
	private static final String ACCEPTED = " accepted ";
	private static final String ADDED_THE_FOLLOWING_OFFER = " added the following offer:\n";
	private static final String POLITIC_CARD_OFFER = "PoliticCardOffer";
	private static final String ACCEPT_MARKET_OFFER = "\n\nAction performed: accept market offer";
	private static final String PERMIT_CARD_OFFER = "PermitCardOffer";
	private static final String ASSISTANTS_OFFER = "AssistantsOffer";
	private static final Logger LOG = Logger.getLogger(MarketActionsTranslator.class.getName());
	private static final String ILLEGAL_CHANGE = "Illegal change";
	private static final String ADD_MARKET_OFFER = "\n\nAction performed: add market offer";
	private ChangesMessage message;

	/**
	 * @param userToken a {@link Token} containing the client user's playerID and username
	 * @param message a string message
	 */
	public MarketActionsTranslator(Token userToken, ChangesMessage message) {
		super(userToken, message);
		this.message = message;
	}

	/**
	 * This method allows to translate the information contained in the
	 * {@link ModelChanges} passed as a parameter into actual changes in the
	 * state of the {@link ClientMap} passed as the second parameter, provided
	 * that these changes originated from a market phase related action. More formally, this
	 * means that the JSONObject contained as a string in the ModelChanges
	 * parameter must contain an "Action" key associated with one of the
	 * following strings:
	 * <ul>
	 * <li>"AcceptAssistantsOffer"
	 * <li>"AcceptPermitCardOffer"
	 * <li>"AcceptPoliticCardOffer"
	 * <li>"AddPoliticCardOffer"
	 * <li>"AddPermitCardOffer"
	 * <li>"AddAssistantsOffer"
	 * </ul>
	 * <br>
	 * If none of the above keys is present, the value of the message field is
	 * not modified, and no additional operation is performed <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	public ChangesMessage translateMarketActionChange(ClientMap gameMap, ModelChanges changes) {

		JSONObject changesAsJson = new JSONObject(changes.getJsonConfigAsString());

		if (changesAsJson.has(MarketActionsHandler.ACTION_KEY)) {
			String actionName = changesAsJson.getString(MarketActionsHandler.ACTION_KEY);

			switch (actionName) {
			case "AcceptAssistantsOffer":
				message = translateAcceptAssistantsOffer(gameMap, changesAsJson);
				break;
			case "AcceptPermitCardOffer":
				message =  translateAcceptPermitCardOffer(gameMap, changesAsJson);
				break;
			case "AcceptPoliticCardOffer":
				message =  translateAcceptPoliticCardOffer(gameMap, changesAsJson);
				break;
			case "AddPoliticCardOffer":
				message =  translateAddPoliticCardOffer(gameMap, changesAsJson);
				break;
			case "AddPermitCardOffer":
				message =  translateAddPermitCardOffer(gameMap, changesAsJson);
				break;
			case "AddAssistantsOffer":
				message =  translateAddAssistantsOffer(gameMap, changesAsJson);
				break;
			default:
				break;
			}
		}
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AcceptAssistantsOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OffererID" key, mapped to the playerID of the player who made the market offer to accept
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>an "Assistants" key, mapped to an integer representing the number of assistants offered 
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAcceptAssistantsOffer(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage(ACCEPT_MARKET_OFFER);
		int actorID = changesAsJson.getInt(AssistantsOffer.ACTOR_KEY);
		int offererID = changesAsJson.getInt(AssistantsOffer.OFFERER_ID_KEY);
		int offerCost = changesAsJson.getInt(AssistantsOffer.OFFER_COST_KEY);
		int assistants = changesAsJson.getInt(AssistantsOffer.ASSISTANTS_KEY);

		PlayerResourcesChart actorResources = getPlayerResourcesFromID(actorID, gameMap);
		PlayerResourcesChart offererResources = getPlayerResourcesFromID(offererID, gameMap);

		String actorUsername = getUsernameFromID(actorID, gameMap);
		String offererUsername = getUsernameFromID(offererID, gameMap);

		OfferChart offer = new OfferChart(offererUsername, ASSISTANTS_OFFER, assistants, offerCost);
		int offerIndex = gameMap.getMarketChart().getIndexOf(offer);
		
		try {
			removeOfferAndTransferMoney(offerIndex, gameMap, actorResources, offererResources, offerCost);
			actorResources.getPlayerAssistants().gain(assistants);
			offererResources.getPlayerAssistants().use(assistants);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

		message.addMessage("\n\n" + actorUsername + ACCEPTED + offererUsername + S_OFFER);
		message.addMessage(actorUsername + SPENT  + offerCost + " money and gained " + assistants + " assistants\n");
		
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AcceptPermitCardOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OffererID" key, mapped to the playerID of the player who made the market offer to accept
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>a "PermitCard" key, mapped to a JSONObject structured in such a way that  the
	 * {@link ClientPermitCardFactory#createPermitCardFromChanges(JSONObject)} method can create an 
	 * appropriate {@link ClientPermitCard} from it
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAcceptPermitCardOffer(ClientMap gameMap, JSONObject changesAsJson) {
		
		message = new ChangesMessage(ACCEPT_MARKET_OFFER);
		int actorID = changesAsJson.getInt(PermitCardOffer.ACTOR_KEY);
		int offererID = changesAsJson.getInt(PermitCardOffer.OFFERER_ID_KEY);
		int offerCost = changesAsJson.getInt(PermitCardOffer.OFFER_COST_KEY);
		JSONObject permitAsJson = changesAsJson.getJSONObject(PermitCardOffer.PERMIT_CARD_KEY);

		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		ClientPermitCard offeredCard = permitFactory.createPermitCardFromChanges(permitAsJson);

		PlayerResourcesChart actorResources = getPlayerResourcesFromID(actorID, gameMap);
		PlayerResourcesChart offererResources = getPlayerResourcesFromID(offererID, gameMap);

		String actorUsername = getUsernameFromID(actorID, gameMap);
		String permitRegion = getPermitRegionFromPermit(offeredCard,gameMap);
		String offererUsername = getUsernameFromID(offererID, gameMap);

		OfferChart offer = new OfferChart(offererUsername, PERMIT_CARD_OFFER, offeredCard,permitRegion, offerCost);
		int offerIndex = gameMap.getMarketChart().getIndexOf(offer);

		UserHand actorUserHand = null;
		OpponentHand actorOpponentHand = null;
		UserHand offererUserHand = null;
		OpponentHand offererOpponentHand = null;

		if (actorID == gameMap.getUserResources().getPlayerID()) {
			actorUserHand = gameMap.getUserHand();
			offererOpponentHand = getOpponentHandFromID(offererID, gameMap);
		}
		if (offererID == gameMap.getUserResources().getPlayerID()) {
			offererUserHand = gameMap.getUserHand();
			actorOpponentHand = getOpponentHandFromID(actorID, gameMap);
		}
		if (offererID != gameMap.getUserResources().getPlayerID()
				&& actorID != gameMap.getUserResources().getPlayerID()) {
			offererOpponentHand = getOpponentHandFromID(offererID, gameMap);
			actorOpponentHand = getOpponentHandFromID(actorID, gameMap);
		}

		try {
			removeOfferAndTransferMoney(offerIndex, gameMap, actorResources, offererResources, offerCost);
			transferPermitCard(offeredCard, actorUserHand, offererUserHand, actorOpponentHand, offererOpponentHand);
			assignResources(actorID, gameMap, changesAsJson);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

		message.addMessage("\n\n" + actorUsername + ACCEPTED + offererUsername + S_OFFER);
		message.addMessage(
				actorUsername + SPENT + offerCost + " money and gained the following permit card :\n" + offeredCard + "\n");
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AcceptPoliticCardOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OffererID" key, mapped to the playerID of the player who made the market offer to accept
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>a "PoliticCard" key, mapped to a JSONObject containing a "color" key associated with the string 
	 * representation of a color
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAcceptPoliticCardOffer(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage(ACCEPT_MARKET_OFFER);
		int actorID = changesAsJson.getInt(PoliticCardOffer.ACTOR_KEY);
		int offererID = changesAsJson.getInt(PoliticCardOffer.OFFERER_ID_KEY);
		int offerCost = changesAsJson.getInt(PoliticCardOffer.OFFER_COST_KEY);
		ClientPoliticCard offeredCard = getPoliticCardFromChanges(PoliticCardOffer.POLITIC_CARD_KEY, changesAsJson);

		PlayerResourcesChart actorResources = getPlayerResourcesFromID(actorID, gameMap);
		PlayerResourcesChart offererResources = getPlayerResourcesFromID(offererID, gameMap);

		String actorUsername = getUsernameFromID(actorID, gameMap);
		String offererUsername = getUsernameFromID(offererID, gameMap);

		OfferChart offer = new OfferChart(offererUsername, POLITIC_CARD_OFFER, offeredCard, offerCost);
		int offerIndex = gameMap.getMarketChart().getIndexOf(offer);

		UserHand actorUserHand = null;
		OpponentHand actorOpponentHand = null;
		UserHand offererUserHand = null;
		OpponentHand offererOpponentHand = null;

		if (actorID == gameMap.getUserResources().getPlayerID()) {
			actorUserHand = gameMap.getUserHand();
			offererOpponentHand = getOpponentHandFromID(offererID, gameMap);
		}
		if (offererID == gameMap.getUserResources().getPlayerID()) {
			offererUserHand = gameMap.getUserHand();
			actorOpponentHand = getOpponentHandFromID(actorID, gameMap);
		}
		if (offererID != gameMap.getUserResources().getPlayerID()
				&& actorID != gameMap.getUserResources().getPlayerID()) {
			offererOpponentHand = getOpponentHandFromID(offererID, gameMap);
			actorOpponentHand = getOpponentHandFromID(actorID, gameMap);
		}

		try {
			removeOfferAndTransferMoney(offerIndex, gameMap, actorResources, offererResources, offerCost);
			transferPoliticCard(offeredCard, actorUserHand, offererUserHand, actorOpponentHand, offererOpponentHand);
			assignResources(actorID, gameMap, changesAsJson);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

		message.addMessage("\n\n" + actorUsername + ACCEPTED + offererUsername + S_OFFER);
		message.addMessage(
				actorUsername + SPENT + offerCost + " money and gained the following politic card :\n" + offeredCard + "\n");
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AddPoliticCardOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>a "OfferedItem" key, mapped to a JSONObject containing a "color" key associated with the string 
	 * representation of a color
	 * </ul>
	 * <br>
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAddPoliticCardOffer(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage(ADD_MARKET_OFFER);
		int actorID = changesAsJson.getInt(MarketActionsHandler.ACTOR_ID_KEY);
		int offerCost = changesAsJson.getInt(MarketActionsHandler.OFFER_COST_KEY);
		ClientPoliticCard offeredCard = getPoliticCardFromChanges(MarketActionsHandler.OFFERED_ITEM_KEY, changesAsJson);
		String offeringPlayerUsername = getUsernameFromID(actorID, gameMap);

		OfferChart offerToAdd = new OfferChart(offeringPlayerUsername, POLITIC_CARD_OFFER, offeredCard, offerCost);

		gameMap.getMarketChart().addOffer(offerToAdd);
		message.addMessage(offeringPlayerUsername + ADDED_THE_FOLLOWING_OFFER + offerToAdd.toString());
		
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AddPermitCardOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>a "OfferedItem" key, mapped to a JSONObject structured in such a way that  the
	 * {@link ClientPermitCardFactory#createPermitCardFromChanges(JSONObject)} method can create an 
	 * appropriate {@link ClientPermitCard} from it
	 * </ul>
	 * <br>
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAddPermitCardOffer(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage(ADD_MARKET_OFFER);
		int actorID = changesAsJson.getInt(MarketActionsHandler.ACTOR_ID_KEY);
		int offerCost = changesAsJson.getInt(MarketActionsHandler.OFFER_COST_KEY);
		JSONObject permitCardAsJson = changesAsJson.getJSONObject(MarketActionsHandler.OFFERED_ITEM_KEY);
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();
		ClientPermitCard offeredCard = permitFactory.createPermitCardFromChanges(permitCardAsJson);
		String permitRegion = getPermitRegionFromPermit(offeredCard,gameMap);
		String offeringPlayerUsername = getUsernameFromID(actorID, gameMap);

		OfferChart offerToAdd = new OfferChart(offeringPlayerUsername, PERMIT_CARD_OFFER, offeredCard,permitRegion, offerCost);

		gameMap.getMarketChart().addOffer(offerToAdd);
		message.addMessage("\n\n" + offeringPlayerUsername + ADDED_THE_FOLLOWING_OFFER + offerToAdd.toString());
		
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * action "AddPermitCardOffer". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "OfferCost" key, mapped to an integer representing the money needed to accept the offer
	 * <li>a "OfferedItem" key, mapped to an integer representing the number of assistants offered
	 * </ul>
	 * <br>
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateAddAssistantsOffer(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage(ADD_MARKET_OFFER);
		int actorID = changesAsJson.getInt(MarketActionsHandler.ACTOR_ID_KEY);
		int offerCost = changesAsJson.getInt(MarketActionsHandler.OFFER_COST_KEY);
		int offeredAssistants = changesAsJson.getInt(MarketActionsHandler.OFFERED_ITEM_KEY);
		String offeringPlayerUsername = getUsernameFromID(actorID, gameMap);

		OfferChart offerToAdd = new OfferChart(offeringPlayerUsername, ASSISTANTS_OFFER, offeredAssistants, offerCost);

		gameMap.getMarketChart().addOffer(offerToAdd);
		message.addMessage("\n\n" + offeringPlayerUsername + ADDED_THE_FOLLOWING_OFFER + offerToAdd.toString());
		
		return message;
	}
	
	/**
	 * Returns a string representing the name of the same in-game region of the permit cards deck where the
	 * {@link PermitCard} mirroring the {@link ClientPermitCard} passed as a parameter comes from.
	 * This method works under the assumption that what defines a permit card's region is the list of city
	 * names in it, which can only belong to a single region per permit card
	 * @param permitCard a generic ClientPermitCard
	 * @param gameMap the {@link ClientMap} where the requested region is located
	 * @return a string representing the name of a region, or null if no region is found which matches the
	 * search criteria
	 */
	private String getPermitRegionFromPermit(ClientPermitCard permitCard,ClientMap gameMap) {
		
		List<ClientCity> cities = gameMap.getCities();
		
		for(ClientCity city : cities){
			if(permitCard.getCityNames().contains(city.getName()))
				return city.getRegionName();
		}
		LOG.log(Level.SEVERE,"region not found");
		return null;
	}

	/**
	 * This method transfer the amount of money passed as a parameter from the first {@link PlayerResourcesChart}
	 * passed as a parameter to the second one, and removes the {@link OfferChart} at the 'offerIndex' position
	 * in the list contained in the {@link MarketChart} belonging to the {@link ClientMap} passed as a parameter 
	 * @param offerIndex the position of the offer to remove in the list of offers contained in the {@link MarketChart}
	 * @param gameMap  the {@link ClientMap} where the offer is located
	 * @param actorResources the PlayerResourcesChart of the accepting player
	 * @param offererResources the PlayerResourcesChart of the offering player
	 * @param offerCost the cost of the offer to remove
	 * @throws IllegalResourceException if the offer index is negative
	 */
	private void removeOfferAndTransferMoney(int offerIndex, ClientMap gameMap, PlayerResourcesChart actorResources,
			PlayerResourcesChart offererResources, int offerCost) throws IllegalResourceException {

		if (offerIndex < 0)
			throw new IllegalResourceException();

		gameMap.getMarketChart().removeOffer(offerIndex);
		offererResources.getPlayerMoney().gain(offerCost);
		actorResources.getPlayerMoney().spend(offerCost);
	}

	/**
	 * Transfers a {@link ClientPermitCard} from the hand of a player to another's.
	 * The 'Hand' parameters passed to this method can be null if the hand of either the offering or the
	 * receiving player is not of that specific type, {@link UserHand} or {@link OpponentHand}.
	 * @param offeredCard a ClientPermitCard present in the hand of the offering player
	 * @param actorUserHand not null if the accepting player is the user
	 * @param offererUserHand not null if the offering player is the user
	 * @param actorOpponentHand not null if the accepting player is not the user
	 * @param offererOpponentHand not null if the offering player is not the user
	 */
	private void transferPermitCard(ClientPermitCard offeredCard, UserHand actorUserHand, UserHand offererUserHand,
			OpponentHand actorOpponentHand, OpponentHand offererOpponentHand) {

		if (offererUserHand == null && actorUserHand != null) {
			actorUserHand.addPermitCard(offeredCard);
			removeOffererPermitCard(offeredCard,null,offererOpponentHand);
		}
		if (offererUserHand != null && actorUserHand == null) {
			actorOpponentHand.addPermitCard(offeredCard);
			removeOffererPermitCard(offeredCard,offererUserHand,null);
		}
		if (actorOpponentHand != null && offererOpponentHand != null) {
			actorOpponentHand.addPermitCard(offeredCard);
			removeOffererPermitCard(offeredCard,null,offererOpponentHand);
		}
	}

	private void removeOffererPermitCard(ClientPermitCard offeredCard, UserHand offererUserHand,
			OpponentHand offererOpponentHand) {
		
		ClientPermitCard permitRemoved;
		
		if(offererUserHand == null){
			permitRemoved = offererOpponentHand.removePermitCard(offeredCard);
			if(permitRemoved == null)
				offererOpponentHand.removeUsedPermitCard(offeredCard);
		}else{
			permitRemoved = offererUserHand.removePermitCard(offeredCard);
			if(permitRemoved == null)
				offererUserHand.removeUsedPermitCard(offeredCard);
		}
		
	}

	/**
	 * Transfers a {@link ClientPoliticCard} from the hand of a player to another's.
	 * The 'Hand' parameters passed to this method can be null if the hand of either the offering or the
	 * receiving player is not of that specific type, {@link UserHand} or {@link OpponentHand}.
	 * @param offeredCard a ClientPoliticsCard present in the hand of the offering player
	 * @param actorUserHand not null if the accepting player is the user
	 * @param offererUserHand not null if the offering player is the user
	 * @param actorOpponentHand not null if the accepting player is not the user
	 * @param offererOpponentHand not null if the offering player is not the user
	 */
	private void transferPoliticCard(ClientPoliticCard offeredCard, UserHand actorUserHand, UserHand offererUserHand,
			OpponentHand actorOpponentHand, OpponentHand offererOpponentHand) {

		if (offererUserHand == null && actorUserHand != null) {
			actorUserHand.addPoliticCard(offeredCard);
			offererOpponentHand.removePoliticCard();
		}
		if (offererUserHand != null && actorUserHand == null) {
			actorOpponentHand.addPoliticCard();
			offererUserHand.removePoliticCard(offeredCard);
		}
		if (actorOpponentHand != null && offererOpponentHand != null) {
			actorOpponentHand.addPoliticCard();
			offererOpponentHand.removePoliticCard();
		}

	}

}

