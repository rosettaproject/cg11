/**
 * 
 */
package it.polimi.ingsw.cg11.client.model.exceptions;

/**
 * Exception signaling an issue with the connection to the game server
 * @author Federico
 *
 */
public class ConnectionException extends Exception{


	private static final long serialVersionUID = -7443989531468913660L;
	
	/**
	 * Exception signaling an issue with the connection to the game server
	 * @param cause
	 */
	public ConnectionException(Throwable cause) {
		super(cause);
	}
	
}
