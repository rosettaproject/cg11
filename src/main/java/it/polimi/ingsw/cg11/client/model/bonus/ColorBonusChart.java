package it.polimi.ingsw.cg11.client.model.bonus;

import java.awt.Color;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * This object extends a {@link BonusChart} object adding a color property to it 
 * @author francesco
 *
 */
public class ColorBonusChart extends BonusChart {

	private static final Logger LOG = Logger.getLogger(ColorBonusChart.class.getName());
	private final Color color;
	
	/**
	 * 
	 * Sets the {@link Map} passed as a parameter as the final chart parameter of the object, 
	 * and the {@link Color} passed as a parameter is used to set the color field of this object
	 * @param chart
	 * @param color
	 */
	protected ColorBonusChart(Map<String, Integer> chart,Color color) {
		super(chart);
		this.color = color;
	}

	/**
	 * @return the {@link Color} field for this object 
	 */
	public Color getColor() {
		return color;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColorBonusChart other = (ColorBonusChart) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String toString = "";
		
		try {
			toString += ColorMap.getString(color) + " COLOR BONUS";
			toString += "\n" + super.toString();
			return toString;
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE,"Color not found",e);
			return "";
		}
	}

}
