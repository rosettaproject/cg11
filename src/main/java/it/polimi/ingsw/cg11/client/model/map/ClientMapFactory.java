package it.polimi.ingsw.cg11.client.model.map;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.bonus.BonusChartFactory;
import it.polimi.ingsw.cg11.client.model.bonus.ColorBonusChart;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.player.ClientEmporium;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerEmporiums;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.resources.Assistants;
import it.polimi.ingsw.cg11.server.model.player.resources.Money;
import it.polimi.ingsw.cg11.server.model.player.resources.NobilityPoints;
import it.polimi.ingsw.cg11.server.model.player.resources.VictoryPoints;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * Object used to create a {@link ClientMap} 
 * @author francesco
 *
 */
public class ClientMapFactory {

	private static final String COLOR = "color";
	private static final String INITIALIZATION_ERROR = "Initialization error";
	private static final Logger LOG = Logger.getLogger(ClientMapFactory.class.getName());
	private static final List<Color> generatedColors = new ArrayList<>();
	private static final int PLAYER_STRTING_EMPORIUMS = 10;
	private static final int STARTING_POLITICS_NUMBER = 6;
	private static final int BALCONY_SIZE = 4;
	private static final int FACE_UP_PERMITS_SIZE = 2;

	/**
	 * Returns a {@link ClientMap} obtained from a {@link SerializableMapStartupConfig}
	 * @param startupConfig contains a JSONObject used to initialize the map
	 * @param startingHand contains a JSONObject containing the colors of the {@link PoliticCards}
	 * in the hand of the user
	 * @param user the user's {@link Token}
	 * @param opponents a list containing the opponents' Tokens
	 * @return a ClientMap
	 */
	public ClientMap createClientMap(SerializableMapStartupConfig startupConfig, ModelChanges startingHand, Token user,
			List<Token> opponents) {

		JSONObject startupConfigAsJson = new JSONObject(startupConfig.getJsonConfigAsString());
		JSONObject startingHandAsJson = new JSONObject(startingHand.getJsonConfigAsString());
		JSONArray councillorPool = startupConfigAsJson.getJSONArray("councillorPool");
		CouncillorPoolManager councillorPoolManager;

		ClientMap gameMap;
		UserHand userHand;
		PlayerResourcesChart userResources;
		List<PlayerResourcesChart> opponentsResources;
		List<OpponentHand> opponentsHands;
		List<ClientRegion> regions;
		List<ClientCity> cities;
		List<BonusChart> nobilityPath;
		List<BonusChart> kingBonuses;
		List<Councillor> kingCouncilList;
		List<ColorBonusChart> colorBonus;
		ClientCity kingCity;
		String kingCityName;

		try {
			councillorPoolManager = new CouncillorPoolManager(councillorPool);
		} catch (ColorNotFoundException e) {
			councillorPoolManager = null;
			LOG.log(Level.SEVERE, INITIALIZATION_ERROR, e);
		}

		kingCityName = startupConfigAsJson.getString("KingCity");

		userHand = setUserHand(startingHandAsJson);
		userResources = setUserResources(user);
		opponentsResources = setOpponentsResources(opponents);
		opponentsHands = setOpponentsHands(opponents);
		regions = setRegions(startupConfigAsJson, councillorPoolManager);
		cities = setCities(startupConfigAsJson, kingCityName);
		kingCouncilList = fillCouncil(councillorPoolManager);
		nobilityPath = getBonusListFromJson(startupConfigAsJson, "NobilityPath");
		kingBonuses = getBonusListFromJson(startupConfigAsJson, "KingBonuses");
		colorBonus = getColorBonusListFromJson(startupConfigAsJson, "CityColorBonusList");
		kingCity = setKingCity(cities, kingCityName);

		gameMap = new ClientMap(userHand, userResources, cities, kingCouncilList, kingCity, nobilityPath, kingBonuses);

		for (OpponentHand opponentHand : opponentsHands)
			gameMap.addOpponentHand(opponentHand);
		for (PlayerResourcesChart opponentResources : opponentsResources)
			gameMap.addOpponentsResources(opponentResources);
		for (ClientRegion region : regions)
			gameMap.addRegion(region);

		gameMap.addColorBonus(colorBonus);
		gameMap.setPoolManager(councillorPoolManager);
		buildExtraEmporiums(startupConfigAsJson,gameMap);

		return gameMap;
	}

	/**
	 * Sets this object's king city among the cities passed as a parameter
	 * @param cities a list of {@link ClientCity}s
	 * @param kingCityName a string representing the name of the king's city
	 * @return a {@link ClientCity} such that it's name matches the string passed as a parameter,
	 * or null if there is no such a city
	 */
	private ClientCity setKingCity(List<ClientCity> cities, String kingCityName) {

		for (ClientCity city : cities) {
			if (city.getName().equals(kingCityName))
				return city;
		}
		return null;
	}

	/**
	 * Returns a list of {@link ColorBonusChart} obtained from a JSONObject containing a JSONArray of bonus as
	 * JSONObjects 
	 * @param startupConfigAsJson
	 * @param arrayKey the key to the JSONArray of keys
	 * @return a list of {@link ColorBonusChart}
	 */
	private List<ColorBonusChart> getColorBonusListFromJson(JSONObject startupConfigAsJson, String arrayKey) {

		BonusChartFactory bonusFactory = new BonusChartFactory();
		JSONArray bonusesAsJson = startupConfigAsJson.getJSONArray(arrayKey);
		List<ColorBonusChart> bonusList = new ArrayList<>();

		Iterator<Object> pathIterator = bonusesAsJson.iterator();

		while (pathIterator.hasNext()) {
			try {
				ColorBonusChart colorBonus;
				colorBonus = bonusFactory.createColorBonusChart((JSONObject) pathIterator.next());
				bonusList.add(colorBonus);
			} catch (ColorNotFoundException e) {
				LOG.log(Level.SEVERE, INITIALIZATION_ERROR, e);
			}
		}
		return bonusList;
	}

	/**
	 * Returns a list of {@link BonusChart} obtained from a JSONObject containing a JSONArray of bonus as
	 * JSONObjects 
	 * @param startupConfigAsJson
	 * @param arrayKey the key to the JSONArray of keys
	 * @return a list of {@link BonusChart}
	 */
	private List<BonusChart> getBonusListFromJson(JSONObject startupConfigAsJson, String arrayKey) {

		BonusChartFactory bonusFactory = new BonusChartFactory();
		JSONArray bonusesAsJson = startupConfigAsJson.getJSONArray(arrayKey);
		List<BonusChart> bonusList = new ArrayList<>();

		Iterator<Object> pathIterator = bonusesAsJson.iterator();

		while (pathIterator.hasNext()) {
			BonusChart bonus;
			bonus = bonusFactory.createBonusChart((JSONObject) pathIterator.next());
			bonusList.add(bonus);
		}
		return bonusList;
	}

	/**
	 * Builds extra dummy emporiums as indicated by the rules for a 2 players game
	 * @param startupConfigAsJson
	 * @param gameMap
	 */
	private void buildExtraEmporiums(JSONObject startupConfigAsJson, ClientMap gameMap) {

		if (startupConfigAsJson.has("DummyEmporiums")) {
			JSONArray cities = startupConfigAsJson.getJSONArray("DummyEmporiums");

			List<String> cityNames = new ArrayList<>();
			List<ClientCity> clientCities = gameMap.getCities();

			Iterator<Object> cityNamesIterator = cities.iterator();

			while (cityNamesIterator.hasNext())
				cityNames.add((String) cityNamesIterator.next());

			Iterator<ClientCity> citiesIterator = clientCities.listIterator();

			while (citiesIterator.hasNext()) {
				ClientCity city = citiesIterator.next();
				if (cityNames.contains(city.getName()))
					city.buildEmporium(new ClientEmporium(Color.DARK_GRAY, "Dummy"));
			}
		}
	}

	/**
	 * Sets the list of cities for this map
	 * @param startupConfigAsJson
	 * @param kingCityName
	 * @return
	 */
	private List<ClientCity> setCities(JSONObject startupConfigAsJson, String kingCityName) {

		JSONArray citiesAsJson = startupConfigAsJson.getJSONArray("Cities");
		JSONArray cityBonusAsJson = startupConfigAsJson.getJSONArray("CityBonuses");
		JSONObject adjacentsAsJson = startupConfigAsJson.getJSONObject("Adjacents");

		BonusChartFactory bonusFactory = new BonusChartFactory();
		List<ClientCity> cities = new ArrayList<>();

		int cityIndex;
		int bonusIndex;
		for (cityIndex = 0, bonusIndex = cityIndex; cityIndex < citiesAsJson.length(); cityIndex++, bonusIndex++) {

			JSONObject cityAsJson;
			String cityName;
			String cityRegion;
			Color cityColor;
			BonusChart cityBonus;

			cityAsJson = citiesAsJson.getJSONObject(cityIndex);

			cityName = cityAsJson.getString("City");
			cityRegion = cityAsJson.getString("Region");
			try {
				cityColor = ColorMap.getColor(cityAsJson.getString(COLOR));
			} catch (ColorNotFoundException e) {
				cityColor = null;
				LOG.log(Level.SEVERE, INITIALIZATION_ERROR, e);
			}
			// if the city is the king city the bonus at the 'bonusIndex'
			// position needs to be re used
			if (!cityName.equalsIgnoreCase(kingCityName))
				cityBonus = bonusFactory.createBonusChart(cityBonusAsJson.getJSONObject(bonusIndex));
			else {
				bonusIndex--;
				cityBonus = bonusFactory.createBonusChart(new JSONObject());
				;
			}

			ClientCity city = new ClientCity(cityName, cityRegion, cityBonus, cityColor);
			
			JSONArray nearbyCities = adjacentsAsJson.getJSONArray(cityName);
			List<String> nearbyCitiesAsString = new ArrayList<>();
			Iterator<Object> nearbyCitiesIterator = nearbyCities.iterator();
			
			while(nearbyCitiesIterator.hasNext())
				nearbyCitiesAsString.add((String)nearbyCitiesIterator.next());
			city.addAdjacentCities(nearbyCitiesAsString);
			cities.add(city);
		}
		return cities;
	}

	/**
	 * Sets the regions for this map and 
	 * @param startupConfigAsJson
	 * @param councillorPoolManager
	 * @return
	 */
	private List<ClientRegion> setRegions(JSONObject startupConfigAsJson, CouncillorPoolManager councillorPoolManager) {

		JSONArray regionConfigArray = startupConfigAsJson.getJSONArray("Regions");
		JSONArray regionDecks = startupConfigAsJson.getJSONArray("regionDecks");

		List<ClientRegion> regions = new ArrayList<>();

		int arrayIndex;
		for (arrayIndex = 0; arrayIndex < regionConfigArray.length(); arrayIndex++) {
			regions.add(createRegionFromConfig((JSONObject) regionConfigArray.get(arrayIndex),
					(JSONArray) regionDecks.get(arrayIndex), councillorPoolManager));
		}
		return regions;
	}

	/**
	 * Constructs a {@link ClientRegion} from a JSONObject containing it's configuration
	 * @param regionConfig
	 * @param regionDeck
	 * @param councillorPoolManager
	 * @return a ClientRegion
	 */
	private ClientRegion createRegionFromConfig(JSONObject regionConfig, JSONArray regionDeck,
			CouncillorPoolManager councillorPoolManager) {

		String regionName;
		BonusChart regionBonus;
		VisibleBalcony balcony;
		List<ClientPermitCard> faceUpPermits;
		List<Councillor> councillors;

		BonusChartFactory bonusFactory = new BonusChartFactory();
		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();

		regionBonus = bonusFactory.createBonusChart(regionConfig.getJSONObject("Bonus"));
		regionName = regionConfig.getString("Name");

		councillors = fillCouncil(councillorPoolManager);

		balcony = new VisibleBalcony(councillors);

		faceUpPermits = new ArrayList<>();

		int index;
		for (index = 0; index < FACE_UP_PERMITS_SIZE; index++) {
			faceUpPermits.add(permitFactory.createPermitCardFromConfig(regionDeck.getJSONArray(index)));
		}

		return new ClientRegion(regionName, regionBonus, faceUpPermits, balcony);
	}

	/**
	 * Obtains a list of {@link Councillor}s from a {@link CouncillorPoolManager}
	 * @param councillorPoolManager
	 * @return  a list of Councillors
	 */
	private List<Councillor> fillCouncil(CouncillorPoolManager councillorPoolManager) {

		List<Councillor> councillors = new ArrayList<>();

		int index;
		for (index = 0; index < BALCONY_SIZE; index++) {
			try {
				councillors.add(councillorPoolManager.obtainPieceFromPool());
			} catch (NoSuchCouncillorException e) {
				LOG.log(Level.SEVERE, INITIALIZATION_ERROR, e);
			}

		}
		return councillors;
	}

	/**
	 * Initializes a list of {@link OpponentHand}s from a list of {@link Token}s
	 * @param opponents
	 * @return a list of OpponentHands
	 */
	private List<OpponentHand> setOpponentsHands(List<Token> opponents) {

		List<OpponentHand> opponentsHands = new ArrayList<>();

		for (Token opponent : opponents) {
			List<ClientPermitCard> permitsInHand = new ArrayList<>();
			OpponentHand opponentHand;

			if (opponent.getPlayerID() != 0)
				opponentHand = new OpponentHand(opponent.getPlayerID(), permitsInHand, STARTING_POLITICS_NUMBER);
			else
				opponentHand = new OpponentHand(opponent.getPlayerID(), permitsInHand, STARTING_POLITICS_NUMBER + 1);

			opponentsHands.add(opponentHand);
		}
		return opponentsHands;
	}

	/**
	 * Initializes a list of {@link PlayerResourcesChart}s from a list of {@link Token}s,
	 * representing the opponents' resources
	 * @param opponents
	 * @return a list of PlayerResourcesCharts
	 */
	private List<PlayerResourcesChart> setOpponentsResources(List<Token> opponents) {

		List<PlayerResourcesChart> opponentsResources = new ArrayList<>();

		for (Token opponent : opponents) {
			PlayerResourcesChart opponentResources = setUserResources(opponent);

			opponentsResources.add(opponentResources);

		}

		return opponentsResources;
	}

	/**
	 * Initializes the user's {@link ToPlayerResourcesChartken} from it's {@link Token}
	 * @param player
	 * @return a PlayerResourcesChart
	 */
	private PlayerResourcesChart setUserResources(Token player) {

		int startingAssistants = 1 + player.getPlayerID();
		int startingMoney = 10 + player.getPlayerID();

		Color playerColor = generatePlayerColor();

		Assistants playerAssistants = new Assistants();
		Money playerMoney = new Money();
		NobilityPoints playerNobilityPoints = new NobilityPoints();
		VictoryPoints playerVictoryPoints = new VictoryPoints();

		String playerUsername = player.getClientUsername();

		try {
			playerAssistants.gain(startingAssistants);
			playerMoney.gain(startingMoney);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, "ConfigurationError", e);
		}

		List<ClientEmporium> emporiumList = new ArrayList<>();

		int index;
		for (index = 0; index < PLAYER_STRTING_EMPORIUMS; index++) {
			emporiumList.add(new ClientEmporium(playerColor, playerUsername));
		}
		PlayerEmporiums emporiums = new PlayerEmporiums(emporiumList);

		return new PlayerResourcesChart(player, playerColor, playerAssistants, playerMoney, playerNobilityPoints,
				playerVictoryPoints, emporiums);
	}

	/**
	 * Initializes the user's {@link UserHand} from it's {@link Token}
	 * @param player
	 * @return a UserHand
	 */
	private UserHand setUserHand(JSONObject startingHandAsJson) {

		List<ClientPoliticCard> politicCards = new ArrayList<>();
		List<ClientPermitCard> permitCard = new ArrayList<>();

		Iterator<String> keysIterator = startingHandAsJson.keys();

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			Color color;
			JSONObject cardConfig = (JSONObject) startingHandAsJson.get(key);

			try {
				color = ColorMap.getColor(cardConfig.getString(COLOR));
			} catch (ColorNotFoundException e) {
				LOG.log(Level.SEVERE, "Unknown color", e);
				color = null;
			}
			politicCards.add(new ClientPoliticCard(color));

		}

		return new UserHand(politicCards, permitCard);
	}

	/**
	 * 
	 * @return a randomly generated {@link Color}
	 */
	private Color generatePlayerColor() {

		Random randomGenerator = new Random();
		Color playerColor;

		do {
			int red = randomGenerator.nextInt(255);
			int green = randomGenerator.nextInt(255);
			int blue = randomGenerator.nextInt(255);
			playerColor = new Color(red, green, blue);

		} while (generatedColors.contains(playerColor));

		generatedColors.add(playerColor);
		return playerColor;
	}
}
