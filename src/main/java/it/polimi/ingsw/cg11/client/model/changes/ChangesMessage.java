package it.polimi.ingsw.cg11.client.model.changes;

/**
 * This object represents a message meant to inform the user about a change in the 
 * state of the game, generally caused by an action performed by a player
 * @author francesco
 *
 */
public class ChangesMessage {

	private String message;
	private int playerID;

	private boolean isResponse;

	/**
	 * This constructor sets the isResponse boolean private field to true and the playerID private field to -1 
	 * @param message a string containing a message
	 */
	public ChangesMessage(String message) {
		this.message = message;
		playerID = -1;
	}
	
	/**
	 * 
	 * @return a string containing the message carried by this object
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Attaches the new message passed as a parameter to the one already present, separating the two with a 
	 * newline character
	 * @param newMessage a string representing a message
	 */
	public void addMessage(String newMessage) {
		this.message = message+"\n"+newMessage;
	}

	/**
	 * @return true if the message is meant to be a response message, false otherwise.
	 * The field is set at true by default
	 */
	public boolean isResponse() {
		return isResponse;
	}

	/**
	 * Sets the isResponse boolean field at either true or false
	 * @param isResponse the value at which the isResponse field is meant to be set
	 */
	public void setResponse(boolean isResponse) {
		this.isResponse = isResponse;
	}

	/**
	 * @return the playerID integer field of this object
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * Sets the playerID integer field of this object to the integer value passed as a parameter
	 * @param playerID the value at which this object's playerID field is meant to be set
	 */
	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	
}
