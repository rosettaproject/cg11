package it.polimi.ingsw.cg11.client.view.cli;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.gameclient.InitialCLI;
import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.exceptions.NotWellFormedInputCommandException;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.view.common.GraphicView;
import it.polimi.ingsw.cg11.client.view.connections.ClientView;
import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.server.model.utils.ResourcesPath;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This class manages the command line interface
 * 
 * @author Federico
 *
 */
public class CLI implements GraphicView {

	private static final Logger LOG = Logger.getLogger(InitialCLI.class.getName());

	private static final String HELP_FILE_PATH = "/src/main/resources/help.txt";
	private static final String SHOW = "SHOW";
	private static final String SPACE = "\\s";
	private static final String EMPTY = "";

	private ClientView playerView;
	private SerializableAction action;

	private ModelShows modelShows;
	private Scanner inputScanner;
	private String helpAsString;
	private InputParser inputParser;
	private Token playerToken;

	private boolean inputIsQuit;
	private boolean gameHasStarted;
	private boolean waitingForServerResponse;

	/**
	 * Constructor for cli. Takes as parameter a {@link ClientView}
	 * 
	 * @param playerView
	 */
	public CLI(ClientView playerView) {
		inputScanner = new Scanner(System.in);
		inputParser = new InputParser();

		try {
			this.playerToken = playerView.getToken();
			this.playerView = playerView;
			this.gameHasStarted = false;
			this.waitingForServerResponse = false;
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, "\n\nConnection error\n\n", e);
		}

	}

	@Override
	public void start() {
		String inputCommand;

		helpAsString = getHelpFromFile();
		inputIsQuit = false;

		writeUsername();
		while (!inputIsQuit) {

			inputCommand = inputScanner.nextLine();
			controlInput(inputCommand);
		}
		inputScanner.close();
	}

	/**
	 * Controls the input command
	 * 
	 * @param inputCommand
	 */
	private void controlInput(String inputCommand) {
		try {
			if (gameHasStarted) {
				if (inputCommand.toLowerCase().contains("quit")) {
					inputIsQuit = true;
					disconect();
					ScreenFormat.displayOnScreen("\nGood bye!\n\n\n");
				}
				if (!waitingForServerResponse) {
					if (!inputIsQuit) { // NOSONAR
						analyzeAndExecuteCommand(inputCommand);
					}
				} else {
					ScreenFormat.displayOnScreen("\n\nWaiting for response\n\n");
				}
			} else {
				ScreenFormat.displayOnScreen("\n\nWait for the game to start before typing commands\n\n");
			}
		} catch (NoSuchElementException e) { // NOSONAR
			ScreenFormat.displayOnScreen("\nGood bye!\n\n\n");
			inputScanner.close();
		}
	}

	/**
	 * Starts the disconnection procedure
	 */
	private void disconect() {
		try {
			playerView.disconnect();
		} catch (RemoteException e) {
			LOG.log(Level.FINE, "Error during disconnection", e);
			ScreenFormat.displayOnScreen("\n\nError during disconnection\n\n");
		}
	}

	/**
	 * Analyzes the input command previously read
	 * 
	 * @param inputCommand
	 */
	private void analyzeAndExecuteCommand(String inputCommand) {
		String lowerCaseInputCommand;
		lowerCaseInputCommand = inputCommand.toLowerCase();

		if (lowerCaseInputCommand.startsWith("show")) {
			show(lowerCaseInputCommand);
		} else if (lowerCaseInputCommand.startsWith("help")) {
			printHelp();
		} else if (lowerCaseInputCommand.startsWith("clear")) {
			clearScreen();
		} else {
			parseCommand(inputCommand);
		}
	}

	/**
	 * Clears the screen
	 */
	private void clearScreen() {
		ScreenFormat.clearScreen();
		writeUsername();
	}

	/**
	 * Displays on screen the help
	 */
	private void printHelp() {
		ScreenFormat.displayOnScreen(helpAsString);
		writeUsername();
	}

	/**
	 * Parses the input command
	 * 
	 * @param inputCommand
	 */
	private void parseCommand(String inputCommand) {
		int playerID;

		playerID = playerToken.getPlayerID();
		try {
			action = inputParser.getActionFromInputCommand(inputCommand, playerID);

			if (!isChatMessage(action)) {
				waitingForServerResponse = true;
			}
			playerView.sendToServer(action);

		} catch (NotWellFormedInputCommandException e) { // NOSONAR
			ScreenFormat.displayOnScreen("\n\nCommand not found. Write \"help\" to see how to write a command\n");
			writeUsername();
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, "\nCan not send action to server\n", e);
		}

	}

	/**
	 * Controls if the action is a chat message
	 * 
	 * @param actionToControl
	 * @return true if the actions is a chat message
	 */
	private boolean isChatMessage(SerializableAction actionToControl) {
		JSONObject actionAsJson;
		String actionName;

		actionAsJson = new JSONObject(actionToControl.getJsonConfigAsString());
		actionName = actionAsJson.getString("ActionName");

		if (actionName.contains("chat"))
			return true;

		return false;
	}

	/**
	 * Manages the show command options
	 * 
	 * @param lowerCaseCommand
	 */
	private void show(String lowerCaseCommand) {
		String showToDisplay = "";
		String whatToShow;
		int beginIndex;

		beginIndex = SHOW.length();
		whatToShow = lowerCaseCommand.substring(beginIndex).replaceAll(SPACE, EMPTY);

		switch (whatToShow) {
		case "mydata":
			showToDisplay += modelShows.getCurrentPlayerAsString();
			break;
		case "allplayers":
			showToDisplay += modelShows.getAllPlayersAsString();
			break;
		case "colorbonus":
			showToDisplay += modelShows.colorBonusAsString();
			break;
		case "cities":
			showToDisplay += modelShows.citiesAsString();
			break;
		case "regions":
			showToDisplay += modelShows.regionsAsString();
			break;
		case "king":
			showToDisplay += modelShows.kingInformationAsString();
			break;
		case "nobility":
			showToDisplay += modelShows.nobilityPathInformationAsString();
			break;
		case "councillors":
			showToDisplay += modelShows.getCouncillorPoolAsString();
			break;
		case "market":
			showToDisplay += modelShows.getMarketOfferAsString();
			break;
		default:
			showToDisplay = "\n\nError: " + lowerCaseCommand
					+ " is not accepted. Write help to see what you can show.\n\n";
			break;
		}
		ScreenFormat.displayOnScreen(showToDisplay);
		writeUsername();
	}

	@Override
	public synchronized void update(ChangesMessage message) {
		int messagePlayerID = message.getPlayerID();
		String messageToString = "";

		messageToString += message.getMessage();
		ScreenFormat.displayOnScreen(messageToString);
		if (message.isResponse() && playerToken.getPlayerID() == messagePlayerID) {
			waitingForServerResponse = false;
		}
		if (!message.getMessage().isEmpty())
			writeUsername();
	}

	/**
	 * Saves a file into a string, so it is read only once
	 */
	private String getHelpFromFile() {
		String pathAsString;
		Path helpPath;
		byte[] helpFile = null;

		pathAsString = ResourcesPath.getPomPath() + HELP_FILE_PATH;
		helpPath = Paths.get(pathAsString);

		try {
			helpFile = Files.readAllBytes(helpPath);
		} catch (IOException e) { // NOSONAR
			LOG.log(Level.WARNING, "\nError path of help.txt file\n", e);
			ScreenFormat.displayOnScreen("\nError path of help.txt file\n");

		}
		return new String(helpFile);
	}

	@Override
	public void setGameMap(ClientMap gameMap) {
		inputParser.setGameMap(gameMap);
		modelShows = new ModelShows(gameMap);
		gameHasStarted = true;
	}

	/**
	 * Write the username on screen
	 */
	private void writeUsername() {
		String toDisplay;
		String username;

		username = playerToken.getClientUsername();
		toDisplay = "\n\n" + username + "$: ";

		ScreenFormat.displayOnScreen(toDisplay);
	}

}
