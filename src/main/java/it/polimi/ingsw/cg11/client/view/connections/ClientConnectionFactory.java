package it.polimi.ingsw.cg11.client.view.connections;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.client.model.exceptions.ConnectionException;
import it.polimi.ingsw.cg11.client.view.cli.ScreenFormat;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.server.view.rmi.RemoteFactory;

public class ClientConnectionFactory {

	private static final Logger LOG = Logger.getLogger(ClientConnectionFactory.class.getName());

	private static final int LOCALE_RMI_PORT = 0;
	private static final int SOCKET_PORT = 9998;
	private static final int RMI_PORT = 9999;
	private static final int EXPORT_PORT = 0;

	private static final String CLIENT_RMI_NAME = "clientRMI";
	private static final String IMPL_FACTORY_NAME = "ImplFactory";

	private static ClientConnectionFactory connectionFactory;

	private ClientView clientRMIViewSkeleton;
	private ClientView view;
	private Registry localeRegistry;
	private Registry remoteRegistry;
	private RemoteFactory implFactoryStub;

	private InetAddress ipAddress;
	private Token token;
	private ClientController controller;

	/**
	 * Private constructor called by getConnectionHandler method
	 * 
	 * @param controller
	 *            of the current game
	 * @param token
	 *            of the player
	 * @throws ConnectionException
	 */
	private ClientConnectionFactory(ClientController controller, Token token, String ipAsString) throws ConnectionException {
	
		try {
			ipAddress = InetAddress.getByName(ipAsString);
		} catch (UnknownHostException e1) {
			LOG.log(Level.FINE, "Cannot found IP: " + ipAsString, e1);
			ScreenFormat.displayOnScreen(
					"\n\nCannot found IP: " + ipAsString + ". Trying to connect to localhost server.\n\n");
			ipAddress = InetAddress.getLoopbackAddress();
		}
		
		ScreenFormat.displayOnScreen("IP address actual server: " + ipAddress.getHostAddress() + "\n");
		
		this.token = token;
		this.controller = controller;
		this.view = null;

		try {
			rmiSetup();
		} catch (RemoteException | NotBoundException e) {
			throw new ConnectionException(e);
		}

	}

	/**
	 * Return a connection factory. This object is a singleton
	 * 
	 * @param controller
	 * @param token
	 * @return a singleton connections factory
	 * @throws ConnectionException
	 */
	public static ClientConnectionFactory getConnectionHandler(ClientController controller, Token token, String ipAsString)
			throws ConnectionException {
		if (connectionFactory == null) {
			connectionFactory = new ClientConnectionFactory(controller, token, ipAsString);
		}
		return connectionFactory;
	}

	/**
	 * Creates and returns a sub class of {@link ClientView}
	 * 
	 * @return a {@link ClientRMIView}.
	 */
	public ClientView createRMIView() {
		try {

			view = new ClientRMIView(token, controller);
			ClientRMIView clientRMIView = (ClientRMIView) view;

			localeRegistry = LocateRegistry.createRegistry(LOCALE_RMI_PORT);
			clientRMIViewSkeleton = (ClientView) UnicastRemoteObject.exportObject(view, EXPORT_PORT);
			localeRegistry.rebind(CLIENT_RMI_NAME, clientRMIViewSkeleton);

			clientRMIView.setImplFactoryStub(implFactoryStub);
			implFactoryStub.createRMIView(clientRMIViewSkeleton, token);

		} catch (NotBoundException | RemoteException e) {
			LOG.log(Level.SEVERE, "\nCan not create RMI view\n", e);
		}
		return view;
	}

	/**
	 * Creates and returns a sub class of {@link ClientView}
	 * 
	 * @return a {@link ClientSocketView}.
	 */
	public ClientView createSocketView() {

		try {
			view = new ClientSocketView(ipAddress, SOCKET_PORT, token, controller);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "\nCan not create socket view\n", e);
		}
		return view;
	}

	/**
	 * Set up the RMI connection
	 * 
	 * @throws RemoteException
	 *             if the reference could not be created
	 * @throws NotBoundException
	 *             if name is not currently bound
	 */
	private void rmiSetup() throws RemoteException, NotBoundException {
		remoteRegistry = LocateRegistry.getRegistry(ipAddress.getHostAddress(), RMI_PORT);
		implFactoryStub = (RemoteFactory) remoteRegistry.lookup(IMPL_FACTORY_NAME);
	}

}
