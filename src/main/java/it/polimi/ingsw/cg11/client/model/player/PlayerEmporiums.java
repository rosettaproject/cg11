package it.polimi.ingsw.cg11.client.model.player;

import java.util.List;

/**
 * <this objects contains a player's emporiums in the client
 * @author francesco
 *
 */
public class PlayerEmporiums {
	
	private final List<ClientEmporium> emporiums;
	
	/**
	 * 
	 * @param emporiums the starting emporiums as a list
	 */
	public PlayerEmporiums(List<ClientEmporium> emporiums) {
		this.emporiums = emporiums;
	}

	/**
	 * 
	 * @return the size of this object's emporiums list
	 */
	public int getEmporiumNumber() {
		return emporiums.size();
	}

	/**
	 * 
	 * @return removes the head of this object's emporiums list and returns it
	 */
	public ClientEmporium removeEmporium() {
		return emporiums.remove(0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Emporiums [amount=" + emporiums.size() + "]";
	}
	
	
	
}
