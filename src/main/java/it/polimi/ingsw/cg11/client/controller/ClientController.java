package it.polimi.ingsw.cg11.client.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg11.client.gameclient.ClientGameInitializer;
import it.polimi.ingsw.cg11.client.model.changes.ChangesMessage;
import it.polimi.ingsw.cg11.client.model.changes.ChangesTranslator;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.view.common.GraphicView;
import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.messages.ErrorMessage;
import it.polimi.ingsw.cg11.messages.GenericMessage;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.model.utils.observerpattern.Observable;
import it.polimi.ingsw.cg11.server.model.utils.observerpattern.Observer;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.ClientVisitor;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This class analyzes the message that the client receive, edits the model of client and updates the view
 * 
 * @author Federico
 *
 */
public class ClientController implements ClientVisitor, Observer<Visitable>, Observable<ChangesMessage> {

	private ChangesMessage message;
	private ChangesTranslator translator;
	private SerializableMapStartupConfig mapStartupConfig;
	private ClientMap gameMap;
	private Token user;

	private List<Observer<ChangesMessage>> observers;
	private GraphicView graphicView;

	private boolean waitingForPlayerHand;

	/**
	 * ClientController's constructor
	 * 
	 * @param user
	 */
	public ClientController(Token user) {
		this.message = new ChangesMessage("");
		this.observers = new ArrayList<>();
		this.user = user;
	}

	@Override
	public void update(Visitable message) {
		message.accept(this);
	}

	@Override
	public void visit(SerializableMapStartupConfig serializableMapStartupConfig) {
		String update;

		mapStartupConfig = serializableMapStartupConfig;
		waitingForPlayerHand = true;

		update = "Game is started. Print \"help\" (without quotes) " + "to see which command you can write.\n\n";
		message = new ChangesMessage(update);
		message.setResponse(false);

		notifyToObservers(message);
	}

	@Override
	public void visit(ModelChanges changes) {
		String update;
		int actorID;

		if (waitingForPlayerHand) {
			ClientGameInitializer initializer = new ClientGameInitializer();
			gameMap = initializer.initializeGame(mapStartupConfig, changes, user);
			translator = new ChangesTranslator(user, message);
			setGameMap(gameMap);
			waitingForPlayerHand = false;
		} else {
			if (!translator.isIgnoreNextChange()) {
				message = translator.translateChange(gameMap, changes);
				update = message.getMessage();
				actorID = translator.getActorIDFromChanges(changes);

				message = new ChangesMessage(update);
				message.setResponse(true);
				message.setPlayerID(actorID);
				notifyToObservers(message);
			} else {
				translator.setIgnoreNextChange(false);
			}
		}
	}

	@Override
	public void visit(ErrorMessage errorMessage) {
		String errorMessageToString = errorMessage.getMessage();
		int actorID;
		
		actorID = errorMessage.getPlayerToken().getPlayerID();
		message = new ChangesMessage("\n" + errorMessageToString + "\n");
		message.setResponse(true);
		message.setPlayerID(actorID);
		notifyToObservers(message);

	}

	@Override
	public void visit(GenericMessage genericMessage) {
		String genericMessageToString = genericMessage.getMessage();

		message = new ChangesMessage(genericMessageToString);
		message.setResponse(false);
		notifyToObservers(message);
	}

	@Override
	public void visit(SerializableChatMessage chatMessage) {
		String chatMessageToString = chatMessage.getMessage();
		String toPrint;

		toPrint = "\n\nMessage from " + chatMessage.getSender() + ": ";
		toPrint += chatMessageToString + "\n\n";

		message = new ChangesMessage(toPrint);
		message.setResponse(false);
		notifyToObservers(message);

	}

	/**
	 * Return the game map
	 * @return client game map
	 */
	public ClientMap getGameMap() {
		return gameMap;
	}

	/**
	 * Set the current graphic view
	 * @param graphicView
	 */
	public void setGraphicView(GraphicView graphicView) {
		this.graphicView = graphicView;
		addObserver(graphicView);
	}

	@Override
	public void notifyToObservers(ChangesMessage message) {
		Iterator<Observer<ChangesMessage>> graphicViewIterator = observers.iterator();
		Observer<ChangesMessage> tmpObserver;

		while (graphicViewIterator.hasNext()) {
			tmpObserver = graphicViewIterator.next();
			tmpObserver.update(message);
		}
	}

	@Override
	public boolean addObserver(Observer<ChangesMessage> graphicView) {
		return observers.add(graphicView);
	}

	@Override
	public void removeObserver(Observer<ChangesMessage> observer) {
		observers.remove(observer);
	}

	/**
	 * Set the game map into the graphic view
	 * @param gameMap
	 */
	private void setGameMap(ClientMap gameMap) {
		graphicView.setGameMap(gameMap);
	}

}
