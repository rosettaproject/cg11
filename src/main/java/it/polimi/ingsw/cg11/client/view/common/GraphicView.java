package it.polimi.ingsw.cg11.client.view.common;

import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.server.model.utils.observerpattern.ObserverChange;

/**
 * Represents a generic graphic view, such as CLI or GUI 
 * 
 * @author Federico
 *
 */
public interface GraphicView extends ObserverChange{
	
	/**
	 * Starts the graphic view
	 */
	public void start();
	
	/**
	 * Set the first informations of the game
	 * @param gameMap
	 */
	public void setGameMap(ClientMap gameMap);
	
}
