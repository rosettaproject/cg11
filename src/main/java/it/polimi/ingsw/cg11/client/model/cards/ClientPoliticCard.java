package it.polimi.ingsw.cg11.client.model.cards;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Immutable object representing a politic card in the client. Contains a {@link Color} defining the color of the
 * card
 * @author francesco
 *
 */
public class ClientPoliticCard {

	private final Color color;
	private static final Logger LOG = Logger.getLogger(ClientPoliticCard.class.getName());
	
	/**
	 * 
	 * @param color the color of this politic card
	 */
	public ClientPoliticCard(Color color){
		this.color = color;
	}
	
	/**
	 * 
	 * @return  a {@link Color} representing the color of this politic card 
	 */
	public Color getColor(){
		return color;
	}
	
	/**
	 * 
	 * @return a string representation of this card's {@link Color} obtained through the {@link ColorMap} class.
	 * If the color of this card is not mapped, returns null
	 */
	public String getColorAsString(){
		try {
			return ColorMap.getString(color);
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE,"unknown color", e);
		}
		return null;
	}
	
	/**
	 * @return a string representation of this object with the following structure: 
	 * ColorMap.getString(color) + " politic card\n". If the color of this card is unknown, returns
	 * the following string: "Politic card of unknown color"
	 */
	@Override
	public String toString() {
		try {
			return getColorAsString();
		} catch (NullPointerException e) {
			LOG.log(Level.SEVERE,"unknown color", e);
			return "Politic card of unknown color";
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientPoliticCard other = (ClientPoliticCard) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		return true;
	}
	
}
