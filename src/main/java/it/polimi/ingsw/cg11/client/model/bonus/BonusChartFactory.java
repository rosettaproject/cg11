package it.polimi.ingsw.cg11.client.model.bonus;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * This object allows to create a {@link BonusChart} or a
 * {@link ColorBonusChart} from a {@link JSONObject} containing a mapping of the
 * keys and values used to create the {@link Map} given as a parameter to the
 * BonusChart's constructor
 * 
 * @author francesco
 *
 */
public class BonusChartFactory {

	/**
	 * Returns a {@link BonusChart} containing the keys and values given as a
	 * parameter in the form of a {@link JSONObject}
	 * 
	 * @param bonusConfig
	 * @return
	 */
	public BonusChart createBonusChart(JSONObject bonusConfig) {

		Iterator<String> keysIterator = bonusConfig.keys();
		Map<String, Integer> chart = new HashMap<>();

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			if (bonusConfig.getInt(key) > 0)
				chart.put(key, bonusConfig.getInt(key));
		}
		return new BonusChart(chart);
	}

	/**
	 * Returns a {@link ColorBonusChart} containing the keys and values given as
	 * a parameter in the form of a {@link JSONObject}
	 * 
	 * @param bonusConfig
	 * @return
	 */
	public ColorBonusChart createColorBonusChart(JSONObject bonusConfig) throws ColorNotFoundException {

		Iterator<String> keysIterator = bonusConfig.keys();
		Map<String, Integer> chart = new HashMap<>();
		Color bonusColor = null;

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			if (!"color".equals(key))
				chart.put(key, bonusConfig.getInt(key));
			else {
				bonusColor = ColorMap.getColor(bonusConfig.getString(key));
			}
		}
		return new ColorBonusChart(chart, bonusColor);
	}
}
