package it.polimi.ingsw.cg11.client.model.map;

import java.awt.Color;


/**
 * This objects represents in the client the in-game piece "King", it has a {@link ClienCity} field which
 * indicates the piece's current position on the map, and a {@link Color} only used for eventual
 * GUI implementation purposes
 * @author francesco
 *
 */
public class ClientKing {

	private final Color color;
	private ClientCity currentCity;
	
	/**
	 * 
	 * @param startingCity the king's {@link ClientCity}
	 */
	public ClientKing(ClientCity startingCity) {
		this.color = Color.WHITE;
		this.setCurrentCity(startingCity);
	}

	/**
	 * 
	 * @return the {@link Color} of this piece
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * 
	 * @return the current {@link ClientCity} in which this piece is located
	 */
	public ClientCity getCurrentCity() {
		return currentCity;
	}

	/**
	 * Allows to set a new {@link ClientCity} for this object, which from an in-game perspective means 
	 * moving this piece to the ClientCity passed as a parameter 
	 * @param currentCity
	 */
	public void setCurrentCity(ClientCity currentCity) {
		this.currentCity = currentCity;
	}

	/**
	 * Returns a string containing all the relevant informations on the state of this object in the following
	 * format: ""\n Current city: [nameOfTheCurrentCity]"
	 */
	@Override
	public String toString() {
		String toString = "";
		
		toString += "\nCurrent city: " + currentCity.getName();
		
		return toString;
	}
}
