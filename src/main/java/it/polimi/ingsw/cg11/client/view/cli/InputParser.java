package it.polimi.ingsw.cg11.client.view.cli;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.exceptions.NotWellFormedInputCommandException;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientRegion;
import it.polimi.ingsw.cg11.client.model.map.OfferChart;
import it.polimi.ingsw.cg11.client.model.map.VisibleBalcony;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.messages.SerializableAcceptMarketOffer;
import it.polimi.ingsw.cg11.messages.SerializableAction;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithKing;
import it.polimi.ingsw.cg11.messages.SerializableBuildEmporiumWithPermitCard;
import it.polimi.ingsw.cg11.messages.SerializableChangePermitCards;
import it.polimi.ingsw.cg11.messages.SerializableChatMessage;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraBonus;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermit;
import it.polimi.ingsw.cg11.messages.SerializableChooseExtraPermitBonus;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillor;
import it.polimi.ingsw.cg11.messages.SerializableElectCouncillorWithOneAssistant;
import it.polimi.ingsw.cg11.messages.SerializableEngageAssistant;
import it.polimi.ingsw.cg11.messages.SerializableFinish;
import it.polimi.ingsw.cg11.messages.SerializableOfferAssistants;
import it.polimi.ingsw.cg11.messages.SerializableOfferPermitCard;
import it.polimi.ingsw.cg11.messages.SerializableOfferPoliticCard;
import it.polimi.ingsw.cg11.messages.SerializableOneMoreMainAction;
import it.polimi.ingsw.cg11.messages.SerializableTakePermitCard;
import it.polimi.ingsw.cg11.server.model.actionsfactory.AcceptMarketOfferFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.BuildEmporiumWithKingFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.BuildEmporiumWithPermitCardFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChangePermitCardsFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraBonusFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraPermitBonusFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ChooseExtraPermitFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ElectCouncillorFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.ElectCouncillorWithOneAssistantFactory;
import it.polimi.ingsw.cg11.server.model.actionsfactory.TakePermitCardFactory;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.market.AssistantsOffer;
import it.polimi.ingsw.cg11.server.model.market.PermitCardOffer;
import it.polimi.ingsw.cg11.server.model.market.PoliticCardOffer;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Parses commands: main actions, quick actions, market actions and choices due
 * to bonuses in the nobility path
 * 
 * @author paolasanfilippo
 *
 */
public class InputParser {

	private ClientMap gameMap;
	private StringTokenizer tagTokenizer;
	private JSONObject actionData;

	private static final String ACTION_DELIMITER = ":";
	private static final String PARAMETERS_DELIMITER = ",";
	private static final String VALUES_OF_PARAMETER_DELIMITER = " ";

	private static final String SPACE = "\\s";
	private static final String EMPTY = "";

	private static final String PLAYER = "player";
	private static final String ALL_PLAYERS = "allPlayers";

	private static final String MESSAGE = "message";
	private static final String BALCONY = "balcony";
	private static final String REGION = "region";
	private static final String COLOR = "color";
	private static final String POLITIC_CARDS = "politiccards";
	private static final String CITY = "city";

	private static final String ASSISTANTS = "assistants";
	private static final String PERMIT_CARD = "permitcard";
	private static final String POLITIC_CARD = "politiccard";
	private static final String COST = "cost";
	private static final String OFFER = "offer";

	private static final String CHAT = "chat";

	private static final Logger LOG = Logger.getLogger(InputParser.class.getName());

	private SerializableAction actionToSend;
	private String actionName;
	private String allParametersAsString;
	private List<String> allParameterAsList;

	private Map<String, String> multipleValueParameters;
	private List<String> indexBasedParameters;

	/**
	 * Default constructor
	 */
	public InputParser() {
		multipleValueParameters = new HashMap<>();
		multipleValueParameters.put(POLITIC_CARDS, COLOR);
		multipleValueParameters.put(MESSAGE, EMPTY);
		indexBasedParameters = new ArrayList<>();
		indexBasedParameters.add(PERMIT_CARD);
		indexBasedParameters.add(POLITIC_CARD);
		indexBasedParameters.add(OFFER);
	}

	/**
	 * Parses the input command, create a relative action and return it
	 * 
	 * @param input
	 *            command inserted by the player
	 * @param playerID
	 *            is the ID of the player
	 * @return the action created
	 * @throws NotWellFormedInputCommandException
	 *             when the input command is not well formed
	 */
	public SerializableAction getActionFromInputCommand(String input, int playerID)
			throws NotWellFormedInputCommandException {
		StringTokenizer actionTokenizer;
		String actionToken;

		resetAction();

		try {
			actionTokenizer = new StringTokenizer(input, ACTION_DELIMITER);
			actionToken = actionTokenizer.nextToken();

			actionName = actionToken.replaceAll(SPACE, EMPTY).toLowerCase();

			actionData.put("PlayerID", playerID);
			actionData.put("ActionName", actionName);

			if (actionTokenizer.hasMoreTokens()) {
				allParametersAsString = actionTokenizer.nextToken();
				allParameterAsList = parametersFromStringToList(allParametersAsString);
			}

			addParametersToActionData();
			return actionToSend;

		} catch (Exception e) {
			resetAction();
			throw new NotWellFormedInputCommandException(e);
		}

	}

	/**
	 * Extracts the parameters of the string and puts them into a list
	 * 
	 * @param paramAsString
	 * @return list of parameters with the corrisponding values
	 */
	private List<String> parametersFromStringToList(String paramAsString) {
		List<String> paramAsList = new ArrayList<>();

		StringTokenizer parameterTokenizer;
		String singleParameter;

		parameterTokenizer = new StringTokenizer(paramAsString, PARAMETERS_DELIMITER);

		while (parameterTokenizer.hasMoreTokens()) {
			singleParameter = parameterTokenizer.nextToken();

			if (singleParameter.toLowerCase().contains(MESSAGE) && parameterTokenizer.hasMoreTokens()) {

				singleParameter += PARAMETERS_DELIMITER;
				while (parameterTokenizer.hasMoreTokens()) {
					singleParameter += parameterTokenizer.nextToken();
				}
			}

			paramAsList.add(singleParameter);
		}

		return paramAsList;
	}

	/**
	 * Adds parameters to the actionData of main and quick actions
	 * 
	 * @throws NotWellFormedInputCommandException
	 *             if some error jumps out
	 */
	private void addParametersToActionData() throws NotWellFormedInputCommandException {

		switch (actionName) {
		// main actions
		case "electcouncillor":
			addElectCouncillorParameters();
			actionToSend = new SerializableElectCouncillor();
			break;
		case "takepermitcard":
			addTakePermitCardParameters();
			actionToSend = new SerializableTakePermitCard();
			break;
		case "buildemporium":
			addBuildEmporiumParameters();
			actionToSend = new SerializableBuildEmporiumWithPermitCard();
			break;
		case "buildemporiumwithking":
			addBuildEmporiumWithKingParameters();
			actionToSend = new SerializableBuildEmporiumWithKing();
			break;
		// quick actions
		case "electcouncillorwithassistant":
			addElectCouncillorParameters();
			actionToSend = new SerializableElectCouncillorWithOneAssistant();
			break;
		case "changefaceuppermits":
			addChangeFaceUpPermitsParameters();
			actionToSend = new SerializableChangePermitCards();
			break;
		case "engageassistant":
			actionToSend = new SerializableEngageAssistant();
			break;
		case "onemoremainaction":
			actionToSend = new SerializableOneMoreMainAction();
			break;
		default:
			addParametersToActionData2();
			break;
		}
		actionToSend.setJsonConfigAsString(actionData);
	}

	/**
	 * Add parameters to the different commands during the market phase and
	 * choices to be taken during the nobility path
	 * 
	 * @throws NotWellFormedInputCommandException
	 *             if some error jumps out
	 */
	private void addParametersToActionData2() throws NotWellFormedInputCommandException { // NOSONAR
		switch (actionName) {
		// market
		case "assistantsoffer":
			addAssistantsOfferParameters();
			actionToSend = new SerializableOfferAssistants();
			break;
		case "politiccardoffer":
			addPoliticCardOfferParameters();
			actionToSend = new SerializableOfferPoliticCard();
			break;
		case "permitcardoffer":
			addPermitCardOfferParameters();
			actionToSend = new SerializableOfferPermitCard();
			break;
		case "acceptoffer":
			addAcceptOfferParameters();
			actionToSend = new SerializableAcceptMarketOffer();
			break;
		// nobility path bonuses
		case "extrabonus":
			addExtraBonusParameters();
			actionToSend = new SerializableChooseExtraBonus();
			break;
		case "extrapermit":
			addExtraPermitParameters();
			actionToSend = new SerializableChooseExtraPermit();
			break;
		case "extrapermitbonus":
			addExtraPermitParameters();
			actionToSend = new SerializableChooseExtraPermitBonus();
			break;
		case "chat":
			addChatToPlayerParameter();
			actionToSend = new SerializableChatMessage();
			break;
		case "chatall":
			addChatToAllParameter();
			actionToSend = new SerializableChatMessage();
			break;
		case "finish":
			actionToSend = new SerializableFinish();
			break;
		default:
			throw new NotWellFormedInputCommandException();
		}
		actionToSend.setJsonConfigAsString(actionData);
	}

	/**
	 * Set the parameter expected in chat message to player command and searches
	 * those parameter inside the input command
	 * 
	 * @throws NotWellFormedInputCommandException
	 *             if some parameter is not founded or some error jumps out
	 */
	private void addChatToPlayerParameter() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { PLAYER, MESSAGE };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Set the parameter expected in chat message to all command and searches
	 * those parameter inside the input command
	 * 
	 * @throws NotWellFormedInputCommandException
	 *             if some parameter is not founded or some error jumps out
	 */
	private void addChatToAllParameter() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { MESSAGE };
		actionData.put(PLAYER, ALL_PLAYERS);
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link ChooseExtraPermitFactory} or {@link ChooseExtraPermitBonusFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addExtraPermitParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { REGION, PERMIT_CARD };
		searchParameterAndBuildActionData(neededParametersAsString);

	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link ChooseExtraBonusFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addExtraBonusParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { CITY };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link AcceptMarketOfferFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addAcceptOfferParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { OFFER };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by {@link PermitCardOffer}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addPermitCardOfferParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { COST, PERMIT_CARD };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by {@link PoliticCardOffer}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addPoliticCardOfferParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { COST, POLITIC_CARD };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by {@link AssistantsOffer}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addAssistantsOfferParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { COST, ASSISTANTS };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link ChangePermitCardsFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addChangeFaceUpPermitsParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { REGION };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link BuildEmporiumWithKingFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addBuildEmporiumWithKingParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { CITY, POLITIC_CARDS };
		searchParameterAndBuildActionData(neededParametersAsString);

	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link BuildEmporiumWithPermitCardFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addBuildEmporiumParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { CITY, PERMIT_CARD };
		searchParameterAndBuildActionData(neededParametersAsString);

	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link TakePermitCardFactory}
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addTakePermitCardParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { REGION, PERMIT_CARD, POLITIC_CARDS };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Add parameters to actionData, that'll be used by
	 * {@link ElectCouncillorFactory} or by
	 * {@link ElectCouncillorWithOneAssistantFactory}, respectively if the
	 * action name is electCouncillor or electCouncillorWithAssistant
	 * 
	 * @throws NotWellFormedInputCommandException
	 * 
	 */
	private void addElectCouncillorParameters() throws NotWellFormedInputCommandException {
		String[] neededParametersAsString = { COLOR, BALCONY };
		searchParameterAndBuildActionData(neededParametersAsString);
	}

	/**
	 * Reset the state of action
	 */
	private void resetAction() {
		allParameterAsList = new ArrayList<>();
		actionData = new JSONObject();
	}

	/**
	 * Adds index of action to actionData
	 * 
	 * @param parameterName
	 * @param parameterIndex
	 * @throws NotWellFormedInputCommandException
	 */
	private void addIndicizedObjectToActionData(String parameterName, int parameterIndex)
			throws NotWellFormedInputCommandException {

		switch (parameterName) {
		case PERMIT_CARD:
			if ("takepermitcard".equalsIgnoreCase(actionName) || "extrapermit".equalsIgnoreCase(actionName))
				addPermitCardFromFaceUps(parameterIndex);
			else
				addPermitCardFromHand(parameterIndex);
			break;
		case POLITIC_CARD:
			addPoliticCardFromHand(parameterIndex);
			break;
		case OFFER:
			addOfferToActionData(parameterIndex);
			break;
		default:
			break;
		}

	}

	/**
	 * Adds offer to action data starting from parameterIdex
	 * 
	 * @param parameterIndex
	 * @throws NotWellFormedInputCommandException
	 */
	private void addOfferToActionData(int parameterIndex) throws NotWellFormedInputCommandException {

		if (parameterIndex < 0 || parameterIndex > gameMap.getMarketChart().numberOfOffers() - 1)
			throw new NotWellFormedInputCommandException();

		OfferChart offer = gameMap.getMarketChart().getOffer(parameterIndex);
		String offeringPlayerUsername = offer.getOfferingPlayerUsername();

		if (offeringPlayerUsername.equalsIgnoreCase(gameMap.getUserResources().getPlayerUsername()))
			throw new NotWellFormedInputCommandException();

		actionData.put("OfferType", offer.getOfferType());
		actionData.put("OfferingPlayer", getOfferingPlayerIDFromUsername(offer.getOfferingPlayerUsername()));
		actionData.put("OfferCost", offer.getOfferCost());

		switch (offer.getOfferType()) {
		case "AssistantsOffer":
			actionData.put(ASSISTANTS, offer.getAssistantsOfferedAmount());
			break;
		case "PoliticCardOffer": // NOSONAR
			try {
				actionData.put(POLITIC_CARD, getJsonobjectOfPoliticCard(offer.getPoliticCardOffered()));
			} catch (ColorNotFoundException e) {
				LOG.log(Level.SEVERE, "Unexpected unknown color", e);
			}
			break;
		case "PermitCardOffer":
			actionData.put(REGION, offer.getPermitRegionName());
			actionData.put(PERMIT_CARD, getJsonarrayOfPermitCard(offer.getPermitOffered()));
			break;
		default:
			break;
		}
	}

	/**
	 * Adds politic card from hand to ActionData
	 * 
	 * @param parameterIndex
	 * @throws NotWellFormedInputCommandException
	 */
	private void addPoliticCardFromHand(int parameterIndex) throws NotWellFormedInputCommandException {

		ClientPoliticCard politicCard;

		if (parameterIndex < 0 || parameterIndex > gameMap.getUserHand().numberOfPoliticCards() - 1)
			throw new NotWellFormedInputCommandException();

		politicCard = gameMap.getUserHand().getPoliticCard(parameterIndex);
		try {
			actionData.put(POLITIC_CARD, getJsonobjectOfPoliticCard(politicCard));
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "Unexpected unknown color", e);
		}
	}

	/**
	 * Add permit card from hand to actionData
	 * 
	 * @param parameterIndex
	 * @throws NotWellFormedInputCommandException
	 */
	private void addPermitCardFromHand(int parameterIndex) throws NotWellFormedInputCommandException {

		ClientPermitCard permitCard;

		if (parameterIndex < 0)
			throw new NotWellFormedInputCommandException();

		if (parameterIndex > gameMap.getUserHand().numberOfPermitCards() - 1) {

			int usedPermitIndex = parameterIndex - gameMap.getUserHand().numberOfPermitCards();

			if (usedPermitIndex >= 0 && usedPermitIndex < gameMap.getUserHand().numberOfUsedPermit())
				permitCard = gameMap.getUserHand().getUsedPermitCard(parameterIndex);
			else
				throw new NotWellFormedInputCommandException();
		} else
			permitCard = gameMap.getUserHand().getPermitCard(parameterIndex);

		actionData.put(REGION, getRegionFromPermit(permitCard));
		actionData.put(PERMIT_CARD, getJsonarrayOfPermitCard(permitCard));

	}

	/**
	 * Returns the region name of the permit card
	 * 
	 * @param permitCard
	 * @return the region name
	 * @throws NotWellFormedInputCommandException
	 */
	private String getRegionFromPermit(ClientPermitCard permitCard) throws NotWellFormedInputCommandException {

		String cityName = permitCard.getCityNames().get(0);

		List<ClientCity> cities = gameMap.getCities();

		for (ClientCity city : cities)
			if (city.getName().equalsIgnoreCase(cityName))
				return city.getRegionName().toLowerCase();
		throw new NotWellFormedInputCommandException();

	}

	/**
	 * Adds permit card from face up permit to actionData
	 * 
	 * @param parameterIndex
	 * @throws NotWellFormedInputCommandException
	 */
	private void addPermitCardFromFaceUps(int parameterIndex) throws NotWellFormedInputCommandException {
		String regionName;
		ClientPermitCard permitCard;
		ClientRegion permitRegion = null;

		List<ClientRegion> regions = gameMap.getRegions();
		Iterator<ClientRegion> regionsIterator = regions.listIterator();

		regionName = actionData.getString(REGION);

		while (regionsIterator.hasNext()) {
			ClientRegion region = regionsIterator.next();
			if (region.getRegionName().equalsIgnoreCase(regionName))
				permitRegion = region;
		}
		if (permitRegion == null)
			throw new NotWellFormedInputCommandException();
		if (parameterIndex < 0 || parameterIndex > VisibleBalcony.MAX_SIZE - 1)
			throw new NotWellFormedInputCommandException();

		permitCard = permitRegion.getFaceUpPermit(parameterIndex);

		actionData.put(PERMIT_CARD, getJsonarrayOfPermitCard(permitCard));
	}

	/**
	 * Returns a jSON Object representing the politic card
	 * 
	 * @param politicCard
	 * @return the jSON Object representing the politic card
	 * @throws ColorNotFoundException
	 */
	private JSONObject getJsonobjectOfPoliticCard(ClientPoliticCard politicCard) throws ColorNotFoundException {

		JSONObject cardAsJson = new JSONObject();
		String cardColorAsString = ColorMap.getString(politicCard.getColor());
		cardAsJson.put(COLOR, cardColorAsString);

		return cardAsJson;
	}

	/**
	 * Returns a jSON Object representing the permit card
	 * 
	 * @param permitCard
	 * @return the jSON Object representing the permit card
	 */
	private JSONArray getJsonarrayOfPermitCard(ClientPermitCard permitCard) {

		BonusChart permitBonus = permitCard.getBonus();
		List<String> cityNames = permitCard.getCityNames();
		JSONArray permitConfig = new JSONArray();

		JSONArray citiesArray = new JSONArray();

		for (String cityName : cityNames) {
			citiesArray.put(cityName);
		}
		permitConfig.put(citiesArray);

		Set<String> bonusKeys = permitBonus.getKeys();
		Iterator<String> keysIterator = bonusKeys.iterator();
		JSONObject permitBonusAsJson = new JSONObject();

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			permitBonusAsJson.put(key, permitBonus.getBonusValue(key));
		}
		permitConfig.put(permitBonusAsJson);

		return permitConfig;
	}

	/**
	 * Build the action (actionData)
	 * 
	 * @param neededParametersAsString
	 * @throws NotWellFormedInputCommandException
	 */
	private void searchParameterAndBuildActionData(String[] neededParametersAsString) // NOSONAR
			throws NotWellFormedInputCommandException {

		List<String> neededParametersAsList = new ArrayList<>();
		Collections.addAll(neededParametersAsList, neededParametersAsString);

		int indexToRemove;

		int parameterIndex;
		String parameterValue;
		String parameterName;

		for (String parameter : allParameterAsList) {
			tagTokenizer = new StringTokenizer(parameter, VALUES_OF_PARAMETER_DELIMITER);
			parameterName = tagTokenizer.nextToken().toLowerCase();

			if (indexBasedParameters.contains(parameterName)) {
				parameterValue = tagTokenizer.nextToken();
				parameterIndex = Integer.parseInt(parameterValue);

				addIndicizedObjectToActionData(parameterName, parameterIndex);
			}

			if (multipleValueParameters.containsKey(parameterName)) {
				addMultipleParameterToActionData(parameterName);
			}

			if (!multipleValueParameters.containsKey(parameterName) && !indexBasedParameters.contains(parameterName)) {
				parameterValue = tagTokenizer.nextToken();

				if (!parameterName.equalsIgnoreCase(PLAYER))
					parameterValue = parameterValue.toLowerCase();

				actionData.put(parameterName, parameterValue);
			}

			indexToRemove = neededParametersAsList.indexOf(parameterName);
			if (indexToRemove < 0)
				throw new NotWellFormedInputCommandException();
			neededParametersAsList.remove(indexToRemove);

		}

		if (!neededParametersAsList.isEmpty())

		{
			throw new NotWellFormedInputCommandException();
		}
	}

	/**
	 * Used to add multiple parameter to actionData
	 * 
	 * @param parameterName
	 */
	private void addMultipleParameterToActionData(String parameterName) {
		JSONObject singleValue;
		JSONArray arrayOfValue;
		String keyValue;
		String message;

		if (actionName.startsWith(CHAT)) {

			message = "";
			while (tagTokenizer.hasMoreTokens()) {

				message += tagTokenizer.nextToken() + " ";
			}
			actionData.put(parameterName, message);

		} else {

			keyValue = multipleValueParameters.get(parameterName);
			arrayOfValue = new JSONArray();
			while (tagTokenizer.hasMoreTokens()) {
				singleValue = new JSONObject();
				singleValue.put(keyValue, tagTokenizer.nextToken());
				arrayOfValue.put(singleValue);
			}
			actionData.put(parameterName, arrayOfValue);

		}
	}

	/**
	 * Returns the playerID from the username 
	 * @param playerUsername
	 * @return player ID
	 */
	private int getOfferingPlayerIDFromUsername(String playerUsername) {

		List<PlayerResourcesChart> opponentsResources = gameMap.getOpponentsResources();
		Iterator<PlayerResourcesChart> opponentsIterator = opponentsResources.listIterator();

		while (opponentsIterator.hasNext()) {
			PlayerResourcesChart opponent = opponentsIterator.next();
			if (opponent.getPlayerUsername().equalsIgnoreCase(playerUsername))
				return opponent.getPlayerID();
		}
		LOG.log(Level.SEVERE, "Opponent Id not found from username");
		return -1;
	}

	/**
	 * Return the client map
	 * @return the client map
	 */
	public ClientMap getGameMap() {
		return gameMap;
	}

	/**
	 * Set the client game map
	 * @param gameMap is the client map
	 */
	public void setGameMap(ClientMap gameMap) {
		this.gameMap = gameMap;
	}

}
