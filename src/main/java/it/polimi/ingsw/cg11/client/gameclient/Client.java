package it.polimi.ingsw.cg11.client.gameclient;

import java.io.IOException;

/**	
 * This is the starting point of the client. This main is called when a player wants to start a game
 * @author Federico
 *
 */
public class Client {

	
	private Client() {}

	/**
	 * Starts the set up procedure
	 * 
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {		

		InitialCLI cli = new InitialCLI();
		cli.setUpInitialGame();
		
	}
}
