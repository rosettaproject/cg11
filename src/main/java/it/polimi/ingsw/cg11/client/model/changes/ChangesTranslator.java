package it.polimi.ingsw.cg11.client.model.changes;

import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.player.actionspool.PlayerActionExecutor;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This object can be used to translate any change in the state of the game recorded in a {@link ModelChanges}
 * object into an actual change in the state of a {@link ClientMap}
 * @author francesco
 *
 */
public class ChangesTranslator extends ActionsTranslator {

	private static final String EMPTY_MESSAGE = "no message";
	private Token userToken;
	private ChangesMessage message;
	private boolean ignoreNextChange;

	/**
	 * 
	 * @param userToken a {@link Token} containing the client user's playerID and username
	 * @param message a string message
	 */
	public ChangesTranslator(Token userToken, ChangesMessage message) {
		super(userToken, message);
		this.userToken = userToken;
		this.message = message;
		this.ignoreNextChange = false;
	}

	/**
	 * 
	 * @return this object's current {@link ChangesMssage}
	 */
	public ChangesMessage getMessage() {
		return message;
	}

	/**
	 * Translates the changes to the state of the game contained in the {@link ModelChanges} parameter into
	 * actual changes in the state of the {@link ClientMap} parameter, and returns a {@link ChagesMessage}
	 * containing a summary of the changes applied in the form of a string. If the information contained in
	 * the ModelChanges passed as a parameter contains improper values and\or an exception is thrown making
	 * the translation impossible, the return value is set to null
	 * @param gameMap the ClientMap where the changes will be applied 
	 * @param changes the ModelChanges object containing informations about the change to apply
	 * @return a ChanesMessage
	 */
	public ChangesMessage translateChange(ClientMap gameMap, ModelChanges changes) {
		message = new ChangesMessage(EMPTY_MESSAGE);

		MainActionsTranslator mainActionTranslator = new MainActionsTranslator(userToken, message);
		QuickActionsTranslator quickActionsTranslator = new QuickActionsTranslator(userToken, message);
		MarketActionsTranslator marketActionsTranslator = new MarketActionsTranslator(userToken, message);
		ChoicesTranslator choicesTranslator = new ChoicesTranslator(userToken, message);

		message = mainActionTranslator.translateMainActionChange(gameMap, changes);
		
		if(message != null && message.getMessage().equals(EMPTY_MESSAGE))
			message = quickActionsTranslator.translateQuickActionChange(gameMap, changes);
		if(message != null && message.getMessage().equals(EMPTY_MESSAGE))
			message = marketActionsTranslator.translateMarketActionChange(gameMap, changes);
		if(message != null && message.getMessage().equals(EMPTY_MESSAGE))
			message = choicesTranslator.translateChoiceChange(gameMap, changes);
		if(message != null && message.getMessage().equals(EMPTY_MESSAGE))
			translateFinish(gameMap,changes);
		return message;
	}

	/**
	 * Actualizes the changes caused by an action of type "Finish". The {@link ModelChanges} object passed as a
	 * parameter must contain a string representing a JSONObject with either a "DrawingID" key associated with the
	 * user's playerID  and a "PoliticCard" key associated with a JSONObject containing the configuration needed
	 * to create an appropriate {@link ClientPoliticCard} to add to the user's hand, or a "BlindDrawID" key 
	 * indicating the playerID of an opponent who must draw a politic card. 
	 * <p>The ModelChanges object can also contain a "resources" key, containing
	 * a mapping of the victory points to assign to each player if the Finish action performed also
	 * marked the end of the game. The mapping uses each player's playerID as key, and associates it with the
	 * corresponding amount of victory points to assign
	 * @param gameMap
	 * @param changes
	 */
	private void translateFinish(ClientMap gameMap, ModelChanges changes) {
		
		JSONObject changesAsJson = new JSONObject(changes.getJsonConfigAsString());
		message = new ChangesMessage("");

		if (changesAsJson.has(PlayerActionExecutor.EMPTY_DECK_MESSAGE)) {
			message.addMessage(changesAsJson.getString(PlayerActionExecutor.EMPTY_DECK_MESSAGE));
		} else {
			if (changesAsJson.has(PlayerActionExecutor.DRAWING_PLAYER_ID_KEY)) {

				int drawingPlayer = changesAsJson.getInt(PlayerActionExecutor.DRAWING_PLAYER_ID_KEY);
				String drawingPlayerUsername = getUsernameFromID(drawingPlayer, gameMap);

				ClientPoliticCard cardToDraw = getPoliticCardFromChanges(PlayerActionExecutor.POLITIC_CARD,changesAsJson);
				gameMap.getUserHand().addPoliticCard(cardToDraw);
				message.addMessage("\n" + drawingPlayerUsername + " has drawn " + cardToDraw.toString());
			}
			if (changesAsJson.has(PlayerActionExecutor.BLIND_DRAW_ID)) {
				if(userToken.getPlayerID() == changesAsJson.getInt(PlayerActionExecutor.BLIND_DRAW_ID)){
					return;
				}
				int drawingPlayer = changesAsJson.getInt(PlayerActionExecutor.BLIND_DRAW_ID);
				OpponentHand opponentHand;
				String drawingPlayerUsername = getUsernameFromID(drawingPlayer, gameMap);
				opponentHand = getOpponentHandFromID(drawingPlayer,gameMap);
				opponentHand.addPoliticCard();
				message.addMessage("\n" + drawingPlayerUsername+" has drawn a card");
			}
			if (changesAsJson.has(ModelChanges.RESOURCES)) {
				message.addMessage("Assigned victory resources");
				assignVictoryResources(gameMap,changesAsJson);
				
			}
		}
	}

	/**
	 * This method uses the mapping contained in the JSONObject associated with the "resources" key present in
	 * the JSONObject passed as a parameter to give the appropriate amount of victory points to each player who's
	 * playerID is present as a key in the mapping
	 * @param gameMap
	 * @param changesAsJson
	 */
	private void assignVictoryResources(ClientMap gameMap, JSONObject changesAsJson) {
		
		JSONObject victoryPointsPerPlayer = changesAsJson.getJSONObject(ModelChanges.RESOURCES);
		
		PlayerResourcesChart userResources = gameMap.getUserResources();
		
		if(victoryPointsPerPlayer.has(String.valueOf(userResources.getPlayerID()))){
			int victoryPoints = victoryPointsPerPlayer.getInt(String.valueOf(userResources.getPlayerID()));
			userResources.getPlayerVictoryPoints().gain(victoryPoints);
				
			message.addMessage(userResources.getPlayerUsername()+" gained "+victoryPoints+" victory points");
		}

		List<PlayerResourcesChart> opponents = gameMap.getOpponentsResources();
		Iterator<PlayerResourcesChart> opponentsIterator = opponents.listIterator();
		
		while(opponentsIterator.hasNext()){
			
			PlayerResourcesChart opponentResources = opponentsIterator.next();
			
			if(victoryPointsPerPlayer.has(String.valueOf(opponentResources.getPlayerID()))){
				
				int victoryPoints = victoryPointsPerPlayer.getInt(String.valueOf(opponentResources.getPlayerID()));
				opponentResources.getPlayerVictoryPoints().gain(victoryPoints);
				message.addMessage(opponentResources.getPlayerUsername()+" gained "+victoryPoints+" victory points");
			}
		}
		
		String winner = getUsernameFromID(changesAsJson.getInt("WinnerID"),gameMap);
		message.addMessage(winner+" has won the game!");
	}

	/**
	 * 
	 * @return the current value of the ignoreNextChange boolean private field of this object, indicating weather
	 * the next {@link ModelChanges} should be properly translated or ignored
	 */
	public boolean isIgnoreNextChange() {
		return ignoreNextChange;
	}

	/**
	 * Allows to set the value of the ignoreNextChange boolean private field of this object, indicating weather
	 * the next {@link ModelChanges} should be properly translated or ignored
	 * @param value
	 */
	public void setIgnoreNextChange(boolean value){
		ignoreNextChange = value;
	}

}
