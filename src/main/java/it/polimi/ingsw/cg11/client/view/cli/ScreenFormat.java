package it.polimi.ingsw.cg11.client.view.cli;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used to display string on screen
 * 
 * @author Federico
 *
 */
public class ScreenFormat {

	private static final Logger LOG = Logger.getLogger(ScreenFormat.class.getName());
	private static PrintWriter printWriter = new PrintWriter(System.out, true); //NOSONAR
	
	private ScreenFormat() {}

	/**
	 * Display on standard output the message
	 * @param message
	 */
	public static void displayOnScreen(String message) {
		printWriter.print(message);
		printWriter.flush();
	}

	/**
	 * Clears the screen
	 */
	public static void clearScreen() {
		final String os = System.getProperty("os.name");
		
		try {
			if (os.contains("Windows")) {
				Runtime.getRuntime().exec("cls");
			} else {
				Runtime.getRuntime().exec("clear");
			}
		} catch (IOException e) {
			LOG.log(Level.FINE, "\n\nCannot clear the screen\n\n", e);
		}
	}

}
