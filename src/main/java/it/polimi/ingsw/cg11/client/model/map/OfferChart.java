package it.polimi.ingsw.cg11.client.model.map;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;

/**
 * This object represents an offer in the market
 * @author francesco
 *
 */
public class OfferChart {

	private final String offeringPlayerUsername;
	private final String offerType;
	private final int assistantsOfferedAmount;
	private final ClientPermitCard permitOffered;
	private final ClientPoliticCard politicCardOffered;
	private final int offerCost;
	private String permitRegionName;

	/**
	 * This constructor creates an assistants' offer
	 * @param offeringPlayerUsername
	 * @param offerType
	 * @param assistantsOfferedAmount
	 * @param offerCost
	 */
	public OfferChart(String offeringPlayerUsername, String offerType, int assistantsOfferedAmount, int offerCost) {
		this.offeringPlayerUsername = offeringPlayerUsername;
		this.offerType = offerType;
		this.assistantsOfferedAmount = assistantsOfferedAmount;
		this.permitOffered = null;
		this.politicCardOffered = null;
		this.offerCost = offerCost;
		this.permitRegionName = null;
	}

	/**
	 * This constructor creates a politic card offer
	 * @param offeringPlayerUsername
	 * @param offerType
	 * @param assistantsOfferedAmount
	 * @param offerCost
	 */
	public OfferChart(String offeringPlayerUsername, String offerType, ClientPoliticCard politicCardOffered,
			int offerCost) {
		this.offeringPlayerUsername = offeringPlayerUsername;
		this.offerType = offerType;
		this.assistantsOfferedAmount = 0;
		this.permitOffered = null;
		this.politicCardOffered = politicCardOffered;
		this.offerCost = offerCost;
		this.permitRegionName = null;
	}

	/**
	 * This constructor creates a permit card offer
	 * @param offeringPlayerUsername
	 * @param offerType
	 * @param assistantsOfferedAmount
	 * @param offerCost
	 */
	public OfferChart(String offeringPlayerUsername, String offerType, ClientPermitCard permitCardOffered,
			String permitRegionName, int offerCost) {
		this.offeringPlayerUsername = offeringPlayerUsername;
		this.offerType = offerType;
		this.assistantsOfferedAmount = 0;
		this.permitOffered = permitCardOffered;
		this.politicCardOffered = null;
		this.offerCost = offerCost;
		this.permitRegionName = permitRegionName;
	}

	/**
	 * This offer's offering player username
	 * @return
	 */
	public String getOfferingPlayerUsername() {
		return offeringPlayerUsername;
	}

	/**
	 * Returns a string representing the type of this object
	 * @return
	 */
	public String getOfferType() {
		return offerType;
	}

	/**
	 * 
	 * @return the amount of assistants offered. If this offer does not offer any assistants,returns 0
	 */
	public int getAssistantsOfferedAmount() {
		return assistantsOfferedAmount;
	}

	/**
	 * 
	 * @return the {@link ClientPermitCard} offered. If this offer does not offer any assistants,returns null
	 */
	public ClientPermitCard getPermitOffered() {
		return permitOffered;
	}

	/**
	 * 
	 * @return the {@link ClientPoliticCard} offered. If this offer does not offer any assistants,returns null
	 */
	public ClientPoliticCard getPoliticCardOffered() {
		return politicCardOffered;
	}

	/**
	 * 
	 * @return the money cost of this offer
	 */
	public int getOfferCost() {
		return offerCost;
	}

	/**
	 * 
	 * @return a string representing the name of the {@link ClientRegion} associated with this offer's offered
	 * {@link ClientPermitCard} if any
	 */
	public String getPermitRegionName() {
		return permitRegionName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assistantsOfferedAmount;
		result = prime * result + offerCost;
		result = prime * result + ((offerType == null) ? 0 : offerType.hashCode());
		result = prime * result + ((offeringPlayerUsername == null) ? 0 : offeringPlayerUsername.hashCode());
		result = prime * result + ((permitOffered == null) ? 0 : permitOffered.hashCode());
		result = prime * result + ((permitRegionName == null) ? 0 : permitRegionName.hashCode());
		result = prime * result + ((politicCardOffered == null) ? 0 : politicCardOffered.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfferChart other = (OfferChart) obj;
		if (assistantsOfferedAmount != other.assistantsOfferedAmount)
			return false;
		if (offerCost != other.offerCost)
			return false;
		if (offerType == null) {
			if (other.offerType != null)
				return false;
		} else if (!offerType.equals(other.offerType))
			return false;
		if (offeringPlayerUsername == null) {
			if (other.offeringPlayerUsername != null)
				return false;
		} else if (!offeringPlayerUsername.equals(other.offeringPlayerUsername))
			return false;
		if (permitOffered == null) {
			if (other.permitOffered != null)
				return false;
		} else if (!permitOffered.equals(other.permitOffered))
			return false;
		if (permitRegionName == null) {
			if (other.permitRegionName != null)
				return false;
		} else if (!permitRegionName.equals(other.permitRegionName))
			return false;
		if (politicCardOffered == null) {
			if (other.politicCardOffered != null)
				return false;
		} else if (!politicCardOffered.equals(other.politicCardOffered))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String toString = "";

		toString += "Offerer: " + offeringPlayerUsername;
		toString += "\nType: " + offerType;
		toString += "\nCost: " + offerCost + "\n";

		if (assistantsOfferedAmount > 0)
			toString += "\nAssistants offered: " + assistantsOfferedAmount;
		if (politicCardOffered != null)
			toString += "\nPolitic card: " + politicCardOffered.toString();
		if (permitOffered != null) {
			toString += "\nPermit card";
			toString += "\nRegion: " + permitRegionName + "\n";
			toString += permitOffered.toString();
		}

		toString += "\n";

		return toString;
	}

}
