package it.polimi.ingsw.cg11.client.model.map;

import java.util.List;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * This object represents a region in the {@link ClientMap}
 * @author francesco
 *
 */
public class ClientRegion {

	private final String regionName;
	private final BonusChart regionBonus;
	private final List<ClientPermitCard> faceUpPermits;
	private final VisibleBalcony regionBalcony;
	
	/**
	 * @param regionName
	 * @param regionBonus 
	 * @param faceUpPermits
	 * @param regionBalcony
	 */
	public ClientRegion(String regionName,BonusChart regionBonus, List<ClientPermitCard> faceUpPermits, VisibleBalcony regionBalcony) {
		this.regionBonus = regionBonus;
		this.faceUpPermits = faceUpPermits;
		this.regionBalcony = regionBalcony;
		this.regionName = regionName;
	}

	/**
	 * 
	 * @return this region's {@link BonusChart}
	 */
	public BonusChart getRegionBonus() {
		return regionBonus;
	}

	/**
	 * 	
	 * @param index
	 * @return the {@link ClientPermitCard} at the given index of this region's face up permits
	 */
	public ClientPermitCard getFaceUpPermit(int index) {
		return faceUpPermits.get(index);
	}
	
	/**
	 *Removes the {@link ClientPermitCard} passed as a parameter from this region's face up permits if present
	 *@see {@link List#remove(Object)}
	 * @param permitCard
	 * @return true if this region's face up permits list contained the specified element
	 */
	public boolean removeFaceUpPermit(ClientPermitCard permitCard){
		return faceUpPermits.remove(permitCard);
	}
	
	/**
	 * Returns true if the permit card passed as a parameter is contained in this region's face up permits
	 * @see {@link List#contains(Object)}
	 * @param permitCard
	 * @return true if the permit card passed as a parameter is contained in this region's face up permits, false
	 * otherwise
	 */
	public boolean contains(ClientPermitCard permitCard){
		return faceUpPermits.contains(permitCard);
	}
	
	/**
	 * Adds the {@link ClientPermitCard} passed as a parameter to this region's face up permits list
	 * @param newPermit
	 */
	public void addFaceUpPermit(ClientPermitCard newPermit){
		faceUpPermits.add(newPermit);
	}
	
	/**
	 * Replaces the current face up permits list of this object with the list of {@link ClientPermitCard}s passed as
	 * a parameter
	 * @param newPermits
	 */
	public void changeFaceUpPermits(List<ClientPermitCard> newPermits){
		
		int index;
		int size = faceUpPermits.size();
		for(index = 0; index < size;index++)
			faceUpPermits.remove(0);
		
		faceUpPermits.addAll(newPermits);
	}

	/**
	 * 
	 * @return this region's balcony
	 */
	public VisibleBalcony getRegionBalcony() {
		return regionBalcony;
	}

	/**
	 * 
	 * @return a string representing this region's name
	 */
	public String getRegionName() {
		return regionName;
	}


	@Override
	public String toString() {
		String toString = "";
		
		toString += "<<Region>> " + regionName;
		toString += "\n\n<<Bonus>>\n" + regionBonus.toString();
		toString += "\n\n<<Face up permits>>\n";
		toString += AsString.cardsAsString(faceUpPermits);
		toString += "<<Balcony>>\n" + regionBalcony.toString();
		
		return toString;
		
	}
	
}
