package it.polimi.ingsw.cg11.client.model.map;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.player.ClientEmporium;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.AsString;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;

/**
 * Immutable object representing a city in the client
 * 
 * @author francesco
 *
 */
public class ClientCity {

	private static final Logger LOG = Logger.getLogger(ClientCity.class.getName());

	private final String name;
	private final String region;
	private final BonusChart bonus;
	private final Color color;
	private final List<ClientEmporium> emporiumsBuilt;
	private final List<String> adjacentCitiesNames;

	/**
	 * 
	 * @param name a string representing the name of the city
	 * @param region a string representing the name of the {@link Region} of the city
	 * @param bonus the {@link BonusChart} associated with the city
	 * @param color the {@link Color} of he city
	 */
	public ClientCity(String name, String region, BonusChart bonus, Color color) {
		this.name = name;
		this.region = region;
		this.bonus = bonus;
		this.color = color;
		this.emporiumsBuilt = new ArrayList<>();
		this.adjacentCitiesNames = new ArrayList<>();
	}

	/**
	 * 
	 * @return a string representing the name of the city
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return a string representing the name of the {@link Region} of the city
	 */
	public String getRegionName() {
		return region;
	}

	/**
	 * 
	 * @return the {@link BonusChart} associated with the city
	 */
	public BonusChart getBonus() {
		return bonus;
	}

	/**
	 * 
	 * @return the {@link Color} of he city
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * 
	 * @return a string representation of the {@link Color} of he city obtained through the {@link ColorMap}
	 */
	public String getColorAsString() {
		try {
			return ColorMap.getString(color);
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "unknown color", e);
		}
		return null;
	}

	/**
	 * Adds the {@link ClientEmporium} passed as a parameter to the list of emporiums built on the city
	 * @param emporium the ClientEmporium to add to the list of emporiums built on the city
	 */
	public void buildEmporium(ClientEmporium emporium) {
		emporiumsBuilt.add(emporium);
	}

	public boolean hasEmporiumBuilt(PlayerResourcesChart player) {
		return emporiumsBuilt.contains(new ClientEmporium(player.getPlayerColor(), player.getPlayerUsername()));
	}

	public List<String> getAdjacentCityNames() {
		
		List<String> adjacentsCopy = new ArrayList<>();
		
		adjacentsCopy.addAll(adjacentCitiesNames);
		
		return adjacentsCopy;
	}
	
	public void addAdjacentCities(List<String> adjacentCities){
		
		if(adjacentCitiesNames.isEmpty())
			adjacentCitiesNames.addAll(adjacentCities);
	}

	/**
	 * Returns a string containing all the relevant informations on the state of this object in the following
	 * format: "[cityName] ( [cityColor] ) \n [cityBonus] \n [{@link AsString#asString(List, boolean)}]"
	 */
	@Override
	public String toString() {
		String toString = "";

		toString += name + " (" + getColorAsString() + ")";
		toString += "\n" + bonus;
		toString += "Emporiums:\n" + AsString.asStringWithDashes(emporiumsBuilt);
		toString += "Adjacent cities: \n" + AsString.asStringWithDashes(adjacentCitiesNames);

		return toString;
	}

}
