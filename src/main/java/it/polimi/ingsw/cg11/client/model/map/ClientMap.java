package it.polimi.ingsw.cg11.client.model.map;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.bonus.ColorBonusChart;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;

/**
 * This object contains all the information relative to the state of a game needed in the client.
 * <p>
 * The hands of the players are stored in the form of a {@link UserHand} and a list of {@link OpponenHand}s,
 * while the resources of each player are stored in a {@link PlayerResourcesChart}.
 * <p>
 * The components of the map are a list of {@link ClientRegion} and a list of {@link ClientCity}, a list of
 * {@link BonusChart}s that represents the nobility path, another which represents the king bonuses,
 * a list of {@link ColoredBonusChart}s representing the bonus obtained by building an emporium on each city 
 * of a given {@link Color}, a {@link ClientKing} representing the king piece, a {@link VisibleBalcony}
 * representing the king's balcony and a {@link MarketChart} containing all the market offers.
 * <p>
 * A {@link CouncillorPoolManager} is used to keep track of which {@link Councillor}s are available at any
 * given time
 * @author francesco
 *
 */
public class ClientMap {

	private final List<OpponentHand> opponentsHands = new ArrayList<>();
	private final List<PlayerResourcesChart> opponentsResources = new ArrayList<>();
	private final List<ClientRegion> regions = new ArrayList<>();
	private final List<ClientCity> cities;
	private final List<BonusChart> nobilityPath;
	private final List<BonusChart> kingBonuses;
	private List<ColorBonusChart> colorBonus;
	private final UserHand userHand;
	private final PlayerResourcesChart userResources;
	private final ClientKing king;
	private final VisibleBalcony kingBalcony;
	private final MarketChart marketChart;
	private CouncillorPoolManager poolManager;

	/**
	 * 
	 * @param userHand a {@link UserHand}
	 * @param userResources a {@link PlayerResourcesChart} representing the resources of the user
	 * @param cities the {@link ClientCity}s of the map
	 * @param councillors the {@link Councillor}s to initialize the {@link CluncillorPoolManager}
	 * @param startingCity the king's starting city
	 * @param nobilityPath a list of {@link BonusChart}s representing the nobility path
	 * @param kingBonuses a list of {@link BonusChart}s representing the king's bonuses
	 */
	protected ClientMap(UserHand userHand, PlayerResourcesChart userResources, List<ClientCity> cities,
			List<Councillor> councillors, ClientCity startingCity, List<BonusChart> nobilityPath,
			List<BonusChart> kingBonuses) {

		this.userHand = userHand;
		this.userResources = userResources;
		this.cities = cities;
		this.king = new ClientKing(startingCity);
		this.kingBalcony = new VisibleBalcony(councillors);
		this.nobilityPath = nobilityPath;
		this.kingBonuses = kingBonuses;
		this.marketChart = new MarketChart();
	}

	/**
	 * 
	 * @return the reference to the list containing the {@link OpponentHand}s if this map
	 */
	public List<OpponentHand> getOpponentsHands() {
		return opponentsHands;
	}

	/**
	 * 
	 * @return the reference to the {@link UserHand} of this map
	 */
	public UserHand getUserHand() {
		return userHand;
	}

	/**
	 * 
	 * @return the reference to the list of {@link ClientRegion}s of this map
	 */
	public List<ClientRegion> getRegions() {
		return regions;
	}

	/**
	 * 
	 * @return the reference to the list of the opponents' {@link PlayerResourcesChart}s of this map 
	 */
	public List<PlayerResourcesChart> getOpponentsResources() {
		return opponentsResources;
	}

	/**
	 * 
	 * @return the reference to the user's {@link PlayerResourcesChart} of this map
	 */
	public PlayerResourcesChart getUserResources() {
		return userResources;
	}

	/**
	 * 
	 * @return the reference to the {@link ClientKing} of this map
	 */
	public ClientKing getKing() {
		return king;
	}

	/**
	 * 
	 * @return the reference to a list of {@link BonusChart}s representing this map's nobility path
 	 */
	public List<BonusChart> getNobilityPath() {
		return nobilityPath;
	}

	/**
	 * 
	 * @return the reference to a list of {@link BonusChart}s representing this map's king bonus
	 */
	public List<BonusChart> getKingBonuses() {
		return kingBonuses;
	}

	/**
	 * 
	 * @return a reference to this map's king balcony
	 */
	public VisibleBalcony getKingBalcony() {
		return kingBalcony;
	}

	/**
	 * 
	 * @return the reference to a list of {@link BonusChart}s representing this map's 'color bonus'
	 * @see ClientMap
	 */
	public List<ColorBonusChart> getColorBonus() {
		return colorBonus;
	}

	/**
	 * 
	 * @return the reference to a list of  {@link ClientCity} containing the cities of this map
	 */
	public List<ClientCity> getCities() {
		return cities;
	}

	/**
	 * 
	 * @return a {@link MarketChart} containing the {@link OfferChart}s of this map
	 */
	public MarketChart getMarketChart() {
		return marketChart;
	}

	/**
	 * 
	 * @return the reference to the {@link CouncillorPoolManager} of this map
	 */ 
	public CouncillorPoolManager getCouncillorPoolManager() {
		return poolManager;
	}

	/**
	 * Allows to set this object's {@link CouncillorPoolManager}. Should only be used before the start of
	 * the game for configuration purposes
	 * @param poolManager the object to set as this map's CouncillorPoolManager
	 */
	public void setPoolManager(CouncillorPoolManager poolManager) {
		this.poolManager = poolManager;
	}

	/**
	 * Allows to add a {@link ClientRegion} to this map. Used during configuration
	 * @param region the ClientRegion to add
	 */
	public void addRegion(ClientRegion region) {
		regions.add(region);
	}

	/**
	 * Allows to add an {@link OpponentHand} to the corresponding list of this map. Used during configuration
	 * @param hand the OpponentHand to add
	 */
	public void addOpponentHand(OpponentHand hand) {
		opponentsHands.add(hand);
	}

	/**
	 * Allows to add a {@link PlayerResourcesChart} to the corresponding list of this map. Used during configuration
	 * @param hand the PlayerResourcesChart to add
	 */
	public void addOpponentsResources(PlayerResourcesChart resources) {
		opponentsResources.add(resources);
	}

	/**
	 * Allows to set a list of {@link ColorBonusChart} for this map.Used during configuration
	 * @param colorBonus the ColorBonusChart list to set
	 */
	public void addColorBonus(List<ColorBonusChart> colorBonus) {
		this.colorBonus = colorBonus;
	}

}
