package it.polimi.ingsw.cg11.client.view.cli;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.bonus.ColorBonusChart;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientKing;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientRegion;
import it.polimi.ingsw.cg11.client.model.map.MarketChart;
import it.polimi.ingsw.cg11.client.model.map.VisibleBalcony;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * This class is used for print the client model informations
 * 
 * @author Federico
 *
 */
public class ModelShows {

	private static final String DASHES = "------------------------------";

	private ClientMap gameMap;

	private List<ClientCity> cities;
	private List<ClientRegion> regions;
	private List<BonusChart> nobilityPath;
	private List<BonusChart> kingBonuses;
	private List<ColorBonusChart> colorBonus;
	private ClientKing king;
	private VisibleBalcony kingBalcony;
	private CouncillorPoolManager councillorsPool;
	private MarketChart marketChart;

	/**
	 * Public default constructor. Initializes the rep of the class
	 * 
	 * @param gameMap
	 */
	public ModelShows(ClientMap gameMap) {
		this.gameMap = gameMap;

		cities = gameMap.getCities();
		regions = gameMap.getRegions();
		nobilityPath = gameMap.getNobilityPath();
		kingBonuses = gameMap.getKingBonuses();
		colorBonus = gameMap.getColorBonus();
		king = gameMap.getKing();
		kingBalcony = gameMap.getKingBalcony();
		councillorsPool = gameMap.getCouncillorPoolManager();
		marketChart = gameMap.getMarketChart();
	}

	/**
	 * Returns a string that contains the available councillor, ready to be
	 * printed
	 * 
	 * @return a list of available councillors pool as string
	 */
	public String getCouncillorPoolAsString() {
		String toString = "";

		toString += "\n" + DASHES + "\n";
		toString += councillorsPool.toString();
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains the market offers, ready to be printed
	 * 
	 * @return a list of market offers as string
	 */
	public String getMarketOfferAsString() {
		String toString = "";

		toString += marketChart.toString();

		return toString;
	}

	/**
	 * Returns a string that contains the whole map, ready to be printed
	 * 
	 * @return a list of regions, cities, balconies, king information and color
	 *         bonus as string
	 */
	public String getMapAsString() {
		String toString = "";

		toString += citiesAsString();
		toString += kingInformationAsString();
		toString += nobilityPathInformationAsString();
		toString += colorBonusAsString();

		return toString;
	}

	/**
	 * Returns a string that contains the color bonuses, ready to be printed
	 * @return a list of color bonuses as string
	 */
	public String colorBonusAsString() {
		String toString = "";

		toString += "\n" + DASHES + "\n";
		toString += "\n\n" + AsString.asString(colorBonus, false);
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains the nobility path, ready to be printed
	 * @return a list of positions -with relative bonuses- as string
	 */
	public String nobilityPathInformationAsString() {
		String toString = "";

		toString += "\n" + DASHES + "\n";
		toString += "\n<<Nobility Path>>\n\n";
		toString += AsString.pathAsString(nobilityPath);
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains the king information, ready to be printed
	 * @return current city of king, king balcony and king bonuses as string
	 */
	public String kingInformationAsString() {
		String toString = "";

		toString += "\n" + DASHES + "\n";
		toString += "\n<<King>>";
		toString += king.toString();

		toString += "\n\nBalcony\n";
		toString += kingBalcony.toString();

		toString += "\nKing Bonuses\n";
		toString += AsString.asString(kingBonuses, false);
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains all regions, ready to be printed
	 * @return a list of regions as string
	 */
	public String regionsAsString() {
		String toString = "";

		Iterator<ClientRegion> regionIterator = regions.iterator();
		ClientRegion tmpRegion;

		while (regionIterator.hasNext()) {
			tmpRegion = regionIterator.next();

			toString += "\n" + DASHES + "\n";
			toString += tmpRegion.toString();
		}

		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains the all cities, ready to be printed
	 * @return a list of cities, with their bonus, as string
	 */
	public String citiesAsString() {
		String toString = "";
		String citiesOfRegionAsString;

		Iterator<ClientRegion> regionIterator = regions.iterator();
		ClientRegion tmpRegion;

		while (regionIterator.hasNext()) {
			tmpRegion = regionIterator.next();
			citiesOfRegionAsString = getCitiesAsStringFromRegionName(tmpRegion.getRegionName());

			toString += DASHES + "\n";
			toString += "Cities of " + tmpRegion.getRegionName() + " region\n\n";
			toString += citiesOfRegionAsString;
		}
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains all cities of regionName, ready to be printed
	 * @param regionName
	 * @return a list of cities of regionName
	 */
	private String getCitiesAsStringFromRegionName(String regionName) {
		String toString = "";

		Iterator<ClientCity> cityIterator = cities.iterator();
		ClientCity tmpCity;

		while (cityIterator.hasNext()) {
			tmpCity = cityIterator.next();

			if (tmpCity.getRegionName().equals(regionName))
				toString += tmpCity.toString() + "\n";
		}

		return toString;
	}

	/**
	 * Returns a string that contains the informations of all players, ready to be printed
	 * @return a list of players as string
	 */
	public String getAllPlayersAsString() {
		String toString = "";

		toString += getCurrentPlayerAsString();
		toString += getOpponentsPlayerResourcesAsString();

		return toString;
	}

	/**
	 * Returns a string that contains the informations of the current player, ready to be printed
	 * @return the information of current player as string
	 */
	public String getCurrentPlayerAsString() {
		String toString = "";
		UserHand userHand;
		PlayerResourcesChart userResources;

		userHand = gameMap.getUserHand();
		userResources = gameMap.getUserResources();

		toString += "\n" + DASHES + "\n";
		toString += userResources.toString();
		toString += "\n\n";
		toString += userHand.toString();
		toString += "\n" + DASHES + "\n";

		return toString;
	}

	/**
	 * Returns a string that contains the informations of opponent players, ready to be printed
	 * @return a list of opponent player as string
	 */
	private String getOpponentsPlayerResourcesAsString() {
		String toString = "";
		int playerIndex;

		List<PlayerResourcesChart> opponentsResources;
		List<OpponentHand> opponentsHands;

		ListIterator<PlayerResourcesChart> resourceIterator;

		PlayerResourcesChart tmpResource;
		OpponentHand tmpHand;

		opponentsResources = gameMap.getOpponentsResources();
		opponentsHands = gameMap.getOpponentsHands();

		resourceIterator = opponentsResources.listIterator();

		while (resourceIterator.hasNext()) {
			playerIndex = resourceIterator.nextIndex();
			tmpResource = resourceIterator.next();
			tmpHand = opponentsHands.get(playerIndex);

			toString += "\n" + DASHES + "\n";
			toString += tmpResource.toString();
			toString += "\n\n";
			toString += tmpHand.toString();
			toString += "\n" + DASHES + "\n";
		}
		return toString;
	}
}
