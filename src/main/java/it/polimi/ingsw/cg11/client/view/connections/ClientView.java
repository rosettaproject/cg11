/**
 * 
 */
package it.polimi.ingsw.cg11.client.view.connections;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg11.server.model.utils.observerpattern.VisitableObservable;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This interface defines a view, used to communicate with the server
 * 
 * @author Federico
 *
 */
public interface ClientView extends VisitableObservable, Remote {

	/**
	 * Sets up the connection
	 * @throws RemoteException
	 */
	public void connect() throws RemoteException;

	/**
	 * Disconnects the client
	 * @throws RemoteException
	 */
	public void disconnect() throws RemoteException;
	
	/**
	 * Sends a {@link Visitable} message to the server
	 * @param message
	 * @throws RemoteException
	 */
	public void sendToServer(Visitable message) throws RemoteException;

	/**
	 * Reads a {@link Visitable} message sent from server
	 * @param message
	 * @throws RemoteException
	 */
	public void readMessageFromServer(Visitable message) throws RemoteException;

	/**
	 * Return the token of the player of this view
	 * @return player {@link Token}
	 * @throws RemoteException
	 */
	public Token getToken() throws RemoteException;
	
	/**
	 * Sets the ID of the player
	 * 
	 * @param playerID
	 * @throws RemoteException
	 */
	public void setPlayerID(int playerID) throws RemoteException;
	
	/**
	 * Sets the updated username to the player (useful in case of namesakes)
	 * @param username
	 * @throws RemoteException
	 */
	public void setUpdatedUsername(String username) throws RemoteException; 

}
