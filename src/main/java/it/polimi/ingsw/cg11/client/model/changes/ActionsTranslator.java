package it.polimi.ingsw.cg11.client.model.changes;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.client.model.map.ClientCity;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientRegion;
import it.polimi.ingsw.cg11.client.model.map.VisibleBalcony;
import it.polimi.ingsw.cg11.client.model.player.OpponentHand;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.client.model.player.UserHand;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This abstract class stores different methods used to actualize the changes
 * caused by an action performed by any of the players, so that the user's
 * {@link ClientMap } reflects them properly
 * 
 * @author francesco
 *
 */
public abstract class ActionsTranslator {

	public static final String OBTAINED_CARDS = "ObtainedCards";
	public static final String RESOURCES = "resources";
	public static final String COLOR = "color";
	private static final Logger LOG = Logger.getLogger(ActionsTranslator.class.getName());
	private static final String ACTOR_ID = "ActorID";
	private Token userToken;
	private ChangesMessage message;

	/**
	 * 
	 * @param userToken
	 *            the token used by the client user to identify itself with the
	 *            game server
	 * @param message
	 *            used only to initialize the message final field
	 */
	public ActionsTranslator(Token userToken, ChangesMessage message) {
		this.userToken = userToken;
		this.message = message;
	}

	/**
	 * Method used to assign the resources specified in the JSONObject passed as
	 * a parameter to the player indicated with an integer representing it's
	 * playerID
	 * 
	 * @param actorID
	 *            an integer representing the playerID of the player who is
	 *            performing the action
	 * @param ClientMap
	 *            where the changes are being performed
	 * @param JSONObject
	 *            representing the changes caused in the state of the game by an
	 *            action. This JSONObject must contain the following keys:
	 *            <ul>
	 *            <li>The "resources" key, associated with a JSONObject
	 *            containing any combination of the following keys:
	 *            <ul>
	 *            <li>"Assistants"
	 *            <li>"Money"
	 *            <li>"Nobility"
	 *            <li>"VictoryPoints"
	 *            <li>"PoliticCards" 
	 *            <li> an integer representing the playerID of a player<br><br>
	 *        each associated with an integer representing the amount of the
	 *        corresponding resource (in the case of the playerID, the resource
	 *        is victory points)
	 */
	protected void assignResources(int actorID, ClientMap gameMap, JSONObject changesAsJson)
			throws IllegalResourceException {

		PlayerResourcesChart resourcesChart;
		OpponentHand opponentHand;
		UserHand userHand;

		if (changesAsJson.has(RESOURCES)) {
			JSONObject resources = changesAsJson.getJSONObject(RESOURCES);

			if (actorID == userToken.getPlayerID()) {
				resourcesChart = gameMap.getUserResources();
				userHand = gameMap.getUserHand();
				opponentHand = null;
			} else {
				resourcesChart = getOpponentResourcesFromID(actorID, gameMap);
				opponentHand = getOpponentHandFromID(actorID, gameMap);
				userHand = null;
			}

			Iterator<String> keysIterator = resources.keys();

			while (keysIterator.hasNext()) {
				String key = keysIterator.next();
				int amount = resources.getInt(key);

				switch (key) {
				case "Assistants":
					resourcesChart.getPlayerAssistants().gain(amount);
					break;
				case "Money":
					resourcesChart.getPlayerMoney().gain(amount);
					break;
				case "Nobility":
					resourcesChart.getPlayerNobilityPoints().gain(amount);
					break;
				case "VictoryPoints":
					resourcesChart.getPlayerVictoryPoints().gain(amount);
					break;
				case "PoliticCards":
					assignPoliticCard(changesAsJson, opponentHand, userHand, amount);
					break;
				default:
					break;
				}
				message.addMessage("gained " + amount + " " + key);

			}
		} else {
			LOG.log(Level.FINE, "No resources to assign");
		}
	}

	/**
	 * Returns the playerID of the player who performed the action that caused
	 * the changes stored in the JSONObject contained as a string in the
	 * {@link ModelChanges} passed as a parameter. Such object must contain an
	 * "ActorID" key associated with the integer representing the playerID
	 * 
	 * @param modelChanges
	 * @return the actor's ID
	 */
	public int getActorIDFromChanges(ModelChanges modelChanges) {
		int actorID = -1;
		JSONObject changesAsJson;

		changesAsJson = new JSONObject(modelChanges.getJsonConfigAsString());
		if (changesAsJson.has(ACTOR_ID))
			actorID = changesAsJson.getInt(ACTOR_ID);

		return actorID;
	}

	/**
	 * Method used by a translator to give a player a politic card. For this
	 * method to function properly, either:
	 * <li>in the JSONObject passed as a parameter, with the key "ObtainedCards"
	 * must be associated a JSONObject representing a politic card, with the
	 * following structure: {"color":"[a {@link Color} as string]"} and the
	 * userHand parameter must be not null
	 * <li>at least one of the previous conditions is not met, and the
	 * opponentHand parameter is not null
	 * 
	 * @param changesAsJson
	 * @param opponentHand
	 * @param userHand
	 */
	protected void assignPoliticCard(JSONObject changesAsJson, OpponentHand opponentHand, UserHand userHand, 
			int politicCardsNumber) {
		if (userHand != null && changesAsJson.has(OBTAINED_CARDS)) {
			List<ClientPoliticCard> obtainedCards = createPoliticCardsFromConfig(changesAsJson, OBTAINED_CARDS);
			for (ClientPoliticCard card : obtainedCards)
				userHand.addPoliticCard(card);
		} else {
			if (opponentHand != null){
				opponentHand.addPoliticCards(politicCardsNumber);	
			}
		}
	}

	/**
	 * Returns a {@link VisibleBalcony} present in the {@link ClientMap} passed
	 * as a parameter. If the balconyName string passed as a parameter is equal
	 * to the name of a region present on the map, that region's balcony is
	 * returned. Otherwise the king balcony is returned
	 * 
	 * @param balconyName
	 *            the name of the balcony requested
	 * @param gameMap
	 *            map where the requested balcony is supposed to be
	 * @return a {@link VisibleBalcony}
	 */
	protected VisibleBalcony getBalconyFromName(String balconyName, ClientMap gameMap) {

		List<ClientRegion> regions = gameMap.getRegions();

		for (ClientRegion region : regions)
			if (region.getRegionName().equalsIgnoreCase(balconyName))
				return region.getRegionBalcony();
		return gameMap.getKingBalcony();
	}

	/**
	 * Returns a list of {@link ClientPoliticCard}s representing the cards that
	 * a player must discard from his\her hand. In the JSONObject passed as a
	 * parameter must be contained a "DiscardedCards" key associated with a
	 * JSONArray containing JSONObjects representing each politic card that must
	 * be discarded. If the string associated with the "color" key is not
	 * contained in the {@link ColorMap}, the color field of the card is set to
	 * null
	 * 
	 * @param changesAsJson
	 *            a JSONObject representing the changes caused by a player's
	 *            action
	 * @return a list of politic cards
	 */
	protected List<ClientPoliticCard> createPoliticCardsFromConfig(JSONObject changesAsJson, String key) {

		JSONArray cardsAsJson = changesAsJson.getJSONArray(key);

		List<ClientPoliticCard> politicCards = new ArrayList<>();

		Iterator<Object> cardsIterator = cardsAsJson.iterator();

		while (cardsIterator.hasNext()) {

			JSONObject cardAsJson = (JSONObject) cardsIterator.next();
			Color color;
			try {
				color = ColorMap.getColor(cardAsJson.getString(COLOR));
			} catch (ColorNotFoundException e) {
				LOG.log(Level.SEVERE, "Unknown color", e);
				color = null;
			}
			politicCards.add(new ClientPoliticCard(color));

		}
		return politicCards;
	}

	/**
	 * Uses the string passed as a parameter to retrieve a JSONObject
	 * representing a politic card present in the JSONObject passed as the
	 * second parameter and from the JSONObject representing the card extracts
	 * the card color and returns a newly created {@link ClientPoliticCard} of
	 * that color
	 * 
	 * @param cardKey
	 *            the key used to retrieve the JSONObject containing the color
	 *            of the card
	 * @param changesAsJson
	 *            the JSONObject containing the card
	 * @return a new ClientPoliticCard with the color specified in the
	 *         JSONObject contained in the JSONObject passed as a parameter
	 */
	protected ClientPoliticCard getPoliticCardFromChanges(String cardKey, JSONObject changesAsJson) {

		JSONObject cardAsJson = changesAsJson.getJSONObject(cardKey);
		Color color;
		try {
			color = ColorMap.getColor(cardAsJson.getString(COLOR));
		} catch (ColorNotFoundException e) {
			LOG.log(Level.SEVERE, "Unknown color", e);
			color = null;
		}
		return new ClientPoliticCard(color);
	}

	/**
	 * Returns the first {@link PlayerResourcesChart} returned by the list
	 * contained in the {@link ClientMap} passed as a parameter such that it's
	 * playerID field matches the integer passed as a parameter. If no matches
	 * are found for the given playerID, null is returned
	 * 
	 * @param actorID
	 *            an integer representing a playerID
	 * @param gameMap
	 *            the {@link ClientMap} where the PlayerResourcesChart requested
	 *            is supposedly located
	 * @return a PlayerResourcesChart
	 */
	protected PlayerResourcesChart getOpponentResourcesFromID(int actorID, ClientMap gameMap) {

		for (PlayerResourcesChart opponent : gameMap.getOpponentsResources())
			if (opponent.getPlayerID() == actorID)
				return opponent;
		LOG.log(Level.SEVERE, "Opponent not found");
		return null;
	}

	/**
	 * Returns the first {@link OpponentHand} returned by the list of
	 * OpponentHand contained in the {@link ClientMap} passed as a parameter
	 * such that it's playerID field matches the integer passed as a parameter.
	 * If no matches are found for the given playerID, null is returned
	 * 
	 * @param actorID
	 *            an integer representing a playerID
	 * @param gameMap
	 *            the ClientMap where the OpponentHand requested is supposedly
	 *            located
	 * @return an OpponentHand
	 */
	protected OpponentHand getOpponentHandFromID(int actorID, ClientMap gameMap) {

		for (OpponentHand opponent : gameMap.getOpponentsHands()) {
			if (opponent.getPlayerID() == actorID)
				return opponent;
		}
		LOG.log(Level.SEVERE, "Opponent not found");
		return null;
	}

	/**
	 * Returns the first {@link ClientCity} returned by a listIterator of the
	 * list of ClientCity present in the ClientMap passed as a parameter, such
	 * that city.getName().equalsIgnoreCase(cityName) returns true, where city
	 * is the ClientCity returned by the iterator, and cityName is the string
	 * passed as the first parameter of this method. If no element is found to
	 * satisfy the condition, null is returned
	 * 
	 * @param cityName
	 *            the name of an in-game city
	 * @param gameMap
	 *            the {@link ClientMap} where the ClientCity requested is
	 *            supposedly located
	 * @return a ClientCity
	 */
	protected ClientCity getCityFromName(String cityName, ClientMap gameMap) {

		List<ClientCity> cities = gameMap.getCities();
		Iterator<ClientCity> citiesIterator = cities.listIterator();

		while (citiesIterator.hasNext()) {
			ClientCity city = citiesIterator.next();
			if (city.getName().equalsIgnoreCase(cityName))
				return city;
		}
		LOG.log(Level.SEVERE, "City not found");
		return null;
	}

	/**
	 * Returns the first {@link ClientRegion} returned by a listIterator of the
	 * list of ClientRegion present in the ClientMap passed as a parameter, such
	 * that region.getRegionName().equalsIgnoreCase(ClientRegion) returns true,
	 * where region is the ClientRegion returned by the iterator, and regionName
	 * is the string passed as the first parameter of this method. If no element
	 * is found to satisfy the condition, null is returned
	 * 
	 * @param regionName
	 *            the name of an in-game region
	 * @param gameMap
	 *            the {@link ClientMap} where the ClientRegion requested is
	 *            supposedly located
	 * @return a ClientRegion
	 */
	protected ClientRegion getRegionFromName(String regionName, ClientMap gameMap) {

		List<ClientRegion> regions = gameMap.getRegions();
		Iterator<ClientRegion> regionsIterator = regions.listIterator();

		while (regionsIterator.hasNext()) {
			ClientRegion region = regionsIterator.next();
			if (region.getRegionName().equalsIgnoreCase(regionName))
				return region;
		}
		LOG.log(Level.SEVERE, "Region not found");
		return null;
	}

	/**
	 * Removes the {@link ClientPermitCard} at the 'index' position in the list
	 * of "faceUpPermits" present in any of the {@link ClientRegion}s contained
	 * in the {@link ClientMap} passed as a parameter such that
	 * permitCard.equals(faceUpPermits.get(index)) returns true, where
	 * permitCard is the ClientPermitCard passed as a parameter. if no element
	 * is found to satisfy this condition, no action is performed
	 * 
	 * @param permitCard
	 * @param gameMap
	 */
	protected void removePermitCardFromFaceUpPermits(ClientPermitCard permitCard, ClientMap gameMap) {

		List<ClientRegion> regions = gameMap.getRegions();
		Iterator<ClientRegion> regionsIterator = regions.listIterator();

		while (regionsIterator.hasNext()) {
			ClientRegion region = regionsIterator.next();
			if (region.contains(permitCard)) {
				region.removeFaceUpPermit(permitCard);
				return;
			}
		}
		LOG.log(Level.SEVERE, "Face up permit not found");
	}
	
	protected void addPermitCardToFaceUpPermits(ClientPermitCard permitCard, ClientMap gameMap) {

		String cityName = permitCard.getCityNames().get(0);
		String regionName = null;
		
		List<ClientCity> cities = gameMap.getCities();
		
		for(ClientCity city : cities)
			if(city.getName().equalsIgnoreCase(cityName))
				 regionName = city.getRegionName();
		
		List<ClientRegion> regions = gameMap.getRegions();
		ListIterator<ClientRegion> regionIterator = regions.listIterator();
		
		while(regionIterator.hasNext()){
			ClientRegion region = regionIterator.next();
			if(region.getRegionName().equalsIgnoreCase(regionName))
				region.addFaceUpPermit(permitCard);
		}
		
		
	}

	/**
	 * Returns a {@link PlayerResourcesChart} contained in the {@link ClientMap}
	 * passed as a parameter such that its PlayerID matches the integer passed
	 * as the first parameter. Note that if no PlayerResourcesChart is found to
	 * satisfy this condition, null is returned The returned
	 * PlayerResourcesChart is either found by calling
	 * getOpponentResourcesFromID(actorID, gameMap) or, if the playerID matches
	 * the playerID of the user, by calling
	 * {@link ClientMap#getUserResources(int,ClientMap)}, where actorID and
	 * gameMap are the parameter passed to this method.
	 * 
	 * @param actorID
	 *            an integer representing a PlayerID
	 * @param gameMap
	 *            the ClientMap where the requested PlayerResourcesChart is
	 *            supposedly located
	 * @return a PlayerResourcesChart
	 */
	protected PlayerResourcesChart getPlayerResourcesFromID(int actorID, ClientMap gameMap) {

		PlayerResourcesChart playerResources;

		if (actorID == userToken.getPlayerID())
			playerResources = gameMap.getUserResources();
		else
			playerResources = getOpponentResourcesFromID(actorID, gameMap);
		return playerResources;
	}

	/**
	 * Returns a string representing a player's username contained in a
	 * {@link PlayerResourcesChart} returned by a call to
	 * {@link ActionsTranslator#getPlayerResourcesFromID(int,ClientMap)} where
	 * PlayerID and gameMap are the parameter passed to this method
	 * 
	 * @param playerID
	 *            an integer representing a PlayerID
	 * @param gameMap
	 *            the ClientMap where the PlayerResourcesChart containing the
	 *            requested player username is supposedly located
	 * @return a string representing a player's username
	 */
	protected String getUsernameFromID(int playerID, ClientMap gameMap) {

		PlayerResourcesChart playerResources;

		playerResources = getPlayerResourcesFromID(playerID, gameMap);

		return playerResources.getPlayerUsername();

	}
}
