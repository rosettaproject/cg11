package it.polimi.ingsw.cg11.client.model.bonus;

import java.util.Map;
import java.util.Set;

import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * An object used to store all the relevant information about an in-game bonus. It uses a {@link Map} to associate 
 * each kind of resource a bonus can grant with the respective amount granted upon obtaining that specific bonus
 * The keys meant to be in this object's map are the following:
 * <li> "Assistants"
 * <li> "Money"
 * <li> "Nobility"
 * <li> "VictoryPoints"
 * <li> "PoliticCards"
 * @author francesco
 *
 */
public class BonusChart {

	private final Map<String,Integer> chart;
	
	/**
	 * Sets the {@link Map} passed as a parameter as the final chart parameter of the object
	 * @param chart
	 */
	protected BonusChart(Map<String,Integer> chart){
		this.chart = chart;
	}
	
	/**
	 * The string passed as a parameter is used to call the <b>get(String key)<b> method of the private {@link Map}
	 * @param bonusKey
	 * @return the value associated with the key, or null if there is no such key in the map
	 */
	public int getBonusValue(String bonusKey){
		return chart.get(bonusKey);
	}
	
	/**
	 * @param bonusKey
	 * @return true if the specified key is contained into this object's {@link Map}
	 */
	public boolean containsKey(String key){
		return chart.containsKey(key);
	}
	
	/**
	 * 
	 * @return a {@link Set} view of the keys contained in this object's {@link Map}
	 */
	public Set<String> getKeys(){
		return chart.keySet();
	}

	/**
	 * Returns a string of the type "BonusChart [chart=" + chart + "]\n" where chart is a {@link Map} implementation
	 */
	@Override
	public String toString() {
		String toString = "";
		
		toString += AsString.asString(chart, false);
		
		return toString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chart == null) ? 0 : chart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BonusChart other = (BonusChart) obj;
		if (chart == null) {
			if (other.chart != null)
				return false;
		} else if (!chart.equals(other.chart))
			return false;
		return true;
	}
	
}