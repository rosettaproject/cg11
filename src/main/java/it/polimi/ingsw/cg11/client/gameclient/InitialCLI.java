package it.polimi.ingsw.cg11.client.gameclient;

import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.client.model.exceptions.ConnectionException;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.client.view.cli.CLI;
import it.polimi.ingsw.cg11.client.view.cli.ScreenFormat;
import it.polimi.ingsw.cg11.client.view.common.GraphicView;
import it.polimi.ingsw.cg11.client.view.connections.ClientConnectionFactory;
import it.polimi.ingsw.cg11.client.view.connections.ClientView;

/**
 * This class is used to set the name of players, the type of graphic view and
 * the type of connection
 * 
 * @author paolasanfilippo
 *
 */
public class InitialCLI {

	private static final Logger LOG = Logger.getLogger(InitialCLI.class.getName());

	private static final String REMOTE = "remote";
	private static final String LOCAL = "local";
	private static final String CLI = "cli";
	private static final String GUI = "gui";
	private static final String RMI = "rmi";
	private static final String SOCKET = "socket";

	private static final String TRY_AGAIN = "Oops! Try again!\n";

	private String viewType;

	private Scanner inputScanner;
	private Token playerToken;
	private ClientController controller;

	private boolean isGUIavailable;

	/**
	 * Default constructor
	 */
	public InitialCLI() {
		inputScanner = new Scanner(System.in);
		isGUIavailable = false;

		printWelcomeMessage();
	}

	/**
	 * Sets up the player username, creates a controller, choose the graphic
	 * view and starts the game
	 */
	public void setUpInitialGame() {
		ClientView playerView;
		String username;
		String ipAsString;

		viewType = getUserInterface();
		username = getPlayerUsername();

		playerToken = new Token(username);
		controller = new ClientController(playerToken);

		ipAsString = getRemoteServerIP();
		try {
			playerView = getPlayerView(ipAsString);
			startGame(playerView);
		} catch (ConnectionException e) { // NOSONAR
			ScreenFormat.displayOnScreen("\n\nError: unreachable server\n\n");
		}

	}
	
	/**
	 * Return the IP of remote server. If the server is on the local machine returns an empty string
	 * @return IP of the remote server as string or empty string
	 */
	private String getRemoteServerIP() {
		String ipAsString;
		String serverChoice = "";
		
		while (!(serverChoice.equalsIgnoreCase(REMOTE) || serverChoice.equalsIgnoreCase(LOCAL))) {

			ScreenFormat.displayOnScreen("\nThe server is LOCAL or REMOTE? ");
			serverChoice = inputScanner.nextLine();

			if (!(serverChoice.equalsIgnoreCase(REMOTE) || serverChoice.equalsIgnoreCase(LOCAL)))
				ScreenFormat.displayOnScreen(TRY_AGAIN);
		}
		
		if(serverChoice.equalsIgnoreCase(LOCAL))
			ipAsString = "";
		else {
			ScreenFormat.displayOnScreen("\n\nInsert the IPv4 address of the remote server (you can not edit it afterwards): ");
			ipAsString = inputScanner.nextLine();
		}
		
		return ipAsString;
	}

	/**
	 * Calls the start method of {@link GraphicView}
	 * 
	 * @param playerView
	 */
	private void startGame(ClientView playerView) {
		GraphicView cli;

		if (playerView != null) {
			if (viewType.equalsIgnoreCase(CLI)) {
				cli = new CLI(playerView);
				controller.setGraphicView(cli);
				cli.start();
			} else {
				ScreenFormat.displayOnScreen("\nGUI not yet available\n");
			}
		}
	}

	/**
	 * Returns the name of the graphic view choosen by the user
	 * 
	 * @return name of graphic view
	 */
	private String getUserInterface() {
		String toPrint;
		String viewChosen = "";

		if (!isGUIavailable)
			viewChosen = CLI;

		while (!(viewChosen.equalsIgnoreCase(CLI) || viewChosen.equalsIgnoreCase(GUI))) {

			ScreenFormat.displayOnScreen("\n\nWhich view do you want to use? CLI or GUI? ");
			viewChosen = inputScanner.nextLine();

			if (!(viewChosen.equalsIgnoreCase(CLI) || viewChosen.equalsIgnoreCase(GUI)))
				ScreenFormat.displayOnScreen(TRY_AGAIN);
		}

		toPrint = "\n\nYou are playing with ";
		toPrint += viewChosen.toUpperCase();
		toPrint += "\n\nLaunching...\n";

		ScreenFormat.displayOnScreen(toPrint);

		return viewChosen;
	}

	/**
	 * Return the {@link ClientView} (socket or RMI) used for communicate with
	 * server
	 * 
	 * @return generic connection view
	 * @throws ConnectionException
	 */
	private ClientView getPlayerView(String ipAsString) throws ConnectionException {
		ClientView playerView = null;
		ClientConnectionFactory connectionFactory;
		String toPrint = "";
		String userInput;
		boolean goodChoice;

		connectionFactory = ClientConnectionFactory.getConnectionHandler(controller, playerToken, ipAsString);
		goodChoice = false;
		while (!goodChoice) {

			ScreenFormat.displayOnScreen("\nWhich connection would you like to use? Socket or RMI? ");

			userInput = inputScanner.nextLine();

			if (SOCKET.equalsIgnoreCase(userInput)) {

				playerView = connectionFactory.createSocketView();
				try {
					playerView.connect();
					goodChoice = true;
				} catch (RemoteException e) {
					LOG.log(Level.SEVERE, "\n\nCan not connect to server (socket)", e);
				}
			} else if (RMI.equalsIgnoreCase(userInput)) {

				playerView = connectionFactory.createRMIView();
				goodChoice = true;
			} else {
				ScreenFormat.displayOnScreen(TRY_AGAIN);
			}
		}
		toPrint += "\nConnected to the server";
		toPrint += "\n\n\nThe game will start when there will be at least 2 player...\n";

		ScreenFormat.displayOnScreen(toPrint);

		return playerView;
	}

	/**
	 * Prints a welcome message
	 */
	private void printWelcomeMessage() {
		String welcomeMessage = "";

		welcomeMessage += "_______________________________________________________________________________________________________________________\n";
		welcomeMessage += " _______  _______  __   __  __    _  _______  ___   ___        _______  _______    _______  _______  __   __  ______   \n";
		welcomeMessage += "|       ||       ||  | |  ||  |  | ||       ||   | |   |      |       ||       |  |       ||       ||  | |  ||    _ |  \n";
		welcomeMessage += "|       ||   _   ||  | |  ||   |_| ||       ||   | |   |      |   _   ||    ___|  |    ___||   _   ||  | |  ||   | ||  \n";
		welcomeMessage += "|       ||  | |  ||  |_|  ||       ||       ||   | |   |      |  | |  ||   |___   |   |___ |  | |  ||  |_|  ||   |_||_ \n";
		welcomeMessage += "|      _||  |_|  ||       ||  _    ||      _||   | |   |___   |  |_|  ||    ___|  |    ___||  |_|  ||       ||    __  |\n";
		welcomeMessage += "|     |_ |       ||       || | |   ||     |_ |   | |       |  |       ||   |      |   |    |       ||       ||   |  | |\n";
		welcomeMessage += "|_______||_______||_______||_|  |__||_______||___| |_______|  |_______||___|      |___|    |_______||_______||___|  |_|\n";
		welcomeMessage += "_______________________________________________________________________________________________________________________\n";
		welcomeMessage += "\nWelcome! You're playing Council of Four!\n";

		ScreenFormat.displayOnScreen(welcomeMessage);
	}

	/**
	 * Return the username that the player has chosen
	 * @return
	 */
	private String getPlayerUsername() {
		String username;

		ScreenFormat.displayOnScreen("\n\nInsert username: ");
		username = inputScanner.nextLine();

		return username;
	}

}
