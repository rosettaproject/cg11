
package it.polimi.ingsw.cg11.client.model.cards;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * Immutable class representing a permit card in the client. Contains a {@link BonusChart} representing the permit
 * bonus, and a {@link List} of strings representing the names of the cities where the card represented by this
 * object allows to build an emporium
 * @author francesco
 *
 */
public class ClientPermitCard {

	private final BonusChart bonus;
	private final List<String> cityNames;

	/**
	 * @param bonus :  a {@link BonusChart} representing this card's bonus
	 * @param cityNames :  a list of names of in-game cities 
	 */
	public ClientPermitCard(BonusChart bonus,List<String> cityNames){
		this.bonus = bonus;
		this.cityNames = cityNames;
	}

	/**
	 * 
	 * @return this object's {@link BonusChart}
	 */
	public BonusChart getBonus() {
		return bonus;
	}

	/**
	 * 
	 * @return this object's {@link List} of city names
	 */
	public List<String> getCityNames() {
		return new ArrayList<>(cityNames);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		result = prime * result + ((cityNames == null) ? 0 : cityNames.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientPermitCard other = (ClientPermitCard) obj;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (!bonus.equals(other.bonus))
			return false;
		if (cityNames == null) {
			if (other.cityNames != null)
				return false;
		} else if (!cityNames.equals(other.cityNames))
			return false;
		return true;
	}


	@Override
	public String toString() {
		String toString = "";
		
		toString += bonus.toString();
		toString += AsString.asString(cityNames, false);
		
		return toString;
	}
	
	
	
}

