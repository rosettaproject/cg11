package it.polimi.ingsw.cg11.client.model.changes;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCardFactory;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientRegion;
import it.polimi.ingsw.cg11.client.model.map.VisibleBalcony;
import it.polimi.ingsw.cg11.client.model.player.PlayerResourcesChart;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.server.model.exceptions.player.resources.IllegalResourceException;
import it.polimi.ingsw.cg11.server.model.exceptions.tokens.NoSuchCouncillorException;
import it.polimi.ingsw.cg11.server.model.exceptions.utils.ColorNotFoundException;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.player.actionspool.QuickActionsHandler;
import it.polimi.ingsw.cg11.server.model.utils.ColorMap;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This object is an extension of the {@link ActionsTranslator} abstract class,
 * specialized in translating {@link ModelChanges} generated by quick actions.
 * 
 * @author francesco
 *
 */
public class QuickActionsTranslator extends ActionsTranslator {

	private static final String ILLEGAL_CHANGE = "Illegal change";
	private static final Logger LOG = Logger.getLogger(QuickActionsTranslator.class.getName());
	private ChangesMessage message;

	public QuickActionsTranslator(Token userToken, ChangesMessage message) {
		super(userToken, message);
		this.message = message;
	}

	/**
	 * This method allows to translate the information contained in the
	 * {@link ModelChanges} passed as a parameter into actual changes in the
	 * state of the {@link ClientMap} passed as the second parameter, provided
	 * that these changes originated from a quick action. More formally, this
	 * means that the JSONObject contained as a string in the ModelChanges
	 * parameter must contain an "Action" key associated with one of the
	 * following strings:
	 * <ul>
	 * <li>"PerformAnAdditionalMainAction"
	 * <li>"ChangePermitCard"
	 * <li>"ElectCouncillorWithOneAssistant"
	 * <li>"HireOneAssistant"
	 * </ul>
	 * <br>
	 * If none of the above keys is present, the value of the message field is
	 * not modified, and no additional operation is performed <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	public ChangesMessage translateQuickActionChange(ClientMap gameMap, ModelChanges changes) {

		JSONObject changesAsJson = new JSONObject(changes.getJsonConfigAsString());

		if (changesAsJson.has(QuickActionsHandler.ACTION_KEY)) {
			String actionName = changesAsJson.getString(QuickActionsHandler.ACTION_KEY);

			switch (actionName) {
			case "PerformAnAdditionalMainAction":
				message = translatePerformAnAdditionalMainAction(gameMap, changesAsJson);	
				break;
			case "ChangePermitCard":
				message = translateChangePermitCard(gameMap, changesAsJson);
				break;
			case "ElectCouncillorWithOneAssistant":
				message = translateElectCouncillorWithOneAssistant(gameMap, changesAsJson);
				break;
			case "HireOneAssistant":
				message = translateHireOneAssistant(gameMap, changesAsJson);
				break;
			default:
				break;
			}
		}
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * main action "HireOneAssistant". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>a "MoneyCost" key, mapped to an integer representing the money
	 * required to hire an assistant
	 * <li>a "Assistant" key, mapped to an integer representing the
	 * amount of assistants gained, in this case it should be equal to 1 
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateHireOneAssistant(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage("\n\nAction performed: hire an assistant");
		int actorID = changesAsJson.getInt(QuickActionsHandler.ACTOR_KEY);
		int neededMoney = changesAsJson.getInt(QuickActionsHandler.MONEY_COST_KEY);
		int gainedAssistants = changesAsJson.getInt(QuickActionsHandler.ASSISTANT_KEY);

		PlayerResourcesChart playerResources;

		String actorUsername = getUsernameFromID(actorID, gameMap);

		playerResources = getPlayerResourcesFromID(actorID, gameMap);

		try {
			playerResources.getPlayerMoney().spend(neededMoney);
			playerResources.getPlayerAssistants().gain(gainedAssistants);
			message.addMessage(actorUsername + " loses " + neededMoney + " money");
			message.addMessage(actorUsername + " gains an assistants");
			
			return message;
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * main action "ElectCouncillorWithOneAssistant". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>a "BalconyName" key, mapped to a string representing the name of a {@link VisibleBalcony}, more
	 * precisely representing the name of either the region that contains it or, if the balcony
	 * is the king's balcony, the string "King"
	 * <li>a "Councillor" key, mapped to a string which is in turn mapped to a {@link Color} in the 
	 * {@link ColorMap}
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateElectCouncillorWithOneAssistant(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage("\n\nAction performed: elect councillor with an assistant");
		int actorID = changesAsJson.getInt(QuickActionsHandler.ACTOR_KEY);
		String balconyName = changesAsJson.getString(QuickActionsHandler.BALCONY_KEY);
		VisibleBalcony balcony;
		CouncillorPoolManager poolManager = gameMap.getCouncillorPoolManager();
		PlayerResourcesChart playerResources = getPlayerResourcesFromID(actorID, gameMap);

		balcony = getBalconyFromName(balconyName, gameMap);

		String actorUsername = getUsernameFromID(actorID, gameMap);

		try {
			Color councillorColor = ColorMap.getColor(changesAsJson.getString(QuickActionsHandler.COUNCILLOR_KEY));
			balcony.addCouncillorAndRemoveTail(poolManager.obtainPieceFromPool(councillorColor),poolManager);

			playerResources.getPlayerAssistants().use(1);

			message.addMessage("electedcouncillor color: " + ColorMap.getString(councillorColor));
			message.addMessage(actorUsername + " uses 1 assistant");

			return message;
		} catch (IllegalResourceException | NoSuchCouncillorException | ColorNotFoundException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * main action "ChangePermitCard". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "AssistantsCost" key, mapped to an integer representing the number of assistants needed to perform
	 * this action
	 * <li>a "RegionName" key, mapped to a string representing the name of the {@link ClientRegion}
	 * where the face up permits are located
	 * <li>a "FirstPermit" key, mapped to a JSONObject structured in such a way that the{@link ClientPermitCardFactory#createPermitCardFromChanges(JSONObject)} 
	 * method can create an 
	 * appropriate {@link ClientPermitCard} from it, representing the first of the new permits drew from 
	 * the deck
	 * <li>a "SecondPermit" key, mapped to a JSONObject structured in such a way that  the
	 * {@link ClientPermitCardFactory#createPermitCardFromChanges(JSONObject)} method can create an 
	 * appropriate {@link ClientPermitCard} from it representing the second of the new permits drew from 
	 * the deck
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translateChangePermitCard(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage("\n\nAction performed: change face up permit cards");
		int actorID = changesAsJson.getInt(QuickActionsHandler.ACTOR_KEY);
		int assistantsCost = changesAsJson.getInt(QuickActionsHandler.ASSISTANT_COST_KEY);
		String regionName = changesAsJson.getString(QuickActionsHandler.REGION_NAME_KEY);

		JSONObject firstPermitAsJson = changesAsJson.getJSONObject(QuickActionsHandler.FIRST_PERMIT_KEY);
		JSONObject secondPermitAsJson = changesAsJson.getJSONObject(QuickActionsHandler.SECOND_PERMIT_KEY);

		ClientPermitCardFactory permitFactory = new ClientPermitCardFactory();

		ClientPermitCard firstPermit = permitFactory.createPermitCardFromChanges(firstPermitAsJson);
		ClientPermitCard secondPermit = permitFactory.createPermitCardFromChanges(secondPermitAsJson);
		ClientRegion region = getRegionFromName(regionName, gameMap);
		PlayerResourcesChart playerResources = getPlayerResourcesFromID(actorID, gameMap);

		List<ClientPermitCard> newPermits = new ArrayList<>();
		newPermits.add(firstPermit);
		newPermits.add(secondPermit);

		String actorUsername = getUsernameFromID(actorID, gameMap);

		try {
			playerResources.getPlayerAssistants().use(assistantsCost);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

		region.changeFaceUpPermits(newPermits);

		message = new ChangesMessage(actorUsername + " uses " + assistantsCost + " assistants");
		message = new ChangesMessage("region where face up permits were changed : " + regionName);
		message = new ChangesMessage("new permits :\n" + firstPermit.toString() + "\n" + secondPermit.toString());
		
		return message;
	}

	/**
	 * Allows to translate the changes in the state of the game caused by the
	 * main action "PerformAnAdditionalMainAction". The JSONObject passed as a parameter
	 * must contain the following:
	 * <ul>
	 * <li>an "ActorID" key,mapped to the playerID of the player who performed
	 * the action
	 * <li>an "AssistantsCost" key, mapped to an integer representing the number of assistants needed to perform
	 * this action
	 * </ul>
	 * <br>
	 * If the information contained in the ModelChanges passed as a parameter
	 * contains improper values and\or an exception is thrown making the
	 * translation impossible, the return value is set to null
	 * 
	 * @param gameMap
	 *            the ClientMap where the changes will be applied
	 * @param changes
	 *            the ModelChanges containing the necessary information to apply
	 *            the changes
	 * @return a {@link ChagesMessage} containing a summary of the changes
	 *         applied in the form of a string
	 */
	private ChangesMessage translatePerformAnAdditionalMainAction(ClientMap gameMap, JSONObject changesAsJson) {

		message = new ChangesMessage("\n\nAction performed: gain additional main action");
		int actorID = changesAsJson.getInt(QuickActionsHandler.ACTOR_KEY);
		int assistantsCost = changesAsJson.getInt(QuickActionsHandler.ASSISTANT_COST_KEY);
		PlayerResourcesChart playerResources = getPlayerResourcesFromID(actorID, gameMap);

		String actorUsername = getUsernameFromID(actorID, gameMap);

		try {
			playerResources.getPlayerAssistants().use(assistantsCost);
		} catch (IllegalResourceException e) {
			LOG.log(Level.SEVERE, ILLEGAL_CHANGE, e);
			return null;
		}

		message.addMessage(actorUsername + " uses " + assistantsCost + " assistants");
		message.addMessage(actorUsername + " can perform an additional main action");
		
		return message;
	}

}
