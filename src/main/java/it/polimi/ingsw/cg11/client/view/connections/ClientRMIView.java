package it.polimi.ingsw.cg11.client.view.connections;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg11.server.model.utils.observerpattern.Observer;
import it.polimi.ingsw.cg11.server.model.utils.visitorpattern.Visitable;
import it.polimi.ingsw.cg11.server.view.Token;
import it.polimi.ingsw.cg11.server.view.rmi.RemoteFactory;
import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;

/**
 * This class is used to communicate with server throw a RMI connection
 * 
 * @author Federico
 *
 */
public class ClientRMIView implements ClientView {

	private static final Logger LOG = Logger.getLogger(ClientRMIView.class.getName());

	private List<Observer<Visitable>> observers;
	private RemoteFactory implFactoryStub;

	private final Token playerToken;
	private String gameIdentifier;

	/**
	 * Default constructor
	 * 
	 * @param token of the player
	 * @param controller of the current game
	 * @throws RemoteException 
	 * @throws NotBoundException
	 */
	protected ClientRMIView(Token token, ClientController controller) throws RemoteException, NotBoundException {

		this.playerToken = token;

		observers = new ArrayList<>();
		addObserver(controller);
	}

	@Override
	public void sendToServer(Visitable message) throws RemoteException{
		message.setTopic(gameIdentifier);
		message.setPlayerToken(playerToken);
		getImplFactoryStub().readMessage(message, playerToken);
	}

	@Override
	public void readMessageFromServer(Visitable message) throws RemoteException {
		if (message instanceof SerializableMapStartupConfig) {
			gameIdentifier = message.getTopic();
			playerToken.setCurrentGame(message.getTopic());
		}
		notifyToObservers(message);

	}

	/**
	 * @return the token of player
	 */
	@Override
	public Token getToken() throws RemoteException {
		return playerToken;
	}

	@Override
	public void notifyToObservers(Visitable message) {
		Iterator<Observer<Visitable>> iterator = observers.iterator();
		Observer<Visitable> observer;
		while (iterator.hasNext()) {
			observer = iterator.next();
			observer.update(message);
		}

	}

	@Override
	public boolean addObserver(Observer<Visitable> observer) {
		return observers.add(observer);
	}

	@Override
	public void removeObserver(Observer<Visitable> observer) {
		int observerIndex;

		observerIndex = observers.indexOf(observer);
		if (observerIndex >= 0) {
			observers.remove(observerIndex);
		}
	}

	@Override
	public void connect() {
		LOG.log(Level.WARNING, "Not supported method");
	}

	/**
	 * @return the implFactoryStub
	 */
	public RemoteFactory getImplFactoryStub() {
		return implFactoryStub;
	}

	/**
	 * @param implFactoryStub
	 *            the implFactoryStub to set
	 */
	public void setImplFactoryStub(RemoteFactory implFactoryStub) {
		this.implFactoryStub = implFactoryStub;
	}

	@Override
	public void disconnect() throws RemoteException {
		getImplFactoryStub().deleteRMIView(playerToken);
	}

	/**
	 * @return the gameIdentifier
	 */
	public String getGameIdentifier() {
		return gameIdentifier;
	}

	/**
	 * @param gameIdentifier
	 *            the gameIdentifier to set
	 */
	public void setGameIdentifier(String gameIdentifier) {
		this.gameIdentifier = gameIdentifier;
	}

	@Override
	public void setPlayerID(int playerID) throws RemoteException {
		playerToken.setPlayerID(playerID);
	}

	@Override
	public void setUpdatedUsername(String updatedUsername) throws RemoteException {
		playerToken.setClientUsername(updatedUsername);
	}

}
