package it.polimi.ingsw.cg11.client.model.player;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * This object represents an opponent's hand in the client
 * @author francesco
 *
 */
public class OpponentHand {

	private final List<ClientPermitCard> permitCards;
	private final List<ClientPermitCard> usedPermitCards;
	private int politicCards;
	private final int playerID;

	/**
	 * 
	 * @param playerID
	 * @param permitCards a list of {@link PermitCard}s
	 * @param politicCardsInHand an integer representing the number of cards in the opponent's hand
	 */
	public OpponentHand(int playerID, List<ClientPermitCard> permitCards, int politicCardsInHand) {
		this.playerID = playerID;
		this.permitCards = permitCards;
		this.politicCards = politicCardsInHand;
		this.usedPermitCards = new ArrayList<>();
	}

	/**
	 * 
	 * @param index
	 * @return the {@link ClientPermitCard} at the given position in this object's list
	 */
	public ClientPermitCard getPermitCard(int index) {
		return permitCards.get(index);
	}

	/**
	 * 
	 * @param index
	 * @return the used {@link ClientPermitCard} at the given position in this list
	 */
	public ClientPermitCard getUsedPermitCard(int index) {
		return usedPermitCards.get(index);
	}

	/**
	 * Removes the {@link ClientPermitCard} passed as a parameter and adds it to this object's list of used permit cards
	 * @param card
	 */
	public void usePermitCard(ClientPermitCard card) {
		int permitIndex = permitCards.indexOf(card);
		usedPermitCards.add(permitCards.remove(permitIndex));
	}
	
	/**
	 * @param cardToRemove
	 * @return the {@link ClientPermitCard} passed as a parameter from this object's list of permit cards
	 * if it has been removed from the list, null otherwise
	 */
	public ClientPermitCard removePermitCard(ClientPermitCard cardToRemove) {
		
		int index = permitCards.indexOf(cardToRemove);
		
		if(index >= 0)
			return permitCards.remove(index);
		else
			return null;
	}
	
	
	/**
	 * @param cardToRemove
	 * @return the {@link ClientPermitCard} passed as a parameter from this object's list of used permit cards
	 * if it has been removed from the list, null otherwise
	 */
	public ClientPermitCard removeUsedPermitCard(ClientPermitCard cardToRemove) {
		
		int index = usedPermitCards.indexOf(cardToRemove);
		
		if(index >= 0)
			return usedPermitCards.remove(index);
		else
			return null;
	}

	/**
	 * Decreases the integer representing the politic cards in this hand by 1
	 */
	public void removePoliticCard() {
		politicCards--;
	}

	/**
	 * Adds the {@link ClientPermitCard} passed as a parameter to this object's list of permit cards
	 * @param card
	 */
	public void addPermitCard(ClientPermitCard card) {
		permitCards.add(card);
	}

	/**
	 * 
	 * @return the size of this object's list of permit cards
	 */
	public int numberOfPermitCards() {
		return permitCards.size();
	}

	public void addPoliticCard() {
		politicCards++;
	}

	/**
	 * Increases the integer representing the politic cards in this hand by 1
	 */
	public void removePoliticCards(int number) {
		politicCards -= number;
	}

	/**
	 * Increases the integer representing the politic cards in this hand by the integer passed as a parameter
	 */
	public void addPoliticCards(int number) {
		politicCards += number;
	}

	/**
	 * 
	 * @return the integer representing the politic cards in this hand
	 */
	public int getPoliticCards() {
		return politicCards;
	}

	/**
	 * 
	 * @return this object's playerID
	 */
	public int getPlayerID() {
		return playerID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "";
		
		toString += "Politic cards: " + politicCards;
		
		toString += "\n\nPermit cards:\n";
		toString += AsString.cardsAsString(permitCards);
		
		toString += "\nUsed permit cards:\n";
		toString += AsString.cardsAsStringWithStartIndex(usedPermitCards, permitCards.size());
		
		return toString;
	}
	
	

}
