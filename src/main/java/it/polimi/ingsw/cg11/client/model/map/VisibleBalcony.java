package it.polimi.ingsw.cg11.client.model.map;

import java.awt.Color;
import java.util.List;

import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.Councillor;
import it.polimi.ingsw.cg11.server.model.mapcomponents.tokens.CouncillorPoolManager;
import it.polimi.ingsw.cg11.server.model.utils.AsString;


/**
 * Contains a list of {@link Councillor}s such that the user can only see the color of the councillors or
 * remove the tail of the list while adding another councillor 
 * @author francesco
 *
 */
public class VisibleBalcony {

	public static final int MAX_SIZE = 4;
	private static final int HEAD_INDEX = 0;
	
	private final List<Councillor> councillors;

	/**
	 * 
	 * @param councillors a list of the {@link Councillor}s present in this balcony
	 */
	public VisibleBalcony(List<Councillor> councillors) {
		this.councillors = councillors;
	}

	/**
	 * 
	 * @param position
	 * @return the {@link Color} of the councillor at the given position in this balcony
	 */
	public Color getCouncillorColor(int position) {
		return councillors.get(position).getColor();
	}

	/**
	 * Adds the {@link Councillor} taken as a parameter to the last position in the list and removes the head
	 * @param councilorToAdd
	 * @param poolManager this object's {@link ClientMap}'s {@link CouncillorPoolManager}
	 */
	public void addCouncillorAndRemoveTail(Councillor councilorToAdd,CouncillorPoolManager poolManager) {
		poolManager.addPieceToPool(councillors.remove(HEAD_INDEX));	
		councillors.add(MAX_SIZE-1,councilorToAdd);
	}
	
	@Override
	public String toString() {
		String toString = "";

		toString += AsString.asString(councillors, true);
		
		return toString;
	}
	
}
