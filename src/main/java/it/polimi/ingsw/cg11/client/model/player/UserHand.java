package it.polimi.ingsw.cg11.client.model.player;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import it.polimi.ingsw.cg11.client.model.cards.ClientPermitCard;
import it.polimi.ingsw.cg11.client.model.cards.ClientPoliticCard;
import it.polimi.ingsw.cg11.server.model.utils.AsString;

/**
 * This object represents the user's hand in the client
 * @author francesco
 *
 */
public class UserHand {

	private final List<ClientPoliticCard> politicCards;
	private final List<ClientPermitCard> permitCards;
	private final List<ClientPermitCard> usedPermitCards;
	
	/**
	 * 
	 * @param politicCards the starting politic cards
	 * @param permitCards the starting permit cards
	 */
	public UserHand(List<ClientPoliticCard> politicCards, List<ClientPermitCard> permitCards) {
		this.politicCards = politicCards;
		this.permitCards = permitCards;
		this.usedPermitCards = new ArrayList<>();
	}
	
	/**
	 * @param index
	 * @return the element at the given position in the list of {@link ClientPoliticCard}s of this object
	 */
	public ClientPoliticCard getPoliticCard(int index){
		return politicCards.get(index);
	}
	
	/**
	 * @param color
	 * @return a {@link ClientPoliticCard} of the given {@link Color} and removes it from this object's list of
	 * politic cards if present, otherwise returns null 
	 */
	public ClientPoliticCard getPoliticCard(Color color){
		int index = politicCards.indexOf(new ClientPoliticCard(color));
		if(index >= 0){
			return politicCards.get(index);
		}else
			return null;
	}
	
	/**
	 * @return the size of this object's list of {@link ClientPermitCard}s
	 */
	public int numberOfPermitCards(){
		return permitCards.size();
	}
	
	/**
	 * @return the size of this object's list of {@link ClientPoliticCard}s
	 */
	public int numberOfPoliticCards(){
		return politicCards.size();
	}
	
	/**
	 * @return the size of this object's list of  used {@link ClientPermitCard}s
	 */
	public int numberOfUsedPermit(){
		return usedPermitCards.size();
	}
	
	/**
	 * @param index
	 * @return the element at the given position in the list of {@link ClientPermitCard}s of this object
	 */
	public ClientPermitCard getPermitCard(int index){
		return permitCards.get(index);
	}
	
	/**
	 * @param index
	 * @return the element at the given position in the list of used {@link ClientPermitCard}s of this object
	 */
	public ClientPermitCard getUsedPermitCard(int index) {
		return usedPermitCards.get(index);
	}

	/**
	 * Adds the permit card passed as a parameter to this object's list of {@link ClientPermitCard}s
	 * @param card
	 */
	public void addPermitCard(ClientPermitCard card){
		permitCards.add(card);
	}
	
	/**
	 * Adds the permit card passed as a parameter to this object's list of used{@link ClientPermitCard}s
	 * and removes it from the list of usable {@link ClientPermitCard}s
	 * @param card
	 */
	public void usePermitCard(ClientPermitCard card){
		int index = permitCards.indexOf(card);
		usedPermitCards.add(permitCards.remove(index));
	}
	
	/**
	 * @param cardToRemove
	 * @return the {@link ClientPermitCard} passed as a parameter from this object's list of permit cards
	 * if it has been removed from the list, null otherwise
	 */
	public ClientPermitCard removePermitCard(ClientPermitCard cardToRemove) {
		
		int index = permitCards.indexOf(cardToRemove);
		
		if(index >= 0)
			return permitCards.remove(index);
		else
			return null;
	}
	
	/**
	 * @param cardToRemove
	 * @return the {@link ClientPermitCard} passed as a parameter from this object's list of used permit cards
	 * if it has been removed from the list, null otherwise
	 */
	public ClientPermitCard removeUsedPermitCard(ClientPermitCard cardToRemove) {
		
		int index = usedPermitCards.indexOf(cardToRemove);
		
		if(index >= 0)
			return usedPermitCards.remove(index);
		else
			return null;
	}
	
	/**
	 * Removes the card passed as a parameter from this object's list of {@link ClientPoliticCard}
	 * @param card
	 * @return the removed {@link ClientPoliticCard}
	 */
	public ClientPoliticCard removePoliticCard(ClientPoliticCard card){
		int index = politicCards.indexOf(card);
		return politicCards.remove(index);
	}
	
	/**
	 * Removes the list of cards passed as a parameter from this object's list of {@link ClientPoliticCard}
	 * @param card
	 * @return the removed {@link ClientPoliticCard}
	 */
	public void removePoliticCards(List<ClientPoliticCard> cards){
		ListIterator<ClientPoliticCard> cardsIterator = cards.listIterator();
		
		while(cardsIterator.hasNext())
			politicCards.remove(cardsIterator.next());
	}
	
	/**
	 * Adds the {@link ClientPoliticCard} passed as a parameter to this object's list 
	 * @param card
	 */
	public void addPoliticCard(ClientPoliticCard card){
		politicCards.add(card);
	}
	
	/**
	 * Adds all the {@link ClientPoliticCard}s in the list passed as a parameter to this object's list 
	 * @param card
	 */
	public void addAllPoliticCard(List<ClientPoliticCard> cards){
		politicCards.addAll(cards);
	}

	@Override
	public String toString() {
		String toString = "";
		
		toString += "Politic cards:\n";
		toString += AsString.asString(politicCards, true);
		
		toString += "\nPermit cards:\n";
		toString += AsString.cardsAsString(permitCards);
		
		toString += "\nUsed permit cards:\n";
		toString += AsString.cardsAsStringWithStartIndex(usedPermitCards, permitCards.size());
		
		return toString;
	}

}

