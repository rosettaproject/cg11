package it.polimi.ingsw.cg11.client.gameclient;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.controller.ClientController;
import it.polimi.ingsw.cg11.client.model.map.ClientMap;
import it.polimi.ingsw.cg11.client.model.map.ClientMapFactory;
import it.polimi.ingsw.cg11.messages.ModelChanges;
import it.polimi.ingsw.cg11.messages.SerializableMapStartupConfig;
import it.polimi.ingsw.cg11.server.view.Token;

/**
 * This class is used to initialize the game
 * 
 * @author paolasanfilippo
 */
public class ClientGameInitializer {

	private static final String PLAYER_ID = "PlayerID";
	private static final String USERNAME = "Username";
	private static final String TOKENS = "Tokens";

	/**
	 * Used by the {@link ClientController} to initialize the game
	 * 
	 * @param startupConfig
	 *            : a {@link SerializableMapStartupConfig}
	 * @param startingHand
	 * @param user
	 * @return clientMap : initialized client map
	 */
	public ClientMap initializeGame(SerializableMapStartupConfig startupConfig, ModelChanges startingHand, Token user) {

		List<Token> tokens = createOpponentsTokenList(startupConfig, user);

		ClientMapFactory mapFactory = new ClientMapFactory();

		return mapFactory.createClientMap(startupConfig, startingHand, user, tokens);

	}

	/**
	 * Returns list of tokens owned by the opponent players
	 * 
	 * @param startupConfig
	 * @param user
	 * @return list of tokens
	 */
	private List<Token> createOpponentsTokenList(SerializableMapStartupConfig startupConfig, Token user) {

		JSONObject startupConfigAsJson = new JSONObject(startupConfig.getJsonConfigAsString());
		JSONArray tokensAsJson = startupConfigAsJson.getJSONArray(TOKENS);

		List<Token> tokens = new ArrayList<>();

		Iterator<Object> tokensIterator = tokensAsJson.iterator();

		while (tokensIterator.hasNext()) {
			JSONObject tokenAsJson = (JSONObject) tokensIterator.next();
			if (!tokenAsJson.getString(USERNAME).equalsIgnoreCase(user.getClientUsername())) {
				Token opponentToken = new Token(tokenAsJson.getString(USERNAME));
				opponentToken.setPlayerID(tokenAsJson.getInt(PLAYER_ID));
				tokens.add(opponentToken);
			}
		}
		return tokens;
	}
}
