
package it.polimi.ingsw.cg11.client.model.cards;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.cg11.client.model.bonus.BonusChart;
import it.polimi.ingsw.cg11.client.model.bonus.BonusChartFactory;

/**
 * An object used to create a {@link ClientPermitCard} from a permitConfig in two different formats:
 * <br>The first used in the map configuration files, and the second used in the JSONObject storing model changes
 * @author francesco
 *
 */
public class ClientPermitCardFactory {

	/**
	 * The {@link JSONArray} passed as a parameter must have the following format:
	 * <ul>
	 * <li> the first element is a JSONArray containing strings representing names of in-game cities
	 * <li> the second element is a JSONObject used to create a {@link BonusChart} from a {@link BonusChartFactory}
	 * 
	 * @param permitConfig the configuration of the card to create, containing the card's bonus and a list 
	 * of in-game cities
	 * @return a {@link ClientPermitCard} with the characteristics specified in it's configuration
	 */
	public ClientPermitCard createPermitCardFromConfig(JSONArray permitConfig){
		
		JSONArray cityNamesAsJson;
		JSONObject permitBonusAsJson;

		cityNamesAsJson = permitConfig.getJSONArray(0);
		permitBonusAsJson = permitConfig.getJSONObject(1);
		
		return createPermit(permitBonusAsJson,cityNamesAsJson);
	}
	
	/**
	 * The {@link JSONObject} passed as a parameter must have the following format:
	 * <ul>
	 * <li> the first element under the "PermitCities" key is a JSONArray containing strings representing names of in-game cities
	 * <li> the second element under the "PermitBonus" is a JSONObject used to 
	 * create a {@link BonusChart} from a {@link BonusChartFactory}
	 * 
	 * @param permitConfig the configuration of the card to create, containing the card's bonus and a list 
	 * of in-game cities
	 * @return a {@link ClientPermitCard} with the characteristics specified in it's configuration
	 */
	public ClientPermitCard createPermitCardFromChanges(JSONObject permitConfig){
		
		JSONArray cityNamesAsJson;
		JSONObject permitBonusAsJson;
		
		cityNamesAsJson = permitConfig.getJSONArray("PermitCities");
		permitBonusAsJson = permitConfig.getJSONObject("PermitBonus");
		
		return createPermit(permitBonusAsJson,cityNamesAsJson);
	}
	
	/**
	 * Returns a {@link ClientPermitCard} from a JSONObject containing a mapping of bonus types and values, used to construct a 
	 * {@link BonusChart} and a JSONArray containing strings representing the names of in-game cities
	 * @param permitBonusAsJson
	 * @param cityNamesAsJson
	 * @return a {@link ClientPermitCard} with the characteristics specified through this method's parameters
	 */
	private ClientPermitCard createPermit(JSONObject permitBonusAsJson,JSONArray cityNamesAsJson){
		
		List<String> cityNames;
		BonusChart permitBonus;
		
		BonusChartFactory bonusFactory = new BonusChartFactory();
		permitBonus = bonusFactory.createBonusChart(permitBonusAsJson);
		
		cityNames = new ArrayList<>();
		
		int index;
		for(index = 0; index < cityNamesAsJson.length();index++){
			cityNames.add(cityNamesAsJson.getString(index));
		}
		return new ClientPermitCard(permitBonus,cityNames);
	}
}

